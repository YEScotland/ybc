[![Pipeline Status](https://gitlab.com/YEScotland/ybc/badges/master/pipeline.svg)](https://gitlab.com/YEScotland/ybc/-/commits/master) [![Coverage Report](https://gitlab.com/YEScotland/ybc/badges/master/coverage.svg)](https://gitlab.com/YEScotland/ybc/-/commits/master)

# Young Enterprise Scotland Business Challenge (YBC)

## Backend
The backend can be started using:
```bash
bash run_backend.bash y
```

## Frontend
The frontend can be started using:
```bash
cd frontend
npm start
```

## Deployment
Instructions on how to deploy the application can be found in the [wiki attached with this project](https://gitlab.com/YEScotland/ybc/-/wikis/home)

# Contributors

This application has been developed by:

[Jim Carty](@QuestioWo)
