/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

// jest-dom adds custom jest matchers for asserting on DOM nodes.
// allows you to do things like:
// expect(element).toHaveTextContent(/react/i)
// learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import { configure } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

/* Definitions */
function setupLocalStorage() {
	localStorage["username"] = "team";
	localStorage["user_type"] = "Team";
	localStorage["tokens"] = JSON.stringify({"access" : "", "refresh" : ""});

	jest.useFakeTimers("modern");
	jest.setSystemTime(new Date('2022-03-20'));
}

function setupAllNecessaryVariables() {
	setupLocalStorage();
}

/* main */
configure({ adapter: new Adapter() });

setupAllNecessaryVariables();