/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

interface Config {
	apiURL: string,
	rootURL: string
}

const dev: Config = {
	rootURL: "http://localhost:8000",
	apiURL: "http://localhost:8000/api",
}

// NOTE: window.location.origin returns the root of the url i.e https://user.pythonanywhere.com
// this means that the website cannot be deployed to a root of a url without the api becoming
// inaccessible under this current setup. The reason for the absolute root of the site being
// needed is so that the pdf rendering in the marketplace can work correctly (through google's
// api)
const prod: Config = {
	rootURL: window.location.origin,
	apiURL: window.location.origin + "/api",
};

const config: Config = process.env.REACT_APP_CONFIG === 'prod' ?
	prod : dev;

export default config;