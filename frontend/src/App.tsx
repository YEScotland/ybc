/*
  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React, { ReactElement } from 'react';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { HomeView } from './components/HomeView';
import { LoginView } from './components/LoginView';
import { MarketplaceView } from './components/MarketplaceView';
import { MarketplaceSpecificView } from './components/MarketplaceSpecificView';
import { SchoolsView } from './components/SchoolsView';
import { TeamsView } from './components/TeamsView';
import { GlossaryView } from './components/GlossaryView';
import { IssuesView } from './components/IssuesView';
import { SettingsView } from './components/SettingsView';
import { SettingsDeadlinesView } from './components/SettingsDeadlinesView';
import { JudgesView } from './components/JudgesView';
import { TeachersView } from './components/TeachersView';
import { MarkedSubmissionView } from './components/MarkedSubmissionView';
import { BusinessDecisionsView } from './components/BusinessDecisionsView';

import { AlertBarUpdater, PageProps, Tokens } from './components/elements';

import './App.css';

interface Props { }

interface State extends PageProps { }

export default class App extends React.Component<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			"alert": {
				"show": false,
				"message": "No alert text",
				"variant": "success"
			},
			"updateAlertBar": this.updateAlertBar,
			"setDisplayMilestoneBar": this.setDisplayMilestoneBar,
			displayMilestoneBar: true,
			testRun: false
		}
	}

	componentDidMount() {
		if (localStorage.getItem("tokens") === null) {
			const tokens: Tokens = {
				"access": "",
				"refresh": ""
			}

			localStorage.setItem("tokens", JSON.stringify(tokens));
		}

		if (localStorage.getItem("username") === null) {
			localStorage.setItem("username", "");
		}

		if (localStorage.getItem("user_type") === null) {
			localStorage.setItem("user_type", "");
		}
	}

	updateAlertBar = async (message: string | ReactElement, variant: string, show: boolean) => {
		const updater: AlertBarUpdater = {
			"message": message,
			"variant": variant,
			"show": show
		};
		this.setState({ "alert": updater });
	}

	setDisplayMilestoneBar = async (value: boolean) => {
		this.setState({ displayMilestoneBar: value });
	}

	render() {
		return (
			<React.Fragment>
				<Router>
					<Switch>
						<Route exact path="/" render={(props) => <HomeView {...props} {...this.state} />} />
						<Route exact path="/marketplace" render={(props) => <MarketplaceView {...props} {...this.state} />} />
						<Route exact path="/marketplace/:username" render={(props) => <MarketplaceSpecificView {...props} {...this.state} />} />
						<Route exact path="/login" render={(props) => <LoginView {...props} {...this.state} />} />
						<Route exact path="/schools" render={(props) => <SchoolsView {...props} {...this.state} />} />
						<Route exact path="/teams" render={(props) => <TeamsView {...props} {...this.state} />} />
						<Route exact path="/glossary" render={(props) => <GlossaryView {...props} {...this.state} />} />
						<Route exact path="/issues" render={(props) => <IssuesView {...props} {...this.state} />} />
						<Route exact path="/decisions" render={(props) => <BusinessDecisionsView {...props} {...this.state} /> } />
						<Route exact path="/settings" render={(props) => <SettingsView {...props} {...this.state} />} />
						<Route exact path="/settings/deadlines" render={(props) => <SettingsDeadlinesView {...props} {...this.state} />} />
						<Route exact path="/judges" render={(props) => <JudgesView {...props} {...this.state} />} />
						<Route exact path="/teachers" render={(props) => <TeachersView {...props} {...this.state} />} />
						<Route exact path="/marked" render={(props) => <MarkedSubmissionView {...props} {...this.state} />} />

						{/* Redirect for, namely PWAs, that puts the user back to the homepage after a bad navigation. 
						    Can happen when navigating to /static/index.html (such as ain PWAs with the start_url), or any other
							URL defined in the Django urls.py, which point to the react app, but do not have Routes defined.
							This may also occur if there is a url implemented in urls.py but not in React, such as during
							development, or if such changes were to make it to production.
							Mostly for PWAs, but also acts as a safe guard in case of any bugs being introduced. */}
						<Route path="*" render={() => <Redirect to="/" />} />
					</Switch>
				</Router>
			</React.Fragment>
		);
	}
}
