/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import * as React from 'react';

import { mount, shallow } from 'enzyme';

import { BusinessDecisionsView, BusinessDecisionsViewProps } from './BusinessDecisionsView';
import { MemoryRouter as Router } from 'react-router';

describe('BusinessDecisionsView', () => {
	const getProps = (): BusinessDecisionsViewProps => (
		{
			history: {} as any,
			location: {} as any,
			match: {} as any,
			updateAlertBar: async () => { },
			alert: {} as any,
			setDisplayMilestoneBar: async () => { },
			displayMilestoneBar: {} as any,
			testRun: true
		}
	)

	const getComponent = (props: BusinessDecisionsViewProps) => (
		<BusinessDecisionsView {...props} />
	);


	it('Should render correctly with shallow', () => {
		const component = shallow(
			getComponent(getProps())
		);
		expect(component.debug()).toMatchSnapshot();
	});

	it('Should render correctly with mount', () => {
		// have to include router as will fail with react-router problem due to use of specific functions
		const component = mount(
			<Router>
				{getComponent(getProps())}
			</Router>
		);
		expect(component.debug()).toMatchSnapshot();
	});
});