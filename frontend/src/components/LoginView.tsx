/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Row, Container, Col } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';

import { CredentialForm, PageProps, AlertBar } from './elements';

import './LoginView.css';

export interface LoginViewProps extends RouteComponentProps, PageProps { }

interface State { }

export class LoginView extends React.Component<LoginViewProps, State> {
	render() {
		return (
			<React.Fragment>
				<div className='mainbody'>
					<Container>
						<Row>
							<Col />

							<Col xs={10} md={6} className='login-header-image'>
								<Row>
									<img
										width="1836"
										height="736"
										alt='YE Scotland logo'
										src={`${process.env.PUBLIC_URL}/assets/YEScotland.png`}
										className='login-logo' />
								</Row>
							</Col>

							<Col />
						</Row>
						<Row>
							<Col />

							<Col xs={10} md={6} className='login-header-title'>
								YE Scotland Business Challenge
							</Col>

							<Col />
						</Row>

						<Row>
							<AlertBar {...this.props} />
						</Row>



						<Row className='login-credential-form'>
							<Col />

							<Col xs={10} md={6}>
								<CredentialForm {...this.props} />
							</Col>

							<Col />
						</Row>
					</Container>

				</div>
			</React.Fragment >
		);
	}
}
