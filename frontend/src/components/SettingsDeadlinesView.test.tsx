/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import * as React from 'react';

import { mount, shallow } from 'enzyme';

import { SettingsDeadlinesView, SettingsDeadlinesViewProps } from './SettingsDeadlinesView';
import { MemoryRouter as Router } from 'react-router';

describe('SettingsDeadlinesView', () => {
	const getProps = (): SettingsDeadlinesViewProps => (
		{
			history: { push: () => { } } as any,
			location: {} as any,
			match: {} as any,
			updateAlertBar: async () => { },
			alert: {} as any,
			setDisplayMilestoneBar: async () => { },
			displayMilestoneBar: {} as any,
			testRun: true
		}
	)

	const getComponent = (props: SettingsDeadlinesViewProps) => (
		<SettingsDeadlinesView {...props} />
	);


	it('Should render correctly with shallow', () => {
		Date.now = jest.fn(() => new Date(Date.UTC(2022, 3, 19)).valueOf())
		const component = shallow(
			getComponent(getProps())
		);
		expect(component.debug()).toMatchSnapshot();
	});

	it('Should render correctly with mount', () => {
		Date.now = jest.fn(() => new Date(Date.UTC(2022, 3, 19)).valueOf())
		// have to include router as will fail with react-router problem due to use of specific functions
		const component = mount(
			<Router>
				{getComponent(getProps())}
			</Router>
		);
		expect(component.debug()).toMatchSnapshot();
	});
});