/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, Col, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import {FaCalendarTimes, FaDownload} from 'react-icons/fa';

import { BasePage, PageProps, csvDetailRESTLink, csvSummaryRESTLink } from './elements';

import './SettingsView.css';

import config from '../config';

export interface SettingsViewProps extends RouteComponentProps, PageProps { }

interface State {
	downloading_summary: boolean,
	downloading_detail: boolean
}

export class SettingsView extends React.Component<SettingsViewProps, State> {
	constructor(props: SettingsViewProps) {
		super(props);

		this.state = {
			downloading_detail: false,
			downloading_summary: false
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] !== "Admin") {
			this.props.history.push('/');
		}
	}

	downloadCSVContent = async (name: string, link: string) => {
		var element = document.createElement('a');
		element.setAttribute('target', '_blank');
		element.setAttribute('referer', 'noreferer');
		element.setAttribute('href', config.apiURL + link + "?token=" + JSON.parse(localStorage["tokens"])["access"] + "&" + String(Date.now()));
		element.setAttribute('download', name);

		element.style.display = 'none';
		document.body.appendChild(element);

		element.click();

		document.body.removeChild(element);
	}

	handleDownloadDetailedCSV = async () => {
		this.setState({downloading_detail: true});
		
		await this.downloadCSVContent("full_database.csv", csvDetailRESTLink);
		
		this.setState({downloading_detail: false});
	}

	handleDownloadSummaryCSV = async () => {
		this.setState({downloading_summary: true});
		
		await this.downloadCSVContent("summarised_database.csv", csvSummaryRESTLink);
		
		this.setState({downloading_summary: false});
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={true} title={'Game Settings'}>

					<Row style={{marginBottom: ".5rem"}}>
						<Col>
							<label>
								<strong>
									Deadlines:
								</strong>
							</label>
						</Col>
					</Row>
					<Row style={{marginBottom: "1rem"}}>
						<Col>
							<a href={'/settings/deadlines'} style={{textDecoration: "none"}}>
								<Button>
									<FaCalendarTimes /> Update Deadlines
								</Button>
							</a>
						</Col>
					</Row>
					<Row style={{marginBottom: ".5rem", marginTop: "2em"}}>
						<Col>
							<label>
								<strong>
									Downloads:
								</strong>
							</label>
						</Col>
					</Row>
					<Row>
						<Col>
							<Button
								onClick={this.handleDownloadSummaryCSV}
								disabled={this.state.downloading_summary}>
								<FaDownload /> Download Database Summary
							</Button>
						</Col>
					</Row>
					<Row style={{paddingTop: ".5rem"}}>
						<Col>
							<Button
								onClick={this.handleDownloadDetailedCSV}
								disabled={this.state.downloading_detail}>
								<FaDownload /> Download Full Database
							</Button>
						</Col>
					</Row>

				</BasePage>
			</React.Fragment>
		);
	}
}
