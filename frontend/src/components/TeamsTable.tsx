/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaTrash, FaSpinner, FaCog } from 'react-icons/fa';

import { PageProps, resolveGETCall, teamsMilestonesRESTLink, TeamsMilestonesREST, userSpecificOperationsRESTLink, RESTError, MilestoneBar, resolveDELETECall, getActualURL, changePasswordRESTLink, ChangePasswordREST, resolvePUTCall, ErrorMessageText, PlaceholderLoading } from './elements';

import './TeamsTable.css';

export interface TeamsTableProps extends RouteComponentProps, PageProps {
	setLoaded: (loaded: boolean) => Promise<void>,
	teacher?: string
}

interface State extends TeamsMilestonesREST {
	logo_hashes: string,
	loadingResetPassword: boolean,

	loaded: boolean
}

export class TeamsTable extends React.PureComponent<TeamsTableProps, State> {
	constructor(props: TeamsTableProps) {
		super(props);

		this.state = {
			team_milestones: [],
			team_names: [],
			team_logo_stubs: [],
			logo_hashes: "" + Date.now(),

			loadingResetPassword: false,

			loaded: false
		};
	}

	componentDidMount = async () => {
		// load all teams who signed up through this teacher
		const query: string = (this.props.teacher !== undefined) ? ("?teacher=" + this.props.teacher) : "";
		const path: string = teamsMilestonesRESTLink + query;

		const result: Result<TeamsMilestonesREST, RESTError> = await resolveGETCall<TeamsMilestonesREST>(path, true);

		result
			.map(res => {
				this.setState({ ...res, loaded: true });
				
				this.props.setLoaded(true);
				
				return null; // to prevent warnings
			})
			.mapErr(err => {
				console.error(err);
				this.props.updateAlertBar(err.message, "warning", true);
			});
	}

	handleDeletePress = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		const index: number = Number(e.currentTarget.id);
		const selectedTeamName: string = this.state.team_names[index];

		const path: string = userSpecificOperationsRESTLink + selectedTeamName;
		const result: Result<{}, RESTError> = await resolveDELETECall<{}>(path, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully deleted the team", "success", true);

				this.state.team_milestones.splice(index, 1);
				this.state.team_names.splice(index, 1);
				this.state.team_logo_stubs.splice(index, 1);

				this.setState({ logo_hashes: "" + Date.now() });

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	handleResetPassword = async (event: React.MouseEvent<HTMLButtonElement>) => {
		this.setState({loadingResetPassword : true});

		const index: number = Number(event.currentTarget.id);
		const selectedTeamName: string = this.state.team_names[index];

		const path: string = changePasswordRESTLink + selectedTeamName;

		const result : Result<ChangePasswordREST, RESTError> = await resolvePUTCall<ChangePasswordREST, {}>(path, {}, true);

		result
			.map(res => {
				if (res.message !== undefined)
					this.props.updateAlertBar(<ErrorMessageText statusText={"Success"} message={res.message} />, 'success', true);
				else
					this.props.updateAlertBar(<ErrorMessageText statusText={"Success"} message={"Successfully changed user's password"} />, 'success', true);

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, 'danger', true);
			});

		this.setState({loadingResetPassword : false});
	}

	render() {
		return (
			<React.Fragment>
				{this.state.loaded ?
					<React.Fragment>
						<table className='teams-list' style={{width: "100%"}}>
							<thead>
								<tr className='teams-list-header'>
									<th>
										Logo
									</th>
									<th>
										Team name
									</th>
									<th>
										Current Progress
									</th>
									<th>
										Operations
									</th>
								</tr>
							</thead>

							<tbody>
								{this.state.team_milestones.map(
									(milestones, index) => {
										return (
											<tr className='teams-list-elem' key={this.state.team_names[index]}>
												<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])} style={{cursor: "pointer"}}>
													{this.state.team_logo_stubs[index] !== "" ?
														<React.Fragment>
															<div className='teams-table-logo'>
																<img
																	alt={this.state.team_names[index] + "'s logo"}
																	src={getActualURL(this.state.team_logo_stubs[index], this.state.logo_hashes)} />
															</div>
														</React.Fragment>
														:
														<React.Fragment>
															<div>
																<strong>{this.state.team_names[index] + ' '}</strong> have not yet submitted a logo
															</div>
														</React.Fragment>
													}
												</td>
												<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])} style={{cursor: "pointer"}}>
													{this.state.team_names[index]}
												</td>
												<td>
													<MilestoneBar {...this.props} list={milestones} adjusted={false} />
												</td>
												<td>
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleDeletePress}
														variant='danger'
														className={'teams-table-button'}>
														<FaTrash />
													</Button>
													{' '}
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleResetPassword}
														variant='primary'
														disabled={this.state.loadingResetPassword}
														className={'teams-table-button'}>
														{this.state.loadingResetPassword ?
															<React.Fragment>
																<FaSpinner className='spinner' /> Reset Password
															</React.Fragment>
														:
															<React.Fragment>
																<FaCog /> Reset Password
															</React.Fragment>
														}
													</Button>
												</td>
											</tr>
										)
									}
								)

								}
							</tbody>
						</table>
					</React.Fragment>
					:
					<React.Fragment>
						<PlaceholderLoading {...this.props} />
					</React.Fragment>
				}
			</React.Fragment>
		)
	}
}