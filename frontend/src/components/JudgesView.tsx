/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, ProgressBar } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaTrash, FaSpinner, FaCog } from 'react-icons/fa';

import { BasePage, PageProps, resolveGETCall, judgesProgressRESTLink, JudgesProgressREST, userSpecificOperationsRESTLink, RESTError, resolveDELETECall, changePasswordRESTLink, ChangePasswordREST, resolvePUTCall, ErrorMessageText } from './elements';

import './JudgesView.css';

export interface JudgesViewProps extends RouteComponentProps, PageProps { }

interface State extends JudgesProgressREST {
	loaded: boolean,
	loadingResetPassword: boolean,

	hash: string
}

export class JudgesView extends React.Component<JudgesViewProps, State> {
	constructor(props: JudgesViewProps) {
		super(props);

		this.state = {
			judge_progress: [],
			judge_names: [],
			judge_marked: [],
			judge_total: [],
			loaded: false,

			loadingResetPassword: false,

			hash: ""
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Admin") {
			// load all judges who signed up through this teacher
			const result: Result<JudgesProgressREST, RESTError> = await resolveGETCall<JudgesProgressREST>(judgesProgressRESTLink, true);

			result
				.map(res => {
					this.setState({ ...res, loaded: true });
					return null; // to prevent warnings
				})
				.mapErr(err => {
					console.error(err);
					this.props.updateAlertBar(err.message, "warning", true);
				});
		} else
			this.props.history.push('/');
	}

	handleDeletePress = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		const index: number = Number(e.currentTarget.id);
		const selectedTeamName: string = this.state.judge_names[index];

		const path: string = userSpecificOperationsRESTLink + selectedTeamName;
		const result: Result<{}, RESTError> = await resolveDELETECall<{}>(path, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully deleted the judge user", "success", true);

				this.state.judge_progress.splice(index, 1);
				this.state.judge_names.splice(index, 1);
				this.state.judge_marked.splice(index, 1);
				this.state.judge_total.splice(index, 1);

				this.setState({hash: String(Date.now())})

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	handleResetPassword = async (event: React.MouseEvent<HTMLButtonElement>) => {
		this.setState({loadingResetPassword : true});

		const index: number = Number(event.currentTarget.id);
		const selectedTeamName: string = this.state.judge_names[index];

		const path: string = changePasswordRESTLink + selectedTeamName;

		const result : Result<ChangePasswordREST, RESTError> = await resolvePUTCall<ChangePasswordREST, {}>(path, {}, true);

		result
			.map(res => {
				if (res.message !== undefined)
					this.props.updateAlertBar(<ErrorMessageText statusText={"Success"} message={res.message} />, 'success', true);
				else
					this.props.updateAlertBar(<ErrorMessageText statusText={"Success"} message={"Successfully changed user's password"} />, 'success', true);

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, 'danger', true);
			});

		this.setState({loadingResetPassword : false});
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded} title={'Judges'}>
					<table className='judges-list' style={{width: "100%"}}>
						<thead>
							<tr className='judges-list-header'>
								<th>
									Judges Username
								</th>
								<th>
									Number of Marked
								</th>
								<th>
									Current Progress
								</th>
								<th>
									Operations
								</th>
							</tr>
						</thead>

						<tbody>
							{this.state.judge_names.map(
								(name, index) => {
									return (
										<tr className='judges-list-elem' key={this.state.judge_names[index]}>
											<td>
												{this.state.judge_names[index]}
											</td>
											<td>
												<span className='key-value'>
													{this.state.judge_marked[index]}/{this.state.judge_total[index]}
												</span>
											</td>
											<td>
												{/* <MilestoneBar {...this.props} list={milestones} /> */}
												<ProgressBar striped now={this.state.judge_progress[index]} variant='primary' />
											</td>
											<td>
												<Button
													size={'sm'}
													id={String(index)}
													onClick={this.handleDeletePress}
													variant='danger'
													className={'judges-table-button'}>
													<FaTrash />
												</Button>
												{' '}
												<Button
													size={'sm'}
													id={String(index)}
													onClick={this.handleResetPassword}
													variant='primary'
													disabled={this.state.loadingResetPassword}
													className={'judges-table-button'}>
													{this.state.loadingResetPassword ?
														<React.Fragment>
															<FaSpinner className='spinner' /> Reset Password
														</React.Fragment>
													:
														<React.Fragment>
															<FaCog /> Reset Password
														</React.Fragment>
													}
												</Button>
											</td>
										</tr>
									)
								}
							)

							}
						</tbody>
					</table>

				</BasePage>
			</React.Fragment >
		);
	}
}

