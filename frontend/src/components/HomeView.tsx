/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, Col, Form, FormControl, Modal, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaCog, FaSpinner, FaUpload } from 'react-icons/fa';

import { BasePage, PageProps, resolveGETCall, SecretKeyREST, SecretKeyRESTIndividual, RESTError, secretKeyRESTLink, IntroContentREST, introContentRESTLink, changePasswordRESTLink, ChangePasswordRESTSubmit, ChangePasswordREST, resolvePUTCall, ErrorMessageText, getActualURL } from './elements';

import './HomeView.css';

export interface HomeViewProps extends RouteComponentProps, PageProps { }

interface State extends SecretKeyRESTIndividual, ChangePasswordRESTSubmit {
	displayChangePassword: boolean,
	changePasswordLoading: boolean,
	introVideoHash: string,
	introContent?: IntroContentREST
}

export class HomeView extends React.Component<HomeViewProps, State> {
	constructor(props: HomeViewProps) {
		super(props);

		this.state = {
			secret_key: "",
			key_type: "",
			school: "",
			areas: [],

			displayChangePassword: false,
			changePasswordLoading: false,

			new_password: "",

			introVideoHash: "" + Date.now()
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Teacher") {
			const result: Result<SecretKeyREST, RESTError> = await resolveGETCall<SecretKeyREST>(secretKeyRESTLink, true);

			result
				.map(res => {
					this.setState({ secret_key: res.keys[0].secret_key });

					return null; // to prevent warnings
				})
				.mapErr(err => {
					console.error(err);

					this.props.updateAlertBar(err.message, "warning", true);
				})
		}

		const introResult: Result<IntroContentREST, RESTError> = await resolveGETCall<IntroContentREST>(introContentRESTLink, true);

		introResult
			.map(res => {
				console.log(res)

				
				this.setState({ introContent: res });

				return null;
			})
			.mapErr(err => {
				console.log("Error encountered")
				console.log(err);
			})
	}

	handleChangePassword = async () => {
		this.setState({displayChangePassword : true});
	}

	handleChangePasswordSubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		this.setState({ changePasswordLoading: true });

		const path: string = changePasswordRESTLink + localStorage['username'];

		const data: ChangePasswordRESTSubmit = {
			new_password: this.state.new_password
		};

		const result: Result<ChangePasswordREST, RESTError> = await resolvePUTCall<ChangePasswordREST, ChangePasswordRESTSubmit>(path, data, true);

		result
			.map(res => {
				this.setState({ new_password: "" });

				if (res.message === undefined)
					this.props.updateAlertBar(<ErrorMessageText statusText="Success" message="Successfully changed password" />, 'success', true);
				else 
					this.props.updateAlertBar(<ErrorMessageText statusText="Success" message={res.message} />, 'success', true);

				return null; // necessary to prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, 'danger', true);
			});

		this.setState({ displayChangePassword : false, changePasswordLoading: false });
	}

	render() {
		return (
			<React.Fragment>
				<Modal show={this.state.displayChangePassword || this.state.changePasswordLoading} onHide={() => { this.setState({displayChangePassword: false}); }}>
					<Form onSubmit={this.handleChangePasswordSubmit}>
						<Modal.Header closeButton>
							<Modal.Title>
								Change Password
							</Modal.Title>
						</Modal.Header>

						<Modal.Body>
							<Form.Label>
								New Password
							</Form.Label>
							<FormControl
								type='password'
								onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ new_password : event.currentTarget.value }); }} />
						</Modal.Body>

						<Modal.Footer>
							<Button type='submit' variant='primary' disabled={this.state.changePasswordLoading}>
								{this.state.changePasswordLoading ?
									<React.Fragment>
										<FaSpinner className='spinner' /> Change Password
									</React.Fragment>
								:
									<React.Fragment>
										<FaUpload /> Change Password
									</React.Fragment>
								}
							</Button>
						</Modal.Footer>
					</Form>
				</Modal>


				<BasePage {...this.props} loaded={true} title={'Introduction'}>

					{this.state.introContent?.introText && (
						<Row>
							<Col>
								{this.state.introContent.introText}
							</Col>
						</Row>)}
					{this.state.introContent?.introVideoUrl && 
						<Row id="introVideoRow">
							<Col>
								<video id="introVideo" controls preload="auto">
									<source src={getActualURL(this.state.introContent.introVideoUrl, this.state.introVideoHash)} 
									type="video/mp4" />
									Your browser does not support the video tag.
								</video>
							</Col>
						</Row>}
					{this.state.introContent?.introVideoUrl && this.state.introContent?.introYtUrl &&
						<Row id="IntroYtLinkRow">
							<Col>
								Video not playing? Watch it on YouTube <a href={this.state.introContent.introYtUrl} target="_blank"
								rel="noreferrer noopener">here</a>
							</Col>
						</Row>}
						

					{(localStorage['user_type'] === "Teacher") &&
						<Row>
							<Col>
								<div className='secret-key-box'>
									Secret Key: {' '}
									{this.state.secret_key !== ''
										?
										<span className='key-value'>{this.state.secret_key}</span>
										:
										<span className='key-value'>LOADING...</span>
									}
								</div>
							</Col>
						</Row>
					}

					<Row>
						<Col>
							<h3>
								Settings:
							</h3>
						</Col>
					</Row>
					<Row>
						<Col>
							<Button
								variant='primary'
								onClick={this.handleChangePassword}>
								<FaCog /> Change Password
							</Button>
						</Col>
					</Row>

				</BasePage>
			</React.Fragment>
		);
	}
}
