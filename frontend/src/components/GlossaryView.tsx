/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, Form, InputGroup } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { FaTimes } from 'react-icons/fa';

import { BasePage, PageProps } from './elements';
import { GLOSSARY_ENTRIES } from './elements/GlossaryEntries';

import './GlossaryView.css';

export interface GlossaryViewProps extends RouteComponentProps, PageProps { }

interface State {
	search_phrase: string
}

export class GlossaryView extends React.Component<GlossaryViewProps, State> {
	constructor(props: GlossaryViewProps) {
		super(props);

		this.state = {
			search_phrase: ""
		};
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={true} title={'Glossary'}>
				
					<Form.Group style={{paddingBottom : "2rem"}}>
						<Form.Label>
							Search Glossary
						</Form.Label>
						<InputGroup>
							<Form.Control
								value={this.state.search_phrase}
								placeholder={"European Union"}
								onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.setState({search_phrase : event.currentTarget.value })} />
							<Button onClick={() => this.setState({search_phrase : ""})}>
								<FaTimes />
							</Button>
						</InputGroup>
					</Form.Group>

					<table className='glossary-list'>
						<thead>
							<tr className='glossary-list-header'>
								<th>
									<span>
										<strong>Word/Phrase</strong>
									</span>
								</th>
								<th>
									<span>
										<strong>Definition</strong>
									</span>
								</th>
							</tr>
						</thead>
						<tbody>
							{GLOSSARY_ENTRIES
								.filter(elem => {
									return (
										elem.phrase.toLowerCase().includes(this.state.search_phrase.toLowerCase()) 
										||
										elem.definition.toLowerCase().includes(this.state.search_phrase.toLowerCase()) 
										||
										(elem.aka !== undefined && elem.aka.toLowerCase().includes(this.state.search_phrase.toLowerCase()))
									);
								})
								.map(elem => {
									return (
										<React.Fragment key={elem.phrase}>
											<tr className='glossary-list-elem'>
												<td>
													<span>
														<i>{elem.phrase + ((elem.aka !== undefined) ? " (" + elem.aka + ")" : "")}</i>
													</span>
												</td>
												<td>
													<span>
														{elem.definition}
													</span>
												</td>
											</tr>
										</React.Fragment>
									);
								})
							}
						</tbody>
					</table>

				</BasePage>
			</React.Fragment >
		);
	}
}

