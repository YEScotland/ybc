/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, Col, Form, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';

import { BasePage, PageProps, resolveGETCall, DeadlineREST, deadlineRESTLink, RESTError, resolvePUTCall } from './elements';

import './SettingsDeadlinesView.css';
import { FaSpinner, FaUpload } from 'react-icons/fa';

export interface SettingsDeadlinesViewProps extends RouteComponentProps, PageProps { }

interface State extends DeadlineREST {
	is_submitting: boolean
	loaded: boolean
}

export class SettingsDeadlinesView extends React.Component<SettingsDeadlinesViewProps, State> {
	constructor(props: SettingsDeadlinesViewProps) {
		super(props);

		this.state = {
			submission_deadline: new Date().toISOString().split('T')[0],
			voting_deadline: new Date().toISOString().split('T')[0],
			judging_deadline: new Date().toISOString().split('T')[0],

			is_submitting: false,
			loaded: false
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Admin") {
			const result: Result<DeadlineREST, RESTError> = await resolveGETCall<DeadlineREST>(deadlineRESTLink, true);

			result
				.map(res => {
					this.setState({ ...res });

					return null; // to prevent warnings
				})
				.mapErr(err => {
					// Same as marketplace submission, do not alert the user about non-created deadlines as
					// they do not need notified due to them probably creating one while on the apge
					// this.props.updateAlertBar(err.message, "danger", true);
				})

			this.setState({ loaded: true }); // setting loaded here as error codes due to ther being no deadline causes page to never load 
		} else {
			this.props.history.push('/');
		}
	}
 
	handleSubmitDeadline = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		this.setState({is_submitting : true});

		const data: DeadlineREST = {
			submission_deadline : this.state.submission_deadline,
			voting_deadline: this.state.voting_deadline,
			judging_deadline: this.state.judging_deadline
		}

		const result: Result<DeadlineREST, RESTError> = await resolvePUTCall<DeadlineREST, DeadlineREST>(deadlineRESTLink, data, true);
	
		result
			.map(res => {
				this.setState({ ...res });

				return null; // otherwise error
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			})

		this.setState({is_submitting : false});
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded} title={'Deadline Settings'}>

					<Form onSubmit={this.handleSubmitDeadline}>
						<Row>
							<Col md={2} />

							<Col>
								<Form.Group className='form-group mb-3'>
									<Form.Label>
										<strong>Submission Deadline:</strong>
									</Form.Label>
									<label className='deadlines-help-text'>
										<i>
											The deadline for all Teams to submit their marketplace stall. After which, judges are able to mark the submissions and the marketplace opens
										</i>
									</label>
									<Form.Control
										value={this.state.submission_deadline}
										onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({submission_deadline: event.currentTarget.value}); } }
										type={"date"}
										min={"1970-01-01"}
										/>
								</Form.Group>

								<Form.Group className='mb-3'>
									<Form.Label>
										<strong>Voting Deadline:</strong>
									</Form.Label>
									<label className='deadlines-help-text'>
										<i>
											The deadline for all Teams to vote on their favourite marketplace stalls
										</i>
									</label>
									<Form.Control
										value={this.state.voting_deadline}
										onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({voting_deadline: event.currentTarget.value}); } }
										type={"date"}
										min={this.state.submission_deadline}
										/>
								</Form.Group>

								<Form.Group className='mb-3'>
									<Form.Label>
										<strong>Judging Deadline:</strong>
									</Form.Label>
									<label className='deadlines-help-text'>
										<i>
											The deadline for all Judges to submit their marking
										</i>
									</label>
									<Form.Control
										value={this.state.judging_deadline}
										onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({judging_deadline: event.currentTarget.value}); } }
										type={"date"}
										min={this.state.submission_deadline}
										/>
								</Form.Group>
							</Col>

							<Col md={2} />
						</Row>

						<Button
							type="submit"
							style={{float: "right"}}
							disabled={this.state.is_submitting}
							variant={this.state.is_submitting ? "outline-primary" : "primary"}>
							{this.state.is_submitting ?
								<React.Fragment>
									<FaSpinner className="spinner" /> Upload
								</React.Fragment>
							:
								<React.Fragment>
									<FaUpload /> Upload
								</React.Fragment>
							}
						</Button>
					</Form>

				</BasePage>
			</React.Fragment>
		);
	}
}
