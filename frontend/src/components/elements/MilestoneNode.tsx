/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { OverlayTrigger, Popover } from 'react-bootstrap';
import { FaCheck, FaEllipsisH, FaTimes } from 'react-icons/fa';
import { Col, Row } from 'react-bootstrap';

import { ChecklistMilestone, PageProps } from './utils';

import './MilestoneNode.css';

interface Props extends PageProps, ChecklistMilestone {
	adjusted?: boolean
}
interface State {
	focussed: boolean
}

export const PROGRESS_BAR_COLOUR_COMPLETE: string = "var(--primary)";
export const PROGRESS_BAR_COLOUR_CURRENT: string = "lightgrey";
export const PROGRESS_BAR_COLOUR_INCOMPLETE: string = "var(--secondary)";
export const PROGRESS_BAR_NODE_COLOUR_COMPLETE: string = "green";
export const PROGRESS_BAR_NODE_COLOUR_INCOMPLETE: string = "crimson";
export const PROGRESS_BAR_NODE_COLOUR_CURRENT: string = "orange";

export class MilestoneNode extends React.PureComponent<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			focussed: false
		};
	}

	handleToggle = (event: React.ChangeEvent<HTMLDetailsElement>) => {
		this.setState({ focussed: event.currentTarget.hasAttribute("open") });
	}

	handleBlur = async (event: React.ChangeEvent<HTMLElement>) => {
		this.setState({focussed: false});
		event.currentTarget.removeAttribute("open");
	}

	render() {
		var colour: string;
		if (this.props.current) {
			colour = PROGRESS_BAR_COLOUR_CURRENT;
		} else if (this.props.completed) {
			colour = PROGRESS_BAR_COLOUR_COMPLETE;
		} else {
			colour = PROGRESS_BAR_COLOUR_INCOMPLETE;
		}

		return (
			<OverlayTrigger show={this.state.focussed} placement="bottom" overlay={
				<Popover>
					<Popover.Header
						as="h4">
						{this.props.name}
					</Popover.Header>
					<Popover.Body>
						{this.props.tasks.map((task) => {
							return (
								<Row className='milestone-task-row' key={task.text}>
									<Col md={1}>
										{task.completed ?
											<FaCheck className='progress-bar-node-task-icon' style={{ color: PROGRESS_BAR_NODE_COLOUR_COMPLETE }} />
											:
											<React.Fragment>
												{this.props.current ?
													<FaEllipsisH className='progress-bar-node-task-icon' style={{ color: PROGRESS_BAR_NODE_COLOUR_CURRENT }} />
													:
													<FaTimes className='progress-bar-node-task-icon' style={{ color: PROGRESS_BAR_NODE_COLOUR_INCOMPLETE }} />
												}
											</React.Fragment>
										}
									</Col>
									<Col>
										<div style={{ width: '100%', textAlign: 'right' }}>
											<a href={task.link} >
												{task.text}
											</a>
										</div>
									</Col>
								</Row>
							);
						}
						)}
					</Popover.Body>
				</Popover>
			}>
				<details
					style={{ backgroundColor: colour }}
					className='progress-bar-milestone'
					onBlur={this.handleBlur}
					onToggle={this.handleToggle}>
					<summary className={this.props.adjusted ? "adjusted" : ""}>{""}</summary>
				</details>
			</OverlayTrigger>
		);
	}
}