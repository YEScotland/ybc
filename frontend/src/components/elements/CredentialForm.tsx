/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React, { ReactElement } from 'react';

import { Result } from 'neverthrow';
import { Button, Col, Form, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';

import { TokenREST, RegistrationRESTSubmit, resolvePOSTCall, resolvePUTCall, PageProps, loginRESTLink, registrationRESTLink, RESTError } from './utils';

import './CredentialForm.css';

interface Props extends RouteComponentProps, PageProps { }

interface State extends RegistrationRESTSubmit {
	register: boolean,
	actual_username: string
}

export class CredentialForm extends React.PureComponent<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			username: "",
			actual_username: "",

			password: "",
			register: false,
			secret_key: ""
		};
	}

	handlSubmitLogin = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		const data: RegistrationRESTSubmit = {
			username: this.state.actual_username,
			password: this.state.password,
			secret_key: this.state.secret_key
		};

		var result: Result<TokenREST, RESTError>;
		if (this.state.register)
			result = await resolvePOSTCall<TokenREST, RegistrationRESTSubmit>(registrationRESTLink, data);
		else
			result = await resolvePUTCall<TokenREST, RegistrationRESTSubmit>(loginRESTLink, data);

		result
			.map(res => {
				localStorage.setItem("tokens", JSON.stringify(res.tokens));
				localStorage.setItem("username", res.username);
				localStorage.setItem("user_type", res.user_type);

				this.props.updateAlertBar("Successfully logged in!", "success", true);

				this.props.history.push('/');

				return null; // necessary to silence warning
			})
			.mapErr(err => {
				var message: string | ReactElement;

				if (err.type === "Error")
					message = "Internal server error, please retry in a couple minutes";
				else
					message = err.message;

				this.props.updateAlertBar(message, "danger", true);
			});
	}

	render() {
		return (
			<React.Fragment>
				
				<Row style={{ textAlign: 'center' }}>
					<h1 className="credential-form-title">
						{this.state.register ?
							"Sign Up"
							:
							"Log In"
						}
					</h1>
				</Row>

				<Row>
					<Form onSubmit={this.handlSubmitLogin}>
						<Form.Group className="mb-3" controlId="username">
							<Form.Label className='credential-form-label'>Username: </Form.Label>
							<Form.Floating>
								<Form.Control
									required
									type="text"
									placeholder="schoolname.teamname"
									value={this.state.username}
									onChange={async (event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ username: event.target.value, actual_username: event.target.value.replace(/\s+/g, '') }) }} />
								<label>schoolname.teamname</label>
							</Form.Floating>
						</Form.Group>
						{this.state.register &&
							<React.Fragment>
								<Form.Group className="mb-3" controlId="username">
									<Form.Floating>
										<Form.Control
											disabled
											value={this.state.actual_username} />
										<label>Your username will be:</label>
									</Form.Floating>
								</Form.Group>
							</React.Fragment>
						}

						<Form.Group className="mb-3" controlId="password">
							<Form.Label className='credential-form-label'>Password: </Form.Label>
							<Form.Floating>
								<Form.Control
									required
									type="password"
									placeholder="Password"
									value={this.state.password}
									onChange={async (event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ password: event.target.value }) }} />
								<label>Password</label>
							</Form.Floating>
						</Form.Group>

						{this.state.register &&
							<React.Fragment>
								<Form.Group className="mb-3" controlId="secret_key">
									<Form.Label className='credential-form-label'>Secret Key: </Form.Label>
									<Form.Floating>
										<Form.Control
											required
											type="text"
											placeholder="1234efGHIJ"
											value={this.state.secret_key}
											onChange={async (event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ secret_key: event.target.value }) }} />
										<label>1234efGHIJ</label>
									</Form.Floating>
								</Form.Group>
							</React.Fragment>
						}

						<Row>
							<Col>
								<Button onClick={() => this.setState(prev => ({ register: !prev.register }))} variant="outline-primary">
									{this.state.register ? "Already registered?" : "Not registered?"}
								</Button>
							</Col>
							<Col>
								<Button variant="outline-primary" type="submit" style={{ float: "right" }}>
									{this.state.register ? "Register" : "Log in"}
								</Button>
							</Col>
						</Row>
					</Form>
				</Row>
			</React.Fragment>
		);
	}
}
