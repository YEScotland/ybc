/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Col, Row } from 'react-bootstrap';
import { Grid } from 'react-loading-icons';

import { PageProps } from './utils';

import './PlaceholderLoading.css';

interface Props extends PageProps { }
interface State { }

export class PlaceholderLoading extends React.Component<Props, State> {
	render() {
		return (
			<React.Fragment>
				<Row>
					<Col>
						<div className='center-aligned-div'>
							<Grid
								fill='#06bcee'
								width='5rem'
								fillOpacity='1'
								strokeOpacity='1'
								speed='1'
							/>
						</div>
					</Col>
				</Row>
				<Row>
					<Col>
						<div className='center-aligned-div'>
							<p>
								<strong>
									Loading...
								</strong>
							</p>
						</div>
					</Col>
				</Row>
			</React.Fragment>
		);
	}
}