/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { DOCUMENT_ONLY_EXTENSION_REGEX, getActualURL } from './utils';

export interface ImageOrPDFViewerProps {
	stub: string,
	hash: string,
	alt: string,
	title: string
}

export class ImageOrPDFViewer extends React.Component<ImageOrPDFViewerProps, {}> {
	render() {
		return(
			<React.Fragment>
				{DOCUMENT_ONLY_EXTENSION_REGEX.test(this.props.stub) ?
					<iframe
						title={this.props.title}
						src={`https://docs.google.com/viewer?url=${getActualURL(this.props.stub, this.props.hash)}&embedded=true`} />
					:
					<img
						src={getActualURL(this.props.stub, this.props.hash)}
						alt={this.props.alt} />
				}
			</React.Fragment>
		)
	}
}