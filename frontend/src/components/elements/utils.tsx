/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import axios, { AxiosResponse } from 'axios';
import { ok, err, Result } from 'neverthrow';
import React, { ReactElement } from 'react';

import config from './config';

/* file upload regulation constants */
export const MAX_FILE_SUBMISSION_SIZE_Bytes: number = 10485760; // enforced by django
export const MAX_FILE_SUBMISSION_SIZE: string = "10Mb"; // enforced by django

export const ACCEPTABLE_PICTURE_FILE_EXTENSIONS: string[] = [ // copy of the list in the django views.py
	"apng",
	"avif",
	"gif",
	"jpg", "jpeg", "jfif", "pjpeg", "pjp",
	"png",
	"svg",
	"webp",
	"bmp",
	"ico", "cur",
	"tif", "tiff"
]

export const ACCEPTABLE_DOCUMENT_EXTENSIONS: string[] = [ // copy of the list in the django views.py
	'doc',
	'docx',
	"pdf",
	"apng",
	"avif",
	"gif",
	"jpg", "jpeg", "jfif", "pjpeg", "pjp",
	"png",
	"svg",
	"webp",
	"bmp",
	"ico", "cur",
	"tif", "tiff"
];

export const DOCUMENT_ONLY_EXTENSION_REGEX = /(pdf|doc|docx)$/;

/* Authentication headers strings for internal REST call authentication */
interface AuthenticationHeaders {
	headers: {
		Authorization: string
	}
}

/* Alert bar updater type that allows errors to persist across pages */
export interface AlertBarUpdater {
	show: boolean,
	message: string | ReactElement,
	variant: string
}

/* General props inherited by all pages from App.tsx */
export interface PageProps {
	updateAlertBar: (message: string | ReactElement, variant: string, show: boolean) => Promise<void>,
	setDisplayMilestoneBar: (value: boolean) => Promise<void>,
	alert: AlertBarUpdater, // only added as App.tsx's state is destructed and this is included as it is a psuedo-global set of value so that the alertbar can be shared across multiple pages
	displayMilestoneBar: boolean,
	testRun: boolean
}

/* JWT Tokens types */
export interface Tokens {
	access: string,
	refresh: string
}

/* Local storage type */
export interface LStorage extends TokenREST { }

/* Internal refresh token types */
interface RefreshTokensRESTSubmit {
	refresh: string
}

interface RefreshTokensREST {
	access: string
}

/* Secret key creation type */
export const secretKeyRESTLink: string = '/auth/secret_key';

export interface SecretKeyRESTSubmit {
	key_type: string,
	school: string,
	areas: string[]
}

export interface SecretKeyRESTIndividual extends SecretKeyRESTSubmit {
	secret_key: string
}

export interface SecretKeyREST {
	keys: SecretKeyRESTIndividual[]
}

/* Teacher's team's milestones REST */
export const teamsMilestonesRESTLink: string = '/user/milestones';
export interface TeamsMilestonesREST {
	team_milestones: ChecklistMilestoneList[],
	team_names: string[],
	team_logo_stubs: string[]
}

/* Issues REST types */
export const issuesRESTLink: string = "/issues";
export interface IssuesREST {
	team_logo_stubs: string[],
	team_names: string[],
	issue_descriptions: string[],
};

/* User's teacher check REST types */
export const usersAdministratorCheckRESTLink: string = "/is_users_admin/";
export interface UsersAdministratorCheckREST {
	is_users_admin: boolean
};

/* Issue specific REST types */
export const issueSpecificRESTLink: string = "/issue/";
export interface IssueSpecificREST {
	message: string
};

export interface IssueSpecificRESTSubmit {
	description: string
};

/* User operations REST types */
export const userSpecificOperationsRESTLink: string = '/user/operations/';

/* Local authorities REST types */
export const localAuthoritiesRESTLink = '/info/local_authorities';

export interface LocalAuthoritiesREST {
	local_authorities: string[]
}

/* Local Authority school REST types */
export const localAuthoritiesRESTLinkPrefix = '/info/local_authorities/';
export const localAuthoritiesRESTLinkSuffix = '/schools';

export interface LocalAuthoritiesSchool {
	value: string,
	local_authority: string
}

export interface LocalAuthoritiesSchoolsREST {
	schools: LocalAuthoritiesSchool[]
}

/* Authentication REST types */
export interface TokenREST {
	user_type: string,
	username: string,
	tokens: Tokens
}

export const loginRESTLink: string = '/auth/login/';
export interface LoginRESTSubmit {
	username: string,
	password: string,
}

export const registrationRESTLink: string = '/auth/register/';
export interface RegistrationRESTSubmit extends LoginRESTSubmit {
	secret_key: string
}

/* Marketplace Like REST types */
export const likeMarketplaceStallRESTLink: string = '/marketplace_like';
export interface LikeMarketplaceStallREST {
	liked: boolean
}

export const canLikeMarketplaceStallRESTLink: string = '/can_like';
export interface CanLikeMarketplaceStallREST {
	can_like: boolean
}

/* Milestones REST Objects */
export interface MilestoneTask {
	completed: boolean,
	text: string,
	link: string,
}

export interface ChecklistMilestone {
	completed: boolean,
	current: boolean,
	name: string,
	tasks: MilestoneTask[]
}

export interface ChecklistMilestoneList {
	milestones: ChecklistMilestone[],
	percentage: number
}

/* Offline Task submission REST call interfaces */
type MarketplaceSubmissionTypes = 'logo' | 'poster' | 'target_customer' | 'tagline' | 'company_name';

export const marketplaceStallRESTLinkPrefix = '/marketplace/';

export interface MarketplaceSubmission {
	encoded_logo: string,
	encoded_logo_extension: string,
	encoded_logo_mime_type: string,
	encoded_poster: string,
	encoded_poster_extension: string,
	encoded_poster_mime_type: string,
	encoded_target_customer: string,
	encoded_target_customer_extension: string,
	encoded_target_customer_mime_type: string,
	company_name: string,
	tagline: string,
	manifest: MarketplaceSubmissionTypes[]
}

export interface MarketplaceStall {
	company_name: string,
	tagline: string,
	logo_stub: string,
	poster_stub: string,
	target_customer_stub: string,
	team_username: string
}

/* Full marketplace REST types */
export const marketplaceStallsRESTLink: string = '/marketplace';

export interface MarketplaceStalls {
	stalls: MarketplaceStall[]
}

/* Objects returned by errors in the REST calls */
export interface RESTError extends ErrorMessageTextProps {
	type: string,
	status: number
}

/* Judge Submission REST types */
export const judgeSubmissionRESTLink: string = '/marketplace_judge/';
type JudgeSubmissionTypes = "target_customer" | "poster" | "tagline" | "logo";
export interface JudgeSubmission {
	target_customer_score: number,
	poster_score: number;
	tagline_score: number;
	logo_score: number;
	manifest: JudgeSubmissionTypes[];
}

/* CSV Downloading constants */
export const csvDetailRESTLink: string = "/csv/detail";
export const csvSummaryRESTLink: string = "/csv/summary";

/* Change Password REST types */
export const changePasswordRESTLink: string = '/auth/change_password/';
export interface ChangePasswordRESTSubmit {
	new_password?: string
};
export interface ChangePasswordREST {
	message?: string
};

/* Teacher list REST types */
export interface JudgesProgressREST {
	judge_progress: number[],
	judge_names: string[],
	judge_marked: number[],
	judge_total : number[]
}
export const judgesProgressRESTLink: string = "/judges_progress";

/* Teacher info REST types */
export interface TeachersInfoREST {
	teacher_names: string[],
	teacher_schools: string[],
	teacher_areas: string[]
}
export const teachersInfoRESTLink: string = '/teacher_info';

/* Notifications REST types */
export const notificationsRESTLink: string = '/number_of?';
export const notificationsIssuesRESTSuffix: string = 'object=Issues';
export interface NotificationsIssuesREST {
	number_of_issues : number
};

/* Deadlines REST types */
export const deadlineRESTLink: string = '/deadline';
export interface DeadlineREST {
	submission_deadline: string, // yyyy-mm-dd
	voting_deadline: string, // yyyy-mm-dd
	judging_deadline: string // yyyy-mm-dd
}

/* Business Decisions REST types and constants */
export interface Question {
	questionId: number,
	questionText: string,
	answerOptions: string[],
	selection?: number,
	animationLink?: string,
	yt_url?: string
};
export type QuestionREST = Question[];

export const businessDecisionRESTLink: string = '/decisions';


export interface IntroContentREST {
	introText?: string,
	introVideoUrl?: string,
	introYtUrl?: string
}

export const introContentRESTLink: string = '/intro';

/* Marked marketplace REST constant */
export const markedMarketplaceRESTLink: string = '/marked_marketplace';

/* React Function component for generating error message text */
export interface ErrorMessageTextProps {
	statusText: string,
	message: string | ReactElement
};
export const ErrorMessageText: React.FC<ErrorMessageTextProps> = ({ statusText, message }) => {
	return (
		<React.Fragment>
			<strong>{statusText}</strong>: {message}
		</React.Fragment>
	);
};

/* Message returned when neither new refresh tokens can be retrieved, nor the API can be successfully reached */
const UnauthenticatedErr: RESTError = {
	message: "Unable to authenticate user. Please register first, or log-in with correct credentials. If this error persists, please try again in a few minutes",
	type: "RESTError",
	status: 403,
	statusText: "Forbidden"
};
UnauthenticatedErr.message = <ErrorMessageText {...UnauthenticatedErr} />;

/* Function for determining what the absolute url of slug/path/url is for proper rendering */
export function getActualURL(stub: string, hash: string): string {
	if (stub.startsWith("https://"))
		return stub + "?" + hash;
	else
		return config.rootURL + stub + "?" + hash;
}

/* JWT Access token refresh function that is called whenever the access token expires */
export async function getNewAccessToken(): Promise<boolean> {
	try {
		const tokens: Tokens = JSON.parse(localStorage.tokens);

		if (tokens.refresh !== "") {
			const data: RefreshTokensRESTSubmit = {
				refresh: tokens.refresh
			};

			const result: Result<RefreshTokensREST, RESTError> = await resolvePOSTCall<RefreshTokensREST, RefreshTokensRESTSubmit>('/auth/refresh_tokens/', data, false, true);

			var errorFree: boolean = true;

			result
				.map(res => {
					tokens.access = res.access;
					localStorage.setItem("tokens", JSON.stringify(tokens));

					return null; // necessary to silence warning
				})
				.mapErr(err => {
					console.error(err.message);
					errorFree = false;
				});

			return errorFree;
		}

		return false;
	} catch (error) {
		return false;
	}
}

/* Helper function for making RESTful GET/retrieval calls */
export async function resolveGETCall<MessageT>(address: string, authentication: boolean = false, recursiveCall: boolean = false): Promise<Result<MessageT, RESTError>> {
	try {
		var res: AxiosResponse<MessageT>;

		if (!authentication) {
			res = await axios.get<MessageT>(config.apiURL + address);
		} else {
			const tokens: Tokens = JSON.parse(localStorage.tokens);
			const headers: AuthenticationHeaders = { headers: { "Authorization": `Bearer ${tokens.access}` } };
			res = await axios.get<MessageT>(config.apiURL + address, headers);
		}

		return ok(res.data);
	} catch (error) {
		if (recursiveCall) {
			var retErr: RESTError;
			const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;

			retErr = {
				message: hasResponseMessage ? error.response.data.message : error.message,
				type: hasResponseMessage ? "RESTError" : error.name,
				status: hasResponseMessage ? error.response.status : 401,
				statusText: hasResponseMessage ? error.response.statusText : "Unauthorized"
			};

			retErr.message = <ErrorMessageText {...retErr} />;

			return err(retErr);
		}

		const successfullyGotNewAccess: boolean = await getNewAccessToken();

		if (successfullyGotNewAccess) {
			return await resolveGETCall<MessageT>(address, authentication, true);
		}

		const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;
		const unauthenticatedError: RESTError = {
			message: hasResponseMessage ? error.response.data.message : UnauthenticatedErr.message,
			type: hasResponseMessage ? "RESTError" : UnauthenticatedErr.type,
			status: hasResponseMessage ? error.response.status : UnauthenticatedErr.status,
			statusText: hasResponseMessage ? error.response.statusText : UnauthenticatedErr.statusText
		};
		if (hasResponseMessage)
			unauthenticatedError.message = <ErrorMessageText {...unauthenticatedError} />

		return err(unauthenticatedError);
	}
}

/* Helper function for making RESTful POST/creation calls */
export async function resolvePOSTCall<MessageT, PayloadT>(address: string, data: PayloadT, authentication: boolean = false, recursiveCall: boolean = false): Promise<Result<MessageT, RESTError>> {
	try {
		var res: AxiosResponse<MessageT>;

		if (!authentication) {
			res = await axios.post<MessageT>(config.apiURL + address, data);
		} else {
			const tokens: Tokens = JSON.parse(localStorage.tokens);
			const headers: AuthenticationHeaders = { headers: { "Authorization": `Bearer ${tokens.access}` } };
			res = await axios.post<MessageT>(config.apiURL + address, data, headers);
		}

		return ok(res.data);
	} catch (error) {
		if (recursiveCall) {
			var retErr: RESTError;
			const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;

			retErr = {
				message: hasResponseMessage ? error.response.data.message : error.message,
				type: hasResponseMessage ? "RESTError" : error.name,
				status: hasResponseMessage ? error.response.status : 401,
				statusText: hasResponseMessage ? error.response.statusText : "Unauthorized"
			};

			retErr.message = <ErrorMessageText {...retErr} />;

			return err(retErr);
		}

		const successfullyGotNewAccess: boolean = await getNewAccessToken();

		if (successfullyGotNewAccess) {
			return await resolvePOSTCall<MessageT, PayloadT>(address, data, authentication, true);
		}

		const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;
		const unauthenticatedError: RESTError = {
			message: hasResponseMessage ? error.response.data.message : UnauthenticatedErr.message,
			type: hasResponseMessage ? "RESTError" : UnauthenticatedErr.type,
			status: hasResponseMessage ? error.response.status : UnauthenticatedErr.status,
			statusText: hasResponseMessage ? error.response.statusText : UnauthenticatedErr.statusText
		};
		if (hasResponseMessage)
			unauthenticatedError.message = <ErrorMessageText {...unauthenticatedError} />

		return err(unauthenticatedError);
	}
}

/* Helper function for making RESTful PUT/update calls */
export async function resolvePUTCall<MessageT, PayloadT>(address: string, data: PayloadT, authentication: boolean = false, recursiveCall: boolean = false): Promise<Result<MessageT, RESTError>> {
	try {
		var res: AxiosResponse<MessageT>;

		if (!authentication) {
			res = await axios.put<MessageT>(config.apiURL + address, data);
		} else {
			const tokens: Tokens = JSON.parse(localStorage.tokens);
			const headers: AuthenticationHeaders = { headers: { "Authorization": `Bearer ${tokens.access}` } };
			res = await axios.put<MessageT>(config.apiURL + address, data, headers);
		}

		return ok(res.data);
	} catch (error) {
		if (recursiveCall) {
			var retErr: RESTError;
			const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;

			retErr = {
				message: hasResponseMessage ? error.response.data.message : error.message,
				type: hasResponseMessage ? "RESTError" : error.name,
				status: hasResponseMessage ? error.response.status : 401,
				statusText: hasResponseMessage ? error.response.statusText : "Unauthorized"
			};

			retErr.message = <ErrorMessageText {...retErr} />;

			return err(retErr);
		}

		const successfullyGotNewAccess: boolean = await getNewAccessToken();

		if (successfullyGotNewAccess) {
			return await resolvePUTCall<MessageT, PayloadT>(address, data, authentication, true);
		}

		const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;
		const unauthenticatedError: RESTError = {
			message: hasResponseMessage ? error.response.data.message : UnauthenticatedErr.message,
			type: hasResponseMessage ? "RESTError" : UnauthenticatedErr.type,
			status: hasResponseMessage ? error.response.status : UnauthenticatedErr.status,
			statusText: hasResponseMessage ? error.response.statusText : UnauthenticatedErr.statusText
		};
		if (hasResponseMessage)
			unauthenticatedError.message = <ErrorMessageText {...unauthenticatedError} />

		return err(unauthenticatedError);
	}
}

/* Helper function for making RESTful DELETE calls */
export async function resolveDELETECall<MessageT>(address: string, authentication: boolean = false, recursiveCall: boolean = false): Promise<Result<MessageT, RESTError>> {
	try {
		var res: AxiosResponse<MessageT>;

		if (!authentication) {
			res = await axios.delete<MessageT>(config.apiURL + address);
		} else {
			const tokens: Tokens = JSON.parse(localStorage.tokens);
			const headers: AuthenticationHeaders = { headers: { "Authorization": `Bearer ${tokens.access}` } };
			res = await axios.delete<MessageT>(config.apiURL + address, headers);
		}

		return ok(res.data);
	} catch (error) {
		if (recursiveCall) {
			var retErr: RESTError;
			const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;

			retErr = {
				message: hasResponseMessage ? error.response.data.message : error.message,
				type: hasResponseMessage ? "RESTError" : error.name,
				status: hasResponseMessage ? error.response.status : 401,
				statusText: hasResponseMessage ? error.response.statusText : "Unauthorized"
			};

			retErr.message = <ErrorMessageText {...retErr} />;

			return err(retErr);
		}

		const successfullyGotNewAccess: boolean = await getNewAccessToken();

		if (successfullyGotNewAccess) {
			return await resolveDELETECall<MessageT>(address, authentication, true);
		}

		const hasResponseMessage: boolean = error.response && error.response.data && error.response.data.message;
		const unauthenticatedError: RESTError = {
			message: hasResponseMessage ? error.response.data.message : UnauthenticatedErr.message,
			type: hasResponseMessage ? "RESTError" : UnauthenticatedErr.type,
			status: hasResponseMessage ? error.response.status : UnauthenticatedErr.status,
			statusText: hasResponseMessage ? error.response.statusText : UnauthenticatedErr.statusText
		};
		if (hasResponseMessage)
			unauthenticatedError.message = <ErrorMessageText {...unauthenticatedError} />

		return err(unauthenticatedError);
	}
}