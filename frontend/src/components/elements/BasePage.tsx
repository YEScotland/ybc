/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Collapse, Button, Offcanvas, Row, Col } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { isBrowser, isMobile } from 'react-device-detect';
import { FaList, FaTimes} from 'react-icons/fa';
import { Result } from 'neverthrow';

import { getNewAccessToken, RESTError, NotificationsIssuesREST, notificationsRESTLink, notificationsIssuesRESTSuffix, PageProps, resolveGETCall } from './utils';

import { AlertBar } from './AlertBar';
import { MilestoneBar } from './MilestoneBar';
import { PlaceholderLoading } from './PlaceholderLoading';

import './BasePage.css';

interface Props extends PageProps, RouteComponentProps {
	loaded: boolean,
	title: string
}

interface State extends NotificationsIssuesREST {
	authenticationChecked: boolean,
	showNavigation: boolean,
}

interface UserTypeLink {
	title: string,
	stub: string
}

interface UserTypeLinks {
	Teacher: UserTypeLink[],
	Admin: UserTypeLink[],
	Judge: UserTypeLink[],
	Team: UserTypeLink[]
}

interface ComponentUserTypeLinks extends React.PureComponent<Props, State> {
	userTypeLinks: UserTypeLinks
}

type UserTypesSideBarElements = "number_of_issues";

export class BasePage extends React.PureComponent<Props, State> implements ComponentUserTypeLinks {
	userTypeLinks: UserTypeLinks;

	constructor(props: Props) {
		super(props);

		this.state = {
			authenticationChecked: false,
			showNavigation: false,
			number_of_issues: 0
		};

		this.userTypeLinks = {
			"Teacher" : [
				{
					"title" : "Introduction",
					"stub": "/"
				},
				{
					"title" : "Teams",
					"stub" : "/teams"
				},
				{
					"title" : "Marketplace",
					"stub" : "/marketplace"
				},
				{
					"title" : "Issues",
					"stub" : "/issues"
				}
			],
			"Admin" : [
				{
					"title" : "Introduction",
					"stub": "/"
				},
				{
					"title" : "Teams",
					"stub" : "/marketplace"
				},
				{
					"title" : "Schools",
					"stub" : "/schools"
				},
				{
					"title" : "Game Settings",
					"stub" : "/settings"
				},
				{
					"title" : "Judges",
					"stub" : "/judges"
				},
				{
					"title" : "Teachers",
					"stub" : "/teachers"
				},
				{
					"title" : "Issues",
					"stub" : "/issues"
				}
			],
			"Judge" : [
				{
					"title" : "Introduction",
					"stub": "/"
				},
				{
					"title" : "Marketplace",
					"stub" : "/marketplace"
				},
				{
					"title" : "Marked Submission",
					"stub" : "/marked"
				}
			],
			"Team" : [
				{
					"title" : "Introduction",
					"stub": "/"
				},
				{
					"title" : "Marketplace",
					"stub" : "/marketplace"
				},
				{
					"title" : "Team Submission",
					"stub" : "/marketplace/" + localStorage['username']
				},
				{
					"title" : "Business Decisions",
					"stub" : "/decisions"
				},
				{
					"title" : "Glossary",
					"stub" : "/glossary"
				}
			]
		};
	}

	redirectToLogin = async () => {
		// this.props.updateAlertBar("Not logged in, please do so", "danger", true);
		await this.props.setDisplayMilestoneBar(false);
		await this.props.updateAlertBar("", "success", false);

		if (this.props.location.pathname !== '/login')
			this.props.history.push('/login');
		else
			this.setState({ authenticationChecked: true });
	}

	componentDidMount = async () => {
		// make sure user is logged in and has valid credentials so we can safely make call for milestones
		try {
			if (!this.props.testRun) {
				if (!localStorage['tokens'] || !JSON.parse(localStorage['tokens'])['access'] || JSON.parse(localStorage['tokens'])['access'] === '' ||
					!localStorage['username'] || localStorage['username'] === '') {
					this.redirectToLogin();
					return;
				}
				// harmless rest call to check that credentials are valid and up to date
				const valid: boolean = await getNewAccessToken();

				if (!valid) {
					this.redirectToLogin();
					return;
				}
			}
		} catch (error) {
			this.redirectToLogin();
			return;
		}

		await this.props.setDisplayMilestoneBar(true);
		this.setState({ authenticationChecked: true });

		if (localStorage['user_type'] === "Admin" || localStorage['user_type'] === "Teacher") {
			const notificationIssueResult: Result<NotificationsIssuesREST, RESTError> = await resolveGETCall(notificationsRESTLink + notificationsIssuesRESTSuffix, true);

			notificationIssueResult
				.map(res => {
					this.setState({ ...res });

					return null; // avoid warning
				})
				.mapErr(err => {
					this.props.updateAlertBar(err.message, "danger", true);
				});
		}
	}

	handleLogOut = async () => {
		localStorage.clear();
		this.props.history.push('/login');
	}

	getPageBodyOffset(): number {
		// returns required y-offset for the page body so that it does not under-lap the mobile view's offcanvas/navigation
		if (isMobile) {
			const borderWidth: number = 3;

			if (this.state.showNavigation) {
				// 2rem padding + 27px/16px body height per navigation entry, large font size divided by 1rem
				// 2 rem for padding on the offcanvas heading
				// .375 * 2 for padding of the navigation button 
				// 1.5 rem for height of button text/icon (NOTE: button padding and text height works out the body height of the offcanvas heading as they are what constrains it)
				// plus 2rem padding + 31/16px body height for the logged in as/logout buttons
				// times rem (relative font height) of the offcanvas, which is annoyingly different to pagebody's font size, otherwise could return a calc()
				// plus 2 px for button border width
				// plus borderWidth / 1.5 px for each entry (not minus one due to logged in as/logout) for the white seperating borders
				// plus borderWidth px for the orange highlight border width
				let entries: number = 0;

				switch (localStorage['user_type']) {
				case "Teacher" :
					entries = this.userTypeLinks.Teacher.length;
					break;
				case "Judge" :
					entries = this.userTypeLinks.Judge.length;
					break;
				case "Admin" :
					entries = this.userTypeLinks.Admin.length;
					break;
				case "Team" :
					entries = this.userTypeLinks.Team.length;
					break;
				default :
					this.props.updateAlertBar("Invalid user type set in localstorage, please correct or contact your YES admin immediately", "danger", true);
					break;
				}

				return (((2 + (27/16)) * entries + 2 + .375 * 2 + 1.5 + (2 + 31/16)) * 16 + 2 + ((borderWidth / 1.5) * entries) + borderWidth);
			}

			// just the height of the header, 2 rem + (.375 * 2)rem + 1.5 rem + 2px + borderWidth px
			return (2 + .375 * 2 + 1.5) * 16 + 2 + borderWidth;
		}

		return 0;
	}

	render() {
		let currentIndex: keyof UserTypeLinks = localStorage['user_type'];
		
		return (
			<React.Fragment>
				<Offcanvas
					backdrop={false}
					show={true}
					scroll={true}
					placement={isMobile ? 'top' : 'start'}
					className={isMobile ? 'mobile' : ''}>
					{isMobile ?
						<React.Fragment>
							<Offcanvas.Header className="mobile">
								<span>
									YE Scotland Challenge
								</span>
								<Button 
									onClick={() => {this.setState(prevState => { return { showNavigation : !prevState.showNavigation }; })}} 
									variant='primary'
									className='navigation-button'
									aria-label='navigation dropdown button'>
									{this.state.showNavigation ?
										<FaTimes style={{color: "var(--secondary)"}} />
										:
										<FaList style={{color: "var(--secondary)"}} />
									}
								</Button>
							</Offcanvas.Header>
						</React.Fragment>
						:
						<React.Fragment>
							<Offcanvas.Header>
								<img
									width="1836"
									height="736"
									alt='YE Scotland logo'
									src={`${process.env.PUBLIC_URL}/assets/white.png`} />
							</Offcanvas.Header>
						</React.Fragment>
					}
					{(this.state.authenticationChecked /* && (isBrowser || this.state.showNavigation)*/) &&
						<Collapse in={isBrowser || this.state.showNavigation}>
							<Offcanvas.Body
								className={isMobile ? ('mobile ' + this.state.showNavigation) : ''}>
									{this.userTypeLinks[currentIndex]
										.map((entry: UserTypeLink) => {
											return (
												<React.Fragment key={entry.title}>
													<div className='basepage-offcanvas-entry' onClick={() => this.props.history.push(entry.stub)} style={{cursor: "pointer"}}>
														<div>
															<a href={entry.stub}>
																{entry.title}
															</a>
														</div>
														{(this.state[("number_of_" + entry.title.toLowerCase()) as UserTypesSideBarElements] !== undefined && this.state[("number_of_" + entry.title.toLowerCase()) as UserTypesSideBarElements] > 0) ?
															<React.Fragment>
																<div className='basepage-offcanvas-entry-notification-bubble'>
																	{this.state[("number_of_" + entry.title.toLowerCase()) as UserTypesSideBarElements]}
																</div>
															</React.Fragment>
															:
															<React.Fragment>
																{/* Explicitly render nothing as sometimes a "0" renders, which messes up sizes */}
															</React.Fragment>
														}
													</div>
												</React.Fragment>
											)})
									}
								{isMobile &&
									<React.Fragment>
										<div className='basepage-offcanvas-entry mobile-login-details'>
											<span>
												{"Hi "}	<strong><i>{localStorage['username']}</i>{" (" + localStorage['user_type'] + ")"}</strong>
											</span>
											<Button
												variant='outline-primary'
												onClick={this.handleLogOut}
												size={'sm'}
												className='logout-button-mobile-navigation'>
												Log out?
											</Button>
										</div>
									</React.Fragment>
								}
							</Offcanvas.Body>
						</Collapse>
					}
				</Offcanvas>

				<div 
					className={"pagebody" + (isMobile ? (' mobile ' + this.state.showNavigation) : '')}
					style={{paddingTop: this.getPageBodyOffset() + 'px' }}>
					<Row className='basepage-header'>
						<Col className='logo-container' md={4} sm={6}>
							{isMobile ?
								<React.Fragment>
									<img
										width="1836"
										height="736"
										alt='YE Scotland logo'
										src={`${process.env.PUBLIC_URL}/assets/YEScotland.png`} />
								</React.Fragment>
								:
								<React.Fragment>
									<div className='logo-title'>
										YE Scotland Business Challenge
									</div>
								</React.Fragment>
							}
						</Col>
						{isBrowser &&
							<React.Fragment>
								<Col md={8} sm={6}>
									<div className={'login-information' + (isMobile ? ' mobile' : '')}>
										{"Logged in as: " + localStorage['username'] + " (" + localStorage['user_type'] + ")"}
										{' '}
										<Button
											variant='outline-secondary'
											onClick={this.handleLogOut}
											size={'sm'}>
											Log out?
										</Button>
									</div>
								</Col>
							</React.Fragment>
						}
					</Row>

					{this.state.authenticationChecked && (this.props.displayMilestoneBar || this.props.testRun) && localStorage['user_type'] === "Team" &&
						<div className='basepage-miletone-bar'>
							Roadmap:
							<MilestoneBar {...this.props} adjusted={true} />
						</div>
					}

					<div className='basepage-title-banner'>
						{this.props.title}
					</div>

					<div className='mainbody'>
						<AlertBar {...this.props} />

						{this.props.loaded ?
							<React.Fragment>
								{this.props.children}
							</React.Fragment>
							:
							<React.Fragment>
								<PlaceholderLoading {...this.props} />
							</React.Fragment>
						}
					</div>
				</div>
			</React.Fragment >
		)
	}
}
