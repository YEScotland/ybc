/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React, { ReactElement } from 'react';

import { Col, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
// import { Link } from 'react-router-dom';
import { Milestone, ProgressBar } from 'react-milestone'; // using react-milestone as react-bootstraps versions dont have customizable milestones
import { Result } from 'neverthrow';

import { ChecklistMilestone, ChecklistMilestoneList, PageProps, resolveGETCall, RESTError } from './utils';
import { MilestoneNode, PROGRESS_BAR_COLOUR_COMPLETE, PROGRESS_BAR_COLOUR_INCOMPLETE } from './MilestoneNode';

import './MilestoneBar.css';

interface Props extends PageProps, RouteComponentProps {
	list?: ChecklistMilestoneList,
	adjusted?: boolean
}

interface State extends ChecklistMilestoneList {}

export class MilestoneBar extends React.PureComponent<Props, State> {
	constructor(props: Props) {
		super(props);

		this.state = {
			milestones: [],
			percentage: 0,
		};
	}

	componentDidMount = async () => {
		if (!this.props.list) {
			const path: string = '/user/milestones';
			const result: Result<ChecklistMilestoneList, RESTError> = await resolveGETCall<ChecklistMilestoneList>(path, true);

			result
				.map((res: ChecklistMilestoneList) => {
					this.setState({ ...res });

					return null; // to silence error
				})
				.mapErr((err: RESTError) => {
					var message: string | ReactElement;

					if (err.type === "Error")
						message = "Internal server error, please retry in a couple minutes";
					else
						message = err.message;

					this.props.updateAlertBar(message, "danger", true);
				});
		} else {
			this.setState({...this.props.list});
		}
	}

	getElementForMilestone = (milestone: Milestone) => {
		const currentMilestone: ChecklistMilestone = this.state.milestones[milestone.index];
		return (
			<MilestoneNode {...this.props} {...currentMilestone} />
		);
	}

	render() {
		return (
			<Row>
				<Col>
					<div className='progress-bar-wrapper'>
						<ProgressBar
							percentage={this.state.percentage}
							milestoneCount={this.state.milestones.length}
							color={PROGRESS_BAR_COLOUR_COMPLETE}
							transitionSpeed={1000}
							Milestone={(milestone) => this.getElementForMilestone(milestone)}
							style={{backgroundColor: PROGRESS_BAR_COLOUR_INCOMPLETE}} />
					</div>
				</Col>
			</Row>
		);
	}
}