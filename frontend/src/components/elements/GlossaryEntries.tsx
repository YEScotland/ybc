/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

interface GlossaryEntry {
	phrase: string,
	aka?: string,
	definition: string
}

export const GLOSSARY_ENTRIES: GlossaryEntry[] = [
	{ phrase: "Automation", definition: "Using machinery to do work that was previously done by people." },
	{ phrase: "Brexit", definition: "The name given to the United Kingdom's departure from the European Union. It is a combination of 'Britain' and 'exit'."},
	{ phrase: "Business Rates", definition: "A tax on the properties from which businesses operate." },
	{ phrase: "Carbon Footprint", definition: "The total amount of carbon dioxide and other greenhouse gases produced by a single person, organization, product, or process, which contributes to global warming." },
	{ phrase: "Corporate Social Responsibility", aka: "CSR", definition: "A self-regulating business model that helps a company be socially accountable—to itself, its stakeholders, and the public. By practicing corporate social responsibility, companies can be conscious of the kind of impact they are having on all aspects of society, including economic, social, and environmental." },
	{ phrase: "Corporation Tax", definition: "Tax paid to the government on the profit made by a company." },
	{ phrase: "Demographics", definition: "Statistical characteristics of human populations (such as income or age), used especially in business to identify markets." },
	{ phrase: "E-Commerce", definition: "Short for 'electronic commerce', which is all about buying and selling online."},
	{ phrase: "Economy", definition: "The system of how money is made and used within a particular country or region. a region's economy is connected with things like how many goods and services are produced and how much money people can spend on these things."},
	{ phrase: "Endorse", definition: "Publicly give support to or approve of." },
	{ phrase: "European Union", aka: "EU", definition: "A political and economic union of 27 member states that are located primarily in Europe with an open border which allows the free flow of goods and people. Any product manufactured in one EU country can be sold to any other member without additional taxes. Practitioners of most services, such as law, medicine, tourism, banking, and insurance, can operate a business in all member countries." },
	{ phrase: "Exchange Rate", definition: "The price of one country's currency compared to another country's currency."},
	{ phrase: "Grey Pound", definition: "The purchasing power of elderly people (typically 65+) as customers." },
	{ phrase: "ICT", definition: "Information communication technology." },
	{ phrase: "Inflation", definition: "The rise in prices over a year." },
	{ phrase: "Interest Rates", definition: "What you pay for borrowing money, and what banks pay you for saving money with them. Interest rates are shown as a percentage of the amount you borrow or save over a year. So if you put £100 into a savings account with a 1% interest rate, you'd have £101 a year later. If you borrowed £100 with an APR (annual percentage rate) of 20% and didn't pay any of it back within the year, you'd owe £120 in a year's time."},
	{ phrase: "Invest", definition: "To put into use for the purpose of making money, e.g. They invested their money in a new company and hoped to get back twice as much." },
	{ phrase: "Lay-Off", definition: "To end a worker's job for reasons other than performance. It is not the same as sacking/firing which results from an inefficient on-the-job performance or unacceptable workplace behaviour. Lay-offs can occur for a range of reasons such as automation, outsourcing or because revenue is in decline."},
	{ phrase: "Legislation", definition: "Laws made by the government." },
	{ phrase: "Loss Leader", definition: "A product sold at a loss, in order to attract customers." },
	{ phrase: "National Minimum Wage", definition: "The lowest wage permitted by law. This is paid to under 23s and apprentices." },
	{ phrase: "Outsourcing", definition: "To purchase (goods or services) from an outside supplier instead of producing (the same goods or services) internally." },
	{ phrase: "Recession", definition: "A period of reduced or declining economic activity, generally occurring due to a widespread drop in spending. Can lead to unemployment, fall in income, rise in poverty, companies going out of business and more." },
	{ phrase: "Redundant", definition: "Where an employee has lost their job due to lay-off." },
	{ phrase: "Revenue", definition: "Money gained from selling or investing." },
	{ phrase: "Sack", definition: "Dismissing someone from employment, sometimes called 'fire', which happens due to inefficient on-the-job performance or unacceptable workplace behaviour."},
	{ phrase: "Stakeholders", definition: "A person or group that has a direct interest in a decision-making process." },
	{ phrase: "Trademark", definition: "A name, symbol, or other mark used to show who made a product. y law, only the company that makes or sells the product may use its trademark." },
	{ phrase: "Unemployment", definition: "Being without paid work." },
	{ phrase: "Value-Added Tax", aka: "VAT", definition: "A sales tax paid on the value added to products and services at each stage of manufacturing and distribution." }
];