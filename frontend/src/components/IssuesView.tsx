/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, ButtonGroup } from 'react-bootstrap';
import { FaCheck, FaSpinner, FaTrash } from 'react-icons/fa';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';

import { BasePage, PageProps, resolveGETCall, RESTError, issuesRESTLink, IssuesREST, getActualURL, IssueSpecificREST, issueSpecificRESTLink, resolveDELETECall, marketplaceStallsRESTLink } from './elements';

import './IssuesView.css';

export interface IssuesViewProps extends RouteComponentProps, PageProps { }

interface State extends IssuesREST {
	loaded_issues: boolean,
	logo_hash: string,
	delete_loading: boolean
}

export class IssuesView extends React.Component<IssuesViewProps, State> {
	constructor(props: IssuesViewProps) {
		super(props);

		this.state = {
			team_names: [],
			team_logo_stubs: [],
			logo_hash: "" + Date.now(),
			issue_descriptions: [],
			loaded_issues: false,
			delete_loading: false
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Teacher" || localStorage['user_type'] === "Admin") {
			const result: Result<IssuesREST, RESTError> = await resolveGETCall<IssuesREST>(issuesRESTLink, true);

			result
				.map(res => {
					this.setState({ ...res, loaded_issues: true });

					return null; // to prevent warnings
				})
				.mapErr(err => {
					console.error(err);

					this.props.updateAlertBar(err.message, "warn", true);
				})
		} else
			this.props.history.push("/");
	}
	
	handleCheckPress = async (event: React.MouseEvent<HTMLButtonElement>) => {
		const index: number = Number(event.currentTarget.id);
		const path: string = issueSpecificRESTLink + this.state.team_names[index];
		const result: Result<IssueSpecificREST, RESTError> = await resolveDELETECall(path, true);

		result
			.map(res => {
				this.props.updateAlertBar(res.message, "success", true);

				this.state.team_names.splice(index, 1);
				this.state.issue_descriptions.splice(index, 1);
				this.state.team_logo_stubs.splice(index, 1);

				this.setState({ logo_hash: "" + Date.now() });

				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	handleDeletePress = async (event: React.MouseEvent<HTMLButtonElement>) => {
		this.setState({delete_loading: true});
		const index: number = Number(event.currentTarget.id);
		const path: string = marketplaceStallsRESTLink + "/" + this.state.team_names[index];
		const result: Result<IssueSpecificREST, RESTError> = await resolveDELETECall(path, true);

		result
			.map(res => {
				this.props.updateAlertBar(res.message, "success", true);
				
				this.state.team_names.splice(index, 1);
				this.state.issue_descriptions.splice(index, 1);
				this.state.team_logo_stubs.splice(index, 1);

				this.setState({ logo_hash: "" + Date.now() });

				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
		this.setState({delete_loading: false});
	}


	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded_issues} title={'Issues'}>

					<table className='issues-list'>
						<thead>
							<tr className='issues-list-header'>
								<th>
									Logo
								</th>
								<th>
									Team name
								</th>
								<th>
									Descriptions
								</th>
								<th>
									Operations
								</th>
							</tr>
						</thead>

						<tbody>
							{this.state.team_names.map(
								(name, index) => {
									return (
										<tr 
											className='issues-list-elem'
											key={this.state.team_names[index]}>
											<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])}>
												{this.state.team_logo_stubs[index] !== "" ?
													<React.Fragment>
															<img
																alt={this.state.team_names[index] + "'s logo"}
																src={getActualURL(this.state.team_logo_stubs[index], this.state.logo_hash)} 
																className='issues-table-logo' />
													</React.Fragment>
													:
													<React.Fragment>
														<div>
															<strong>{this.state.team_names[index] + ' '}</strong> have not yet submitted a logo
														</div>
													</React.Fragment>
												}
											</td>
											<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])}>
												{this.state.team_names[index]}
											</td>
											<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])}>
												<div className='issues-descriptions'>
													<span>
														{this.state.issue_descriptions[index]}
													</span>
												</div>
											</td>
											<td>
												<ButtonGroup>
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleCheckPress}
														variant='success'
														disabled={this.state.delete_loading}>
														<FaCheck />
													</Button>
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleDeletePress}
														variant='danger'
														disabled={this.state.delete_loading}>
														{this.state.delete_loading ?
															<React.Fragment>
																<FaSpinner className='spinner' />
															</React.Fragment>
														:
															<React.Fragment>
																<FaTrash />
															</React.Fragment>
														}
													</Button>
												</ButtonGroup>
											</td>
										</tr>
									)
								}
							)

							}
						</tbody>
					</table>

				</BasePage>
			</React.Fragment>
		);
	}
}
