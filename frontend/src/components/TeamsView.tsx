/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaTrash } from 'react-icons/fa';

import { BasePage, PageProps, resolveGETCall, teamsMilestonesRESTLink, TeamsMilestonesREST, userSpecificOperationsRESTLink, RESTError, MilestoneBar, resolveDELETECall, getActualURL } from './elements';

import './TeamsView.css';

export interface TeamsViewProps extends RouteComponentProps, PageProps { }

interface State extends TeamsMilestonesREST {
	loaded: boolean,
	logo_hashes: string
}

export class TeamsView extends React.Component<TeamsViewProps, State> {
	constructor(props: TeamsViewProps) {
		super(props);

		this.state = {
			team_milestones: [],
			team_names: [],
			team_logo_stubs: [],
			loaded: false,
			logo_hashes: "" + Date.now()
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Teacher") {
			// load all teams who signed up through this teacher
			const result: Result<TeamsMilestonesREST, RESTError> = await resolveGETCall<TeamsMilestonesREST>(teamsMilestonesRESTLink, true);

			result
				.map(res => {
					this.setState({ ...res, loaded: true });
					return null; // to prevent warnings
				})
				.mapErr(err => {
					console.error(err);
					this.props.updateAlertBar(err.message, "warning", true);
				});
		} else
			this.props.history.push('/');
	}

	handleDeletePress = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		const index: number = Number(e.currentTarget.id);
		const selectedTeamName: string = this.state.team_names[index];

		const path: string = userSpecificOperationsRESTLink + selectedTeamName;
		const result: Result<{}, RESTError> = await resolveDELETECall<{}>(path, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully deleted the key", "success", true);

				this.state.team_milestones.splice(index, 1);
				this.state.team_names.splice(index, 1);
				this.state.team_logo_stubs.splice(index, 1);

				this.setState({ logo_hashes: "" + Date.now() });

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded} title={'Teams'}>
					<table className='teams-list' style={{width: "100%"}}>
						<thead>
							<tr className='teams-list-header'>
								<th>
									Logo
								</th>
								<th>
									Team name
								</th>
								<th>
									Current Progress
								</th>
								<th>
									Operations
								</th>
							</tr>
						</thead>

						<tbody>
							{this.state.team_milestones.map(
								(milestones, index) => {
									return (
										<tr className='teams-list-elem' key={this.state.team_names[index]}>
											<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])} style={{cursor: "pointer"}}>
												{this.state.team_logo_stubs[index] !== "" ?
													<React.Fragment>
														<div className='teams-table-logo'>
															<img
																alt={this.state.team_names[index] + "'s logo"}
																src={getActualURL(this.state.team_logo_stubs[index], this.state.logo_hashes)} />
														</div>
													</React.Fragment>
													:
													<React.Fragment>
														<div>
															<strong>{this.state.team_names[index] + ' '}</strong> have not yet submitted a logo
														</div>
													</React.Fragment>
												}
											</td>
											<td onClick={() => this.props.history.push('/marketplace/' + this.state.team_names[index])} style={{cursor: "pointer"}}>
												{this.state.team_names[index]}
											</td>
											<td>
												<MilestoneBar {...this.props} list={milestones} />
											</td>
											<td>
												<Button
													size={'sm'}
													id={String(index)}
													onClick={this.handleDeletePress}
													variant='danger'>
													<FaTrash />
												</Button>
											</td>
										</tr>
									)
								}
							)

							}
						</tbody>
					</table>

				</BasePage>
			</React.Fragment >
		);
	}
}

