/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Col, Button, Row, Form, Modal, InputGroup, FormControl } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { FaUpload, FaSpinner, FaSave, FaFlag, FaTrash, FaThumbsUp, FaThumbsDown } from 'react-icons/fa';
import { Result } from 'neverthrow';

import { BasePage, ACCEPTABLE_DOCUMENT_EXTENSIONS, ACCEPTABLE_PICTURE_FILE_EXTENSIONS, MarketplaceStall, MarketplaceSubmission, MAX_FILE_SUBMISSION_SIZE, MAX_FILE_SUBMISSION_SIZE_Bytes, PageProps, resolveGETCall, resolvePUTCall, RESTError, marketplaceStallRESTLinkPrefix, ImageOrPDFViewer, getActualURL, JudgeSubmission, judgeSubmissionRESTLink, usersAdministratorCheckRESTLink, UsersAdministratorCheckREST, issueSpecificRESTLink, IssueSpecificRESTSubmit, IssueSpecificREST, resolvePOSTCall, marketplaceStallsRESTLink, resolveDELETECall, likeMarketplaceStallRESTLink, LikeMarketplaceStallREST, CanLikeMarketplaceStallREST, canLikeMarketplaceStallRESTLink } from './elements';

import './MarketplaceSpecificView.css';

type FILES_TO_UPLOAD = "logo" | "poster" | "target_customer";
const FILES_TO_UPLOAD_ACCEPTABLE = { "logo": ACCEPTABLE_PICTURE_FILE_EXTENSIONS, "poster": ACCEPTABLE_DOCUMENT_EXTENSIONS, "target_customer": ACCEPTABLE_DOCUMENT_EXTENSIONS };
type TEXT_TO_UPLOAD = 'company_name' | 'tagline';

const JUDGE_SCORE_INPUT_WIDTH_STYLE: React.CSSProperties = {maxWidth: "3.5rem"};

interface MatchParams {
	username: string
}

export interface MarketplaceSpecificViewProps extends RouteComponentProps<MatchParams>, PageProps { }

interface InputBooleans {
	company_name: boolean,
	logo: boolean,
	tagline: boolean,
	poster: boolean,
	target_customer: boolean
}

interface State extends MarketplaceStall, JudgeSubmission, LikeMarketplaceStallREST, CanLikeMarketplaceStallREST {
	submitting: InputBooleans,
	loaded: boolean,
	editted_company_name: string,
	editted_tagline: string,
	logo_hash: string,
	poster_hash: string,
	target_customer_hash: string,

	submitting_score: InputBooleans,
	
	report_modal_show: boolean,
	report_modal_description: string,
	user_is_administrator: boolean,
	delete_loading: boolean,

	like_loading: boolean
}

export class MarketplaceSpecificView extends React.Component<MarketplaceSpecificViewProps, State> {
	constructor(props: MarketplaceSpecificViewProps) {
		super(props);

		this.state = {
			submitting: {
				company_name: false,
				logo: false,
				tagline: false,
				poster: false,
				target_customer: false
			},

			loaded: false,

			company_name: "",
			tagline: "",
			team_username: "",

			editted_company_name: "",
			editted_tagline: "",

			logo_stub: "",
			poster_stub: "",
			target_customer_stub: "",

			logo_hash: "" + Date.now(),
			poster_hash: "" + Date.now(),
			target_customer_hash: "" + Date.now(),

			submitting_score: {
				company_name: false,
				logo: false,
				tagline: false,
				poster: false,
				target_customer: false,
			},
			target_customer_score: 0,
			poster_score: 0,
			tagline_score: 0,
			logo_score: 0,
			manifest: [],
			
			report_modal_show: false,
			report_modal_description: "",

			user_is_administrator: false,
			
			delete_loading: false,

			liked: false,

			can_like: true,

			like_loading: false
		};
	}

	componentDidMount = async () => {
		const path: string = marketplaceStallRESTLinkPrefix + this.props.match.params.username;

		const result: Result<MarketplaceStall, RESTError> = await resolveGETCall<MarketplaceStall>(path, true);

		result
			.map(res => {
				this.setState({ ...res, editted_company_name: res.company_name, editted_tagline: res.tagline });

				return null; // return to stop warning
			})
			.mapErr(err => {
				// do nothing as no marketplace submission will return error but not really a major error to notify the team of, however, other users should be notified
				if (localStorage['username'] !== this.props.match.params.username)
					this.props.updateAlertBar(err.message, "danger", true);
			});


		if (localStorage['user_type'] === "Judge" && this.props.match.params.username !== localStorage['username']) {
			const pathJudge: string = judgeSubmissionRESTLink + this.props.match.params.username;

			const resultJudge: Result<JudgeSubmission, RESTError> = await resolveGETCall<JudgeSubmission>(pathJudge, true);
			
			resultJudge
				.map(res => {
					this.setState({ ...res });

					return null; // return to stop warning
				})
				.mapErr(err => {
					// this.props.updateAlertBar(err.message, "warning", true);
					// do nothing as no judge submission will return error but not really a major error to notify the user of
				});
		}
		
		const isAdministratorPath: string = usersAdministratorCheckRESTLink + this.props.match.params.username;
		const isAdministratorCheckResult: Result<UsersAdministratorCheckREST, RESTError> = await resolveGETCall<UsersAdministratorCheckREST>(isAdministratorPath, true);

		isAdministratorCheckResult
			.map(res => {
				this.setState({ user_is_administrator : res.is_users_admin });

				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			})

		const isMarketplaceLikedPath: string = likeMarketplaceStallRESTLink + '/' + this.props.match.params.username;
		const isMarketplaceLikedResult: Result<LikeMarketplaceStallREST, RESTError> = await resolveGETCall<LikeMarketplaceStallREST>(isMarketplaceLikedPath, true);

		isMarketplaceLikedResult
			.map(res => {
				this.setState({ ...res });

				return null; // prevent warnings
			})
			.mapErr(err => {
				// do nothing as we dont want the user to see if retrieving data about a marketplace errors
			});

		const canLikeMarketplacePath: string = canLikeMarketplaceStallRESTLink + "/" + this.props.match.params.username;
		const canLikeMarketplaceResult: Result<CanLikeMarketplaceStallREST, RESTError> = await resolveGETCall<CanLikeMarketplaceStallREST>(canLikeMarketplacePath, true);
		canLikeMarketplaceResult
			.map(res => {
				this.setState({ ...res });

				return null;
			})
			.mapErr(err => {
				// do nothing
			});
		
		this.setState({ loaded: true });
	}

	handleUploadClick = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		var fileTypeHolding: FILES_TO_UPLOAD | null;

		switch (event.currentTarget.id) {
			case 'poster':
				fileTypeHolding = event.currentTarget.id;
				break;
			case 'target_customer':
				fileTypeHolding = event.currentTarget.id;
				break;
			case 'logo':
				fileTypeHolding = event.currentTarget.id;
				break;
			default:
				fileTypeHolding = null;
				break;
		}

		const fileType: FILES_TO_UPLOAD | null = fileTypeHolding;

		if (fileType != null) {
			const uploadElement = document.createElement('input');
			uploadElement.type = 'file';
			uploadElement.accept = "." + FILES_TO_UPLOAD_ACCEPTABLE[fileType].join(",.");
			uploadElement.id = event.currentTarget.id;
			uploadElement.onchange = () => {
				this.handleFileUpload(uploadElement, fileType);
			};
			uploadElement.click();
		}
	}

	handleFileUpload = async (currentTarget: HTMLInputElement, fileType: FILES_TO_UPLOAD) => {
		const fileList: FileList | null = currentTarget.files;

		if (fileType != null && fileList) {
			this.setState(prevState => {
				var submitting: any = prevState.submitting;
				submitting[fileType] = true;
				return { submitting: submitting };
			});

			const reader = new FileReader();
			var extension: string;
			reader.onload = async () => {
				var settingString: string;
				if (reader.result === null || reader.result instanceof ArrayBuffer) {
					settingString = "";
				} else {
					settingString = reader.result;
				}

				var data: MarketplaceSubmission = {
					encoded_logo: "",
					encoded_logo_extension: "",
					encoded_logo_mime_type: "",
					encoded_poster: "",
					encoded_poster_extension: "",
					encoded_poster_mime_type: "",
					encoded_target_customer: "",
					encoded_target_customer_extension: "",
					encoded_target_customer_mime_type: "",
					company_name: "",
					tagline: "",
					manifest: []
				};

				var acceptableExtension: boolean = false;
				for (var i = 0; i < FILES_TO_UPLOAD_ACCEPTABLE[fileType].length; ++i) {
					const ext: string = FILES_TO_UPLOAD_ACCEPTABLE[fileType][i];
					if (extension === ext) {
						acceptableExtension = true;
						break;
					}
				}

				if (acceptableExtension) {
					if (fileType === "logo") {
						data.encoded_logo = settingString;
						data.encoded_logo_extension = extension;
						data.encoded_logo_mime_type = settingString.split(';')[0].split(':')[1];
					} else if (fileType === "poster") {
						data.encoded_poster = settingString;
						data.encoded_poster_extension = extension;
						data.encoded_poster_mime_type = settingString.split(';')[0].split(':')[1];
					} else {
						data.encoded_target_customer = settingString;
						data.encoded_target_customer_extension = extension;
						data.encoded_target_customer_mime_type = settingString.split(';')[0].split(':')[1];
					}

					data.manifest.push(fileType);

					const path: string = marketplaceStallRESTLinkPrefix + this.props.match.params.username;
					const result: Result<MarketplaceStall, RESTError> = await resolvePUTCall<MarketplaceStall, MarketplaceSubmission>(path, data, true);

					result
						.map(res => {
							this.setState({ ...res });

							// hash elements refresh the associated embeded elements
							this.setState(prevState => {
								var data: any = {};
								data[fileType + "_hash"] = Date.now();
								return data;
							});

							return null; // prevent warning
						})
						.mapErr(err => {
							this.props.updateAlertBar(err.message, "danger", true);
						});
				} else {
					this.props.updateAlertBar(
						<React.Fragment>
							<strong>Not Acceptable</strong>: The chosen <i>{fileList[0].name}</i> {fileType} file is not an acceptable file type
						</React.Fragment>, "warning", true);
				}

				this.setState(prevState => {
					var submitting: any = prevState.submitting;
					submitting[currentTarget.id] = false;
					return { submitting: submitting };
				});
			};

			if (fileList[0].size < MAX_FILE_SUBMISSION_SIZE_Bytes) {
				// extension checks are in reader as it bases file type on headers, so more accurate than extensions
				extension = fileList[0].name.split('.')[fileList[0].name.split('.').length - 1];
				reader.readAsDataURL(fileList[0]);
			} else {
				this.props.updateAlertBar(
					<React.Fragment>
						<strong>Payload Too Large</strong>: The chosen <i>{fileList[0].name}</i> {fileType} file is too large, please select a smaller image
					</React.Fragment>, "warning", true);

				this.setState(prevState => {
					var submitting: any = prevState.submitting;
					submitting[currentTarget.id] = false;
					return { submitting: submitting };
				});
			}
		}
	}

	handleTextFieldSave = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		var textTypeHolding: TEXT_TO_UPLOAD | null;

		switch (event.currentTarget.id) {
			case "tagline":
				textTypeHolding = "tagline";
				break;
			case "company_name":
				textTypeHolding = "company_name";
				break;
			default:
				textTypeHolding = null;
				break;
		}

		const textType: TEXT_TO_UPLOAD | null = textTypeHolding;

		if (textType != null) {
			this.setState(prevState => {
				var submitting = prevState.submitting;
				submitting[textType] = true;
				return { submitting: submitting };
			});

			const data: MarketplaceSubmission = {
				encoded_logo: "",
				encoded_logo_extension: "",
				encoded_logo_mime_type: "",
				encoded_poster: "",
				encoded_poster_extension: "",
				encoded_poster_mime_type: "",
				encoded_target_customer: "",
				encoded_target_customer_extension: "",
				encoded_target_customer_mime_type: "",
				company_name: "",
				tagline: "",
				manifest: []
			};

			if (textType === "company_name")
				data.company_name = this.state.editted_company_name;
			else
				data.tagline = this.state.editted_tagline;

			data.manifest.push(textType);

			const path: string = marketplaceStallRESTLinkPrefix + this.props.match.params.username;
			const result: Result<MarketplaceStall, RESTError> = await resolvePUTCall<MarketplaceStall, MarketplaceSubmission>(path, data, true);

			result
				.map(res => {
					this.setState({ ...res });
					this.setState(() => {
						var data: any = {};
						data["editted_" + textType] = res[textType];
						return data;
					})

					return null; // for warnings
				})
				.mapErr(err => {
					this.props.updateAlertBar(err.message, "danger", true);
				});

			this.setState(prevState => {
				var submitting = prevState.submitting;
				submitting[textType] = false;
				return { submitting: submitting };
			});
		}
	}

	handleUploadScoreClick = async (event: React.MouseEvent<HTMLButtonElement>) => {
		var scoreTypeHolding: TEXT_TO_UPLOAD | FILES_TO_UPLOAD | null;

		switch (event.currentTarget.id) {
			case "tagline":
				scoreTypeHolding = "tagline";
				break;
			case "poster":
				scoreTypeHolding = "poster";
				break;
			case "target_customer":
				scoreTypeHolding = "target_customer";
				break;
			case "logo":
				scoreTypeHolding = "logo";
				break;
			default: // company_name
				scoreTypeHolding = null;
				break;
		}

		const scoreType: TEXT_TO_UPLOAD | FILES_TO_UPLOAD | null = scoreTypeHolding;

		if (scoreType !== null) {
			this.setState(prevState => {
				var submitting_score = prevState.submitting_score;
				submitting_score[scoreType] = true;
				return { submitting_score: submitting_score };
			});

			const data: JudgeSubmission = {
				target_customer_score: 0,
				poster_score: 0,
				tagline_score: 0,
				logo_score: 0,
				manifest: []
			};

			switch (event.currentTarget.id) {
				case "tagline":
					data.tagline_score = this.state.tagline_score;
					break;
				case "poster":
					data.poster_score = this.state.poster_score;
					break;
				case "target_customer":
					data.target_customer_score = this.state.target_customer_score;
					break;
				case "logo":
					data.logo_score = this.state.logo_score;
					break;
			}

			data.manifest.push(scoreType);

			const path: string = judgeSubmissionRESTLink + this.props.match.params.username;
			const result: Result<JudgeSubmission, RESTError> = await resolvePUTCall<JudgeSubmission, JudgeSubmission>(path, data, true);

			result
				.map(res => {
					this.setState({ ...res });

					return null; // for warnings
				})
				.mapErr(err => {
					this.props.updateAlertBar(err.message, "danger", true);
				});

			this.setState(prevState => {
				var submitting_score = prevState.submitting_score;
				submitting_score[scoreType] = false;
				return { submitting_score: submitting_score };
			});
		}
	}
	
	handleReportSubmit = async (event: React.FormEvent) => {
		event.preventDefault();
		
		const path: string = issueSpecificRESTLink + this.props.match.params.username;
		const data: IssueSpecificRESTSubmit = {
			description: this.state.report_modal_description
		};
		const result: Result<IssueSpecificREST, RESTError> = await resolvePOSTCall<IssueSpecificREST, IssueSpecificRESTSubmit>(path, data, true);

		result
			.map(res => {
				this.props.updateAlertBar(res.message, "success", true);
				
				this.props.history.push("/marketplace");

				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);

				this.setState({report_modal_show : false});
			});
	}

	handleDelete = async (event: React.MouseEvent<HTMLButtonElement>) => {
		this.setState({delete_loading: true});
		const path: string = marketplaceStallsRESTLink + "/" + this.props.match.params.username;
		const result: Result<IssueSpecificREST, RESTError> = await resolveDELETECall(path, true);

		result
			.map(res => {
				this.props.updateAlertBar(res.message, "success", true);

				this.props.history.push("/marketplace");
				
				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});

		this.setState({delete_loading: false});
	}

	handleLike = async () => {
		this.setState({like_loading : true});

		const path: string = likeMarketplaceStallRESTLink + "/" + this.props.match.params.username;
		const result: Result<LikeMarketplaceStallREST, RESTError> = await resolvePOSTCall(path, {}, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully liked the marketplace stall", "success", true);
				
				this.setState({...res});

				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	
		this.setState({like_loading : false});
	}

	handleUnlike = async () => {
		this.setState({like_loading : true});

		const path: string = likeMarketplaceStallRESTLink + "/" + this.props.match.params.username;
		const result: Result<LikeMarketplaceStallREST, RESTError> = await resolveDELETECall(path, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully unliked the marketplace stall", "success", true);
				
				this.setState({...res});

				return null;
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	
		this.setState({like_loading : false});
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded} title={'Team Profile'}>
					<Modal show={this.state.report_modal_show} onHide={() => { this.setState({report_modal_show : false}); }}>
						<Form onSubmit={this.handleReportSubmit}>
							<Modal.Header closeButton>
								<Modal.Title>Create a Report</Modal.Title>
							</Modal.Header>

							<Modal.Body>
								<Form.Group>
									<Form.Control
										placeholder='What is the issue with this submission?'
										as='textarea'
										rows={8}
										required
										onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({report_modal_description : event.currentTarget.value}); }} >
									</Form.Control>
								</Form.Group>
							</Modal.Body>

							<Modal.Footer>
								<Button type='submit' variant='primary'>
									<FaUpload /> Submit Report
								</Button>
							</Modal.Footer>
						</Form>
					</Modal>

					<Row>
						<Col>
							<Row className='marketplace company-name'>
								<Form.Group className="mb-3" controlId="company_name">
									<div className='marketplace-field-header'>
										<Form.Label>
											Product Name
										</Form.Label>

										{(localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username']) &&
											<React.Fragment>
												<Button
													size={'sm'}
													className='label-addition'
													id='company_name'
													disabled={this.state.editted_company_name === this.state.company_name || this.state.submitting.company_name}
													variant={(this.state.editted_company_name === this.state.company_name) ? 'outline-primary' : 'primary'}
													onClick={this.handleTextFieldSave}>
													{this.state.submitting.company_name ?
														<React.Fragment>
															<FaSpinner className='spinner' /> Save
														</React.Fragment>
														:
														<React.Fragment>
															<FaSave /> Save
														</React.Fragment>
													}
												</Button>
											</React.Fragment>
										}
									</div>
									<Form.Control
										required
										onChange={(e: React.ChangeEvent<HTMLInputElement>) => { this.setState({ editted_company_name: e.target.value }) }}
										value={this.state.editted_company_name}
										disabled={localStorage['username'] !== this.props.match.params.username} />
								</Form.Group>
							</Row>

							<Row className='marketplace logo'>
								<Form.Group className="mb-3" controlId="logo">
									<div className='marketplace-field-header'>
										<Row>
											<Form.Label column>
												Product Logo
												{localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username'] &&
													<React.Fragment>
														{' '} ({MAX_FILE_SUBMISSION_SIZE} max)
													</React.Fragment>
												}

												{(localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username']) &&
													<React.Fragment>
														<Button
															size={'sm'}
															className='label-addition'
															id='logo'
															onClick={this.handleUploadClick}
															disabled={this.state.submitting.logo}
															variant={this.state.submitting.logo ? 'outline-primary' : 'primary'}
														>
															{this.state.submitting.logo ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
														</Button>
													</React.Fragment>
												}
												
												{(localStorage['user_type'] === "Judge" && this.props.match.params.username !== localStorage['username']) &&
													<React.Fragment>
														<InputGroup className='marketplace-specific-judge-score-group'>
															<FormControl
																value={this.state.logo_score}
																type="number"
																onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.setState({logo_score: Number.parseInt(event.currentTarget.value)}) }
																size={'sm'}
																style={JUDGE_SCORE_INPUT_WIDTH_STYLE}
																disabled={this.state.submitting_score.logo}
																min={0}
																max={25} />
															<InputGroup.Text>/25</InputGroup.Text>
															<Button
																size={'sm'}
																className='label-addition'
																id='logo'
																onClick={this.handleUploadScoreClick}
																disabled={this.state.submitting_score.logo}
																variant={this.state.submitting_score.logo ? 'outline-primary' : 'primary'}
															>
															{this.state.submitting_score.logo ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
															</Button>
														</InputGroup>
													</React.Fragment>
												}

											</Form.Label>
										</Row>

									</div>
									<div className='marketplace-stall-specific-logo'>
										{(this.state.logo_stub !== "") ?
											<img src={getActualURL(this.state.logo_stub, this.state.logo_hash)} className='embeded logo' alt='Product Logo' />
											:
											<span style={{ color: 'red' }}>
												No logo has been uploaded
											</span>
										}
									</div>
								</Form.Group>
							</Row>

							<Row className='marketplace tagline'>
								<Form.Group className="mb-3" controlId="tagline">
									<div className='marketplace-field-header'>
										<Row>
											<Form.Label>
												Tagline

												{(localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username']) &&
													<React.Fragment>
														<Button
															size={'sm'}
															className='label-addition'
															id='tagline'
															disabled={this.state.editted_tagline === this.state.tagline || this.state.submitting.tagline}
															variant={(this.state.editted_tagline === this.state.tagline) ? 'outline-primary' : 'primary'}
															onClick={this.handleTextFieldSave}>
															{this.state.submitting.tagline ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Save
																</React.Fragment>
																:
																<React.Fragment>
																	<FaSave /> Save
																</React.Fragment>
															}
														</Button>
													</React.Fragment>
												}

												{(localStorage['user_type'] === "Judge" && this.props.match.params.username !== localStorage['username']) &&
													<React.Fragment>
														<InputGroup className='marketplace-specific-judge-score-group'>
															<FormControl
																value={this.state.tagline_score}
																type="number"
																onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.setState({tagline_score: Number.parseInt(event.currentTarget.value)}) }
																size={'sm'}
																style={JUDGE_SCORE_INPUT_WIDTH_STYLE}
																disabled={this.state.submitting_score.tagline}
																min={0}
																max={25} />
															<InputGroup.Text>/25</InputGroup.Text>
															<Button
																size={'sm'}
																className='label-addition'
																id='tagline'
																onClick={this.handleUploadScoreClick}
																disabled={this.state.submitting_score.tagline}
																variant={this.state.submitting_score.tagline ? 'outline-primary' : 'primary'}
															>
															{this.state.submitting_score.tagline ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
															</Button>
														</InputGroup>
													</React.Fragment>
												}

											</Form.Label>
										</Row>
									</div>
									<Form.Control
										required
										onChange={(e: React.ChangeEvent<HTMLInputElement>) => { this.setState({ editted_tagline: e.target.value }) }}
										value={this.state.editted_tagline}
										disabled={localStorage['username'] !== this.props.match.params.username} />
								</Form.Group>
							</Row>

							<Row className='marketplace target-customer'>
								<Form.Group className="mb-3" controlId="target_customer">
									<div className='marketplace-field-header'>
										<Row>
											<Form.Label>
												Target Customer
												{localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username'] &&
													<React.Fragment>
														{' '} ({MAX_FILE_SUBMISSION_SIZE} max)
													</React.Fragment>
												}

												{(localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username']) &&
													<React.Fragment>
														<Button
															size={'sm'}
															className='label-addition'
															id='target_customer'
															onClick={this.handleUploadClick}
															disabled={this.state.submitting.target_customer}
															variant={this.state.submitting.target_customer ? 'outline-primary' : 'primary'}
														>
															{this.state.submitting.target_customer ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
														</Button>
													</React.Fragment>
												}

												{(localStorage['user_type'] === "Judge" && this.props.match.params.username !== localStorage['username']) &&
													<React.Fragment>
														<InputGroup className='marketplace-specific-judge-score-group'>
															<FormControl
																value={this.state.target_customer_score}
																type="number"
																onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.setState({target_customer_score: Number.parseInt(event.currentTarget.value)}) }
																size={'sm'}
																style={JUDGE_SCORE_INPUT_WIDTH_STYLE}
																disabled={this.state.submitting_score.target_customer}
																min={0}
																max={25} />
															<InputGroup.Text>/25</InputGroup.Text>
															<Button
																size={'sm'}
																className='label-addition'
																id='target_customer'
																onClick={this.handleUploadScoreClick}
																disabled={this.state.submitting_score.target_customer}
																variant={this.state.submitting_score.target_customer ? 'outline-primary' : 'primary'}
															>
															{this.state.submitting_score.target_customer ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
															</Button>
														</InputGroup>
													</React.Fragment>
												}
											</Form.Label>
										</Row>
									</div>
									<div className='embeded target-customer'>
										{(this.state.target_customer_stub !== "") ?
											<React.Fragment>
												<ImageOrPDFViewer
													stub={this.state.target_customer_stub}
													hash={this.state.target_customer_hash}
													alt='Target Customer file'
													title='target_customer_viewer' />
											</React.Fragment>
											:
											<span style={{ color: 'red' }}>
												No target customer file has been uploaded
											</span>
										}
									</div>
								</Form.Group>
							</Row>

							<Row className='marketplace poster'>
								<Form.Group className="mb-3" controlId="poster">
									<div className='marketplace-field-header'>
										<Row>
											<Form.Label>
												Annotated Poster
												{localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username'] &&
													<React.Fragment>
														{' '} ({MAX_FILE_SUBMISSION_SIZE} max)
													</React.Fragment>
												}

												{(localStorage['user_type'] === "Team" && this.props.match.params.username === localStorage['username']) &&
													<React.Fragment>
														<Button
															size={'sm'}
															className='label-addition'
															id='poster'
															onClick={this.handleUploadClick}
															disabled={this.state.submitting.poster}
															variant={this.state.submitting.poster ? 'outline-primary' : 'primary'}
														>
															{this.state.submitting.poster ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
														</Button>
													</React.Fragment>
												}

												{(localStorage['user_type'] === "Judge" && this.props.match.params.username !== localStorage['username']) &&
													<React.Fragment>
														<InputGroup className='marketplace-specific-judge-score-group'>
															<FormControl
																value={this.state.poster_score}
																type="number"
																onChange={(event: React.ChangeEvent<HTMLInputElement>) => this.setState({poster_score: Number.parseInt(event.currentTarget.value)}) }
																size={'sm'}
																style={JUDGE_SCORE_INPUT_WIDTH_STYLE}
																disabled={this.state.submitting_score.poster}
																min={0}
																max={25} />
															<InputGroup.Text>/25</InputGroup.Text>
															<Button
																size={'sm'}
																className='label-addition'
																id='poster'
																onClick={this.handleUploadScoreClick}
																disabled={this.state.submitting_score.poster}
																variant={this.state.submitting_score.poster ? 'outline-primary' : 'primary'}
															>
															{this.state.submitting_score.poster ?
																<React.Fragment>
																	<FaSpinner className='spinner' /> Upload
																</React.Fragment>
																:
																<React.Fragment>
																	<FaUpload /> Upload
																</React.Fragment>
															}
															</Button>
														</InputGroup>
													</React.Fragment>
												}
											</Form.Label>
										</Row>
									</div>
									<div className='embeded poster'>
										{(this.state.poster_stub !== "") ?
											<React.Fragment>
												<ImageOrPDFViewer
													stub={this.state.poster_stub}
													hash={this.state.poster_hash}
													alt='Annotated Poster file'
													title='poster_viewer' />
											</React.Fragment>
											:
											<span style={{ color: 'red' }}>
												No poster file has been uploaded
											</span>
										}
									</div>
								</Form.Group>
							</Row>
						</Col>
					</Row>

					{/* Admins see school + area, teachers see delete, and judges have scores instead of save */}
					{(localStorage['username'] !== this.props.match.params.username) &&
						<Row>
							<Col>
								{this.state.user_is_administrator ?
								<Button
									variant='danger'
									onClick={this.handleDelete}
									style={{float : 'right'}}
									disabled={this.state.delete_loading}>
									{this.state.delete_loading ?
										<React.Fragment>
											<FaSpinner className='spinner' /> Delete Submission
										</React.Fragment>
									:
										<React.Fragment>
											<FaTrash /> Delete Submission
										</React.Fragment>
									}
								</Button>
								:
								<Row>
									{localStorage['user_type'] === "Team" && localStorage['username'] !== this.props.match.params.username &&
										<Col>
											{this.state.liked ?
												<Button
													variant='primary'
													onClick={this.handleUnlike}
													disabled={this.state.like_loading}>
													{this.state.like_loading ?
														<React.Fragment>
															<FaSpinner className='spinner' /> Unlike
														</React.Fragment>
													:
														<React.Fragment>
															<FaThumbsDown /> Unlike
														</React.Fragment>
													}
												</Button>
											:
												<Button
													variant='primary'
													onClick={this.handleLike}
													disabled={!this.state.can_like || this.state.like_loading}>
													{this.state.like_loading ?
														<React.Fragment>
															<FaSpinner className='spinner' /> Like
														</React.Fragment>
													:
														<React.Fragment>
															<FaThumbsUp /> Like
														</React.Fragment>
													}
													
												</Button>
											}
										</Col>
									}
									<Col>
										<Button
											variant='primary'
											onClick={() => { this.setState({ report_modal_show : true }); }}
											style={{float : "right"}}>
											<FaFlag /> Report Submission
										</Button>
									</Col>
								</Row>
								}
							</Col>
						</Row>
					}
				</BasePage>
			</React.Fragment>
		);
	}
}
