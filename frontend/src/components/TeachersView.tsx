/*
  Copyright Jim Carty © 2022: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaTrash, FaSpinner, FaCog, FaChevronDown, FaChevronUp } from 'react-icons/fa';

import { BasePage, PageProps, resolveGETCall, teachersInfoRESTLink, TeachersInfoREST, userSpecificOperationsRESTLink, RESTError, resolveDELETECall, changePasswordRESTLink, ChangePasswordREST, resolvePUTCall, ErrorMessageText } from './elements';

import { TeamsTable } from './TeamsTable';

import './TeachersView.css';

export interface TeachersViewProps extends RouteComponentProps, PageProps { }

interface State extends TeachersInfoREST {
	loaded: boolean,
	
	loadingResetPassword: boolean,
	loadingShowTeams: string[],
	showTeams: string[],

	hash: string
}

export class TeachersView extends React.Component<TeachersViewProps, State> {
	constructor(props: TeachersViewProps) {
		super(props);

		this.state = {
			teacher_names: [],
			teacher_schools: [],
			teacher_areas: [],

			loaded: false,

			loadingResetPassword: false,
			loadingShowTeams: [],
			showTeams: [],

			hash: ""
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Admin") {
			// load all teachers who signed up through this teacher
			const result: Result<TeachersInfoREST, RESTError> = await resolveGETCall<TeachersInfoREST>(teachersInfoRESTLink, true);

			result
				.map(res => {
					this.setState({ ...res, loaded: true });
					return null; // to prevent warnings
				})
				.mapErr(err => {
					console.error(err);
					this.props.updateAlertBar(err.message, "warning", true);
				});
		} else
			this.props.history.push('/');
	}

	handleDeletePress = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		const index: number = Number(e.currentTarget.id);
		const selectedTeamName: string = this.state.teacher_names[index];

		const path: string = userSpecificOperationsRESTLink + selectedTeamName;
		const result: Result<{}, RESTError> = await resolveDELETECall<{}>(path, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully deleted the teacher user", "success", true);

				this.state.teacher_names.splice(index, 1);
				this.state.teacher_schools.splice(index, 1);
				this.state.teacher_areas.splice(index, 1);

				this.setState({hash: String(Date.now())})

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	handleResetPassword = async (event: React.MouseEvent<HTMLButtonElement>) => {
		this.setState({loadingResetPassword : true});

		const index: number = Number(event.currentTarget.id);
		const selectedTeamName: string = this.state.teacher_names[index];

		const path: string = changePasswordRESTLink + selectedTeamName;

		const result : Result<ChangePasswordREST, RESTError> = await resolvePUTCall<ChangePasswordREST, {}>(path, {}, true);

		result
			.map(res => {
				if (res.message !== undefined)
					this.props.updateAlertBar(<ErrorMessageText statusText={"Success"} message={res.message} />, 'success', true);
				else
					this.props.updateAlertBar(<ErrorMessageText statusText={"Success"} message={"Successfully changed user's password"} />, 'success', true);

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, 'danger', true);
			});

		this.setState({loadingResetPassword : false});
	}

	handleToggleShowTeams = async (event: React.MouseEvent<HTMLButtonElement>) => {
		const index: number = Number(event.currentTarget.id);
		const selectedTeacherName: string = this.state.teacher_names[index];

		if (this.state.showTeams.includes(selectedTeacherName)) {
			const showArrayIndex: number = this.state.showTeams.indexOf(selectedTeacherName);
			this.state.showTeams.splice(showArrayIndex, 1);

			const loadingArrayIndex: number = this.state.loadingShowTeams.indexOf(selectedTeacherName);
			this.state.loadingShowTeams.splice(loadingArrayIndex, 1);
		} else {
			this.state.loadingShowTeams.push(selectedTeacherName);
			this.state.showTeams.push(selectedTeacherName);
		}

		this.setState({ hash: String(Date.now()) });
	}

	getTeamsLoadedFunction = (index: number) => {
		const selectedTeacherName: string = this.state.teacher_names[index];
		
		return async (loaded: boolean) => {
			const arrayIndex: number = this.state.loadingShowTeams.indexOf(selectedTeacherName);

			this.state.loadingShowTeams.splice(arrayIndex, 1);

			this.setState({ hash : String(Date.now()) });
		}
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded} title={'Teachers'}>
					<table className='teachers-list' style={{width: "100%"}}>
						<thead>
							<tr className='teachers-list-header'>
								<th>
									Teachers Username
								</th>
								<th>
									Area
								</th>
								<th>
									School
								</th>
								<th>
									Operations
								</th>
							</tr>
						</thead>

						<tbody>
							{this.state.teacher_names.map(
								(name, index) => {
									return (
										<React.Fragment key={name}>
											<tr className='teachers-list-elem' key={this.state.teacher_names[index]}>
												<td className={this.state.showTeams.includes(this.state.teacher_names[index]) ? "expanded" : ""}>
													{this.state.teacher_names[index]}
												</td>
												<td className={this.state.showTeams.includes(this.state.teacher_names[index]) ? "expanded" : ""}>
													{this.state.teacher_areas[index]}
												</td>
												<td className={this.state.showTeams.includes(this.state.teacher_names[index]) ? "expanded" : ""}>
													{this.state.teacher_schools[index]}
												</td>
												<td className={this.state.showTeams.includes(this.state.teacher_names[index]) ? "expanded" : ""}>
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleDeletePress}
														variant='danger'
														className={'teachers-table-button'}>
														<FaTrash />
													</Button>
													{' '}
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleResetPassword}
														variant='primary'
														disabled={this.state.loadingResetPassword}
														className={'teachers-table-button'}>
														{this.state.loadingResetPassword ?
															<React.Fragment>
																<FaSpinner className='spinner' /> Reset Password
															</React.Fragment>
														:
															<React.Fragment>
																<FaCog /> Reset Password
															</React.Fragment>
														}
													</Button>
													{' '}
													<Button
														size={'sm'}
														id={String(index)}
														onClick={this.handleToggleShowTeams}
														variant='primary'
														disabled={this.state.loadingShowTeams.includes(this.state.teacher_names[index])}
														className={'teachers-table-button'}>
														{this.state.loadingShowTeams.includes(this.state.teacher_names[index]) ?
															<React.Fragment>
																<FaSpinner className='spinner' /> Show Teams
															</React.Fragment>
														:
															<React.Fragment>
																{this.state.showTeams.includes(this.state.teacher_names[index]) ?
																	<React.Fragment>
																		<FaChevronUp /> Hide Teams
																	</React.Fragment>
																	:
																	<React.Fragment>
																		<FaChevronDown /> Show Teams
																	</React.Fragment>
																}
															</React.Fragment>
														}
													</Button>
												</td>
											</tr>
											<tr className={'teachers-table-expanded-teams'}>
												<td colSpan={4} className={this.state.showTeams.includes(this.state.teacher_names[index]) ? "expanded" : ""}>
													{this.state.showTeams.includes(this.state.teacher_names[index]) &&
														<React.Fragment>
															<div>
																<TeamsTable {...this.props} setLoaded={this.getTeamsLoadedFunction(index)} teacher={this.state.teacher_names[index]} />
															</div>
														</React.Fragment>
													}
												</td>
											</tr>
										</React.Fragment>
									)
								}
							)

							}
						</tbody>
					</table>

				</BasePage>
			</React.Fragment >
		);
	}
}

