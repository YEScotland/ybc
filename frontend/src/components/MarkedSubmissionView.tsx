/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Col, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';

import { BasePage, PageProps, resolveGETCall, MarketplaceStalls, RESTError, markedMarketplaceRESTLink, ImageOrPDFViewer, getActualURL } from './elements';

import './MarkedSubmissionView.css';

export interface MarkedSubmissionViewProps extends RouteComponentProps, PageProps { }

interface State extends MarketplaceStalls {
	loaded: boolean,
	logo_hash: string,
	poster_hash: string,
	loading_filter: boolean,
	filter_name_contains: string,
	filter_tagline_contains: string,
	filter_pdf_poster: boolean
}

export class MarkedSubmissionView extends React.Component<MarkedSubmissionViewProps, State> {
	constructor(props: MarkedSubmissionViewProps) {
		super(props);

		this.state = {
			stalls: [],

			loaded: false,

			logo_hash: "" + Date.now(),
			poster_hash: "" + Date.now(),

			loading_filter: false,

			filter_name_contains: "",
			filter_tagline_contains: "",
			filter_pdf_poster: false
		};
	}

	async componentDidMount() {
		await this.retrieveMarketplace();
		this.setState({ loaded: true });
	}

	handleNavigateToStall = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		this.props.history.push('/marketplace/' + event.currentTarget.id);
	}

	async retrieveMarketplace() {
		const path: string = markedMarketplaceRESTLink;
		const result: Result<MarketplaceStalls, RESTError> = await resolveGETCall(path, true);

		result
			.map(res => {
				this.setState({ ...res });
				return null; // prevent warning
			})
			.mapErr(err => {
				console.error(err);
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	render() {
		return (
			<React.Fragment>
				<BasePage {...this.props} loaded={this.state.loaded} title={'Marketplace'}>

					<Col>
						<Row className='marketplace-container'>
							{this.state.stalls.map(stall => {
								return (
									<React.Fragment key={stall.logo_stub}>
										<div
											className='marketplace-stall'
											id={stall.team_username}
											onClick={this.handleNavigateToStall}>
											<Row>
												<Col>
													<h4 className='marketplace-stall-heading'>
														{stall.company_name}
													</h4>
												</Col>
											</Row>
											<Row>
												<Col>
													<div className='marketplace-stall-logo'>
														<img
															alt='Stall logo'
															src={getActualURL(stall.logo_stub, this.state.logo_hash)} />
													</div>
												</Col>
											</Row>

											<Row>
												<Col>
													Tagline
												</Col>
											</Row>
											<Row>
												<Col>
													<div className='marketplace-stall-tagline'>
														<span>
															{stall.tagline}
														</span>
													</div>
												</Col>
											</Row>

											<Row>
												<Col>
													Annotated Poster
												</Col>
											</Row>
											<Row>
												<Col>
													<div className='marketplace-stall-poster'>
														<ImageOrPDFViewer
															stub={stall.poster_stub}
															hash={this.state.poster_hash}
															alt={stall.company_name + "'s company poster"}
															title={stall.company_name + 's_poster_viewer'} />
													</div>
												</Col>
											</Row>
										</div>
									</React.Fragment>
								);
							})
							}
						</Row>
					</Col>

				</BasePage>
			</React.Fragment>
		);
	}
}
