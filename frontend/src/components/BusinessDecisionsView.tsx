/*
  Copyright Nikolaos Terzis and Jim Carty © 2022: nickterzis.18@gmail.com and cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React, { useEffect, useState } from 'react';

import { Row, Col, Container, ProgressBar, Button, Modal } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';

import { resolveGETCall, resolvePOSTCall, getActualURL, PageProps, QuestionREST, businessDecisionRESTLink, RESTError } from './elements/utils';
import { BasePage } from './elements/BasePage';

import './BusinessDecisionsView.css';

export interface BusinessDecisionsProps extends RouteComponentProps, PageProps {};

export const BusinessDecisionsView: React.FC<BusinessDecisionsProps> = (props : BusinessDecisionsProps) => {
	const [questions, setQuestions] = useState<QuestionREST>();
	const [currentQuestion, setCurrentQuestion] = useState<number>(-1);
	const [selection, setSelection] = useState<number>();
    const [progress, setProgress] = useState<number>(0);
	const [show, setShow] = useState<boolean>(true);

	const handleClose = () => setShow(false);
	const handleShow = () => setShow(true);

	const updateSelection = async (answerIndex: number) => {
		setSelection(answerIndex);
		if (questions === undefined) {
			props.updateAlertBar("Cannot make selection, questions haven't been loaded", "danger", true);
			return;
		}

		let data = {"questionId": questions[currentQuestion].questionId,
					"selection": answerIndex};
		resolvePOSTCall(businessDecisionRESTLink, data, true);
		setQuestions(prev => {
			if (prev === undefined) {
				props.updateAlertBar("Cannot make selection successfully. Please reload and try again", "danger", true);
				return;
			}
			
			let a = [...prev];
			a[currentQuestion].selection = answerIndex;
			return [...a];
		});
	}

	const handleButtonClick = (event: React.MouseEvent<HTMLButtonElement>) => {
		if (questions === undefined) {
			props.updateAlertBar("Cannot move to next question when questions have not been loaded", "danger", true);
			return;
		}
		
		let nextQuestion: number = 0;
		if (event.currentTarget.id === 'prev' && currentQuestion > 0) {
			nextQuestion = currentQuestion-1;
			setShow(false);
		} else if (currentQuestion < (questions.length-1)) {
			nextQuestion = currentQuestion + 1;
			setShow(true);
		} else {
			props.updateAlertBar("Error when moving to next question, please contact you Teacher to contact their YES admin", "danger", true);
			return;
		}
		setCurrentQuestion(nextQuestion);
		setSelection(questions[nextQuestion].selection);
        setProgress((nextQuestion / questions.length)*100)
	};

	// Set progress bar to 100% and display message that answers have been saved
	const handleSave = () => {
		setProgress(100);
		props.updateAlertBar("Your answers have been saved. You can still change them up until the submission deadline.", "success", true);
	}

	const updateAlertBar = props.updateAlertBar;

	useEffect(() => {
		let fetchPromise: Promise<Result<QuestionREST, RESTError>> = resolveGETCall<QuestionREST>(businessDecisionRESTLink, true);
		fetchPromise
			.then(result => {
				result
					.map(res => {
						setQuestions(res);
						setCurrentQuestion(0);
						setSelection(res[0].selection);
						
						return null; // prevent warning
					})
					.mapErr(err => {
						updateAlertBar(err.message, "danger", true);
					});
			});
	}, [updateAlertBar]);

	if (questions === undefined || currentQuestion === -1) {
		return (
			<React.Fragment>
				<BasePage {...props} loaded={false} title={'Business Decisions'} />
			</React.Fragment>
		);
	}

	let animationSrc: string = "";
	let currentAnimationLink: string | undefined = questions[currentQuestion].animationLink;
	if (currentAnimationLink !== undefined) {
		animationSrc = getActualURL(currentAnimationLink, String(Date.now()));
	}
	let animationYtUrl: string | undefined = questions[currentQuestion].yt_url;

	return (
		<React.Fragment>
		<BasePage {...props} loaded={true} title={'Business Decisions'}>

			<Button disabled={questions[currentQuestion].animationLink === undefined} variant="primary" onClick={handleShow} style={{marginBottom: "1.2rem"}}>
				Animation
			</Button>
			
			{questions !== undefined &&
				<React.Fragment>
					<Modal id="animationModal" show={questions[currentQuestion].animationLink !== undefined && show} onHide={handleClose}>
						<Modal.Header closeButton>
							<Modal.Title>Question Animation</Modal.Title>
						</Modal.Header>
						<Modal.Body>							
							<video className="animationVideo" controls preload="auto">
								<source src={animationSrc} type="video/mp4" />
								Your browser does not support the video tag.
							</video>
							{animationYtUrl && 
							<p>Animation not playing? Press <a href={animationYtUrl} target="_blank" rel="noreferrer noopener">here</a> to watch on Youtube.</p>}
						</Modal.Body>
						<Modal.Footer>
							<Button variant="secondary" onClick={handleClose}>
								Close
							</Button>
						<Button variant="primary" onClick={handleClose}>
							Got It!
						</Button>
						</Modal.Footer>
					</Modal>
				</React.Fragment>
			}


			<Container className="app">
				<ProgressBar now={progress} label={`${Math.floor(progress)}%`} />
				<Row>
					<Col className="question-section question-text">{questions[currentQuestion].questionText}</Col>
				</Row>
				<Row className="answer-section">
					{questions[currentQuestion].answerOptions.map((answerText, ind) => (
						<Row key={ind}>
							<Button className={`answer ${selection === ind ? "answer-outline-secondary" : "answer-secondary"}`} key={ind} onClick={() => updateSelection(ind)}>{answerText}</Button>
						</Row>
						
					))}
				</Row>
				{ currentQuestion !== 0 && <Button id='prev' className='questionButton' variant="primary" onClick={handleButtonClick}>Previous</Button> }
				{ currentQuestion < questions.length-1 && <Button id='next' className='questionButton next' onClick={handleButtonClick}>Next</Button> }
                { currentQuestion === questions.length-1 && <Button className='questionButton next' onClick={handleSave}>Save</Button>}
			</Container>
		</BasePage>
		</React.Fragment>
	);
}
