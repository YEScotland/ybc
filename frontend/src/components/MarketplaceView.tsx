/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Form, Button, Col, Modal, Row } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaFilter } from 'react-icons/fa';

import { BasePage, PageProps, resolveGETCall, MarketplaceStalls, RESTError, marketplaceStallsRESTLink, ImageOrPDFViewer, getActualURL } from './elements';

import './MarketplaceView.css';

export interface MarketplaceViewProps extends RouteComponentProps, PageProps { }

interface State extends MarketplaceStalls {
	loaded: boolean,
	showFilters: boolean,
	logo_hash: string,
	poster_hash: string,
	loading_filter: boolean,
	filter_name_contains: string,
	filter_tagline_contains: string,
	filter_pdf_poster: boolean
}

export class MarketplaceView extends React.Component<MarketplaceViewProps, State> {
	constructor(props: MarketplaceViewProps) {
		super(props);

		this.state = {
			stalls: [],

			loaded: false,

			logo_hash: "" + Date.now(),
			poster_hash: "" + Date.now(),

			showFilters: false,
			loading_filter: false,

			filter_name_contains: "",
			filter_tagline_contains: "",
			filter_pdf_poster: false
		};
	}

	async componentDidMount() {
		await this.retrieveMarketplace();
		this.setState({ loaded: true });
	}

	handleNavigateToStall = (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => {
		this.props.history.push('/marketplace/' + event.currentTarget.id);
	}

	async retrieveMarketplace() {
		const path: string = marketplaceStallsRESTLink;
		const result: Result<MarketplaceStalls, RESTError> = await resolveGETCall(path, true);

		result
			.map(res => {
				this.setState({ ...res });
				return null; // prevent warning
			})
			.mapErr(err => {
				console.error(err);
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	handleFilterRequest = async (event: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		this.setState({ showFilters: false, loading_filter: true });

		await this.retrieveMarketplace();

		this.setState({ loading_filter: false });
	}

	render() {
		return (
			<React.Fragment>
				<Modal show={this.state.showFilters} onHide={() => { this.setState({ showFilters: false }) }}>
					<Modal.Header closeButton>
						<Modal.Title>Select Filters</Modal.Title>
					</Modal.Header>

					<Modal.Body>
						<Form.Group>
							<Form.Label>
								Company name includes:
							</Form.Label>
							<Form.Control
								value={this.state.filter_name_contains}
								onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ filter_name_contains: event.target.value }); }} />
						</Form.Group>

						<Form.Group>
							<Form.Label>
								Tagline includes:
							</Form.Label>
							<Form.Control
								value={this.state.filter_tagline_contains}
								onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState({ filter_tagline_contains: event.target.value }); }} />
						</Form.Group>

						<Form.Group>
							<Form.Check
								type='checkbox'
								checked={this.state.filter_pdf_poster}
								onChange={(event: React.ChangeEvent<HTMLInputElement>) => { this.setState(prevState => { return { filter_pdf_poster: !prevState.filter_pdf_poster }; }); }}
								label={'Submitted a PDF poster'} />
						</Form.Group>
					</Modal.Body>

					<Modal.Footer>
						<Button variant='primary' onClick={this.handleFilterRequest}>
							<FaFilter /> Apply
						</Button>
					</Modal.Footer>
				</Modal>

				<BasePage {...this.props} loaded={this.state.loaded} title={'Marketplace'}>

					{/* <Row>
						<Col>
							<Button
								variant={this.state.loading_filter ? 'outline-primary' : 'primary'}
								disabled={this.state.loading_filter}
								onClick={() => { this.setState({ showFilters: true }); }}
								style={{ float: 'right' }}>
								{this.state.loading_filter ?
									<React.Fragment>
										<FaSpinner className='spinner' /> Filter
									</React.Fragment>
									:
									<React.Fragment>
										<FaFilter /> Filter
									</React.Fragment>
								}
							</Button>
						</Col>
					</Row> */}

					<Col>
						<Row className='marketplace-container'>
							{this.state.stalls.map(stall => {
								return (
									<React.Fragment key={stall.logo_stub}>
										<div
											className='marketplace-stall'
											id={stall.team_username}
											onClick={this.handleNavigateToStall}>
											<Row>
												<Col>
													<h4 className='marketplace-stall-heading'>
														{stall.company_name}
													</h4>
												</Col>
											</Row>
											<Row>
												<Col>
													<div className='marketplace-stall-logo'>
														<img
															alt='Stall logo'
															src={getActualURL(stall.logo_stub, this.state.logo_hash)} />
													</div>
												</Col>
											</Row>

											<Row>
												<Col>
													Tagline
												</Col>
											</Row>
											<Row>
												<Col>
													<div className='marketplace-stall-tagline'>
														<span>
															{stall.tagline}
														</span>
													</div>
												</Col>
											</Row>

											<Row>
												<Col>
													Annotated Poster
												</Col>
											</Row>
											<Row>
												<Col>
													<div className='marketplace-stall-poster'>
														<ImageOrPDFViewer
															stub={stall.poster_stub}
															hash={this.state.poster_hash}
															alt={stall.company_name + "'s company poster"}
															title={stall.company_name + 's_poster_viewer'} />
													</div>
												</Col>
											</Row>
										</div>
									</React.Fragment>
								);
							})
							}
						</Row>
					</Col>

				</BasePage>
			</React.Fragment>
		);
	}
}
