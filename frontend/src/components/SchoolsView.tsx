/*
  Copyright Jim Carty © 2021: cartyjim1@gmail.com

  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
*/

import React from 'react';

import { Button, Col, Row, Modal, Form } from 'react-bootstrap';
import { RouteComponentProps } from 'react-router';
import { Result } from 'neverthrow';
import { FaPencilAlt, FaUpload, FaTrash } from 'react-icons/fa';
import Select, { SingleValue, MultiValue } from 'react-select';

import { BasePage, PageProps, resolveGETCall, SecretKeyREST, SecretKeyRESTIndividual, SecretKeyRESTSubmit, RESTError, secretKeyRESTLink, LocalAuthoritiesREST, LocalAuthoritiesSchoolsREST, localAuthoritiesRESTLink, localAuthoritiesRESTLinkPrefix, localAuthoritiesRESTLinkSuffix, LocalAuthoritiesSchool, resolvePOSTCall, resolveDELETECall } from './elements';

import './SchoolsView.css';

interface SelectOption {
	value: string,
	label: string
}

interface SchoolSelectOption extends SelectOption, LocalAuthoritiesSchool { }

const SELECT_KEY_TYPE_OPTIONS: SelectOption[] = [
	{ value: "Teacher", label: "Teacher" },
	{ value: "Judge", label: "Judge" }
];

export interface SchoolsViewProps extends RouteComponentProps, PageProps { }

interface State extends SecretKeyREST {
	loaded: boolean,
	showCreateKey: boolean,
	key_type: SelectOption,
	key_school: SchoolSelectOption,
	key_areas: SelectOption[]
	schools_loading: boolean,
	local_authorities_loading: boolean,
	schools: SchoolSelectOption[],
	local_authorities: SelectOption[],
	error_text: string | React.ReactElement,
	hash: string
}

export class SchoolsView extends React.Component<SchoolsViewProps, State> {
	constructor(props: SchoolsViewProps) {
		super(props);

		this.state = {
			keys: [],
			loaded: false,
			showCreateKey: false,
			key_type: SELECT_KEY_TYPE_OPTIONS[0],
			key_areas: [],
			key_school: {
				value: "",
				label: "",
				local_authority: ""
			},
			schools_loading: true,
			local_authorities_loading: true,
			schools: [],
			local_authorities: [],
			error_text: "",
			hash: ""
		};
	}

	async componentDidMount() {
		if (localStorage['user_type'] === "Admin") {
			// load all secret keys created by this admin
			const secretKeysResult: Result<SecretKeyREST, RESTError> = await resolveGETCall<SecretKeyREST>(secretKeyRESTLink, true);

			secretKeysResult
				.map(res => {
					this.setState({ keys: res.keys });

					return null; // to prevent warnings
				})
				.mapErr(err => {
					console.error(err);

					this.props.updateAlertBar(err.message, "warning", true);
				})

			this.setState({ loaded: true });

			// load all local authorities
			const localAuthoritiesResult: Result<LocalAuthoritiesREST, RESTError> = await resolveGETCall<LocalAuthoritiesREST>(localAuthoritiesRESTLink, true);

			localAuthoritiesResult
				.map(res => {
					var localAuthorities: SelectOption[] = [];
					for (var i = 0; i < res.local_authorities.length; ++i) {
						var str: string = res.local_authorities[i];

						if (str !== "Unknown") {
							localAuthorities.push({
								value: str,
								label: str
							});
						}
					}

					localAuthorities.sort((a, b) => {
						if (a.value > b.value) return 1;
						if (a.value < b.value) return -1;
						return 0;
					});

					this.setState({ local_authorities_loading: false, local_authorities: localAuthorities });

					return null; // to prevent warning
				})
				.mapErr(err => {
					console.error(err);
					this.props.updateAlertBar(err.message, "danger", true);
				});

			// load all schools
			this.loadSchoolsInLocalAuthority(this.state.key_areas.map(elem => elem.value));
		} else
			this.props.history.push('/');
	}

	loadSchoolsInLocalAuthority = async (local_authorities: string[]) => {
		var path: string;
		if (local_authorities.length !== 0)
			path = localAuthoritiesRESTLinkPrefix + local_authorities.join('+').replace(' ', '%20') + localAuthoritiesRESTLinkSuffix;
		else 
			path = localAuthoritiesRESTLinkPrefix + "Unknown" + localAuthoritiesRESTLinkSuffix;

		const result: Result<LocalAuthoritiesSchoolsREST, RESTError> = await resolveGETCall<LocalAuthoritiesSchoolsREST>(path, true);

		return result
			.map(res => {
				var schools: SchoolSelectOption[] = [];
				for (var i = 0; i < res.schools.length; ++i) {
					var school: LocalAuthoritiesSchool = res.schools[i];

					schools.push({
						value: school.value,
						label: school.value,
						local_authority: school.local_authority
					});
				}

				schools.sort((a, b) => {
					if (a.value > b.value) return 1;
					if (a.value < b.value) return -1;
					return 0;
				});

				this.setState({ schools_loading: false, schools: schools });

				return null; // to prevent warning
			})
			.mapErr(err => {
				console.error(err);
				this.props.updateAlertBar(err.message, "danger", true);
			});
	}

	handleNewKeySubmit = async (event: React.FormEvent<HTMLFormElement>) => {
		event.preventDefault();

		if (this.state.key_areas[0].value === "Unknown") {
			if (this.state.key_type.value === "Judge") {
				this.setState({ error_text: "Please select at least an area to create a judge key" });
				return;
			} else {
				this.setState({ error_text: "Please select an area and school to create a teacher key" });
				return;
			}
		}

		if (this.state.key_type.value === "Teacher" && this.state.key_school.value === "") {
			this.setState({ error_text: "Please select a school to successfully create a teacher key" });
			return;
		}

		const data: SecretKeyRESTSubmit = {
			key_type: this.state.key_type.value,
			areas: this.state.key_areas.map(elem => elem.value),
			school: this.state.key_school.value
		}
		const result: Result<SecretKeyRESTIndividual, RESTError> = await resolvePOSTCall<SecretKeyRESTIndividual, SecretKeyRESTSubmit>(secretKeyRESTLink, data, true);

		result
			.map(res => {
				this.props.updateAlertBar(
					<React.Fragment>
						{"Successfully created new " + res.key_type + " key for " + res.school + ": "}<strong>{res.secret_key}</strong>
					</React.Fragment>
					, "success", true);

				this.state.keys.push(res);
				this.setState({ showCreateKey: false, error_text: "" });

				return null; // avoid warning
			})
			.mapErr(err => {
				this.setState({ error_text: err.message });
			})
	}

	handleDeletePress = async (e: React.MouseEvent<HTMLButtonElement, MouseEvent>) => {
		const index: number = Number(e.currentTarget.id);
		const selectedKey: string = this.state.keys[index].secret_key;

		const path: string = secretKeyRESTLink + '/' + selectedKey
		const result: Result<{}, RESTError> = await resolveDELETECall<{}>(path, true);

		result
			.map(res => {
				this.props.updateAlertBar("Successfully deleted the key", "success", true);

				this.state.keys.splice(index, 1);
				this.setState({ hash: "" + Date.now() });

				return null; // prevent warning
			})
			.mapErr(err => {
				this.props.updateAlertBar(err.message, "danger", true);
			})
	}

	render() {
		return (
			<React.Fragment>
				<Modal show={this.state.showCreateKey} onHide={() => { this.setState({ showCreateKey: false, error_text: "" }) }}>
					<Modal.Header closeButton>
						<Modal.Title>Secret Key Configurator</Modal.Title>
					</Modal.Header>
					<Form onSubmit={this.handleNewKeySubmit}>
						<Modal.Body>
							<Form.Group controlId='area'>
								<Form.Label>
									Area
								</Form.Label>
								<Select
									isMulti
									options={this.state.local_authorities}
									isLoading={this.state.local_authorities_loading}
									isDisabled={this.state.local_authorities_loading}
									onChange={(e: MultiValue<SelectOption>) => {
										if (e !== null) {
											this.setState({ key_areas: e.map(elem => elem) });
											this.loadSchoolsInLocalAuthority(e.map(elem => elem.value));
										}
									}}
									value={this.state.key_areas} />
							</Form.Group>
							<Form.Group controlId='school'>
								<Form.Label>
									School
								</Form.Label>
								<Select
									options={this.state.schools}
									isLoading={this.state.schools_loading}
									isDisabled={this.state.schools_loading}
									onChange={(e: SingleValue<SchoolSelectOption>) => {
										if (e !== null) {
											if (this.state.key_areas.length === 0) {
												var area: SelectOption[] = [{ value: e.local_authority, label: e.local_authority }];
												this.setState({ key_school: e, key_areas: area });
											} else
												this.setState({ key_school: e });

											this.loadSchoolsInLocalAuthority([e.local_authority]);
										}
									}}
									value={this.state.key_school} />
							</Form.Group>
							<Form.Group controlId='type'>
								<Form.Label>
									Key Type
								</Form.Label>
								<Select
									options={SELECT_KEY_TYPE_OPTIONS}
									onChange={(e: SingleValue<SelectOption>) => { if (e !== null) this.setState({ key_type: e }); }}
									value={this.state.key_type} />
							</Form.Group>
						</Modal.Body>
						<Modal.Footer>
							<span style={{ color: 'red' }}>
								{this.state.error_text}
							</span>
							<Button variant="primary" type='submit'>
								<FaUpload /> Submit New Key
							</Button>
						</Modal.Footer>
					</Form>
				</Modal>


				<BasePage {...this.props} loaded={this.state.loaded} title={'Schools'}>

					<Row style={{ marginBottom: ".5rem" }}>
						<Col>
							<Button onClick={() => { this.setState({ showCreateKey: true }) }} variant='primary' style={{ float: 'right' }} size={'sm'}>
								<FaPencilAlt /> Add new key
							</Button>
						</Col>
					</Row>

					<table className='school-keys-list'>
						<thead>
							<tr className='school-keys-list-header'>
								<th>
									School
								</th>
								<th>
									Areas
								</th>
								<th>
									Secret Key
								</th>
								<th>
									Key Type
								</th>
								<th>
									Operations
								</th>
							</tr>
						</thead>

						<tbody>
							{this.state.keys.map(
								(key, index) => {
									return (
										<tr className='school-keys-list-elem' key={key.secret_key}>
											<td>
												{key.school}
											</td>
											<td>
												{key.areas.join(', ')}
											</td>
											<td>
												{key.secret_key}
											</td>
											<td>
												{key.key_type}
											</td>
											<td>
												<Button
													size={'sm'}
													id={String(index)}
													onClick={this.handleDeletePress}
													variant='danger'>
													<FaTrash />
												</Button>
											</td>
										</tr>
									)
								}
							)

							}
						</tbody>
					</table>

				</BasePage>
			</React.Fragment >
		);
	}
}

