#
#  Copyright Jim Carty and Amie Macmillan © 2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import datetime
import os
import boto3

# Setup Django properly before importing Django models
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'backend.settings')
import django
django.setup()

from backend.models import *
from backend.settings import S3_BUCKET

def populate():
    
    user = [{"username" : "BulletSpace@GlenifferHighSchool"},
            {"username" : "MrBloggs@GlenifferHighSchool"},
            {"username" : "JollyNewsCo@GlenifferHighSchool"},
            {"username" : "Grow@GlenifferHighSchool"},
            {"username" : "Elevate@GryffeHighSchool"},
            {"username" : "MentalMotivation@GryffeHighSchool"},
            {"username" : "MotoDaily@GryffeHighSchool"},
            {"username" : "MrsSmith@GryffeHighSchool"},
            {"username" : "judge1"},
            {"username" : "judge2"},
            {"username" : "admin1"},
            {"username" : "admin2"},]
    
    profile = [{"user" : "BulletSpace@GlenifferHighSchool", "type": "Team"},
               {"user" : "JollyNewsCo@GlenifferHighSchool", "type": "Team"},
               {"user" : "Grow@GlenifferHighSchool", "type": "Team"},
               {"user" : "Elevate@GryffeHighSchool", "type": "Team"},
               {"user" : "MentalMotivation@GryffeHighSchool", "type": "Team"},
               {"user" : "MotoDaily@GryffeHighSchool", "type" : "Team"},
               {"user" : "MrBloggs@GlenifferHighSchool", "type" : "Teacher"},
               {"user" : "MrsSmith@GryffeHighSchool", "type" : "Teacher"},
               {"user" : "admin1", "type" : "Admin"},
               {"user" : "admin2", "type" : "Admin"},
               {"user" : "judge1", "type" : "Judge"},
               {"user" : "judge2", "type" : "Judge"}]

    teacher = [{"user" : "MrBloggs@GlenifferHighSchool", "school" : "Gleniffer High School", "area" : "Renfrewshire", "admin" : "admin1"},
               {"user" : "MrsSmith@GryffeHighSchool", "school" : "Gryffe High School", "area" : "Renfrewshire", "admin" : "admin1"}]

    team = [{"user" : "BulletSpace@GlenifferHighSchool", "teacher" : "MrBloggs@GlenifferHighSchool", "question_answer_dict" : {"1" : 1, "2" : 3, "3" : 2, "4" : 3, "5" : 0, "6" : 1}},
            {"user" : "JollyNewsCo@GlenifferHighSchool", "teacher" : "MrBloggs@GlenifferHighSchool", "question_answer_dict" : {"1" : 0, "2" : 1, "3" : 0, "4" : 1, "5" : 1, "6" : 0} },
            {"user" : "Grow@GlenifferHighSchool", "teacher" : "MrBloggs@GlenifferHighSchool", "question_answer_dict" : {"1" : 0, "2" : 3, "3" : 0, "4" : 1, "5" : 2, "6" : 3}},
            {"user" : "Elevate@GryffeHighSchool", "teacher" : "MrsSmith@GryffeHighSchool", "question_answer_dict" : {"1" : 0, "2" : 1, "3" : 2, "4" : 3, "5" : 2, "6" : 0}},
            {"user" : "MentalMotivation@GryffeHighSchool", "teacher" : "MrsSmith@GryffeHighSchool", "question_answer_dict" : {"1" : 1, "2" : 2, "3" : 3, "4" : 0, "5" : 1, "6" : 2} },
            {"user" : "MotoDaily@GryffeHighSchool", "teacher" : "MrsSmith@GryffeHighSchool","question_answer_dict" : {"1" : 3, "2" : 1, "3" : 1, "4" : 1, "5" : 1, "6" : 1} },]

                    # Bullet Space
    uploadedfile = [{"key" : "target_customer", "media_file" : "Bullet Space Customer Profile.pdf", "mime_type" : "application/pdf", "extension" : "pdf", "team" : "BulletSpace@GlenifferHighSchool"},
                    {"key" : "poster", "media_file" : "Bullet Space poster.jpeg", "mime_type" : "image/jpeg", "extension" : "jpeg", "team" : "BulletSpace@GlenifferHighSchool"},
                    {"key" : "logo", "media_file" : "Bullet Space logo.jpeg", "mime_type" : "image/jpeg", "extension" : "jpeg", "team" : "BulletSpace@GlenifferHighSchool"},
                    
                    # Jolly News Co
                    {"key" : "target_customer", "media_file" : "Jolly News Customer Profile.docx", "mime_type" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "extension" : "docx", "team" : "JollyNewsCo@GlenifferHighSchool"},
                    {"key" : "poster", "media_file" : "Jolly News App poster.png", "mime_type" : "image/png", "extension" : "png", "team" : "JollyNewsCo@GlenifferHighSchool"},
                    {"key" : "logo", "media_file" : "Jolly News App logo.png", "mime_type" : "image/png", "extension" : "png", "team" : "JollyNewsCo@GlenifferHighSchool"},
                   
                    # Grow
                    {"key" : "target_customer", "media_file" : "Grow Customer Profile.docx", "mime_type" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "extension" : "docx", "team" : "Grow@GlenifferHighSchool"},
                    {"key" : "poster", "media_file" : "Grow poster.jpg", "mime_type" : "image/jpeg", "extension" : "jpg", "team" : "Grow@GlenifferHighSchool"},
                    {"key" : "logo", "media_file" : "Grow logo.jpg", "mime_type" : "image/jpeg", "extension" : "jpg", "team" : "Grow@GlenifferHighSchool"},
                    
                    # Elevate
                    {"key" : "target_customer", "media_file" : "Elevate Customer Profile.docx", "mime_type" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "extension" : "docx", "team" : "Elevate@GryffeHighSchool"},
                    {"key" : "poster", "media_file" : "elevate poster.png", "mime_type" : "image/png", "extension" : "png", "team" : "Elevate@GryffeHighSchool"},
                    {"key" : "logo", "media_file" : "Elevate logo.png", "mime_type" : "image/png", "extension" : "png", "team" : "Elevate@GryffeHighSchool"},
                    
                    # Mental Motivation
                    {"key" : "target_customer", "media_file" : "Mental Motivation Customer Profile.pdf", "mime_type" : "application/pdf", "extension" : "docx", "team" : "MentalMotivation@GryffeHighSchool"},
                    {"key" : "poster", "media_file" : "Mental Motivation poster.jpg", "mime_type" : "image/jpeg", "extension" : "jpg", "team" : "MentalMotivation@GryffeHighSchool"},
                    {"key" : "logo", "media_file" : "Mental Motivation logo.gif", "mime_type" : "image/gif", "extension" : "gif", "team" : "MentalMotivation@GryffeHighSchool"},

                    # Moto Daily
                    {"key" : "target_customer", "media_file" : "Moto Daily Customer Profile.docx", "mime_type" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "extension" : "docx", "team" : "MotoDaily@GryffeHighSchool"},
                    {"key" : "poster", "media_file" : "Moto Daily poster.docx", "mime_type" : "application/vnd.openxmlformats-officedocument.wordprocessingml.document", "extension" : "docx", "team" : "MotoDaily@GryffeHighSchool"},
                    {"key" : "logo", "media_file" : "Moto Daily logo.jpeg", "mime_type" : "image/jpeg", "extension" : "jpeg", "team" : "MotoDaily@GryffeHighSchool"},]
    
    admin = [{"user" : "admin1"}, {"user" : "admin2"}]

    judge =[{"user" : "judge1", "areas" : ["Renfrewshire"], "admin" : "admin1"},
            {"user" : "judge2", "areas" : ["Renfrewshire"], "admin" : "admin1"}]

    question = [{"question_id" : 1, "question_text" : "The Government increases the National Minimum Wage.Whilst not every employee will be affected by this increase, 10% of your staff are currently on this rate. How could this affect your company? What will you do?"},
                {"question_id" : 2, "question_text": "Your suppliers have been outed by an environmental group online for contaminating local water sources with their waste product. This has resulted in massive media coverage. How could this affect your company?"},
                {"question_id" : 3, "question_text": "Inefficiencies have been identified in your production process. How could this affect your company? What will you do?"},
                {"question_id" : 4, "question_text": "A new business has opened which is new competition in the market for you as they have a similar product to yours. How could this affect your company? What will you do?"},
                {"question_id" : 5, "question_text": "There is increased interest in healthy lifestyles in society as a whole. How could this affect your company? What will you do?"},
                {"question_id" : 6, "question_text": "The country is facing recession. How could this affect your company? How will you react?"},]
                      
    submission = [{"team" : "BulletSpace@GlenifferHighSchool", "company_name" : "Bullet Space", "tagline" : "Created for teens, by teens" },
                  {"team" : "JollyNewsCo@GlenifferHighSchool", "company_name" : "Jolly News Co", "tagline" : "Jollify Your Day" },
                  {"team" : "Grow@GlenifferHighSchool", "company_name" : "Grow", "tagline" : "Turning over a new leaf" },
                  {"team" : "Elevate@GryffeHighSchool", "company_name" : "Elevate", "tagline" : "Family of learning" },
                  {"team" : "MentalMotivation@GryffeHighSchool", "company_name" : "Mental Motivation", "tagline" : "Get better, Feel better" },
                  {"team" : "MotoDaily@GryffeHighSchool", "company_name" : "Moto Daily", "tagline" : "Sending Happiness" } ]
    
    secretkey = [{"user": "MrBloggs@GlenifferHighSchool" , "key_text" : "YNBuS4DRj0", "type" : "Team", "school":"Gleniffer High School", "areas":["Renfrewshire"]},
                 {"user" : "admin1", "key_text" : "gvugrBC4D9", "type" : "Teacher", "school":"Gleniffer High School", "areas":["Renfrewshire"] },
                 {"user" : "admin1", "key_text" : "dNY87ju5y6", "type" : "Judge", "school":"Gleniffer High School", "areas":["Renfrewshire"] },
                 {"user": "MrsSmith@GryffeHighSchool" , "key_text" : "U8scHBq56I", "type" : "Team", "school":"Gryffe High School", "areas":["Renfrewshire"]},
                 {"user" : "admin1", "key_text" : "4OGhCTg2dg", "type" : "Teacher", "school":"Gryffe High School", "areas":["Renfrewshire"] },
                 {"user" : "admin1", "key_text" : "BI37VBYrG1", "type" : "Judge", "school":"Gryffe High School", "areas":["Renfrewshire"] },]
 

    answer = [{"answer_no": 0, "answer_text": "Reduce staff hours across the company", "answer_dragon_scores" : {"finance_score" : 2, "hr_score" : 0 , "environmental_score" : 2,  "marketing_score" : 3, "customers_score" : 3}, "question": 1 },
              {"answer_no": 1, "answer_text": "Increase your product prices", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 3 , "environmental_score" : 5,  "marketing_score" : 0, "customers_score" : 2}, "question": 1 },
              {"answer_no": 2, "answer_text": "Save costs by using lower quality raw materials", "answer_dragon_scores" : {"finance_score" : 2, "hr_score" : 0 , "environmental_score" : 2,  "marketing_score" : 3, "customers_score" : 3}, "question": 1 },
              {"answer_no": 3, "answer_text": "Do nothing", "answer_dragon_scores" : {"finance_score" : 0, "hr_score" : 5, "environmental_score" : 3,  "marketing_score" : 5, "customers_score" : 5}, "question": 1 },

              {"answer_no": 0, "answer_text": "Change to a more expensive supplier then do nothing further at this point", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 0 , "environmental_score" : 2,  "marketing_score" : 3, "customers_score" : 5}, "question": 2},
              {"answer_no": 1, "answer_text": "Change to a more expensive supplier and increase corporate social responsibility (CSR) budget for environmental projects", "answer_dragon_scores" : {"finance_score" : 3, "hr_score" : 5 , "environmental_score" : 3,  "marketing_score" : 2, "customers_score" : 2}, "question": 2 },
              {"answer_no": 2, "answer_text": "Continue using same supplier, make no further changes at this point", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 0 , "environmental_score" : 0,  "marketing_score" : 0, "customers_score" : 3}, "question": 2 },
              {"answer_no": 3, "answer_text": "Stay with same supplier and increase corporate social responsibility (CSR) budget for environmental projects", "answer_dragon_scores" : {"finance_score" : 3, "hr_score" : 2, "environmental_score" : 2,  "marketing_score" : 5, "customers_score" : 5}, "question": 2}, 

              {"answer_no": 0, "answer_text": "Invest in new production equipment, making some staff redundant due to automation", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 0 , "environmental_score" : 2,  "marketing_score" : 3, "customers_score" : 5}, "question": 3 },
              {"answer_no": 1, "answer_text": "Invest in staff training", "answer_dragon_scores" : {"finance_score" : 3, "hr_score" : 5 , "environmental_score" : 3,  "marketing_score" : 2, "customers_score" : 2}, "question": 3 },
              {"answer_no": 2, "answer_text": "Employ new quality control staff", "answer_dragon_scores" : {"finance_score" : 0, "hr_score" : 3 , "environmental_score" : 5,  "marketing_score" : 5, "customers_score" : 3}, "question": 3 },
              {"answer_no": 3, "answer_text": "Sack inefficient staff, employ new staff across the company", "answer_dragon_scores" : {"finance_score" : 2, "hr_score" : 2, "environmental_score" : 0,  "marketing_score" : 0, "customers_score" : 0}, "question": 3}, 

              {"answer_no": 0, "answer_text": "Improve the quality of your product", "answer_dragon_scores" : {"finance_score" : 3, "hr_score" : 5, "environmental_score" : 3,  "marketing_score" : 3, "customers_score" : 5}, "question": 4 },
              {"answer_no": 1, "answer_text": "Increase your advertising", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 3 , "environmental_score" : 0,  "marketing_score" : 5, "customers_score" : 2}, "question": 4 },
              {"answer_no": 2, "answer_text": "Reduce your product price", "answer_dragon_scores" : {"finance_score" : 2, "hr_score" : 2 , "environmental_score" : 5,  "marketing_score" : 2, "customers_score" : 3}, "question": 4 },
              {"answer_no": 3, "answer_text": "Start a legal challenge over the trademark", "answer_dragon_scores" : {"finance_score" : 0, "hr_score" : 0, "environmental_score" : 2,  "marketing_score" : 0, "customers_score" : 0}, "question": 4}, 

              {"answer_no": 0, "answer_text": "Provide more nutritional information on your packaging ", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 2 , "environmental_score" : 0,  "marketing_score" : 0, "customers_score" : 2}, "question": 5 },
              {"answer_no": 1, "answer_text": "Reduce the sugar/salt/fat/caffeine in your product", "answer_dragon_scores" : {"finance_score" : 3, "hr_score" : 3 , "environmental_score" : 5,  "marketing_score" : 2, "customers_score" : 0}, "question": 5 },
              {"answer_no": 2, "answer_text": "Recruit a celebrity to endorse your product (someone associated with healthy-living)", "answer_dragon_scores" : {"finance_score" : 0, "hr_score" : 0 , "environmental_score" : 2,  "marketing_score" : 5, "customers_score" : 3}, "question": 5 },
              {"answer_no": 3, "answer_text": "Release a new ‘healthy option’ product", "answer_dragon_scores" : {"finance_score" : 2, "hr_score" : 5, "environmental_score" : 3,  "marketing_score" : 3, "customers_score" : 5}, "question": 5}, 

              {"answer_no": 0, "answer_text": "Lower your product prices", "answer_dragon_scores" : {"finance_score" : 0, "hr_score" : 3 , "environmental_score" : 5,  "marketing_score" : 2, "customers_score" : 3}, "question": 6},
              {"answer_no": 1, "answer_text": "Keep your product prices the same but source lower quality raw materials to make them", "answer_dragon_scores" : {"finance_score" : 3, "hr_score" : 0, "environmental_score" : 2,  "marketing_score" : 0, "customers_score" : 0}, "question": 6 },
              {"answer_no": 2, "answer_text": "Introduce special price promotions to attract customersc.	Introduce special price promotions to attract customers", "answer_dragon_scores" : {"finance_score" : 2, "hr_score" : 5, "environmental_score" : 3,  "marketing_score" : 3, "customers_score" : 5}, "question": 6 },
              {"answer_no": 3, "answer_text": "Invest in e-commerce", "answer_dragon_scores" : {"finance_score" : 5, "hr_score" : 2, "environmental_score" : 0,  "marketing_score" : 5, "customers_score" : 2}, "question": 6},               
             ]

    submission_like =[{"submission" : "BulletSpace@GlenifferHighSchool", "team" : "Elevate@GryffeHighSchool"},
                      {"submission" : "BulletSpace@GlenifferHighSchool", "team" : "Grow@GlenifferHighSchool"},
                      {"submission" : "Elevate@GryffeHighSchool", "team" : "Grow@GlenifferHighSchool"},
                      {"submission" : "Elevate@GryffeHighSchool", "team" : "BulletSpace@GlenifferHighSchool"}]

    for i in question:
        q = Question.objects.get_or_create(question_id = i["question_id"], question_text = i["question_text"])[0]
        q.save()
  
    for i in answer:
        a = Answer.objects.get_or_create(answer_no = i["answer_no"], answer_text = i["answer_text"], answer_dragon_scores = i["answer_dragon_scores"],  question_id = i["question"])
 
    for i in user:
        u = User.objects.get_or_create(username = i["username"])[0]
        u.set_password("CustardCreams2022")
        u.save()
        
    # profile type
    for i in profile:
        p = Profile.objects.get(user = User.objects.get(username=i["user"]))
        p.type = i["type"]
        p.save()
  
    for i in admin:
        a = Admin.objects.get_or_create(user = User.objects.get(username=i["user"]))[0]
        a.save()

    for i in teacher:
        t = Teacher.objects.get_or_create(user_id = i["user"], user = User.objects.get(username=i["user"]), school = i["school"], area = i["area"], administrator = Admin.objects.get(user__username=i["admin"]) )[0]
        t.save()
    
    for i in team:
        tm = Team.objects.get_or_create(user = User.objects.get(username=i["user"]), teacher_id = i["teacher"], question_answer_dict = i["question_answer_dict"])[0]
        tm.save()
 
    for i in uploadedfile:
        f = UploadedFile.objects.get_or_create(key = i["key"], mime_type = i["mime_type"], extension = i["extension"], team_id = i["team"])[0]
        f.save()
        
        filepath = "sample_data/" + i["media_file"]
        # with open(os.path.join(os.pardir, os.path.join(os.pardir, filepath)), "rb") as file:
        data = None
        with open(filepath, "rb") as file:
            f.media_file.save(i["media_file"], file)  
        
        with open(filepath, "rb") as file:
            data = file.read()

        # uploaded to s3 and save url if s3 is enabled
        if S3_BUCKET != None :
            s3 = boto3.client('s3')

            s3.put_object(
                ACL='public-read',
                Bucket=S3_BUCKET,
                Key=f.path,
                ContentType=i["mime_type"],
                Body=data
            )

            url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, f.path)
            
            f.s3_url = url
            f.save()

    for i in submission:
        s = Submission.objects.get_or_create(team_id = i["team"], company_name = i["company_name"], tagline = i["tagline"])[0]
 
        s.poster = UploadedFile.objects.get(key = "poster", team_id = i["team"])
        s.target_customer = UploadedFile.objects.get(key = "target_customer", team_id = i["team"])
        s.logo = UploadedFile.objects.get(key = "logo", team_id = i["team"])
        s.save()
    
    for i in submission_like:
        sl = SubmissionLike.objects.get_or_create(submission_id = i["submission"], team_id = i["team"])[0]
        sl.save()

    for i in secretkey:
        k = SecretKey.objects.get_or_create(user_id = i["user"], key_text = i["key_text"], type = i["type"], school = i["school"], areas = i["areas"])[0]
        k.save()
    
    for i in judge:
        j = Judge.objects.get_or_create(user_id = i["user"], areas = i["areas"], administrator = Admin.objects.get(user__username=i["admin"]))[0]
        j.save()

    d = Deadline.objects.get_or_create(submission_deadline = datetime.date(2022, 12, 1), voting_deadline = datetime.date(2022, 12, 10), judging_deadline = datetime.date(2022, 12, 20))
    

if __name__ == '__main__':
    print('Populating ...')
    populate()
    print('Database populated.')
