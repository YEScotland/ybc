#
#  Copyright Martin Borcin © 2022: martin.borcin@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

from rest_framework_simplejwt.authentication import JWTAuthentication

class JWTUrlAuthentication(JWTAuthentication):
    def authenticate(self, request):
        raw_token = request.GET.get("token", None)
        if raw_token is None:
            return None

        validated_token = self.get_validated_token(raw_token)
        return self.get_user(validated_token), validated_token
