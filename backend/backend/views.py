#
#  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import csv

import json
import traceback
import base64
import datetime

from django.conf import settings

from django.contrib.auth import authenticate
from django.db.models import Avg
from django.shortcuts import render, redirect
from django.core.exceptions import ValidationError
from django.http import FileResponse, HttpResponse

from rest_framework.decorators import api_view
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .models import *
from .backends import JWTUrlAuthentication
from .serializers import RegisterUserSerializer, ChangePasswordSerializer
from .utils import (
	ACCEPTABLE_DOCUMENT_EXTENSIONS,
	ACCEPTABLE_PICTURE_EXTENSIONS,
	LOCAL_AUTHORITIES,
	MAXIMUM_NUMBER_OF_LIKES,
	canLikeMarketplaceStall,
	# delete_admin,
	delete_judge,
	delete_teacher,
	delete_team,
	get_judging_deadline,
	get_milestones_for_user,
	get_secret_key_value,
	get_submission_deadline,
	get_team_milestones_for_teacher,
	get_tokens_for_user,
	get_url_to_file_field,
	get_voting_deadline,
	is_secret_key_valid,
	STATUS_CODE_2xx,
	STATUS_CODE_4xx,
	STATUS_CODE_5xx,
	upload_file,
	get_url_to_animation,
	get_url_to_intro_video
)

# API views
class RegisterView(APIView):
	def post(self, request) :
		try :
			req = json.loads(request.body.decode('utf-8'))

			if not is_secret_key_valid(req["secret_key"]) :
				return Response({"message": "Secret key is not valid, please verify it your teacher or YE Scotland representative"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)

			serializer = RegisterUserSerializer(data=req)
			
			if not serializer.is_valid() :
				errors = []

				if "username" in serializer.errors :
					errors += list(serializer.errors["username"])
				if "non_field_errors" in serializer.errors : 
					# if errors in password, index is strange due to django
					errors = list(serializer.errors["non_field_errors"])
					
				message = ", and ".join([str(err).lower()[:-1] if i != 0 else str(err)[:-1] for i, err in enumerate(errors)])
				return Response({"message": message}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

			serializer.save()

			ret_user = User.objects.get(username=req["username"])

			details = SecretKey.objects.get(key_text=req["secret_key"])
			profile = Profile.objects.get(user=ret_user)
			profile.type = details.type
			profile.save()
			if profile.type == "Team" :
				Team.objects.create(user=ret_user, teacher=Teacher.objects.get(user=details.user))

			elif profile.type == "Teacher" :
				Teacher.objects.create(user=ret_user, school=details.school, area=details.areas[0], administrator=Admin.objects.get(user=details.user))
				teacher = Teacher.objects.get(user=ret_user)
				
				if len(SecretKey.objects.filter(user=ret_user)) == 0 :
					key_text = get_secret_key_value()
					SecretKey.objects.create(key_text=key_text, user=ret_user)

				key = SecretKey.objects.filter(user=ret_user)[0]

				key.type = "Team" # teachers only create teams
				key.user = ret_user
				key.school = teacher.school
				key.areas = [teacher.area]

				key.save()

			elif profile.type == "Judge" :
				Judge.objects.create(user=ret_user, areas=details.areas, administrator=Admin.objects.get(user=details.user))

			elif profile.type == "Admin" :
				Admin.objects.create(user=ret_user)
				ret_user.is_staff = True
				ret_user.save()

			tokens = get_tokens_for_user(ret_user)

			return Response({
				"tokens" : tokens,
				"username" : ret_user.username,
				"user_type" : profile.type
			}, status=STATUS_CODE_2xx.CREATED.value)
		
		except Exception :
			traceback.print_exc()
			return Response({"message" : "User could not be created, please try again later"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)


class LoginView(APIView):
	def put(self, request) -> Response:
		try :
			return self.login_user(request)

		except Exception :
			traceback.print_exc()
			return Response({"message" : "Incorrect authentication details supplied, please ensure that the correct password was entered"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)

	def username_exists(self, username) -> bool:
		try:
			User.objects.get(username=username)
			return True
		except User.DoesNotExist:
			return False

	def login_user(self, request):
		req = json.loads(request.body.decode('utf-8'))

		if not self.username_exists(req['username']):
			return Response(
				{"message": "User with supplied username does not exist, please register before logging in"},
				status=STATUS_CODE_4xx.UNAUTHORIZED.value)

		user = authenticate(username=req['username'], password=req['password'])
		if user:
			profile = Profile.objects.get(user=user)

			if user.is_superuser:
				# set super user type to admin when loggin in so dont have to worry about blank field checks
				profile.type = "Admin"
				profile.save()

			tokens = get_tokens_for_user(user)
			return Response({
				"tokens": tokens,
				"username": user.username,
				"user_type": profile.type
			}, status=STATUS_CODE_2xx.ACCEPTED.value)

		else:
			return Response({"message": "Incorrect authentication details supplied, please ensure that the correct password was entered"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)


# Secret key generation view
class SecretKeyView(APIView) :
	permission_classes = (IsAuthenticated, )

	def post(self, request) :
		try :
			user = User.objects.get(username=request.user.username)
			if user.is_superuser :
				key_text = get_secret_key_value()

				SecretKey.objects.create(key_text=key_text, user=user)
				key = SecretKey.objects.get(key_text=key_text)

				key.type = "Admin" # superusers only create admins who can then create teacher accounts
				key.user = user

				key.save()

				return Response({"secret_key": key.key_text, "school" : key.school, "areas" : key.areas, "key_type" : key.type}, status=STATUS_CODE_2xx.CREATED.value)

			profile = Profile.objects.get(user=user)
			if profile.type == "Team" :
				return Response({"message": "Team users cannot create secret keys"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			elif profile.type == "Judge" :
				return Response({"message": "Judge users cannot create secret keys"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			elif profile.type == "Teacher" :
				# create secret keys for students with teacher attachment
				teacher = Teacher.objects.get(user=user)
				
				if len(SecretKey.objects.filter(user=user)) == 0 :
					key_text = get_secret_key_value()
					SecretKey.objects.create(key_text=key_text, user=user)

				key = SecretKey.objects.filter(user=user)[0]

				key.type = "Team" # teachers only create teams
				key.user = user
				key.school = teacher.school
				key.areas = [teacher.area]

				key.save()

				return Response({"secret_key": key.key_text, "school" : key.school, "areas" : key.areas, "key_type" : key.type}, status=STATUS_CODE_2xx.CREATED.value)

			elif user.is_staff or profile.type == "Admin" :
				# create secret keys for teacher and judges with specified data attachments
				req = json.loads(request.body.decode('utf-8'))
				if not "key_type" in req or not "school" in req or not "areas" in req :
					return Response({"message" : "Error with creating key, not enough information supplied, please try again after making the necessary selections"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				key_type = req["key_type"]
				if key_type != "Teacher" and key_type != "Judge" :
					return Response({"message" : "Admins can create teacher or judge accounts, please choose one of these options to create for"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				if key_type == "Judge" and len(req["areas"]) == 0 :
					return Response({"message" : "Judge must be allocated a correct local authority to judge"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				for area in req["areas"] :
					if not area in LOCAL_AUTHORITIES.keys() :
						return Response({"message": area + " local authority does not exist. Please make sure to select the correct local authority and school for Teacher keys"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				schools_local_area = ""
				if key_type == "Teacher":
					is_school_in_local_area = False
					for area in req["areas"] :
						if req["school"] in LOCAL_AUTHORITIES[area] :
							schools_local_area = area
							is_school_in_local_area = True
							break

					if not is_school_in_local_area :
						return Response({"message" : req['school'] + " does not exist in " + req['areas'][0] + ". Please make sure to select the correct local authority and school"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				admin = Admin.objects.get(user=user)
				
				key_text = get_secret_key_value()

				SecretKey.objects.create(key_text=key_text, user=user)
				key = SecretKey.objects.get(key_text=key_text)

				# admins can create teachers or judges				
				key.type = key_type
				key.user = user
				key.school = "" if key_type == "Judge" else req["school"]
				key.areas = [schools_local_area] if key_type == "Teacher" else req["areas"]

				key.save()

				return Response({"secret_key": key.key_text, "school" : key.school, "areas" : key.areas, "key_type" : key.type}, status=STATUS_CODE_2xx.CREATED.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "An error occured while creating the secret key, please logging in again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

	def get(self, request) :
		try :
			results = []

			for key in SecretKey.objects.filter(user=request.user) :
				json_key = {
					"secret_key" : key.key_text,
					"school" : key.school,
					"areas" : key.areas,
					"key_type" : key.type
				}

				results.append(json_key)

			return Response({"keys" : results}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "An error occured while getting the secret keys, please logging in again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)


# Secret key specific operations
class SecretKeySpecificView(APIView) :
	def delete(self, request, secret_key) :
		try :
			key = SecretKey.objects.get(key_text=secret_key)

			if not request.user.is_superuser and request.user != key.user :
				return Response({"message": "You do not have enough priveleges to delete other user's keys"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			# if user is a super admin or the user is the same as who made the key, then delete the key
			key.delete()

			return Response({}, status=STATUS_CODE_2xx.NO_CONTENT.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Unable to find and delete specified key. Please try again later"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


# Issue submission and list view
class IssueControlView(APIView) :
	permission_classes = (IsAuthenticated, )
	
	def get(self, request) :
		try :
			user = User.objects.get(username=request.user.username)
			profile = Profile.objects.get(user=user)

			if profile.type == "Teacher" :
				# return all blacklisted submission for this teacher's teams
				teacher = Teacher.objects.get(user=user)
				submissions = Submission.objects.filter(team__teacher=teacher, is_blacklisted=True)

				team_logo_stubs = []
				team_names = []
				issue_descriptions = []
				for submission in submissions :
					team_logo_stubs.append(get_url_to_file_field(submission.logo))
					team_names.append(submission.team.user.username)
					issue_descriptions.append(submission.blacklisted_reason)

				return Response({"team_logo_stubs" : team_logo_stubs, "team_names" : team_names, "issue_descriptions" : issue_descriptions}, status=STATUS_CODE_2xx.SUCCESS.value)

			elif profile.type == "Admin" :
				# return all blacklisted submissions for the admin's teachers' teams
				admin = Admin.objects.get(user=user)
				submissions = Submission.objects.filter(team__teacher__administrator=admin, is_blacklisted=True)

				team_logo_stubs = []
				team_names = []
				issue_descriptions = []
				for submission in submissions :
					team_logo_stubs.append(get_url_to_file_field(submission.logo))
					team_names.append(submission.team.user.username)
					issue_descriptions.append(submission.blacklisted_reason)

				return Response({"team_logo_stubs" : team_logo_stubs, "team_names" : team_names, "issue_descriptions" : issue_descriptions}, status=STATUS_CODE_2xx.SUCCESS.value)
			
			return Response({"message" : "Cannot display issues for a non-Teacher or Admin user"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error occured while retrieving the issue(s), please try again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)
	

class IssueControlSpecificView(APIView) :
	permission_classes = (IsAuthenticated, )

	def post(self, request, team_username) :
		try :
			submission = Submission.objects.get(team__user__username=team_username)

			if submission.changed_since_blacklisting :
				req = json.loads(request.body.decode('utf-8'))

				submission.is_blacklisted = True
				submission.blacklisted_reason = req['description']
				submission.changed_since_blacklisting = False

				submission.save()

				return Response({"message" : "Thank you for submitting the issue, we have alerted their teacher to review this content"}, status=STATUS_CODE_2xx.ACCEPTED.value)

			else :
				return Response({"message": "This submission has been approved by an administrator. Unless this submisison is changed, it cannot be reported again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error occured while submitting the issue, please try again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

	def delete(self, request, team_username) :
		try :

			team = Team.objects.get(user__username=team_username)
			profile = Profile.objects.get(user=request.user)
			if profile.type == "Admin":
				submission = Submission.objects.get(team__user__username=team_username)

				submission.is_blacklisted = False
				submission.blacklisted_reason = ""
				submission.changed_since_blacklisting = False

				submission.save()
				return Response({"message": "Issue successfully revoked, the submission is now available on the marketplace again"}, status=STATUS_CODE_2xx.ACCEPTED.value)

			elif profile.type == "Teacher":
				teacher = Teacher.objects.get(user=request.user)
				if team.teacher == teacher:
					submission = Submission.objects.get(team__user__username=team_username)

					submission.is_blacklisted = False
					submission.blacklisted_reason = ""
					submission.changed_since_blacklisting = False

					submission.save()
					return Response({"message": "Issue successfully revoked, the submission is now available on the marketplace again"}, status=STATUS_CODE_2xx.ACCEPTED.value)
				else:
					return Response({"message": "A Teacher can only revoke issues of their own teams"}, status=STATUS_CODE_4xx.FORBIDDEN.value)
			else:
				return Response({"message": "Cannot revoke issues as a non-Teacher or Admin user"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error occured while removing the issue flag from the submission, please try again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


# Informational rest endpoints
class InfoLocalAuthorities(APIView) :
	permission_classes = (IsAuthenticated, )

	def get(self, request) :
		try:
			local_authorities = list(LOCAL_AUTHORITIES.keys())
			local_authorities.remove("Unknown")
			return Response({"local_authorities" : local_authorities })
		except Exception :
			traceback.print_exc()
			return Response({"message": "Error occured while retrieving list of local authorities. Please try again later"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class InfoLocalAuthoritiesSchools(APIView) :
	permission_classes = (IsAuthenticated, )

	def get(self, request, local_authority) :
		try:
			local_authorities_string = local_authority.replace('%20', ' ')

			local_authorities = local_authorities_string.split('+')

			result = []

			unknown_in_array = "Unknown" in local_authorities

			for local_authority in local_authorities :
				if not local_authority in LOCAL_AUTHORITIES.keys() :
					return Response({"message" : local_authority + " is not a valid local authority"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				if local_authority == "Unknown" :
					for key, value in LOCAL_AUTHORITIES.items() :
						for v in value :
							result.append({
								"value" : v,
								"local_authority" : key
							})

				elif not unknown_in_array :
					for v in LOCAL_AUTHORITIES[local_authority] :
						result.append({
							"value" : v,
							"local_authority": local_authority
						})

			return Response({"schools" : result}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception :
			return Response({"message": "Error occured while retrieving school from local authority. Please try again later"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


# User operations view
class UserOperationsView(APIView) :
	permission_classes = (IsAuthenticated, )

	def delete(self, request, username) :
		try:
			user = User.objects.get(username=request.user.username)
			
			if user.is_superuser :
				deleting_user = User.objects.get(username=username)
				deleting_user_profile = Profile.objects.get(user=deleting_user)
				if deleting_user_profile.type == "Team" :
					delete_team(username)
					return Response({}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				else :
					return Response({"message" : "Cannot delete users of type " + deleting_user_profile.type}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				# elif deleting_user_profile.type == "Teacher" :
				# 	delete_teacher(username)
				# 	return Response({"message": "Deleted teacher successfully"}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				# elif deleting_user_profile.type == "Judge" :
				# 	delete_judge(username)
				# 	return Response({"message": "Deleted judge successfully"}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				# elif deleting_user.is_staff or deleting_user_profile.type == "Admin" :
				# 	delete_admin(username)
				# 	return Response({"message" : "Deleted admin successfully"}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				# return Response({"message": "User was likely not generated successfully, and as such, cannot be deleted successfully"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			profile = Profile.objects.get(user=user)
# 			if profile.type == "Team" :
# 				if request.user.username != username:
# 					return Response({"message": "Cannot delete an acocunt that is not you"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)

# 				delete_team(username)

# 				return Response({"message": "Deleted team successfully"}, status=STATUS_CODE_2xx.NO_CONTENT.value)

# 			elif profile.type == "Judge" :
# 				if request.user.username != username:
# 					return Response({"message": "Cannot delete an acocunt that is not you"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)

# 				delete_judge(username)

# 				return Response({"message": "Deleted judge successfully"}, status=STATUS_CODE_2xx.NO_CONTENT.value)

			if profile.type == "Teacher" :
				deleting_user = User.objects.get(username=username)
				deleting_user_profile = Profile.objects.get(user=deleting_user)
				if deleting_user_profile.type != "Team" :
					return Response({"message": "Teachers can only delete team users"}, status=STATUS_CODE_4xx.FORBIDDEN.value)
				
				team = Team.objects.get(user=deleting_user)
				if team.teacher != Teacher.objects.get(user=user) :
					return Response({"message": "Teachers can only delete their own team users"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

				delete_team(username)
				return Response({}, status=STATUS_CODE_2xx.NO_CONTENT.value)
			
			

			elif user.is_staff or profile.type == "Admin" :
				deleting_user = User.objects.get(username=username)
				deleting_user_profile = Profile.objects.get(user=deleting_user)
				if deleting_user_profile.type == "Team" :
					delete_team(username)
					return Response({}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				elif deleting_user_profile.type == "Teacher" :
					delete_teacher(username)
					return Response({}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				elif deleting_user_profile.type == "Judge" :
					delete_judge(username)
					return Response({}, status=STATUS_CODE_2xx.NO_CONTENT.value)

				else :
					return Response({"message": "Admins can only delete team, teacher, or judge accounts"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			return Response({"message" : "Users of type " + profile.type + " cannot perform delete operations on themselves or other users"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "An error occured during account deletion, no accounts were deleted. Please make sure that the user exists so it can be deleted"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)


# User milestones view
class UserMilestonesView(APIView):
	permission_classes = (IsAuthenticated, )

	def get(self, request):
		try:
			user = User.objects.get(username=request.user.username)
			profile = Profile.objects.get(user=user)

			if profile.type == "Team" :
				retrieving_user = User.objects.get(username=request.user.username)
				milestones = get_milestones_for_user(retrieving_user)

				send_milestones = []
				total = 0
				for m in milestones :
					send_milestones.append(m)

					# do not add current milestone count as should always be 1 
					total += 1 if m['completed'] else 0

				return Response({"milestones" : send_milestones, "percentage": (100 * total) / (len(send_milestones) - 1)}, status=STATUS_CODE_2xx.SUCCESS.value)

			elif profile.type == "Teacher" :
				teacher = Teacher.objects.get(user=request.user)

				all_milestones, team_names, team_logo_stubs = get_team_milestones_for_teacher(teacher)

				return Response({"team_milestones": all_milestones, "team_names": team_names, "team_logo_stubs" : team_logo_stubs}, status=STATUS_CODE_2xx.SUCCESS.value)

			elif profile.type == "Admin" :
				if not request.GET.__contains__('teacher') :
					return Response({"message" : "Please provide a teacher to recieve team milestones for"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				teacher_username = request.GET.get('teacher')

				if len(User.objects.filter(username=teacher_username)) == 0:
					return Response({"message" : "Teacher must exist for team's milestones to be returned"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				if Profile.objects.get(user__username=teacher_username).type != "Teacher" :
					return Response({"message" : "Specified user must be a teacher to return milestones for their teams"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				teacher = Teacher.objects.get(user__username=teacher_username)

				if teacher.administrator != Admin.objects.get(user=request.user) :
					return Response({"message" : "Cannot retrieve team's milestones for a teacher that you are not the administrator for"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

				all_milestones, team_names, team_logo_stubs = get_team_milestones_for_teacher(teacher)

				return Response({"team_milestones": all_milestones, "team_names": team_names, "team_logo_stubs" : team_logo_stubs}, status=STATUS_CODE_2xx.SUCCESS.value)

			else :
				return Response({"message" : "Cannot retrieve milestones for type " + profile.type}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)


class TeachersView(APIView) :
	permission_classes = (IsAuthenticated, )

	def get(self, request) :
		try :
			if Profile.objects.get(user=request.user).type != "Admin" :
				return Response({"message" : "Cannot return view of teachers as you are not an administrator for any"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			teachers = []

			if request.user.is_superuser :
				teachers = Teacher.objects.all()
			else :
				teachers = Teacher.objects.filter(administrator=Admin.objects.get(user=request.user))

			teacher_areas = []
			teacher_schools = []
			teacher_names = []

			for teacher in teachers :
				teacher_areas.append(teacher.area)
				teacher_schools.append(teacher.school)
				teacher_names.append(teacher.user.username)

			return Response({"teacher_areas" : teacher_areas, "teacher_schools" : teacher_schools, "teacher_names" : teacher_names}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error encountered while retrieving teachers, please try logging out and logging in again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

class TeamMarketplaceSubmissionView(APIView) :
	permission_classes = (IsAuthenticated, )
	
	def put(self, request, username) :
		try:
			if request.user.username != username:
				return Response({"message": "Incorrect authentication, please try logging in"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)

			user = User.objects.get(username=request.user.username)
			profile = Profile.objects.get(user=user)
			if profile.type != "Team" and profile.type != "Admin" :
				return Response({"message": "Please log in as a team to be able to access team marketplace submission capabilities"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)
			
			if get_submission_deadline() < datetime.datetime.now().date() :
				return Response({"message" : "Cannot make changes to your submission after the submission deadline has passed"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			team = Team.objects.get(user=user)

			req = json.loads(request.body.decode('utf-8'))

			company_logo = req["encoded_logo"]
			company_logo_extension = req["encoded_logo_extension"]
			company_logo_mime_type = req["encoded_logo_mime_type"]
			company_poster = req["encoded_poster"]
			company_poster_extension = req["encoded_poster_extension"]
			company_poster_mime_type = req["encoded_poster_mime_type"]
			company_target_customer = req["encoded_target_customer"]
			company_target_customer_extension = req["encoded_target_customer_extension"]
			company_target_customer_mime_type = req["encoded_target_customer_mime_type"]
			company_name = req["company_name"]
			tagline = req["tagline"]
			manifest = req['manifest'] # what fields are to be set

			if 'logo' in manifest and not company_logo_extension in ACCEPTABLE_PICTURE_EXTENSIONS :
				return Response({"message" : "Unnacceptable file type supplied for logo"}, status=STATUS_CODE_4xx.FORBIDDEN.value)
			if 'poster' in manifest and not company_poster_extension in ACCEPTABLE_DOCUMENT_EXTENSIONS :
				return Response({"message" : "Unnacceptable file type supplied for poster"}, status=STATUS_CODE_4xx.FORBIDDEN.value)
			if 'target_customer' in manifest and not company_target_customer_extension in ACCEPTABLE_DOCUMENT_EXTENSIONS :
				return Response({"message" : "Unnacceptable file type supplied for target customer"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			submission, _ = Submission.objects.get_or_create(team=team)

			if submission.is_blacklisted :
				# prevent changes to under reviewal content
				return Response({"message" : "Cannot save marketplace submission as stall is currently under review by your teacher"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			if 'company_name' in manifest :
				submission.company_name = company_name

			if 'tagline' in manifest :
				submission.tagline = tagline

			if 'logo' in manifest :
				logo_removed_header = company_logo.partition(",")[2]
				logo = base64.b64decode(logo_removed_header)

				upload_file(submission, 'logo', company_logo_extension, company_logo_mime_type, logo)
			
			if 'poster' in manifest :
				poster_removed_header = company_poster.partition(",")[2]
				poster = base64.b64decode(poster_removed_header)
				
				upload_file(submission, 'poster', company_poster_extension, company_poster_mime_type, poster)

			if 'target_customer' in manifest :
				target_customer_removed_header = company_target_customer.partition(",")[2]
				target_customer = base64.b64decode(target_customer_removed_header)

				upload_file(submission, 'target_customer', company_target_customer_extension, company_target_customer_mime_type, target_customer)

			submission.changed_since_blacklisting = True
			submission.save()

			return Response({"company_name" : submission.company_name, "tagline": submission.tagline, 
					"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
					"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
					"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
					"team_username" : submission.team.user.username}, status=STATUS_CODE_2xx.ACCEPTED.value) 

		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

	def get(self, request, username) :
		try:
			try :
				user = User.objects.get(username=username)
				team = Team.objects.get(user=user)
				submission = Submission.objects.get(team=team)

				if request.user != user :
					if Profile.objects.get(user=request.user).type == "Team" and (get_voting_deadline() < datetime.datetime.now().date() or get_submission_deadline() > datetime.datetime.now().date()) :
						return Response({"message" : "Cannot view other team's marketplace stalls until the marketplace opens, or after the marketplace closes"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

					elif Profile.objects.get(user=request.user).type == "Judge" and (get_judging_deadline() < datetime.datetime.now().date() or get_submission_deadline() > datetime.datetime.now().date()) :
						return Response({"message" : "Cannot view team's marketplace stalls until judge voting is opened, or after judge voting closes"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

					elif Profile.objects.get(user=request.user).type == "Teacher" and team.teacher != Teacher.objects.get(user=request.user) and (get_voting_deadline() < datetime.datetime.now().date() or get_submission_deadline() > datetime.datetime.now().date()) :
						return Response({"message" : "Cannot view another teacher's student's stalls until the marketplace opens, or after the marketplace has closed"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

				if submission.is_blacklisted and username != request.user.username and Profile.objects.get(user=request.user).type == "Team" :
					return Response({"message" : "Cannot access the requested marketplace entry as it is under reviewal for unacceptable content"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

				return Response({"company_name" : submission.company_name, "tagline": submission.tagline,
						"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
						"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
						"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
						"team_username" : submission.team.user.username}, status=STATUS_CODE_2xx.SUCCESS.value)

			except Exception :
				return Response({"message" : "No marketplace stall has been submitted yet"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

	def delete(self, request, username) :
		try :
			if Profile.objects.get(user=request.user).type != "Teacher" and Profile.objects.get(user=request.user).type != "Admin" :
				return Response({"message" : "Non-teacher or admin users cannot delete marketplace profiles"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			submission = Submission.objects.get(team__user__username=username)

			if Profile.objects.get(user=request.user).type != "Admin" and submission.team.teacher != Teacher.objects.get(user=request.user) :
				return Response({"message": "Teacher can only delete their own students submissions"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			submission.delete()

			return Response({"message" : "Successfully deleted the marketplace stall"}, status=STATUS_CODE_2xx.ACCEPTED.value)

		except Exception as e:
			traceback.print_exc()
			return Response({"message": "Error encountered while deleting the marketplace submission, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class IsUsersAdministratorCheckView(APIView) :
	permission_classes = (IsAuthenticated,)

	def get(self, request, username) :
		try :
			team = Team.objects.get(user__username=username)
			profile = Profile.objects.get(user=request.user)
			if profile.type == "Admin" :
				return Response({"is_users_admin" : True}, status=STATUS_CODE_2xx.SUCCESS.value)

			elif profile.type == "Teacher" :
				teacher = Teacher.objects.get(user=request.user)
				if team.teacher == teacher:
					return Response({"is_users_admin" : True}, status=STATUS_CODE_2xx.SUCCESS.value)

			return Response({"is_users_admin": False}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered while checking if you are the specified user's teacher, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class JudgeSubmissionView(APIView):
	permission_classes = (IsAuthenticated,)

	@staticmethod
	def compose_response_fields(judge_submission):
		response_dict = {"manifest": []}
		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score"]:
			value = getattr(judge_submission, field)
			response_dict[field] = value if value else 0
		return response_dict

	def get_judge_submission(self, judge, submission):
		judge_submission = JudgesSubmission.objects.get(submission=submission, judge=judge)
		response = self.compose_response_fields(judge_submission)
		return response

	def update_judge_submission(self, judge, submission, changes):
		judge_submission = JudgesSubmission.objects.get_or_create(submission=submission, judge=judge)[0]
		for field in changes["manifest"]:
			setattr(judge_submission, field+'_score', changes[field+'_score'])
		# runs the model validators and raises ValidationError if any of the fields is not in the correct range
		judge_submission.full_clean()
		judge_submission.save()

		response = self.compose_response_fields(judge_submission)
		return response

	@staticmethod
	def is_judge(user):
		try:
			profile = Profile.objects.get(user=user)
			if profile.type == "Judge":
				return True
		except Exception:
			traceback.print_exc()
		return False

	def put(self, request, username):
		if not self.is_judge(request.user):
			return Response({"message": "Only Judges can assign scores to submission fields"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

		try:
			judge = Judge.objects.get(user=request.user)
			submission = Submission.objects.get(team__user__username=username)
			data = json.loads(request.body.decode('utf-8'))

			if get_submission_deadline() > datetime.datetime.now().date() or get_judging_deadline() < datetime.datetime.now().date() :
				return Response({"message" : "Cannot update or make judge submissions before the marketplace has opened, or after the judging deadline has passed"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			return Response(self.update_judge_submission(judge, submission, data), status=STATUS_CODE_2xx.ACCEPTED.value)
		except ValidationError:
			return Response({"message": "The submitted scores are not in the correct range (0-25)"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)
		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

	def get(self, request, username):
		if not self.is_judge(request.user):
			return Response({"message": "Only Judges can assign scores to submission fields"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

		try:
			judge = Judge.objects.get(user=request.user)
			submission = Submission.objects.get(team__user__username=username)

			return Response(self.get_judge_submission(judge, submission),  status=STATUS_CODE_2xx.SUCCESS.value)
		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class DownloadCSVDetailView(APIView):
	authentication_classes = [JWTUrlAuthentication]
	permission_classes = (IsAuthenticated,)

	def get(self, request):
		# only allow Admins to download the CSVs
		if not is_profile_type(request.user, "Admin"):
			return Response({"message": "Only Admins can export database data"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

		response = HttpResponse(content_type="text/csv")
		response["Content-Disposition"] = "attachment;filename=yescotland_business_challenge_judge_score_detail.csv"
		writer = csv.writer(response)

		# write header
		writer.writerow([
			"team_username",
			"company_name",
			"school",
			"judge_username",
			"poster_score",
			"logo_score",
			"target_customer_score",
			"tagline_score",
		])

		# the csv will have one row per team
		all_teams = Team.objects.all().select_related()
		for team in all_teams:
			team_submission = Submission.objects.get(team=team)
			judge_submissions = JudgesSubmission.objects.filter(submission=team_submission).select_related()
			if len(judge_submissions) != 0:
				for judge_submission in judge_submissions:
					judge_user = judge_submission.judge.user

					writer.writerow([
						team.user.username,
						team_submission.company_name,
						team.teacher.school,
						judge_user.username,
						judge_submission.poster_score,
						judge_submission.logo_score,
						judge_submission.target_customer_score,
						judge_submission.tagline_score
					])
			else:
				# if there is a team not judged by any judge
				writer.writerow([
					team.user.username,
					team_submission.company_name,
					team.teacher.school,
					None,
					None,
					None,
					None,
					None
				])

		return response


class DownloadCSVSummaryView(APIView):
	authentication_classes = [JWTUrlAuthentication]
	permission_classes = (IsAuthenticated,)

	def get(self, request) :
		if not is_profile_type(request.user, "Admin"):
			return Response({"message": "Only Admins can export database data"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

		response = HttpResponse(content_type="text/csv")
		response["Content-Disposition"] = "attachment;filename=yescotland_business_challenge_score_summary.csv"
		writer = csv.writer(response)

		# write header
		writer.writerow([
			"team_username",
			"company_name",
			"school",
			"peer_votes",
			"avg_judge_score",
			"number_of_judge_ratings",
			"finance_score",
			"hr_score",
			"environmental_score",
			"marketing_score",
			"customers_score",
			"number_of_business_decisions_made"
		])

		# the csv will have one row per team
		all_teams = Team.objects.all().select_related()
		for team in all_teams:
			team_submission = Submission.objects.get(team=team)

			team_judge_submissions = JudgesSubmission.objects.filter(submission=team_submission)
			avg_judge_submission_scores = team_judge_submissions.aggregate(
				Avg('poster_score'), Avg('logo_score'), Avg('target_customer_score'), Avg('tagline_score')).values()
			avg_judge_submission_scores = [val for val in avg_judge_submission_scores if val is not None]
			if len(avg_judge_submission_scores) != 0:
				avg_judge_score = sum(avg_judge_submission_scores) / len(avg_judge_submission_scores)
			else:
				avg_judge_score = None

			team_answers = [Answer.objects.get(question__question_id=int(key), answer_no=val) for key, val in team.question_answer_dict.items()]
			dragon_scores = {"finance_score": 0, "hr_score": 0, "environmental_score": 0, "marketing_score": 0, "customers_score": 0}
			for answer in team_answers:
				dragon_scores = {category: dragon_scores[category] + answer.answer_dragon_scores.get(category, 0) for category in dragon_scores}

			completed_submission = 0
			for judge_submission in team_judge_submissions :
				if judge_submission.poster_score != None and judge_submission.tagline_score != None and judge_submission.target_customer_score != None and judge_submission.logo_score != None :
					completed_submission += 1

			# calculate how many likes this team's submission has got
			peer_votes = SubmissionLike.objects.filter(submission=team_submission).count()

			writer.writerow([
				team.user.username,
				team_submission.company_name,
				team.teacher.school,
				peer_votes,
				avg_judge_score,
				completed_submission,
				dragon_scores["finance_score"],
				dragon_scores["hr_score"],
				dragon_scores["environmental_score"],
				dragon_scores["marketing_score"],
				dragon_scores["customers_score"],
				len(team_answers),
			])

		return response


class ChangePasswordView(APIView) :
	permission_classes = (IsAuthenticated,)

	def put(self, request, username) :
		try :
			req = json.loads(request.body.decode('utf-8'))

			if 'new_password' not in req :
				# Just a general reset, must return new password in message in reponse
				new_password = get_secret_key_value()

				if request.user.username != username :
					changer_profile = Profile.objects.get(user=request.user)

					if changer_profile.type == "Team" :
						return Response({"message" : "Team users cannot reset the passwords of other users"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

					elif changer_profile.type == "Judge" :
						return Response({"message" : "Judge users cannot reset the passwords of other users"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

					elif changer_profile.type == "Teacher" :
						changee_profile = Profile.objects.get(user__username=username)

						if changee_profile.type != "Team" :
							return Response({"message" : "Teacher cannot change non-team users' passwords"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

						if Team.objects.get(user__username=username).teacher != Teacher.objects.get(user=request.user) :
							return Response({"message" : "Cannot reset the passwords of Team users that are not your students"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

					elif changer_profile.type == "Admin" :
						changee_profile = Profile.objects.get(user__username=username)

						if changee_profile.type == "Admin" and not changer_profile.user.is_superuser :
							return Response({"message" : "Admins cannot reset other admin's passwords. Operation can only be done by the superuser"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

				req['username'] = username
				req['new_password'] = new_password
				serializer = ChangePasswordSerializer(data=req)

				# Needs to go through a serializer even though the password will always be secure
				if not serializer.is_valid() : pass

				serializer.save()

				return Response({"message" : "Successfully reset password for " + username + " to " + new_password}, status=STATUS_CODE_2xx.ACCEPTED.value)

			else :
				# Change password to a specific value, don't have to return any message in response
				if request.user.username != username :
					return Response({"message" : "Cannot change password for another user"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

				req['username'] = username
				# new_password field is set in the request
				serializer = ChangePasswordSerializer(data=req)

				if not serializer.is_valid() :
					errors = []

					if "username" in serializer.errors :
						errors += list(serializer.errors["username"])
					if "non_field_errors" in serializer.errors :
						# if errors in password, index is strange due to django
						errors = list(serializer.errors["non_field_errors"])

					message = ", and ".join([str(err).lower()[:-1] if i != 0 else str(err)[:-1] for i, err in enumerate(errors)])
					return Response({"message": message}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				serializer.save()

				return Response({}, status=STATUS_CODE_2xx.ACCEPTED.value)

		except Exception :
			traceback.print_exc()
			return Response({"message" : "Issue encountered while changing or resetting password. Password has not been updated. Please login and try again."}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class DeadlineControlView(APIView) :
	permission_classes = (IsAuthenticated,)

	DATE_FORMAT_STRING = '%Y-%m-%d'

	def create_deadline_json(self, deadline) :
		submission = deadline.submission_deadline.strftime(self.DATE_FORMAT_STRING)
		voting = deadline.voting_deadline.strftime(self.DATE_FORMAT_STRING)
		judging = deadline.judging_deadline.strftime(self.DATE_FORMAT_STRING)

		return {"submission_deadline": submission, "voting_deadline" : voting, "judging_deadline" : judging}

	def read_and_correct_string(self, string) :
		date = datetime.datetime.strptime(string, self.DATE_FORMAT_STRING) # read using agreed upon format
		date = date + datetime.timedelta(days=1) - datetime.timedelta(milliseconds=1) # set to one millisecond before midnight to allow for as many submission as possible
		return date

	def get(self, request) :
		try :
			deadlines = Deadline.objects.all()

			if len(deadlines) == 0 :
				return Response({"message" : "No deadlines have been set yet. Please contact your YES representative about this issue"}, status=STATUS_CODE_4xx.NOT_FOUND.value)

			deadline = deadlines[0]

			return Response(self.create_deadline_json(deadline), status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception :
			traceback.print_exc()
			return Response({"message" : "Failed to retrieve deadlines. Please try again later or contact the website administrator"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

	def put(self, request) :
		try :
			req = json.loads(request.body.decode('utf-8'))

			if Profile.objects.get(user=request.user).type != "Admin" :
				return Response({"message" : "Non-Admin users cannot create or update deadlines"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)

			if 'submission_deadline' not in req or 'voting_deadline' not in req or 'judging_deadline' not in req :
				return Response({"message" : "Deadline data is incomplete. Cannot correctly set deadlines"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

			deadlines = Deadline.objects.all()

			# read in time values
			submission = self.read_and_correct_string(req['submission_deadline'])
			voting = self.read_and_correct_string(req['voting_deadline'])
			judging = self.read_and_correct_string(req['judging_deadline'])

			if submission > voting or submission > judging :
				return Response({"message": "Cannot set dates as supplied: submission deadline must be before the voting and judging deadlines"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

			if len(deadlines) == 0 :
				# if not deadline has been created yet, create one with the correct values
				deadline = Deadline.objects.create(
					submission_deadline=submission,
					voting_deadline=voting,
					judging_deadline=judging
				)
			else :
				# if a deadline has been created, load it, set all the values, and save it
				deadline = deadlines[0]

				deadline.submission_deadline = submission
				deadline.voting_deadline = voting
				deadline.judging_deadline = judging

				deadline.save()

			return Response(self.create_deadline_json(deadline), status=STATUS_CODE_2xx.ACCEPTED.value)

		except Exception :
			traceback.print_exc()
			return Response({"message" : "Failed to set deadlines. Please try again later or contact the website administrator"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class MarketplaceLikeView(APIView) :
	permission_classes = (IsAuthenticated,)

	def get(self, request, username) :
		likes = SubmissionLike.objects.filter(team__user=request.user, submission__team__user__username=username)
		return Response({"liked" : len(likes) > 0}, status=STATUS_CODE_2xx.SUCCESS.value)

	def post(self, request, username) :
		try :
			canLike, message = canLikeMarketplaceStall(request, username)

			if not canLike :
				return Response({"message" : message}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

			_ = SubmissionLike.objects.create(team=Team.objects.get(user=request.user),
						submission=Submission.objects.get(team__user__username=username))

			return Response({"liked" : True}, status=STATUS_CODE_2xx.ACCEPTED.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error encountered while giving team a like, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

	def delete(self, request, username) :
		like = SubmissionLike.objects.filter(team__user=request.user, submission__team__user__username=username)

		if len(like) == 0 :
			return Response({"message" : "Cannot unlike a submisison that you have not liked"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

		like = SubmissionLike.objects.get(team__user=request.user, submission__team__user__username=username)
		like.delete()

		return Response({"liked" : False}, status=STATUS_CODE_2xx.ACCEPTED.value)


class CanLikeMarketplaceStall(APIView) :
	permission_classes = (IsAuthenticated,)

	def get(self, request, username) :
		canLike, _ = canLikeMarketplaceStall(request, username)

		return Response({"can_like" : canLike}, status=STATUS_CODE_2xx.SUCCESS.value)


class NumberOfNotificationsView(APIView) :
	permission_classes = (IsAuthenticated,)

	def issue_object_handler(self, request) :
		user_type = Profile.objects.get(user=request.user).type

		if user_type == "Admin" :
			blacklisted_submissions = Submission.objects.filter(team__teacher__administrator__user=request.user, is_blacklisted=True)
			return len(blacklisted_submissions)

		elif user_type == "Teacher" :
			blacklisted_submissions = Submission.objects.filter(team__teacher__user=request.user, is_blacklisted=True)
			return len(blacklisted_submissions)

		else :
			return -1 # error code

	def default_object_handle(self, request) :
		return -1

	def get(self, request) :
		try:
			if not request.GET.__contains__('object') :
				return Response({"message" : "Cannot return the number of something is the 'object' is not specified"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

			parameters = request.GET.copy()

			object_types = parameters.pop('object')

			object_type_options = {
				"Issues" : self.issue_object_handler
			}

			numbers_of_notifications = {}

			for object_type in object_types :
				number_of_notifications = object_type_options.get(object_type, self.default_object_handle)(request)

				if number_of_notifications == -1 :
					return Response({"message" : "'object' field is of invalid type for this user. Please specifiy a correct object field"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)

				numbers_of_notifications['number_of_' + object_type.lower()] = number_of_notifications

			return Response(numbers_of_notifications, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error encountered while trying to retrieve the number of notifications for the specified object field. Please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class JudgesProgressView(APIView) :
	permission_classes = (IsAuthenticated,)

	def get(self, request) :
		try :
			if Profile.objects.get(user=request.user).type != "Admin" :
				return Response({"message" : "Cannot return information about judge progress when logged in as a non-admin user"}, status=STATUS_CODE_4xx.FORBIDDEN.value)

			admins_judges = []

			if request.user.is_superuser :
				admins_judges = Judge.objects.all()
			else :
				admins_judges = Judge.objects.filter(administrator=Admin.objects.get(user=request.user))

			judge_names = []
			judge_marked = []
			judge_total = []
			judge_progress = []

			for judge in admins_judges :
				judge_names.append(judge.user.username)

				number_marked = 0
				judge_submissions = JudgesSubmission.objects.filter(judge=judge, submission__is_blacklisted=False)
				for judge_submission in judge_submissions :
					if judge_submission.poster_score != None and judge_submission.tagline_score != None and judge_submission.target_customer_score != None and judge_submission.logo_score != None :
						number_marked += 1

				judge_marked.append(number_marked)

				total_submissions = 0
				for area in judge.areas :
					total_submissions += len(Submission.objects.filter(team__teacher__area=area, is_blacklisted=False))

				progress = 0
				if total_submissions == 0 :
					progress = 100
				else :
					progress = (number_marked / total_submissions) * 100

				judge_total.append(total_submissions)
				judge_progress.append(progress)

			return Response({"judge_names" : judge_names, "judge_marked": judge_marked, "judge_progress" : judge_progress, "judge_total" : judge_total}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception :
			traceback.print_exc()
			return Response({"message": "Error encountered while trying to retrieve information about judge progress. Please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class MarketplaceView(APIView) :
	permission_classes = (IsAuthenticated, )

	def get(self, request) :
		try:
			
			user = User.objects.get(username=request.user.username)
			profile = Profile.objects.get(user=user)

			if profile.type == "Team" and datetime.datetime.now().date() <=  get_voting_deadline() and datetime.datetime.now().date() > get_submission_deadline():
				results = []
				submissions = Submission.objects.all().order_by('?')

				for submission in submissions :
					if submission.company_name == "" or submission.tagline == "" or submission.logo == "" or submission.poster == "" or submission.target_customer == ""  or submission.is_blacklisted == True :
						continue

				# don't want to display the team's stall with the other stalls
					if submission.team.user == request.user :
						continue
					results.append(
						{
							"company_name" : submission.company_name, 
							"tagline": submission.tagline, 
							"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
							"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
							"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
							"team_username" : submission.team.user.username
						}
					)
			elif profile.type == "Teacher" and datetime.datetime.now().date() <= get_voting_deadline() and datetime.datetime.now().date() > get_submission_deadline():
				results = []
				teacher = Teacher.objects.get(user = user)
				school = teacher.school
				teachers = Teacher.objects.filter(school=school)
				for teacher in teachers:
					teams = Team.objects.filter(teacher = teacher)
					for team in teams:
						submission = Submission.objects.get(team = team)
						if submission.company_name == "" or submission.tagline == "" or submission.logo == "" or submission.poster == "" or submission.target_customer == ""  or submission.is_blacklisted == True:
							continue
						results.append(
							{
							"company_name" : submission.company_name, 
							"tagline": submission.tagline, 
							"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
							"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
							"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
							"team_username" : submission.team.user.username
							}
						)
			elif profile.type == "Admin":
				results = []
				submissions = Submission.objects.all().order_by('?')
				for submission in submissions :
					if submission.company_name == "" or submission.tagline == "" or submission.logo == "" or submission.poster == "" or submission.target_customer == ""  or submission.is_blacklisted == True:
						continue

					results.append(
						{
							"company_name" : submission.company_name, 
							"tagline": submission.tagline, 
							"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
							"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
							"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
							"team_username" : submission.team.user.username
						}
					)
			elif profile.type == "Judge" and datetime.datetime.now().date() <= get_judging_deadline() and datetime.datetime.now().date() > get_submission_deadline():
				results = []
				judge = Judge.objects.get(user = user)
				areas = judge.areas
				for area in areas:
					teachers = Teacher.objects.filter(area=area)
					for teacher in teachers:
						teams = Team.objects.filter(teacher = teacher)
						for team in teams:
							submission = Submission.objects.get_or_create(team = team)[0]
							judgesubmission = JudgesSubmission.objects.get_or_create(judge = judge,
							submission = submission)[0]
							if submission.company_name == "" or submission.tagline == "" or submission.logo == "" or submission.poster == "" or submission.target_customer == ""  or submission.is_blacklisted == True:
								continue
							
							if judgesubmission.poster_score != None and judgesubmission.logo_score != None and judgesubmission.target_customer_score != None and judgesubmission.tagline_score != None:
								continue
							results.append(
								{
								"company_name" : submission.company_name, 
								"tagline": submission.tagline, 
								"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
								"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
								"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
								"team_username" : submission.team.user.username
								}
							)
			elif  (get_voting_deadline() <= datetime.datetime.now().date() or datetime.datetime.now().date() < get_submission_deadline()) and (profile.type == "Team" or profile.type == "Teacher"):
				return Response({"message" : "Cannot view marketplace before submission deadline or after voting deadline has passed"}, status=STATUS_CODE_4xx.FORBIDDEN.value)
			elif  (get_judging_deadline() <= datetime.datetime.now().date() or datetime.datetime.now().date() < get_submission_deadline()) and profile.type == "Judge":
				return Response({"message" : "Cannot view marketplace before submission deadline or after judging deadline has passed"}, status=STATUS_CODE_4xx.FORBIDDEN.value)


			return Response({"stalls" : results}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class MarkedMarketplaceView(APIView) :
	permission_classes = (IsAuthenticated, )

	def get(self, request) :
		try:
			user = User.objects.get(username=request.user.username)
			profile = Profile.objects.get(user=user)

			if profile.type == "Judge" and datetime.datetime.now().date() <= get_judging_deadline() and datetime.datetime.now().date() > get_submission_deadline():
				results = []
				judge = Judge.objects.get(user = user)
				areas = judge.areas
				for area in areas:
					teachers = Teacher.objects.filter(area=area)
					for teacher in teachers:
						teams = Team.objects.filter(teacher = teacher)
						for team in teams:
							submission = Submission.objects.get_or_create(team = team)[0]
							judgesubmission = JudgesSubmission.objects.get_or_create(judge = judge,
							submission = submission)[0]
							if submission.company_name == "" or submission.tagline == "" or submission.logo == "" or submission.poster == "" or submission.target_customer == "" or submission.is_blacklisted == True :
								continue
							if judgesubmission.poster_score == None or judgesubmission.logo_score == None or judgesubmission.target_customer_score == None or judgesubmission.tagline_score == None:
								continue
							results.append(
								{
								"company_name" : submission.company_name, 
								"tagline": submission.tagline, 
								"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
								"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
								"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
								"team_username" : submission.team.user.username
								}
							)
			elif  (get_judging_deadline() <= datetime.datetime.now().date() or datetime.datetime.now().date() < get_submission_deadline()) and profile.type == "Judge":
				return Response({"message" : "Cannot view marketplace before submission deadline or after judging deadline has passed"}, status=STATUS_CODE_4xx.FORBIDDEN.value)
			
			return Response({"stalls" : results}, status=STATUS_CODE_2xx.SUCCESS.value)

		except Exception:
			traceback.print_exc()
			return Response({"message": "Error encountered, please try logging out and logging in again"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


class BusinessDecisionView(APIView):
    permission_classes = (IsAuthenticated, )

    def get(self, request):
        try:
            if Deadline.objects.all():
                deadline = Deadline.objects.all()[0].submission_deadline
                if datetime.date.today() > deadline:
                    return Response({"message": "Submission deadline has passed"}, status=STATUS_CODE_4xx.NOT_FOUND.value)
            user = User.objects.get(username=request.user.username)
            profile = Profile.objects.get(user=user)
            if profile.type != "Team":
                return Response({"message": "Only teams have access to business decisions"}, status=STATUS_CODE_4xx.UNAUTHORIZED.value)
            team = Team.objects.get(user=user)
            questions = Question.objects.all()
            if not questions:
                return Response({"message": "No business decision questions were found"}, status=STATUS_CODE_4xx.NOT_FOUND.value)
            pairs = team.question_answer_dict
            reply = []
            for question in questions:
                answers = Answer.objects.filter(question=question)
                new_answers = [answer.answer_text for answer in answers]
                new_question = {}
                new_question["questionId"] = question.question_id
                new_question["questionText"] = question.question_text
                new_question["answerOptions"] = new_answers
                if str(question.question_id) in pairs:
                    new_question["selection"] = pairs[str(question.question_id)]
                if Animation.objects.filter(question=question).exists():
                    animation = Animation.objects.get(question=question)
                    new_question["animationLink"] = get_url_to_animation(animation)
                    if animation.yt_url: new_question["yt_url"] = animation.yt_url
                reply.append(new_question)

            return Response(reply, status=200)
        except Exception:
            traceback.print_exc()
            return Response({"message": "Something went wrong while retrieving business decisions"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

    def post(self, request):
        try:
            user = User.objects.get(username=request.user.username)
            profile = Profile.objects.get(user=user)
            if profile.type != "Team":
                return Response({"message": "Only teams have access to business decisions"}, status=401)
            team = Team.objects.get(user=user)
            req = json.loads(request.body.decode('utf-8'))
            pairs = team.question_answer_dict
            pairs[str(req["questionId"])] = req["selection"]
            team.question_answer_dict = pairs
            team.save()
            return Response(status=200)
        except Exception:
            return Response({"Something went wrong while updating business decision selection"}, status=500)


class IntroductionView(APIView):
	permission_classes = (IsAuthenticated, )

	def get(self, request):
		user = User.objects.get(username=request.user.username)
		profile = Profile.objects.get(user=user)

		intro = Intro.objects.all()
		if intro.exists():
			result = {
				"introText": intro[0].intro_text,
				"introYtUrl": intro[0].yt_url
			}
			if intro[0].intro_video:
				result["introVideoUrl"] = get_url_to_intro_video(intro[0])
			return Response(result, status=STATUS_CODE_2xx.SUCCESS.value)
		return Response(status=STATUS_CODE_4xx.NOT_FOUND.value)


# Frontend View
class FrontendView(APIView) :
	def get(self, request, *args, **kwargs):
		# have to add args and kwargs as otherwise paramterised paths will not render correctly
		return render(request, 'index.html')


class RetrieveFrontendFileView(APIView) :
	def get(self, request) :
		try :
			file_location = str(settings.STATIC_ROOT) + request.path

			return FileResponse(open(file_location, "rb"))

		except Exception :
			traceback.print_exc()
			return Response({"message" : "Cannot successfully retrieve requested frontend file"}, status=STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)


@api_view()
def handler404View(request, exception=None) :
	# Redirect for bad static file getting, namely for PWAs
	if request.path.startswith('/static') :
		return redirect('frontend-home')
	
	# Otherwise, return a non-recursive causing sanitised response 
	else :
		return Response({"message" : "Bad URL used to access content. Please notify the website administrator"}, status=STATUS_CODE_4xx.BAD_REQUEST.value)


def is_profile_type(user, profile_type):
	profile = Profile.objects.get(user=user)
	return profile.type == profile_type
