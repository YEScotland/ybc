#
#  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import enum
import random
import string
import boto3

from django.core.files.base import ContentFile

from django.urls import reverse
from rest_framework_simplejwt.tokens import RefreshToken

from .models import SecretKey, Submission, UploadedFile, User, Profile, Team, Deadline, SubmissionLike, Judge, JudgesSubmission, Teacher, Question
from .settings import S3_BUCKET

MAXIMUM_NUMBER_OF_LIKES = 10

# list taken from https://developer.mozilla.org/en-US/docs/Web/Media/Formats/Image_types
ACCEPTABLE_PICTURE_EXTENSIONS = [
	"apng",
	"avif",
	"gif",
	"jpg", "jpeg", "jfif", "pjpeg", "pjp",
	"png", 
	"svg",
	"webp",
	"bmp",
	"ico", "cur",
	"tif", "tiff"
]


ACCEPTABLE_DOCUMENT_EXTENSIONS = [
	"doc",
	"docx",
	"pdf"
] + ACCEPTABLE_PICTURE_EXTENSIONS


LOCAL_AUTHORITIES = {
	"Aberdeen City" : [
		"Aberdeen Grammar School",
		"Bridge Of Don Academy",
		"Bucksburn Academy",
		"Cults Academy",
		"Dyce Academy",
		"Harlaw Academy",
		"Hazlehead Academy",
		"Northfield Academy",
		"Oldmachar Academy",
		"St Machar Academy",
		"Lochside Academy"
	],
	"Aberdeenshire" : [
		"Aboyne Academy",
		"Alford Academy",
		"Banchory Academy",
		"Banff Academy",
		"Ellon Academy",
		"Fraserburgh Academy",
		"Inverurie Academy",
		"Kemnay Academy",
		"Mackie Academy",
		"Mearns Academy",
		"Meldrum Academy",
		"Mintlaw Academy",
		"Peterhead Academy",
		"Portlethen Academy",
		"The Gordon Schools",
		"Turriff Academy",
		"Westhill Academy"
	],
	"Angus" : [
		"Arbroath Academy",
		"Arbroath High School",
		"Brechin High School",
		"Carnoustie High School",
		"Forfar Academy",
		"Monifieth High School",
		"Montrose Academy",
		"Webster's High School"
	],
	"Argyll & Bute" : [
		"Lochgilphead High School",
		"Rothesay Academy",
		"Campbeltown Grammar School",
		"Dunoon Grammar School",
		"Hermitage Academy",
		"Islay High School",
		"Oban High School",
		"Tarbert Academy",
		"Tiree High School",
		"Tobermory High School"
	],
	"Clackmannanshire" : [
		"Alloa Academy",
		"Alva Academy",
		"Lornshill Academy"
	],
	"Dumfries & Galloway": [
		"Moffat Academy",
		"North West Community Campus",
		"Annan Academy",
		"Castle Douglas High School",
		"Dalbeattie High School",
		"Dalry Secondary School",
		"Douglas Ewart High School",
		"Dumfries Academy",
		"Dumfries High School",
		"Kirkcudbright Academy",
		"Langholm Academy",
		"Lockerbie Academy",
		"Sanquhar Academy",
		"St Joseph's College",
		"Stranraer Academy",
		"Wallace Hall Academy"
	],
	"Dundee City" : [
		"Craigie High School",
		"Baldragon Academy",
		"Braeview Academy",
		"Grove Academy",
		"Harris Academy",
		"Morgan Academy",
		"St John's RC High School",
		"St Paul's RC Academy"
	],
	"East Ayrshire" : [
		"Doon Academy",
		"Grange Academy",
		"Loudoun Academy",
		"Kilmarnock Academy",
		"Robert Burns Academy",
		"St Joseph's Academy",
		"Stewarton Academy"
	],
	"East Dunbartonshire" : [
		"Bearsden Academy",
		"Bishopbriggs Academy",
		"Lenzie Academy",
		"St Ninian's High School",
		"East Dunbartonshire Central School",
		"Boclair Academy",
		"Douglas Academy",
		"Kirkintilloch High School",
		"Turnbull High School"
	],
	"East Lothian" : [
		"Knox Academy",
		"Musselburgh Grammar School",
		"Preston Lodge High School",
		"Ross High School",
		"Dunbar Grammar School",
		"North Berwick High School"
	],
	"East Renfrewshire" : [
		"Barrhead High School",
		"Eastwood High School",
		"Mearns Castle High School",
		"St Luke's High School",
		"St Ninian's High School",
		"Williamwood High School",
		"Woodfarm High School"
	],
	"Edinburgh City" : [
		"Leith Academy",
		"Craigmount High School",
		"Drummond Community High School",
		"St Thomas Of Aquin's High School",
		"Balerno Community High School",
		"Boroughmuir High School",
		"Broughton High School",
		"Castlebrae Community High School",
		"Craigroyston Community High School",
		"Currie Community High School",
		"Firrhill High School",
		"Forrester High School",
		"Gracemount High School",
		"Holy Rood RC High School",
		"James Gillespie's High School",
		"Liberton High School",
		"Portobello High School",
		"Queensferry Community High School",
		"St Augustine's High School",
		"The Royal High School",
		"Trinity Academy",
		"Tynecastle High School",
		"Wester Hailes Education Centre"
	],
	"Falkirk": [
		"Denny High School",
		"Grangemouth High School",
		"Larbert High School",
		"Bo'ness Academy",
		"Braes High School",
		"Falkirk High School",
		"Graeme High School",
		"St Mungo's RC High School"
	],
	"Fife": [
		"Auchmuty High School",
		"Balwearie High School",
		"Beath High School",
		"Bell Baxter High School",
		"Dunfermline High School",
		"Glenrothes High School",
		"Glenwood High School",
		"Inverkeithing High School",
		"Kirkcaldy High School",
		"Levenmouth Academy",
		"Lochgelly High School",
		"Madras College",
		"Queen Anne High School",
		"St Andrew's R C High School",
		"St Columba's R C High School",
		"Viewforth High School",
		"Waid Academy",
		"Woodmill High School"
	],
	"Glasgow City" : [
		"Smithycroft Secondary School",
		"Bannerman High School",
		"Cleveden Secondary School",
		"Drumchapel High School",
		"Govan High School",
		"Hillpark Secondary School",
		"John Paul Academy",
		"King's Park Secondary School",
		"Lochend Community High School",
		"Lourdes Secondary School",
		"Rosshall Academy",
		"St Roch's Secondary School",
		"St Thomas Aquinas Secondary School",
		"Whitehill Secondary School",
		"Glasgow Gaelic School",
		"All Saints Secondary School",
		"Bellahouston Academy",
		"Castlemilk High School",
		"Eastbank Academy",
		"Hillhead High School",
		"Holyrood Secondary School",
		"Hyndland Secondary School",
		"Knightswood Secondary School",
		"Notre Dame High School",
		"Shawlands Academy",
		"Springburn Academy",
		"St Andrew's Secondary School",
		"St Margaret Mary's Secondary School",
		"St Mungo's Academy",
		"St Paul's High School"
	],
	"Grant aided" : ["Jordanhill School"],
	"Highland": [
		"Dingwall Academy",
		"Lochaber High School",
		"Millburn Academy",
		"Nairn Academy",
		"Thurso High School",
		"Wick High School",
		"Alness Academy",
		"Ardnamurchan High School",
		"Charleston Academy",
		"Culloden Academy",
		"Dornoch Academy",
		"Farr High School",
		"Fortrose Academy",
		"Gairloch High School",
		"Glen Urquhart High School",
		"Golspie High School",
		"Grantown Grammar School",
		"Invergordon Academy",
		"Inverness High School",
		"Inverness Royal Academy",
		"Kilchuimen Academy",
		"Kingussie High School",
		"Kinlochbervie High School",
		"Kinlochleven High School",
		"Mallaig High School",
		"Plockton High School",
		"Portree High School",
		"Tain Royal Academy",
		"Ullapool High School"
	],
	"Inverclyde": [
		"Clydeview Academy",
		"Notre Dame High School",
		"Inverclyde Academy",
		"Port Glasgow High School",
		"St Columba's High School",
		"St Stephen's High School"
	],
	"Midlothian": [
		"Dalkeith High School",
		"Newbattle High School",
		"Penicuik High School",
		"Beeslack Community High School",
		"Lasswade High School Centre",
		"St David's RC High School"
	],
	"Moray": [
		"Buckie High School",
		"Elgin Academy",
		"Elgin High School",
		"Forres Academy",
		"Keith Grammar School",
		"Lossiemouth High School",
		"Milne's High School",
		"Speyside High School"
	],
	"Na h-Eileanan Siar": [
		"The Nicolson Institute",
		"Castlebay School",
		"Sir E Scott School",
		"Sgoil Lionacleit"
	],
	"North Ayrshire": [
		"Ardrossan Academy",
		"Garnock Community Campus",
		"Arran High School",
		"Auchenharvie Academy",
		"Greenwood Academy",
		"Irvine Royal Academy",
		"Kilwinning Academy",
		"Largs Academy",
		"St Matthew's Academy"
	],
	"North Lanarkshire": [
		"Brannock High School",
		"Caldervale High School",
		"Cumbernauld Academy",
		"Dalziel High School",
		"Airdrie Academy",
		"Bellshill Academy",
		"Braidhurst High School",
		"Calderhead High School",
		"Cardinal Newman High School",
		"Chryston High School",
		"Clyde Valley High School",
		"Coatbridge High School",
		"Coltness High School",
		"Greenfaulds High School",
		"Kilsyth Academy",
		"Our Lady's High School - Cumbernauld",
		"Our Lady's High School - Motherwell",
		"St Aidan's High School",
		"St Ambrose High School",
		"St Andrew's High School",
		"St Margaret's High School",
		"St Maurice's High School",
		"Taylor High School"
	],
	"Orkney Islands": [
		"Kirkwall Grammar School",
		"Sanday Community School",
		"Stronsay Junior High School",
		"Westray Junior High School",
		"Stromness Academy"
	],
	"Perth & Kinross": [
		"Breadalbane Academy",
		"St John's RC Academy",
		"Bertha Park High School",
		"Blairgowrie High School",
		"Crieff High School",
		"Perth Academy",
		"Perth Grammar School",
		"Pitlochry High School",
		"The Community School of Auchterarder",
		"Kinross High School",
		"Perth High School"
	],
	"Renfrewshire": [
		"Castlehead High School",
		"Linwood High School",
		"Paisley Grammar School",
		"Park Mains High School",
		"Renfrew High School",
		"St Benedict's High School",
		"Gleniffer High School",
		"Gryffe High School",
		"Johnstone High School",
		"St Andrew's Academy",
		"Trinity High School"
	],
	"Scottish Borders": [
		"Jedburgh Grammar School",
		"Berwickshire High School",
		"Galashiels Academy",
		"Hawick High School",
		"Peebles High School",
		"Selkirk High School",
		"Earlston High School",
		"Eyemouth High School",
		"Kelso High School"
	],
	"Shetland Islands": [
		"Aith Junior High School",
		"Brae High School",
		"Sandwick Junior High School",
		"Whalsay School",
		"Anderson High School",
		"Baltasound Junior High School",
		"Mid Yell Junior High School"
	],
	"South Ayrshire": [
		"Girvan Academy",
		"Ayr Academy",
		"Belmont Academy",
		"Carrick Academy",
		"Kyle Academy",
		"Marr College",
		"Prestwick Academy",
		"Queen Margaret Academy"
	],
	"South Lanarkshire": [
		"Calderside Academy",
		"Duncanrig Secondary School",
		"Lanark Grammar School",
		"Larkhall Academy",
		"Uddingston Grammar School",
		"Biggar High School",
		"Calderglen High School",
		"Carluke High School",
		"Cathkin High School",
		"Hamilton Grammar School",
		"Holy Cross High School",
		"Lesmahagow High School",
		"St Andrew's and St Bride's High School",
		"St John Ogilvie High School",
		"Stonelaw High School",
		"Strathaven Academy",
		"Trinity High School"
	],
	"Stirling": [
		"St Modan's High School",
		"Wallace High School",
		"Balfron High School",
		"Bannockburn High School",
		"Dunblane High School",
		"McLaren High School",
		"Stirling High School"
	],
	"West Dunbartonshire": [
		"Vale Of Leven Academy",
		"Dumbarton Academy",
		"Clydebank High School",
		"Our Lady & St Patrick's High School",
		"St Peter the Apostle High School"
	],
	"West Lothian": [
		"Deans Community High School",
		"Linlithgow Academy",
		"St Kentigern's Academy",
		"The James Young High School",
		"Armadale Academy",
		"Bathgate Academy",
		"Broxburn Academy",
		"Inveralmond Community High School",
		"St Margaret's Academy",
		"West Calder High School",
		"Whitburn Academy"
	],
	"Independent": [
		"Al Khalil College",
		"Albyn School",
		"Al-Qalam School",
		"Ardfern Learning Centre- Johnstone",
		"Ardvreck School",
		"Aspire Education Kilbirnie",
		"Bachlaw Learning Centre",
		"Basil Paterson School",
		"Belhaven Hill School",
		"Belmont House School",
		"Camphill Rudolf Steiner School",
		"Cargilfield School",
		"Cedars School of Excellence",
		"Clifton Hall School, Edinburgh",
		"Closeburn House",
		"Common Thread School (Dumfries and Kilmarnock)",
		"Compass School, The",
		"Craigclowan Preparatory School",
		"Dollar Academy",
		"Drumduan School",
		"Dunedin School",
		"Edinburgh Academy, The",
		"Edinburgh Montessori Arts School",
		"Edinburgh Steiner School",
		"Erskine Waterfront Campus",
		"Fairview Beaconhurst School",
		"Falkland House School",
		"Fernhill School",
		"Fettes College",
		"George Heriot's School",
		"George Watson's College",
		"Glasgow Academy, The",
		"Glenalmond College",
		"Good Shepherd Secure/Close Support Unit",
		"Gordonstoun School",
		"Hamilton College",
		"High School of Dundee, The",
		"High School of Glasgow, The",
		"Hillside school",
		"Hutchesons' Grammar",
		"International School Aberdeen, The",
		"Kelvinside Academy",
		"Kibble Education and Care Centre",
		"Kilgraston School",
		"Lathallan School",
		"Linn Moor Residential School",
		"Lomond School",
		"Loretto School",
		"Maben House",
		"Mannafields Christian School",
		"Mary Erskine School",
		"Mary Erskine/Stewart's Melville Junior School",
		"Melville-Knox Christian School ",
		"Merchiston Castle School",
		"Mirren Park School ",
		"Moore House Academy Bathgate",
		"Moore House Academy Butterstone",
		"Morrison's Academy",
		"Netherlea School",
		"New Struan School",
		"Northview House School",
		"Ochil Tower School",
		"Olivewood Primary School",
		"OneSchool Global",
		"Pebbles Academy, Dunfermline",
		"Pebbles Academy, Muirkirk",
		"Providence Christian School ",
		"Queen Victoria School",
		"Regius School",
		"Robert Gordon's College",
		"Rossie Secure Accomodation Services",
		"Seamab House School",
		"Spark of Genius (Caledonain School)",
		"Spark of Genius (Harbour Point School)",
		"Spark of Genius (Skypoint School)",
		"St Aloysius' College",
		"St Columba's School",
		"St George's School, Edinburgh",
		"St Leonards School",
		"St Margaret's School for Girls",
		"St Mary's Kenmure",
		"St Mary's Music School",
		"St Mary's School, Melrose",
		"St Philip's School",
		"Starley Hall School",
		"Stewart's Melville College",
		"Strathallan School",
		"Total French School",
		"Troup House School",
		"Wellington School"
	],
	"Unknown" : [] # just has to be a key. is specially handled in backend
	# , "Other": ["Other"]
}


class STATUS_CODE_2xx(enum.Enum):
	SUCCESS = 200
	CREATED = 201
	ACCEPTED = 202
	NO_CONTENT = 204


class STATUS_CODE_4xx(enum.Enum):
	BAD_REQUEST = 400
	UNAUTHORIZED = 401
	FORBIDDEN = 403
	NOT_FOUND = 404
	GONE = 410


class STATUS_CODE_5xx(enum.Enum) :
	INTERNAL_SERVER_ERROR = 500


def get_tokens_for_user(user):
	refresh = RefreshToken.for_user(user)

	return {
		'refresh' : str(refresh),
		'access' : str(refresh.access_token)
	}


def get_url_to_file_field(file_field) :
	if file_field.s3_url != "" :
		return file_field.s3_url
	
	return reverse('media-paths', args=[file_field.media_file.url.split("/media/")[-1]])


def get_url_to_animation(animation):
    if animation.s3_url != "" :
        return animation.s3_url
    
    return reverse('media-paths', args=[animation.animation.path.split("/media/")[-1]])


def get_url_to_intro_video(intro):
	if intro.s3_url != "":
		return intro.s3_url

	return reverse('media-paths', args=[intro.intro_video.url.split("/media/")[-1]])


def is_secret_key_valid(secret_key) :
	try :
		_ = SecretKey.objects.get(key_text=secret_key)
		return True

	except Exception :
		return False


def get_secret_key_value() :
	key_text = None

	try :
		while True :
			key_text = ''.join(random.SystemRandom().choice(string.ascii_letters + string.digits) for _ in range(10))
			_ = SecretKey.objects.get(key_text=key_text)

	except Exception :
		pass

	return key_text


def calculate_milestone_percentage(total, send_milestones) :
	
	return (100 * total) / (len(send_milestones) - 1)




class Milestone :
	tasks: list[dict]
	name: str
	current: bool
	completed: bool

	def __init__(self, name, tasks, current, completed) :
		self.tasks = tasks
		self.name = name
		self.current = current
		self.completed = completed

	def getJSON(self) :
		return {
			'tasks' : self.tasks,
			'completed' : self.completed,
			'current' : self.current,
			'name' : "Preparations"
		}


class Task :
	compeleted: bool
	text: str
	link: str

	def __init__(self, text, completed, link) :
		self.text = text
		self.compeleted = completed
		self.link = link

	def getJSON(self) :
		return {
			'completed' : self.compeleted,
			'text' : self.text,
			'link' : self.link
		}


def get_tasks(team, user) :
	submissionExists = True
	try :
		submission = Submission.objects.get(team=team)
	except Exception :
		submissionExists = False
		
	return [
			{
				'completed' : submissionExists and submission.company_name != "",
				'text' : "Choose a business name",
				'link' : reverse('frontend-marketplace-specific', args=[user.username])
			},
			{
				'completed' : submissionExists and submission.tagline != "",
				'text' : "Submit a company tagline",
				'link' : reverse('frontend-marketplace-specific', args=[user.username])
			},
			{
				'completed' : submissionExists and submission.logo != None,
				'text' : "Submit a company logo",
				'link' : reverse('frontend-marketplace-specific', args=[user.username])
			},
			{
				'completed' : submissionExists and submission.poster != None,
				'text' : "Submit a company poster",
				'link' : reverse('frontend-marketplace-specific', args=[user.username])
			},
			{
				'completed' : submissionExists and submission.target_customer != None,
				'text' : "Submit a target customer",
				'link' : reverse('frontend-marketplace-specific', args=[user.username])
			}
		]


def get_milestones_for_user(user) :
	# TODO: fill in missing functionality
	milestones = []
	
	profile = Profile.objects.get(user=user)
	if profile.type == "Team" :
		team = Team.objects.get(user=user)

		tasks = [
			Task("Get signed up", True, reverse('frontend-login')).getJSON()
		]

		milestoneCompleted = True
		for t in tasks :
			if not t['completed'] :
				milestoneCompleted = False
				break

		milestones.append(
			Milestone("Preperations", tasks, not milestoneCompleted, milestoneCompleted).getJSON()
		)

		tasks = get_tasks(team, user)

		previousMilestoneCompleted = milestoneCompleted
		milestoneCompleted = True
		for t in tasks :
			if not t['completed'] :
				milestoneCompleted = False
				break

		milestones.append(
			{
				'tasks' : tasks,
				'completed' : milestoneCompleted,
				'current' : previousMilestoneCompleted and not milestoneCompleted,
				'name' : "Submit marketplace stall"
			}
		)

		businessDecisionCompelete = len(team.question_answer_dict.keys()) >= len(Question.objects.all())

		tasks = [
			{
				'completed' : businessDecisionCompelete,
				'text' : "Complete the business decisions",
				'link' : reverse('frontend-decisions')
			}
		]

		previousMilestoneCompleted = milestoneCompleted
		milestoneCompleted = True
		for t in tasks :
			if not t['completed'] :
				milestoneCompleted = False
				break

		milestones.append(
			{
				'tasks' : tasks,
				'completed' : milestoneCompleted,
				'current' : previousMilestoneCompleted and not milestoneCompleted,
				'name' : "Business decisions"
			}
		)

		tasks = [
			{
				'completed' : areAllVotesUsed(team),
				'text' : "Given " + str(getVotesUsed(team)) + "/" + str(MAXIMUM_NUMBER_OF_LIKES) + " stalls a like",
				'link' : reverse('frontend-marketplace')
			}
		]

		previousMilestoneCompleted = milestoneCompleted
		milestoneCompleted = True
		for t in tasks :
			if not t['completed'] :
				milestoneCompleted = False
				break

		milestones.append(
			{
				'tasks' : tasks,
				'completed' : milestoneCompleted,
				'current' : previousMilestoneCompleted and not milestoneCompleted,
				'name' : "Vote for other people's stalls"
			}
		)

		tasks = [
			{
				'completed' : False,
				'text' : "Wait for the results to be released!",
				'link' : reverse('frontend-marketplace-specific', args=[user.username]) # TODO: update to regular marketplace reverse
			}
		]

		previousMilestoneCompleted = milestoneCompleted
		milestoneCompleted = True
		for t in tasks :
			if not t['completed'] :
				milestoneCompleted = False
				break

		milestones.append(
			{
				'tasks' : tasks,
				'completed' : milestoneCompleted,
				'current' : previousMilestoneCompleted and not milestoneCompleted,
				'name' : "Wait for the results"
			}
		)

	return milestones


def delete_judge(username) :
	user = User.objects.get(username=username)
	profile = Profile.objects.get(user=user)
	judge = Judge.objects.get(user=user)

	judge_submissions = JudgesSubmission.objects.filter(judge=judge)
	
	for judge_submission in judge_submissions :
		judge_submission.delete()
	
	judge.delete()
	profile.delete()
	user.delete()


def delete_team(username) :
	user = User.objects.get(username=username)
	team = Team.objects.get(user=user)
	profile = Profile.objects.get(user=user)
	submission = Submission.objects.filter(team=team)

	liked = SubmissionLike.objects.filter(team=team)
	for like in liked :
		like.delete()

	# NOTE: Likes are deleted automatically with CASCADE on_delete in model

	if len(submission) > 0 :
		submission[0].delete()
		
	team.delete()
	profile.delete()
	user.delete()


def delete_teacher(username) :
	user = User.objects.get(username=username)
	teacher = Teacher.objects.get(user=user)
	profile = Profile.objects.get(user=user)

	created_teams = Team.objects.filter(teacher=teacher)
	for created in created_teams :
		delete_team(created.user.username)

	for key in SecretKey.objects.filter(user=user) :
		key.delete()

	teacher.delete()
	profile.delete()
	user.delete()


# def delete_admin(username) :
# 	user = User.objects.get(username=username)
# 	admin = Admin.objects.get(user=user)
# 	profile = Profile.objects.get(user=user)

# 	for key in SecretKey.objects.filter(user=user) :
# 		key.delete()

# 	admin.delete()
# 	profile.delete()
# 	user.delete()

def delete_uploaded_file_if_exists(submission_instance, submission_key) :
	try :
		uploaded_file = UploadedFile.objects.get(team=submission_instance.team, key=submission_key)

		# remove from submission
		if submission_key == "target_customer" :
			submission_instance.target_customer = None
		if submission_key == "poster" :
			submission_instance.poster = None
		if submission_key == "logo" :
			submission_instance.logo = None

		submission_instance.save()
		
		uploaded_file.delete()

	except Exception :
		pass

def upload_file(submission_instance, submission_key, file_ext, file_mime_type, file_binary_data) :
	# create file instance, upload to media, retrieve file information from file model then upload to s3
	delete_uploaded_file_if_exists(submission_instance, submission_key)

	# create uploaded file object
	uploaded_file = UploadedFile.objects.create(
		team=submission_instance.team,
		key=submission_key,
		media_file=ContentFile(file_binary_data, name='temp.' + file_ext),
		mime_type=file_mime_type,
		extension=file_ext
	)

	# add to submission
	if submission_key == "target_customer" :
		submission_instance.target_customer = uploaded_file
	if submission_key == "poster" :
		submission_instance.poster = uploaded_file
	if submission_key == "logo" :
		submission_instance.logo = uploaded_file
	submission_instance.save()

	# uploaded to s3 and save url if s3 is enabled
	if S3_BUCKET != None :
		s3 = boto3.client('s3')

		s3.put_object(
			ACL='public-read',
			Bucket=S3_BUCKET,
			Key=uploaded_file.path,
			ContentType=file_mime_type,
			Body=file_binary_data
		)

		url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, uploaded_file.path)
		
		uploaded_file.s3_url = url
		uploaded_file.save()


def get_submission_deadline() :
	return Deadline.objects.all()[0].submission_deadline


def get_voting_deadline() :
	return Deadline.objects.all()[0].voting_deadline


def get_judging_deadline() :
	return Deadline.objects.all()[0].judging_deadline


def getVotesUsed(team) :
	return len(SubmissionLike.objects.filter(team=team))


def areAllVotesUsed(team) :
	return getVotesUsed(team) >= MAXIMUM_NUMBER_OF_LIKES


def canLikeMarketplaceStall(request, username) :
	if Profile.objects.get(user=request.user).type != "Team" :
		return (False, "Non-Team user cannot give likes")

	team = Team.objects.get(user=request.user)

	if areAllVotesUsed(team) :
		return (False, "Cannot give more than " + str(MAXIMUM_NUMBER_OF_LIKES) + " likes")

	if request.user.username == username :
		return (False, "Cannot like your own marketplace stall")

	submission = None
	try :
		submission = Submission.objects.get(team__user__username=username)
	except Exception :
		return (False, "Cannot like a submisison that does not exist")

	try :
		_ = SubmissionLike.objects.get(team=team, submission=submission)
		return (False, "Cannot like a marketplace stall that you have liked already")
	except Exception :
		pass

	return (True, None)


def get_team_milestones_for_teacher(teacher) :
	all_milestones = []
	team_names = []
	team_logo_stubs = []

	for retrieving_team in Team.objects.filter(teacher=teacher) :
		milestones = get_milestones_for_user(retrieving_team.user)

		send_milestones = []
		total = 0
		for m in milestones :
			send_milestones.append(m)

			# do not add current milestone count as should always be 1 
			total += 1 if m['completed'] else 0

		all_milestones.append({"milestones" : send_milestones, "percentage": (100 * total) / (len(send_milestones) - 1)})
		team_names.append(retrieving_team.user.username)

		try :
			submission = Submission.objects.get(team=retrieving_team)
			team_logo_stubs.append(get_url_to_file_field(submission.logo))
		except Exception :
			team_logo_stubs.append("")

	return (all_milestones, team_names, team_logo_stubs)

	
