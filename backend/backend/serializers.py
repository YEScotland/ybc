#
#  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import django.contrib.auth.password_validation as validators

from django.contrib.auth import password_validation
from rest_framework import serializers
from rest_framework import serializers

from .models import User

class RegisterUserSerializer(serializers.ModelSerializer):
	password = serializers.CharField(style={'input_type': 'password'}, write_only=True)
	username = serializers.CharField(style={'input_type': 'username'}, write_only=True)

	class Meta:
		model = User
		fields = ('username', 'password')

	def validate(self, data):
		user = User(**data)

		password = data.get('password')
		username = data.get('username')

		errors = dict() 

		try:
			existing_users = User.objects.filter(username=username)
			if len(existing_users) != 0 :
				errors['username'] = ["User with username already exists."]
		
		except Exception as e: 
			pass # user does not already exit, therefore, success

		validators.validate_password(password=password, user=user)

		if errors:
			raise serializers.ValidationError(errors)

		return super(RegisterUserSerializer, self).validate(data)

	def create(self, validated_data):
		user = User.objects.create_user(**validated_data)
		user.save()
		return user


class ChangePasswordSerializer(serializers.Serializer):
    username = serializers.CharField(max_length=128, write_only=True, required=True)
    new_password = serializers.CharField(max_length=128, write_only=True, required=True)

    def validate(self, data):
        password_validation.validate_password(data['new_password'], User.objects.get(username=data['username']))
        return data

    def save(self, **kwargs):
        password = self.validated_data['new_password']
        user = User.objects.get(username=self.validated_data['username'])
        user.set_password(password)
        user.save()
        return user