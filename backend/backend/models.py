#
#  Copyright Jim Carty and Usmaan Karim © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import boto3

from django.db import models
from django.db.models.deletion import CASCADE
from django.db.models.fields import BooleanField
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser
from django.core.validators import MaxValueValidator, MinValueValidator

from .settings import S3_BUCKET

# Usmaan Karim, 2475568K
# Some code taken from django documentation examples and third-party tutorials
# Create your models here.

# User and Profile
# The below class is from Jim Carty in CS11, he recommended it be added to allow for full support from Django's
# authentication process for the log in method we are using

class User(AbstractUser):
    username = models.CharField(max_length=128, unique=True, primary_key=True, db_index=True)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []
    
    def __str__(self) :
        return self.username


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)

    class ProfileType(models.TextChoices):
        ADMIN = 'Admin'
        TEACHER = 'Teacher'
        JUDGE = 'Judge'
        TEAM = 'Team'

    type = models.CharField(max_length=7, choices=ProfileType.choices)


#signals to create/update Profile when User is created/updated
#solution found here: https://simpleisbetterthancomplex.com/tutorial/2016/07/22/how-to-extend-django-user-model.html#onetoone
@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()


# Admin
class Admin(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)


# Teacher
class Teacher(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    school = models.CharField(max_length=100)
    area = models.CharField(max_length=100)
    administrator = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='admin_teacher_fk', null=True)


# Team
class Team(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    teacher = models.ForeignKey(Teacher, on_delete=models.PROTECT)
    question_answer_dict = models.JSONField(default=dict)


# UploadedFile
def file_directory_path(instance, filename) :
    # file will be uploaded to MEDIA_ROOT/<instance.key>+'s'/<username>.<extension>
    instance.path = '{directory}/{file_name}'.format(directory=(instance.key + 's'), file_name=(instance.team.user.username + '.' + instance.extension))
    return instance.path


class UploadedFile(models.Model) :
    class FileKeyType(models.TextChoices):
        TARGET_CUSTOMER = 'target_customer'
        POSTER = 'poster'
        LOGO = 'logo'

    key = models.TextField(max_length=15, choices=FileKeyType.choices)
    media_file = models.FileField(upload_to=file_directory_path, blank=True)
    mime_type = models.TextField(max_length=256)
    extension = models.TextField(max_length=5)
    team = models.ForeignKey(Team, on_delete=CASCADE)
    s3_url = models.TextField(max_length=128, blank=True)
    path = models.TextField(max_length=256)


@receiver(pre_delete, sender=UploadedFile)
def delete_uploaded_file(sender, instance, **kwargs):
    instance.media_file.delete()

    if S3_BUCKET != None :
        s3 = boto3.client('s3')

        s3.delete_object(
            Bucket=S3_BUCKET,
            Key=(instance.path)
        )


# Submission
class Submission(models.Model):
    team = models.OneToOneField(Team, on_delete=models.CASCADE, primary_key=True)
    company_name = models.CharField(max_length=50, blank=True)
    tagline = models.TextField(blank=True)
    logo = models.ForeignKey(UploadedFile, on_delete=models.SET_NULL, null=True, related_name='logo_fk')
    poster = models.ForeignKey(UploadedFile, on_delete=models.SET_NULL, null=True, related_name='poster_fk')
    target_customer = models.ForeignKey(UploadedFile, on_delete=models.SET_NULL, null=True, related_name='target_customer_fk')

    # could be JSONField depending on what it actually is
    question_answer_list = models.JSONField(blank=True, default=dict)
    peer_score = models.PositiveSmallIntegerField(blank=True, default=0)
    dragon_score = models.PositiveSmallIntegerField(blank=True, default=0)
    overall_score = models.PositiveSmallIntegerField(blank=True, default=0)
    
    # issues attributes
    is_blacklisted = BooleanField(default=False)
    blacklisted_reason = models.CharField(max_length=1024, blank=True, default="")
    changed_since_blacklisting = BooleanField(default=True)
    

@receiver(pre_delete, sender=Submission)
def delete_submission(sender, instance, **kwargs):
    if instance.logo != None :
        instance.logo.delete()
    if instance.target_customer != None :
        instance.target_customer.delete()
    if instance.poster != None :
        instance.poster.delete()


# SubmissionLike
class SubmissionLike(models.Model) :
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE, related_name='submission_fk')
    team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='team_fk')


# Judge
class Judge(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, primary_key=True)
    areas = models.JSONField(default=list)
    submissions = models.ManyToManyField(Submission, through='JudgesSubmission')
    administrator = models.ForeignKey(Admin, on_delete=models.CASCADE, related_name='admin_judge_fk', null=True)


# Judge Submission
class JudgesSubmission(models.Model):
    judge = models.ForeignKey(Judge, on_delete=models.CASCADE)
    submission = models.ForeignKey(Submission, on_delete=models.CASCADE)
    poster_score = models.PositiveSmallIntegerField(null=True, blank=True, validators=[MinValueValidator(0), MaxValueValidator(25)])
    logo_score = models.PositiveSmallIntegerField(null=True, blank=True,  validators=[MinValueValidator(0), MaxValueValidator(25)])
    target_customer_score = models.PositiveSmallIntegerField(null=True, blank=True, validators=[MinValueValidator(0), MaxValueValidator(25)])
    tagline_score = models.PositiveSmallIntegerField(null=True, blank=True, validators=[MinValueValidator(0), MaxValueValidator(25)])


# Deadline
# separated deadline into its own class to avoid duplicated information and to keep it clear
class Deadline(models.Model):
    submission_deadline = models.DateField()
    voting_deadline = models.DateField()
    judging_deadline = models.DateField()


# Question
class Question(models.Model):
    question_id = models.PositiveSmallIntegerField(primary_key=True, unique=True)
    question_text = models.TextField()


def animation_directory_path(instance, file_name):
    instance.path = 'animations/{}'.format(str(instance.question.question_id) + '.mp4')
    return instance.path


class Animation(models.Model):
    # key = models.TextField(default='animation', editable=False)
    animation = models.FileField(upload_to=animation_directory_path, blank=True)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    s3_url = models.TextField(max_length=128, blank=True)
    yt_url = models.TextField(max_length=128, blank=True)
    path = models.TextField()

    def save(self, *args, **kwargs):
        super(Animation, self).save(*args, **kwargs) # save to save the video to media

        if self.animation != None:

            if S3_BUCKET != None :
                s3 = boto3.client('s3')

                data = None
                with open(self.animation.path, "rb") as f :
                    data = f.read()

                s3.put_object(
                    ACL='public-read',
                    Bucket=S3_BUCKET,
                    Key=self.path,
                    ContentType='video/mp4',
                    Body=data
                )

                url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, self.path)
                self.s3_url = url

                super(Animation, self).save(*args, **kwargs) # save to save the s3_url


@receiver(pre_delete, sender=Animation)
def delete_animation(sender, instance, **kwargs):
    instance.animation.delete()

    if S3_BUCKET != None :
        s3 = boto3.client('s3')

        s3.delete_object(
            Bucket=S3_BUCKET,
            Key=(instance.path)
        )


def introvid_directory_path(instance, file_name):
    instance.path = 'introvid/{}'.format(str(instance.id) + '.mp4')
    return instance.path


class Intro(models.Model):
    intro_text = models.TextField()
    intro_video = models.FileField(upload_to=introvid_directory_path, blank=True)
    s3_url = models.TextField(max_length=128, blank=True)
    yt_url = models.TextField(max_length=128, blank=True)
    path = models.TextField()

    def save(self, *args, **kwargs):
        super(Intro, self).save(*args, **kwargs) # save to save the video to media

        if self.intro_video:

            if S3_BUCKET != None :
                s3 = boto3.client('s3')

                data = None
                with open(self.intro_video.path, "rb") as f :
                    data = f.read()

                s3.put_object(
                    ACL='public-read',
                    Bucket=S3_BUCKET,
                    Key=self.path,
                    ContentType='video/mp4',
                    Body=data
                )

                url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, self.path)
                self.s3_url = url

                #might be causing issues because of double save
                super(Intro, self).save(*args, **kwargs) # save to save the s3_url


@receiver(pre_delete, sender=Intro)
def delete_intro(sender, instance, **kwargs):
    instance.intro_video.delete()

    if instance.intro_video and  S3_BUCKET != None :
        s3 = boto3.client('s3')

        s3.delete_object(
            Bucket=S3_BUCKET,
            Key=(instance.path)
        )


# Answer
class Answer(models.Model):
    #django does not support composite primary keys so I am using the default primary key
    #and have made question_id and answer_no unique as a pair
    answer_no = models.PositiveSmallIntegerField()
    answer_text = models.TextField()
    answer_dragon_scores = models.JSONField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    #found in django models documentation
    class Meta:
        constraints = [models.UniqueConstraint(fields=['id', 'question'], name='question_answer_pair')]


# SecretKey
class SecretKey(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    key_text = models.CharField(max_length=30, primary_key=True)

    class KeyType(models.TextChoices):
        JUDGE = 'Judge'
        TEAM = 'Team'
        TEACHER = 'Teacher'
        ADMIN = 'Admin'

    type = models.CharField(max_length=7, choices=KeyType.choices)
    school = models.CharField(max_length=100, blank=True)
    areas = models.JSONField(default=list)
