#
#  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import json
import os
import base64
import boto3
import datetime
import shutil

from django.contrib.auth import get_user_model
from django.core.files.base import ContentFile
from django.urls import reverse
from django.test import TestCase, Client

from .utils import STATUS_CODE_2xx, STATUS_CODE_4xx, STATUS_CODE_5xx, LOCAL_AUTHORITIES, MAXIMUM_NUMBER_OF_LIKES, get_url_to_file_field
from .models import Admin, Judge, Profile, SecretKey, Submission, Teacher, Team, UploadedFile, JudgesSubmission, Deadline, SubmissionLike, Answer, Question, Animation, Intro
from .settings import BASE_DIR, MEDIA_ROOT, API_NAMESPACE, S3_BUCKET

API_NAMESPACE = '/' + API_NAMESPACE

AXIOS_FILE_DATA_PREFIX = "data:image/png;base64,"

User = get_user_model()

SUPERUSER_USERNAME = 'superuser'
SUPERUSER_PASSWORD = 'admin'


def get_headers(access_key) :
	return {
		'HTTP_AUTHORIZATION': 'Bearer ' + access_key,
	}


def create_superuser(client, uname, passw):
	User.objects.create_superuser(username=uname, email=uname, password=passw)
	tokens_response = client.put(
		API_NAMESPACE + '/auth/login/',
		json.dumps({
			"username": uname,
			"password": passw
		}),
		content_type="application/json")
	superuser_credentials = tokens_response.data['tokens']['access']
	return superuser_credentials


def register_user(client, uname, passw, secret_key):
	register_response = client.post(
		API_NAMESPACE + '/auth/register/',
		json.dumps({
			"username": uname,
			"password": passw,
			"secret_key": secret_key
		}),
		content_type="application/json",
	)
	user_credentials = register_response.data['tokens']['access']
	return user_credentials


def get_secret_key(client, key_type, credentials, areas=("Glasgow City",), school="Bellahouston Academy"):
	if key_type == 'Admin' or key_type == 'Team':
		secret_key_response = client.post(
			API_NAMESPACE + '/auth/secret_key',
			**get_headers(credentials))

	elif key_type == 'Teacher':
		secret_key_response = client.post(
			API_NAMESPACE + '/auth/secret_key',
			json.dumps({
				"key_type" : "Teacher",
				"areas" : areas,
				"school": school
			}),
			content_type="application/json",
			**get_headers(credentials))

	elif key_type == 'Judge':
		secret_key_response = client.post(
			API_NAMESPACE + '/auth/secret_key',
			json.dumps({
				"key_type" : "Judge",
				"areas" : areas,
				"school": school
			}),
			content_type="application/json",
			**get_headers(credentials))
	else:
		raise Exception(f'Invalid type of secret key requested: {key_type}')
	try:
		secret_key = secret_key_response.data['secret_key']
	except KeyError as e:
		print("response does not contain a secret_key field:", secret_key_response.data)
		raise e
	return secret_key


def create_submission(team_username):
	submission = Submission.objects.create(
		team=Team.objects.get(user=User.objects.get(username=team_username)))
	with open("testdata/picture1.png", 'rb') as f:
		buffer = f.read()
	submission.logo = UploadedFile.objects.create(
		team=submission.team,
		key="logo",
		media_file=ContentFile(buffer, name='temp.png'),
		mime_type="image/png",
		extension="png"
	)
	if S3_BUCKET != None:
		s3 = boto3.client('s3')

		s3.put_object(
			ACL='public-read',
			Bucket=S3_BUCKET,
			Key=submission.logo.path,
			ContentType=submission.logo.mime_type,
			Body=buffer
		)

		url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, submission.logo.path)

		submission.logo.s3_url = url
		submission.logo.save()
	submission.save()
	return submission

def create_full_submission(team_username):
	submission = Submission.objects.create(
		team=Team.objects.get(user=User.objects.get(username=team_username)))
	#adds logo
	with open("testdata/picture1.png", 'rb') as f:
		buffer = f.read()
	submission.logo = UploadedFile.objects.create(
		team=submission.team,
		key="logo",
		media_file=ContentFile(buffer, name='temp.png'),
		mime_type="image/png",
		extension="png"
	)
	if S3_BUCKET != None:
		s3 = boto3.client('s3')

		s3.put_object(
			ACL='public-read',
			Bucket=S3_BUCKET,
			Key=submission.logo.path,
			ContentType=submission.logo.mime_type,
			Body=buffer
		)

		url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, submission.logo.path)

		submission.logo.s3_url = url
		submission.logo.save()
	
	#adds company name
	submission.company_name = "Hi"
	#adds tagline
	submission.tagline = "Bye"
	#adds poster
	submission.poster = UploadedFile.objects.create(
		team=submission.team,
		key="poster",
		media_file=ContentFile(buffer, name='temp.png'),
		mime_type="image/png",
		extension="png"
	)
	if S3_BUCKET != None:
		s3 = boto3.client('s3')

		s3.put_object(
			ACL='public-read',
			Bucket=S3_BUCKET,
			Key=submission.poster.path,
			ContentType=submission.poster.mime_type,
			Body=buffer
		)

		url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, submission.poster.path)

		submission.poster.s3_url = url
		submission.poster.save()

	#adds target customer
	submission.target_customer = UploadedFile.objects.create(
		team=submission.team,
		key="target_customer",
		media_file=ContentFile(buffer, name='temp.png'),
		mime_type="image/png",
		extension="png"
	)
	if S3_BUCKET != None:
		s3 = boto3.client('s3')

		s3.put_object(
			ACL='public-read',
			Bucket=S3_BUCKET,
			Key=submission.target_customer.path,
			ContentType=submission.target_customer.mime_type,
			Body=buffer
		)

		url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, submission.target_customer.path)

		submission.target_customer.s3_url = url
		submission.target_customer.save()

	submission.save()
	return submission

def turn_submission_to_stalls(submissions):
	stalls = []
	for submission in submissions:
		stalls.append({
					"company_name" : submission.company_name, 
					"tagline": submission.tagline, 
					"logo_stub": get_url_to_file_field(submission.logo) if submission.logo != None else "", 
					"poster_stub" : get_url_to_file_field(submission.poster) if submission.poster != None else "", 
					"target_customer_stub" : get_url_to_file_field(submission.target_customer) if submission.target_customer != None else "",
					"team_username" : submission.team.user.username
					})
	return stalls



def can_log_in(client, username, password) :
	response = client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : username, "password": password}))

	return (response.status_code == STATUS_CODE_2xx.ACCEPTED.value)


def delete_all_deadlines() :
	deadlines = Deadline.objects.all()
	for deadline in deadlines :
		deadline.delete()


def create_active_submission_deadline() :
	delete_all_deadlines()
	Deadline.objects.create(
		submission_deadline=datetime.datetime.now() + datetime.timedelta(days=1),
		judging_deadline=datetime.datetime.now() + datetime.timedelta(days=2),
		voting_deadline=datetime.datetime.now() + datetime.timedelta(days=2)
	)


def create_active_judging_deadline() :
	delete_all_deadlines()
	Deadline.objects.create(
		submission_deadline=datetime.datetime.now() - datetime.timedelta(days=2),
		judging_deadline=datetime.datetime.now() + datetime.timedelta(days=1),
		voting_deadline=datetime.datetime.now() - datetime.timedelta(days=1)
	)


def create_active_voting_deadline() :
	delete_all_deadlines()
	Deadline.objects.create(
		submission_deadline=datetime.datetime.now() - datetime.timedelta(days=2),
		judging_deadline=datetime.datetime.now() - datetime.timedelta(days=1),
		voting_deadline=datetime.datetime.now() + datetime.timedelta(days=1)
	)


def create_passed_deadline() :
	delete_all_deadlines()
	Deadline.objects.create(
		submission_deadline=datetime.datetime.strptime("1970-01-01", "%Y-%m-%d"),
		judging_deadline=datetime.datetime.strptime("1970-01-02", "%Y-%m-%d"),
		voting_deadline=datetime.datetime.strptime("1970-01-02", "%Y-%m-%d")
	)


def create_like(submission_username, liker_username) :
	SubmissionLike.objects.create(team=Team.objects.get(user__username=liker_username), submission=Submission.objects.get(team__user__username=submission_username))


def delete_like(submission_username, liker_username) :
	like = SubmissionLike.objects.get(team=Team.objects.get(user__username=liker_username), submission=Submission.objects.get(team__user__username=submission_username))

	like.delete()


def create_judges_submission(judge_username, team_username) :
	judge_submission = JudgesSubmission.objects.create(judge=Judge.objects.get(user__username=judge_username), submission=Submission.objects.get(team__user__username=team_username))

	judge_submission.tagline_score = 10
	judge_submission.poster_score = 10
	judge_submission.target_customer_score = 10
	judge_submission.logo_score = 10

	judge_submission.save()

	return judge_submission

def create_incomplete_judges_submission(judge_username, team_username) :
	judge_submission = JudgesSubmission.objects.create(judge=Judge.objects.get(user__username=judge_username), submission=Submission.objects.get(team__user__username=team_username))

	judge_submission.tagline_score = 10
	judge_submission.poster_score = 10
	judge_submission.target_customer_score = 10
	#submission is now missing logo score so is incomplete
	#judge_submission.logo_score = 10

	judge_submission.save()

	return judge_submission


class UserRegistrationTestCase(TestCase):
	def setUp(self) :
		self.client = Client()

		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_TEACHER_SECRET_KEY = secret_key_response.data['secret_key']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Judge", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_JUDGE_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEACHER_USERNAME = 'teacher'
		self.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME, "password" : self.TEACHER_PASSWORD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

		User.objects.create(username='foo', password='foo')

	def test_user_registration_unsuccessful_similar_and_short_password(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "user1", "password" : "user1", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "The password is too similar to the username, and this password is too short. it must contain at least 6 characters")

	def test_user_registration_unsuccessful_short_password(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "user1", "password" : "foo", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "This password is too short. It must contain at least 6 characters")

	def test_user_registration_unsuccessful_similar_password(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "user12345", "password" : "user12345", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "The password is too similar to the username")

	def test_user_registration_unsuccessful_short_and_similar_and_common_password(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "admin", "password" : "admin", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "The password is too similar to the username, and this password is too short. it must contain at least 6 characters, and this password is too common")

	def test_user_registration_unsuccessful_common_password(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "admin", "password" : "password", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "This password is too common")

	def test_user_registration_unsuccessful_exisiting_username(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "foo", "password" : "dghjasdja^&*678", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "User with username already exists")

	def test_user_registration_unsuccessful_exisiting_username_and_common_password(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "foo", "password" : "password", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertEqual(response.data['message'], "This password is too common")

	def test_user_registration_unsuccessful_invalid_secret_key(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "bar", "password" : "dghjasdja^&*678", "secret_key": self.TEACHER_SECRET_KEY+"foo"}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 401)
		self.assertEqual(response.data['message'], "Secret key is not valid, please verify it your teacher or YE Scotland representative")

	def test_user_registration_successful(self):
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "bar", "password" : "dghjasdja^&*678", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 201)
		self.assertTrue("username" in response.data)
		self.assertTrue("tokens" in response.data)
		self.assertTrue("user_type" in response.data)
		self.assertTrue("access" in response.data['tokens'])
		self.assertTrue("refresh" in response.data['tokens'])
		self.assertGreater(len(response.data['tokens']['access']), 20)
		self.assertGreater(len(response.data['tokens']['refresh']), 20)

	def test_user_registration_incorrect_methods(self):
		response = self.client.put(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "bar", "password" : "dghjasdja^&*678", "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 405)

		response = self.client.get(API_NAMESPACE + '/auth/register/', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 405)

		response = self.client.delete(API_NAMESPACE + '/auth/register/', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 405)

	def test_user_admin_registration(self) :
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME + "foo", "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 201)
		self.assertTrue("username" in response.data)
		self.assertTrue("tokens" in response.data)
		self.assertTrue("user_type" in response.data)
		self.assertTrue("access" in response.data['tokens'])
		self.assertTrue("refresh" in response.data['tokens'])
		self.assertGreater(len(response.data['tokens']['access']), 20)
		self.assertGreater(len(response.data['tokens']['refresh']), 20)

		self.assertGreater(len(Admin.objects.all()), 1)
		user = User.objects.get(username=self.ADMIN_USERNAME + "foo")
		self.assertTrue(Profile.objects.get(user=user).type == 'Admin')

	def test_user_teacher_registration(self) :
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME + "foo", "password" : self.ADMIN_PASSWORD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 201)
		self.assertTrue("username" in response.data)
		self.assertTrue("tokens" in response.data)
		self.assertTrue("user_type" in response.data)
		self.assertTrue("access" in response.data['tokens'])
		self.assertTrue("refresh" in response.data['tokens'])
		self.assertGreater(len(response.data['tokens']['access']), 20)
		self.assertGreater(len(response.data['tokens']['refresh']), 20)

		self.assertGreater(len(Teacher.objects.all()), 1)
		user = User.objects.get(username=self.TEACHER_USERNAME + "foo")
		self.assertTrue(Profile.objects.get(user=user).type == 'Teacher')
		self.assertGreater(len(SecretKey.objects.filter(user=user)), 0)

	def test_user_judge_registration(self) :
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "judge", "password" : self.ADMIN_PASSWORD, "secret_key" : self.ADMIN_JUDGE_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 201)
		self.assertTrue("username" in response.data)
		self.assertTrue("tokens" in response.data)
		self.assertTrue("user_type" in response.data)
		self.assertTrue("access" in response.data['tokens'])
		self.assertTrue("refresh" in response.data['tokens'])
		self.assertGreater(len(response.data['tokens']['access']), 20)
		self.assertGreater(len(response.data['tokens']['refresh']), 20)

		self.assertGreaterEqual(len(Judge.objects.all()), 1)
		user = User.objects.get(username="judge")
		self.assertTrue(Profile.objects.get(user=user).type == 'Judge')

	def test_user_team_registration(self) :
		response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "bar", "password" : self.ADMIN_PASSWORD, "secret_key" : self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 201)
		self.assertTrue("username" in response.data)
		self.assertTrue("tokens" in response.data)
		self.assertTrue("user_type" in response.data)
		self.assertTrue("access" in response.data['tokens'])
		self.assertTrue("refresh" in response.data['tokens'])
		self.assertGreater(len(response.data['tokens']['access']), 20)
		self.assertGreater(len(response.data['tokens']['refresh']), 20)

		self.assertGreaterEqual(len(Team.objects.all()), 1)
		user = User.objects.get(username="bar")
		self.assertTrue(Profile.objects.get(user=user).type == 'Team')


class UserLoginTestCase(TestCase) :
	def setUp(self):
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEACHER_USERNAME = 'teacher'
		self.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME, "password" : self.TEACHER_PASSWORD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEST_USERNAME = "foo"
		self.TEST_PASSWORD = "gdsakdsja678687&^*"
		self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEST_USERNAME, "password" : self.TEST_PASSWORD, "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))

	def test_user_login_unsuccessful_incorrect_password(self) :
		response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEST_USERNAME, "password" : self.TEST_PASSWORD+"1"}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 401)
		self.assertEqual(response.data['message'], "Incorrect authentication details supplied, please ensure that the correct password was entered")

	def test_user_login_unsuccessful_incorrect_username(self) :
		response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEST_USERNAME+'1', "password" : self.TEST_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 401)
		self.assertEqual(response.data['message'], "User with supplied username does not exist, please register before logging in")

	def test_user_login_unsuccessful_incorrect_username_and_password(self) :
		response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEST_USERNAME+'1', "password" : self.TEST_PASSWORD+"1"}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 401)
		self.assertEqual(response.data['message'], "User with supplied username does not exist, please register before logging in")

	def test_user_login_successful(self) :
		response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEST_USERNAME, "password" : self.TEST_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)
		self.assertTrue("username" in response.data)
		self.assertTrue("tokens" in response.data)
		self.assertTrue("user_type" in response.data)
		self.assertTrue("access" in response.data['tokens'])
		self.assertTrue("refresh" in response.data['tokens'])
		self.assertGreater(len(response.data['tokens']['access']), 20)
		self.assertGreater(len(response.data['tokens']['refresh']), 20)

	def test_user_login_incorrect_methods(self):
		response = self.client.post(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEST_USERNAME, "password" : self.TEST_PASSWORD, "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 405)

		response = self.client.get(API_NAMESPACE + '/auth/login/', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 405)

		response = self.client.delete(API_NAMESPACE + '/auth/login/', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 405)


class FrontendTemplateTestCase(TestCase) :
	def setUp(self) :
		self.client = Client()

	def test_home_view(self):
		url = reverse('frontend-home')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_marketplace_view(self):
		url = reverse('frontend-marketplace-specific', args=[SUPERUSER_USERNAME])
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_marketplace_specific_view(self):
		url = reverse('frontend-marketplace')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_login_view(self):
		url = reverse('frontend-login')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_schools_view(self):
		url = reverse('frontend-schools')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_teams_view(self):
		url = reverse('frontend-teams')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_glossary_view(self):
		url = reverse('frontend-glossary')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_settings_view(self):
		url = reverse('frontend-settings')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_settings_deadlines_view(self):
		url = reverse('frontend-settings-deadlines')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_judges_view(self):
		url = reverse('frontend-judges')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_teachers_view(self):
		url = reverse('frontend-teachers')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_decisions_view(self):
		url = reverse('frontend-decisions')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_issues_view(self):
		url = reverse('frontend-issues')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')

	def test_marked_view(self):
		url = reverse('frontend-marked')
		response = self.client.get(url)
		self.assertEqual(response.status_code, 200)
		self.assertTemplateUsed(response, 'index.html')
		self.assertContains(response, 'YE Scotland')


class UserMilestonesTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.ADMIN_2_USERNAME = 'admin2'
		cls.ADMIN_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_2_credentials = register_user(c, cls.ADMIN_2_USERNAME, cls.ADMIN_2_PASSWORD, cls.superuser_secret_key)
		cls.admin_2_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_2_credentials)
		cls.admin_2_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_2_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_2_teacher_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.submission_1 = create_submission(cls.TEAM_USERNAME)
		cls.submission_1.save()

		cls.TEAM_2_USERNAME = "test_team_2"
		cls.TEAM_2_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2_credentials = register_user(c, cls.TEAM_2_USERNAME, cls.TEAM_2_PASSWORD, cls.teacher_secret_key)

		cls.submission_2 = create_submission(cls.TEAM_2_USERNAME)
		cls.submission_2.save()

		cls.TEAM_3_USERNAME = "test_team_3"
		cls.TEAM_3_PASSWORD = "gdsakdsja678687&^*"
		cls.team_3_credentials = register_user(c, cls.TEAM_3_USERNAME, cls.TEAM_3_PASSWORD, cls.teacher_2_secret_key)

		cls.submission_3 = create_submission(cls.TEAM_3_USERNAME)
		cls.submission_3.save()

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

	def test_team_return_shape(self) :
		self.current_credentials = self.team_credentials
		
		rest_required_keys = ["milestones", "percentage"]
		milestone_required_keys = ["completed", "current", "name", "tasks"]
		task_required_keys = ["completed", "text", "link"]

		response = self.client.get(API_NAMESPACE + '/user/milestones', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 200)

		for req_key in rest_required_keys : 
			self.assertIn(req_key, response.data)

		self.assertGreater(len(response.data["milestones"]), 0)
		for m in response.data["milestones"] :
			for req_key in milestone_required_keys : 
				self.assertIn(req_key, m)

			self.assertGreater(len(m["tasks"]), 0)
			for t in m["tasks"] :
				for req_key in task_required_keys :
					self.assertIn(req_key, t)

	def test_teacher_return_shape(self) :
		self.current_credentials = self.teacher_credentials
		
		rest_required_keys = ["milestones", "percentage"]
		milestone_required_keys = ["completed", "current", "name", "tasks"]
		task_required_keys = ["completed", "text", "link"]

		response = self.client.get(API_NAMESPACE + '/user/milestones', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 200)

		self.assertIn('team_milestones', response.data)
		self.assertGreater(len(response.data["team_milestones"]), 0)
		for team in response.data['team_milestones'] :
			self.assertIn('milestones', team)
			self.assertGreater(len(team['milestones']), 0)

			for req_key in rest_required_keys : 
				self.assertIn(req_key, team)

			for m in team["milestones"] :
				for req_key in milestone_required_keys : 
					self.assertIn(req_key, m)

				self.assertGreater(len(m["tasks"]), 0)
				for t in m["tasks"] :
					for req_key in task_required_keys :
						self.assertIn(req_key, t)

		self.assertIn('team_names', response.data)
		self.assertIn('team_logo_stubs', response.data)
		self.assertEqual(len(response.data['team_names']), len(response.data['team_milestones']))
		self.assertEqual(len(response.data['team_logo_stubs']), len(response.data['team_names']))

		self.assertEqual([self.TEAM_USERNAME, self.TEAM_2_USERNAME], response.data['team_names'])
		self.assertIn('.png', response.data['team_logo_stubs'][0])

	def test_admin_fail_no_teacher(self) :
		response = self.client.get(API_NAMESPACE + '/user/milestones', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn('message', response.data)
		self.assertEqual('Please provide a teacher to recieve team milestones for', response.data['message'])

	def test_admin_return_shape(self) :
		self.submission_1.logo.delete() # to increase test coverage

		rest_required_keys = ["milestones", "percentage"]
		milestone_required_keys = ["completed", "current", "name", "tasks"]
		task_required_keys = ["completed", "text", "link"]

		response = self.client.get(API_NAMESPACE + '/user/milestones?teacher=' + self.TEACHER_USERNAME, **get_headers(self.admin_credentials))
		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)

		self.assertIn('team_milestones', response.data)
		self.assertGreater(len(response.data["team_milestones"]), 0)
		for team in response.data['team_milestones'] :
			self.assertIn('milestones', team)
			self.assertGreater(len(team['milestones']), 0)

			for req_key in rest_required_keys :
				self.assertIn(req_key, team)

			for m in team["milestones"] :
				for req_key in milestone_required_keys :
					self.assertIn(req_key, m)

				self.assertGreater(len(m["tasks"]), 0)
				for t in m["tasks"] :
					for req_key in task_required_keys :
						self.assertIn(req_key, t)

		self.assertIn('team_names', response.data)
		self.assertIn('team_logo_stubs', response.data)
		self.assertEqual(len(response.data['team_names']), len(response.data['team_milestones']))
		self.assertEqual(len(response.data['team_logo_stubs']), len(response.data['team_names']))

		self.assertEqual([self.TEAM_USERNAME, self.TEAM_2_USERNAME], response.data['team_names'])
		self.assertEqual('', response.data['team_logo_stubs'][0])

	def test_admin_return_shape_2(self) :
		rest_required_keys = ["milestones", "percentage"]
		milestone_required_keys = ["completed", "current", "name", "tasks"]
		task_required_keys = ["completed", "text", "link"]

		response = self.client.get(API_NAMESPACE + '/user/milestones?teacher=' + self.TEACHER_2_USERNAME, **get_headers(self.admin_2_credentials))
		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)

		self.assertIn('team_milestones', response.data)
		self.assertGreater(len(response.data["team_milestones"]), 0)
		for team in response.data['team_milestones'] :
			self.assertIn('milestones', team)
			self.assertGreater(len(team['milestones']), 0)

			for req_key in rest_required_keys :
				self.assertIn(req_key, team)

			for m in team["milestones"] :
				for req_key in milestone_required_keys :
					self.assertIn(req_key, m)

				self.assertGreater(len(m["tasks"]), 0)
				for t in m["tasks"] :
					for req_key in task_required_keys :
						self.assertIn(req_key, t)

		self.assertIn('team_names', response.data)
		self.assertIn('team_logo_stubs', response.data)
		self.assertEqual(len(response.data['team_names']), len(response.data['team_milestones']))
		self.assertEqual(len(response.data['team_logo_stubs']), len(response.data['team_names']))

		self.assertEqual([self.TEAM_3_USERNAME], response.data['team_names'])
		self.assertIn('.png', response.data['team_logo_stubs'][0])

	def test_admin_not_a_user(self) :
		response = self.client.get(API_NAMESPACE + '/user/milestones?teacher=' + self.TEAM_USERNAME + "sda", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Teacher must exist for team's milestones to be returned", response.data['message'])

	def test_admin_not_a_teacher(self) :
		response = self.client.get(API_NAMESPACE + '/user/milestones?teacher=' + self.TEAM_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Specified user must be a teacher to return milestones for their teams", response.data['message'])

	def test_admin_not_an_admin_for(self) :
		response = self.client.get(API_NAMESPACE + '/user/milestones?teacher=' + self.TEACHER_2_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot retrieve team's milestones for a teacher that you are not the administrator for", response.data['message'])

	def test_unauthorized_bad_token(self) :
		self.current_credentials = 'foo'
		
		response = self.client.get(API_NAMESPACE + '/user/milestones', **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 401)
		self.assertEqual(response.data['detail'], 'Given token not valid for any token type')

	def test_no_milestones_for_judge(self) :
		response = self.client.get(API_NAMESPACE + '/user/milestones', **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot retrieve milestones for type Judge", response.data['message'])


class TeachersTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.ADMIN_2_USERNAME = 'admin2'
		cls.ADMIN_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_2_credentials = register_user(c, cls.ADMIN_2_USERNAME, cls.ADMIN_2_PASSWORD, cls.superuser_secret_key)
		cls.admin_2_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_2_credentials)
		cls.admin_2_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_2_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEACHER_3_USERNAME = 'teacher3'
		cls.TEACHER_3_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_3_credentials = register_user(c, cls.TEACHER_3_USERNAME, cls.TEACHER_3_PASSWORD, cls.admin_2_teacher_secret_key)
		cls.teacher_3_secret_key = get_secret_key(c, 'Team', cls.teacher_3_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.submission_1 = create_submission(cls.TEAM_USERNAME)
		cls.submission_1.save()

		cls.TEAM_2_USERNAME = "test_team_2"
		cls.TEAM_2_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2_credentials = register_user(c, cls.TEAM_2_USERNAME, cls.TEAM_2_PASSWORD, cls.teacher_secret_key)

		cls.submission_2 = create_submission(cls.TEAM_2_USERNAME)
		cls.submission_2.save()

		cls.TEAM_3_USERNAME = "test_team_3"
		cls.TEAM_3_PASSWORD = "gdsakdsja678687&^*"
		cls.team_3_credentials = register_user(c, cls.TEAM_3_USERNAME, cls.TEAM_3_PASSWORD, cls.teacher_2_secret_key)

		cls.submission_3 = create_submission(cls.TEAM_3_USERNAME)
		cls.submission_3.save()

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

	def test_judge_no_access(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info", **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot return view of teachers as you are not an administrator for any", response.data['message'])

	def test_team_no_access(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info", **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot return view of teachers as you are not an administrator for any", response.data['message'])

	def test_teacher_no_access(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info", **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot return view of teachers as you are not an administrator for any", response.data['message'])

	def test_requires_authorisation(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info")

		self.assertEqual(401, response.status_code)

	def test_admin_return_shape(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("teacher_areas", response.data)
		self.assertIn("teacher_names", response.data)
		self.assertIn("teacher_schools", response.data)

		self.assertGreater(len(response.data['teacher_areas']), 0)
		self.assertEqual(len(response.data['teacher_names']), len(response.data['teacher_areas']))
		self.assertEqual(len(response.data['teacher_schools']), len(response.data['teacher_names']))

		teachers = [self.TEACHER_USERNAME, self.TEACHER_2_USERNAME]
		self.assertEqual(len(teachers), len(response.data['teacher_areas']))

		for teacher in teachers :
			obj = Teacher.objects.get(user__username=teacher)

			self.assertIn(teacher, response.data['teacher_names'])
			index = response.data['teacher_names'].index(teacher)
			self.assertEqual(obj.school, response.data['teacher_schools'][index])
			self.assertEqual(obj.area, response.data['teacher_areas'][index])

	def test_admin_return_different_len_different_admin(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info", **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("teacher_areas", response.data)
		self.assertIn("teacher_names", response.data)
		self.assertIn("teacher_schools", response.data)

		self.assertGreater(len(response.data['teacher_areas']), 0)
		self.assertEqual(len(response.data['teacher_names']), len(response.data['teacher_areas']))
		self.assertEqual(len(response.data['teacher_schools']), len(response.data['teacher_names']))

		teachers = [self.TEACHER_3_USERNAME]
		self.assertEqual(len(teachers), len(response.data['teacher_areas']))

		for teacher in teachers :
			obj = Teacher.objects.get(user__username=teacher)

			self.assertIn(teacher, response.data['teacher_names'])
			index = response.data['teacher_names'].index(teacher)
			self.assertEqual(obj.school, response.data['teacher_schools'][index])
			self.assertEqual(obj.area, response.data['teacher_areas'][index])

	def test_superuser_return_all(self):
		response = self.client.get(API_NAMESPACE + "/teacher_info", **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("teacher_areas", response.data)
		self.assertIn("teacher_names", response.data)
		self.assertIn("teacher_schools", response.data)

		self.assertGreater(len(response.data['teacher_areas']), 0)
		self.assertEqual(len(response.data['teacher_names']), len(response.data['teacher_areas']))
		self.assertEqual(len(response.data['teacher_schools']), len(response.data['teacher_names']))

		teachers = [self.TEACHER_USERNAME, self.TEACHER_2_USERNAME, self.TEACHER_3_USERNAME]
		self.assertEqual(len(teachers), len(response.data['teacher_areas']))

		for teacher in teachers :
			obj = Teacher.objects.get(user__username=teacher)

			self.assertIn(teacher, response.data['teacher_names'])
			index = response.data['teacher_names'].index(teacher)
			self.assertEqual(obj.school, response.data['teacher_schools'][index])
			self.assertEqual(obj.area, response.data['teacher_areas'][index])


class UserMarketplaceSpecificTestCase(TestCase) :
	def setUp(self) :
		self.client = Client()
		
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'

		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		self.ADMIN_CREDENTIALS = self.current_credentials
		admin_teacher_secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_TEACHER_SECRET_KEY = admin_teacher_secret_key_response.data['secret_key']
		admin_judge_secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type": "Judge", "areas": ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_JUDGE_SECRET_KEY = admin_judge_secret_key_response.data['secret_key']

		self.JUDGE_USERNAME = 'judge'
		self.JUDGE_PASSWORD = 'dbshadHGUDBJH789&*'
		judge_register_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username": self.JUDGE_USERNAME, "password": self.JUDGE_PASSWORD, "secret_key": self.ADMIN_JUDGE_SECRET_KEY}), content_type="application/json", **get_headers(self.ADMIN_CREDENTIALS))
		self.JUDGE_CREDENTIALS = judge_register_response.data["tokens"]["access"]

		self.TEACHER_USERNAME = 'teacher'
		self.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME, "password" : self.TEACHER_PASSWORD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		self.TEACHER_CREDENTIALS = self.current_credentials
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEACHER2_USERNAME = 'teacher2'
		self.TEACHER2_PASSWORD = 'dbshadHGUDBJH789&*'
		teacher2_register_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER2_USERNAME, "password" : self.TEACHER2_PASSWORD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.ADMIN_CREDENTIALS))
		self.TEACHER2_CREDENTIALS = teacher2_register_response.data["tokens"]["access"]

		self.TEST_USERNAME = "foo"
		self.TEST_PASSWORD = "gdsakdsja678687&^*"

		self.TEST_USERNAME_NONE = "bar"
		self.TEST_PASSWORD_NONE = "gdsakdsja678687&^*"
		
		self.good_tokens = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEST_USERNAME, "password" : self.TEST_PASSWORD, "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.none_tokens = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEST_USERNAME_NONE, "password" : self.TEST_PASSWORD_NONE, "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))

		self.TEST_CREDENTIALS = self.good_tokens.data["tokens"]["access"]
		self.INVALID_USERNAME = 'invalid_username'

		with open("testdata/picture1.png", 'rb') as f:
			self.picture1_str = str(base64.b64encode(f.read()), "utf-8")
			self.picture1_send = AXIOS_FILE_DATA_PREFIX + self.picture1_str
		with open("testdata/picture2.png", 'rb') as f:
			self.picture2_str = str(base64.b64encode(f.read()), "utf-8")
			self.picture2_send = AXIOS_FILE_DATA_PREFIX + self.picture2_str

		item_media_root = MEDIA_ROOT + "/"
		if not os.path.exists(item_media_root + "logos/"):
			os.mkdir(item_media_root + "logos/")
		if not os.path.exists(item_media_root + "posters/"):
			os.mkdir(item_media_root + "posters/")
		if not os.path.exists(item_media_root + "target_customers/"):
			os.mkdir(item_media_root + "target_customers/")

		with open(item_media_root + "logos/" + self.TEST_USERNAME + ".png" , "wb") as f :
			f.write(base64.b64decode(self.picture1_str))
		with open(item_media_root + "posters/" + self.TEST_USERNAME + ".png" , "wb") as f :
			f.write(base64.b64decode(self.picture1_str))
		with open(item_media_root + "target_customers/" + self.TEST_USERNAME + ".png" , "wb") as f :
			f.write(base64.b64decode(self.picture1_str))

		self.s3 = boto3.client('s3')

		create_active_submission_deadline()

	def test_get_return_shape_files_exist_but_not_values(self) :
		self.current_credentials = self.good_tokens.data['tokens']['access']

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 400)
		self.assertIn("message", response.data)
		self.assertEqual(response.data["message"], "No marketplace stall has been submitted yet")

	def test_post_return_shape(self) :
		self.current_credentials = self.good_tokens.data['tokens']['access']
		
		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture1_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys : 
			self.assertIn(req_key, response.data)

	def test_put_creates_image(self) :
		self.current_credentials = self.none_tokens.data['tokens']['access']
		
		item_media_root = MEDIA_ROOT + "/logos/"
		if not os.path.exists(item_media_root):
			os.mkdir(item_media_root)
		for l in os.listdir(item_media_root) :
			if l.startswith(self.TEST_USERNAME) or l.startswith(self.TEST_USERNAME_NONE) :
				os.remove(item_media_root + '/' + l)

		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture1_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		original_length_files = len(os.listdir(item_media_root))
		original_uploaded_files = len(UploadedFile.objects.all())
		if S3_BUCKET != None :
			original_length_s3 = len(self.s3.list_objects(Bucket=S3_BUCKET, Prefix="logos/")['Contents'])
		
		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME_NONE, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys : 
			self.assertIn(req_key, response.data)

		self.assertIn(self.TEST_USERNAME_NONE + ".png", os.listdir(item_media_root))
		self.assertEqual(original_length_files + 1, len(os.listdir(item_media_root)))
		if S3_BUCKET != None :
			objects = self.s3.list_objects(Bucket=S3_BUCKET, Prefix="logos/")['Contents']
			self.assertEqual(original_length_s3 + 1, len(objects))
			
			isInS3 = False
			index = None
			for i, f in enumerate(objects) :
				if self.TEST_USERNAME_NONE in f['Key'] :
					isInS3 = True
					index = i
					break
			self.assertTrue(isInS3)
			self.assertEqual(len(base64.b64decode(self.picture1_str)), objects[index]['Size'])

		self.assertEqual(original_uploaded_files + 1, len(UploadedFile.objects.all()))

	def test_put_overwrites_existing_image(self) :
		self.current_credentials = self.none_tokens.data['tokens']['access']
		
		item_media_root = MEDIA_ROOT + "/logos"
		with open(item_media_root + "/" + self.TEST_USERNAME_NONE + ".jpeg" , "wb") as f :
			f.write(base64.b64decode(self.picture1_str))

		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture2_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME_NONE, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys : 
			self.assertIn(req_key, response.data)
		
		self.assertIn(self.TEST_USERNAME_NONE + ".png", os.listdir(item_media_root))
		with open(item_media_root + "/" + self.TEST_USERNAME_NONE + ".png", "rb") as f :
			self.assertEqual(base64.b64decode(self.picture2_str), f.read())

		if S3_BUCKET != None :
			objects = self.s3.list_objects(Bucket=S3_BUCKET, Prefix="logos/")['Contents']
			
			isInS3 = False
			index = None
			for i, f in enumerate(objects) :
				if self.TEST_USERNAME_NONE in f['Key'] :
					isInS3 = True
					index = i
					break
			self.assertTrue(isInS3)
			self.assertEqual(len(base64.b64decode(self.picture2_str)), objects[index]['Size'])

	def test_put_overwrites_existing_image_different_extension(self) :
		self.current_credentials = self.none_tokens.data['tokens']['access']
		
		item_media_root = MEDIA_ROOT + "/logos"
		with open(item_media_root + "/" + self.TEST_USERNAME_NONE + ".jpeg" , "wb") as f :
			f.write(base64.b64decode(self.picture1_str))

		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture2_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME_NONE, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys : 
			self.assertIn(req_key, response.data)
		
		self.assertIn(self.TEST_USERNAME_NONE + ".png", os.listdir(item_media_root))
		with open(item_media_root + "/" + self.TEST_USERNAME_NONE + ".png", "rb") as f :
			self.assertEqual(base64.b64decode(self.picture2_str), f.read())

		if S3_BUCKET != None :
			objects = self.s3.list_objects(Bucket=S3_BUCKET, Prefix="logos/")['Contents']
			
			isInS3 = False
			index = None
			for i, f in enumerate(objects) :
				if self.TEST_USERNAME_NONE in f['Key'] :
					isInS3 = True
					index = i
					break
			self.assertTrue(isInS3)
			self.assertEqual(len(base64.b64decode(self.picture2_str)), objects[index]['Size'])

	def test_put_rejects_bad_extension(self) :
		self.current_credentials = self.none_tokens.data['tokens']['access']
		
		item_media_root = MEDIA_ROOT + "/logos"
		if not os.path.exists(item_media_root):
			os.mkdir(item_media_root)
		for l in os.listdir(item_media_root) :
			if l.startswith(self.TEST_USERNAME) or l.startswith(self.TEST_USERNAME_NONE) :
				os.remove(item_media_root + '/' + l)

		original_length_files = len(os.listdir(item_media_root))
		original_uploaded_files = len(UploadedFile.objects.all())
		if S3_BUCKET != None :
			original_length_s3 = len(self.s3.list_objects(Bucket=S3_BUCKET, Prefix="logos/")['Contents'])
		
		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture1_send, "encoded_logo_extension": "exe", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME_NONE, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 403)
		self.assertIn("message", response.data)
		self.assertEqual(response.data["message"], "Unnacceptable file type supplied for logo")

		self.assertEqual(original_length_files, len(os.listdir(item_media_root)))
		if S3_BUCKET != None :
			objects = self.s3.list_objects(Bucket=S3_BUCKET, Prefix="logos/")['Contents']
			self.assertEqual(original_length_s3, len(objects))

		self.assertEqual(original_uploaded_files, len(UploadedFile.objects.all()))

	def test_put_rejects_if_submission_blacklisted(self):
		# create blacklisted submission for team
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.is_blacklisted = True
		submission.save()

		# attempt to update logo
		self.current_credentials = self.good_tokens.data['tokens']['access']
		data = {"tagline": "sda", "company_name": "dbsa", "encoded_logo": self.picture1_send,
				"encoded_logo_extension": "png", "encoded_poster": self.picture1_send,
				"encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send,
				"encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type": "image/png",
				"encoded_poster_mime_type": "image/png", "encoded_target_customer_mime_type": "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(response.data["message"], "Cannot save marketplace submission as stall is currently under review by your teacher")

	def test_get_gives_error_on_access_by_other_team_when_blacklisted(self):
		# create blacklisted submission for team
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.is_blacklisted = True
		submission.save()

		create_active_voting_deadline()

		# attempt to get as another user
		self.current_credentials = self.none_tokens.data['tokens']['access']
		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(response.data["message"], "Cannot access the requested marketplace entry as it is under reviewal for unacceptable content")

	def test_delete_teacher_their_own_teams_submission_succeeds(self):
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		# attempt to delete it as teacher(1)
		response = self.client.delete(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.TEACHER_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertEqual(response.data["message"], "Successfully deleted the marketplace stall")
		self.assertEqual(len(Submission.objects.filter(team=team)), 0)

	def test_delete_teacher_someone_elses_teams_submission_fails(self):
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		# attempt to delete it as teacher2
		response = self.client.delete(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.TEACHER2_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(response.data["message"], "Teacher can only delete their own students submissions")
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

	def test_delete_teacher_invalid_username_fails(self):
		response = self.client.delete(API_NAMESPACE + '/marketplace/' + self.INVALID_USERNAME, **get_headers(self.TEACHER_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)
		self.assertEqual(response.data["message"], "Error encountered while deleting the marketplace submission, please try logging out and logging in again")

	def test_delete_admin_succeeds(self):
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		# attempt to delete it as teacher(1)
		response = self.client.delete(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.ADMIN_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertEqual(response.data["message"], "Successfully deleted the marketplace stall")
		self.assertEqual(len(Submission.objects.filter(team=team)), 0)

	def test_delete_judge_fails(self):
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		# attempt to delete it as judge
		response = self.client.delete(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.JUDGE_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(response.data["message"], "Non-teacher or admin users cannot delete marketplace profiles")
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

	def test_delete_team_fails(self):
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		# attempt to delete it as judge
		response = self.client.delete(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.TEST_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(response.data["message"], "Non-teacher or admin users cannot delete marketplace profiles")
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

	def test_submission_deadline_passed_judging_update(self) :
		self.current_credentials = self.good_tokens.data['tokens']['access']

		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture1_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_judging_deadline()

		response2 = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response2.status_code)
		self.assertIn("message", response2.data)
		self.assertEqual("Cannot make changes to your submission after the submission deadline has passed", response2.data['message'])

	def test_submission_deadline_passed_voting_update(self) :
		self.current_credentials = self.good_tokens.data['tokens']['access']

		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture1_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_voting_deadline()

		response2 = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response2.status_code)
		self.assertIn("message", response2.data)
		self.assertEqual("Cannot make changes to your submission after the submission deadline has passed", response2.data['message'])

	def test_submission_deadline_passed_all_update(self) :
		self.current_credentials = self.good_tokens.data['tokens']['access']

		data = {"tagline":"sda", "company_name": "dbsa", "encoded_logo": self.picture1_send, "encoded_logo_extension": "png", "encoded_poster": self.picture1_send, "encoded_poster_extension": "png", "encoded_target_customer": self.picture1_send, "encoded_target_customer_extension": "png", "manifest": ["logo"], "encoded_logo_mime_type" : "image/png", "encoded_poster_mime_type" : "image/png", "encoded_target_customer_mime_type" : "image/png"}

		response = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, 202)

		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_passed_deadline()

		response2 = self.client.put(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, json.dumps(data), content_type="application/json", **get_headers(self.current_credentials))
		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response2.status_code)
		self.assertIn("message", response2.data)
		self.assertEqual("Cannot make changes to your submission after the submission deadline has passed", response2.data['message'])

	def test_same_team_access_whenever(self) :
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		self.current_credentials = self.good_tokens.data['tokens']['access']

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_voting_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_judging_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_passed_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

	def test_different_team_access_only_voting(self) :
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		self.current_credentials = self.none_tokens.data['tokens']['access']

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view other team's marketplace stalls until the marketplace opens, or after the marketplace closes", response.data['message'])

		create_active_voting_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_judging_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view other team's marketplace stalls until the marketplace opens, or after the marketplace closes", response.data['message'])

		create_passed_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view other team's marketplace stalls until the marketplace opens, or after the marketplace closes", response.data['message'])

	def test_same_teacher_access_whenever(self) :
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		self.current_credentials = self.TEACHER_CREDENTIALS

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_voting_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_judging_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_passed_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

	def test_different_teacher_access_only_voting(self) :
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		self.current_credentials = self.TEACHER2_CREDENTIALS

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view another teacher's student's stalls until the marketplace opens, or after the marketplace has closed", response.data['message'])

		create_active_voting_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_active_judging_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view another teacher's student's stalls until the marketplace opens, or after the marketplace has closed", response.data['message'])

		create_passed_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view another teacher's student's stalls until the marketplace opens, or after the marketplace has closed", response.data['message'])

	def test_judge_access_only_judging(self) :
		# create a submission for the 'test' team (which belongs to teacher(1))
		user = User.objects.get(username=self.TEST_USERNAME)
		team = Team.objects.get(user=user)
		submission, _ = Submission.objects.get_or_create(team=team)
		submission.save()
		self.assertEqual(len(Submission.objects.filter(team=team)), 1)

		self.current_credentials = self.JUDGE_CREDENTIALS

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view team's marketplace stalls until judge voting is opened, or after judge voting closes", response.data['message'])

		create_active_voting_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view team's marketplace stalls until judge voting is opened, or after judge voting closes", response.data['message'])

		create_active_judging_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertIn(req_key, response.data)

		create_passed_deadline()

		response = self.client.get(API_NAMESPACE + '/marketplace/' + self.TEST_USERNAME, **get_headers(self.current_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		rest_required_keys = ["logo_stub", "tagline", "company_name", "poster_stub", "target_customer_stub", "team_username"]
		for req_key in rest_required_keys :
			self.assertNotIn(req_key, response.data)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot view team's marketplace stalls until judge voting is opened, or after judge voting closes", response.data['message'])

	def tearDown(self) :
		item_media_root = MEDIA_ROOT + "/logos"
		if os.path.exists(item_media_root):
			for l in os.listdir(item_media_root) :
				if l.startswith(self.TEST_USERNAME) or l.startswith(self.TEST_USERNAME_NONE) :
					os.remove(item_media_root + '/' + l)

					# delete S3
					if S3_BUCKET != None :
						s3 = boto3.client('s3')

						s3.delete_object(
							Bucket=S3_BUCKET,
							Key=("logos/" + l)
						)
			
		item_media_root = MEDIA_ROOT + "/posters"
		if os.path.exists(item_media_root):
			for l in os.listdir(item_media_root) :
				if l.startswith(self.TEST_USERNAME) or l.startswith(self.TEST_USERNAME_NONE) :
					os.remove(item_media_root + '/' + l)
			
					# delete S3
					if S3_BUCKET != None :
						s3 = boto3.client('s3')

						s3.delete_object(
							Bucket=S3_BUCKET,
							Key=("posters/" + l)
						)
			
		item_media_root = MEDIA_ROOT + "/target_customers"
		if os.path.exists(item_media_root):
			for l in os.listdir(item_media_root) :
				if l.startswith(self.TEST_USERNAME) or l.startswith(self.TEST_USERNAME_NONE) :
					os.remove(item_media_root + '/' + l)
			
					# delete S3
					if S3_BUCKET != None :
						s3 = boto3.client('s3')

						s3.delete_object(
							Bucket=S3_BUCKET,
							Key=("target_customers/" + l)
						)
			

class CreateSecretKeyTestCase(TestCase) :
	def setUp(self) :
		self.client = Client()
		
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_TEACHER_SECRET_KEY = secret_key_response.data['secret_key']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Judge", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_JUDGE_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEACHER_USERNAME = 'teacher'
		self.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME, "password" : self.TEACHER_PASSWORD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

	def test_incorrect_authentication_prevention(self) :
		self.current_credentials = 'foo'
		response = self.client.post(API_NAMESPACE + '/auth/secret_key', content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)
		self.assertEqual(response.data['detail'], 'Given token not valid for any token type')

	def test_admin_key_creation(self) :
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_2xx.CREATED.value)
		self.assertIn('secret_key', secret_key_response.data)
		self.assertIn('key_type', secret_key_response.data)
		self.assertIn('areas', secret_key_response.data)
		self.assertIn('school', secret_key_response.data)

		self.assertEqual("Admin", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).type)
		self.assertEqual([], SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).areas)
		self.assertEqual("", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).school)
		self.assertEqual(User.objects.get(username=SUPERUSER_USERNAME), SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).user)

	def test_teacher_key_creation_not_enough_information(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher"}), content_type='application/json', **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn('message', secret_key_response.data)
		self.assertEqual("Error with creating key, not enough information supplied, please try again after making the necessary selections", secret_key_response.data['message'])

	def test_teacher_key_creation_populated(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "school" : "Bellahouston Academy", "areas" : ["Glasgow City"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_2xx.CREATED.value)
		self.assertIn('secret_key', secret_key_response.data)
		self.assertIn('key_type', secret_key_response.data)
		self.assertIn('areas', secret_key_response.data)
		self.assertIn('school', secret_key_response.data)
		
		self.assertEqual("Teacher", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).type)
		self.assertEqual(["Glasgow City"], SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).areas)
		self.assertEqual("Bellahouston Academy", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).school)
		self.assertEqual(User.objects.get(username=self.ADMIN_USERNAME), SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).user)

	def test_teacher_key_creation_populated_wrong_area(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "school" : "Bellahouston Academy", "areas" : ["East Renfrewshire"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertNotIn('secret_key', secret_key_response.data)
		self.assertNotIn('key_type', secret_key_response.data)
		self.assertNotIn('areas', secret_key_response.data)
		self.assertNotIn('school', secret_key_response.data)

		self.assertIn("message", secret_key_response.data)
		self.assertEqual("Bellahouston Academy does not exist in East Renfrewshire. Please make sure to select the correct local authority and school", secret_key_response.data['message'])

	def test_teacher_key_creation_populated_bad_area(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "school" : "Bellahouston Academy", "areas" : ["East Renfrewshi"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertNotIn('secret_key', secret_key_response.data)
		self.assertNotIn('key_type', secret_key_response.data)
		self.assertNotIn('areas', secret_key_response.data)
		self.assertNotIn('school', secret_key_response.data)

		self.assertIn("message", secret_key_response.data)
		self.assertEqual("East Renfrewshi local authority does not exist. Please make sure to select the correct local authority and school for Teacher keys", secret_key_response.data['message'])

	def test_teacher_key_creation_populated_bad_school(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "school" : "Bellahousto Academy", "areas" : ["Glasgow City"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertNotIn('secret_key', secret_key_response.data)
		self.assertNotIn('key_type', secret_key_response.data)
		self.assertNotIn('areas', secret_key_response.data)
		self.assertNotIn('school', secret_key_response.data)

		self.assertIn("message", secret_key_response.data)
		self.assertEqual("Bellahousto Academy does not exist in Glasgow City. Please make sure to select the correct local authority and school", secret_key_response.data['message'])

	def test_admin_key_creation_unauthorized(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Admin", "school" : "Bellahouston Academy", "areas" : ["Glasgow City"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertNotIn('secret_key', secret_key_response.data)
		self.assertNotIn('key_type', secret_key_response.data)
		self.assertNotIn('areas', secret_key_response.data)
		self.assertNotIn('school', secret_key_response.data)

		self.assertIn('message', secret_key_response.data)

		self.assertEqual("Admins can create teacher or judge accounts, please choose one of these options to create for", secret_key_response.data['message'])

	def test_judge_key_creation_unpopulated(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Judge", "school" : "", "areas" : []}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertNotIn('secret_key', secret_key_response.data)
		self.assertNotIn('key_type', secret_key_response.data)
		self.assertNotIn('areas', secret_key_response.data)
		self.assertNotIn('school', secret_key_response.data)

		self.assertIn("message", secret_key_response.data)
		self.assertEqual("Judge must be allocated a correct local authority to judge", secret_key_response.data['message'])
		
	def test_judge_key_creation_populated(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Judge", "school" : "", "areas" : ["Glasgow City"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_2xx.CREATED.value)
		self.assertIn('secret_key', secret_key_response.data)
		self.assertIn('key_type', secret_key_response.data)
		self.assertIn('areas', secret_key_response.data)
		self.assertIn('school', secret_key_response.data)

		self.assertEqual("Judge", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).type)
		self.assertEqual(["Glasgow City"], SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).areas)
		self.assertEqual("", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).school)
		self.assertEqual(User.objects.get(username=self.ADMIN_USERNAME), SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).user)

	def test_judge_key_creation_populated_ignore_school(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.ADMIN_USERNAME, "password": self.ADMIN_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))

		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Judge", "school" : "Bellahouston Academy", "areas" : ["Glasgow City"]}), content_type="application/json", **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_2xx.CREATED.value)
		self.assertIn('secret_key', secret_key_response.data)
		self.assertIn('key_type', secret_key_response.data)
		self.assertIn('areas', secret_key_response.data)
		self.assertIn('school', secret_key_response.data)
		
		self.assertEqual("Judge", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).type)
		self.assertEqual(["Glasgow City"], SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).areas)
		self.assertEqual("", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).school)
		self.assertEqual(User.objects.get(username=self.ADMIN_USERNAME), SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).user)

	def test_team_key_creation(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEACHER_USERNAME, "password" : self.TEACHER_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_2xx.CREATED.value)
		self.assertIn('secret_key', secret_key_response.data)
		self.assertIn('key_type', secret_key_response.data)
		self.assertIn('areas', secret_key_response.data)
		self.assertIn('school', secret_key_response.data)
		
		self.assertEqual("Team", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).type)
		self.assertEqual(["Glasgow City"], SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).areas)
		self.assertEqual("Bellahouston Academy", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).school)
		self.assertEqual(User.objects.get(username=self.TEACHER_USERNAME), SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).user)
		self.assertEqual(1, len(SecretKey.objects.filter(user=User.objects.get(username=self.TEACHER_USERNAME))))

	def test_team_key_creation_repeated(self) :
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : self.TEACHER_USERNAME, "password" : self.TEACHER_PASSWORD}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		previous_key = secret_key_response.data['secret_key']
		
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.assertEqual(previous_key, secret_key_response.data['secret_key'])
		previous_key = secret_key_response.data['secret_key']

		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.assertEqual(previous_key, secret_key_response.data['secret_key'])
		previous_key = secret_key_response.data['secret_key']

		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.assertEqual(previous_key, secret_key_response.data['secret_key'])
		previous_key = secret_key_response.data['secret_key']

		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.assertEqual(previous_key, secret_key_response.data['secret_key'])
		previous_key = secret_key_response.data['secret_key']

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_2xx.CREATED.value)
		self.assertIn('secret_key', secret_key_response.data)

		self.assertEqual("Team", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).type)
		self.assertEqual(["Glasgow City"], SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).areas)
		self.assertEqual("Bellahouston Academy", SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).school)
		self.assertEqual(User.objects.get(username=self.TEACHER_USERNAME), SecretKey.objects.get(key_text=secret_key_response.data['secret_key']).user)
		self.assertEqual(1, len(SecretKey.objects.filter(user=User.objects.get(username=self.TEACHER_USERNAME))))

	def test_team_key_creation_call_error(self) :
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "foo", "password" : "bdashjdJHVDHJS78^&*67", "secret_key" : self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertIn('message', secret_key_response.data)
		self.assertEqual("Team users cannot create secret keys", secret_key_response.data['message'])

	def test_judge_key_creation_call_error(self) :
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : "judge", "password" : "bdashjdJHVDHJS78^&*67", "secret_key" : self.ADMIN_JUDGE_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))

		self.assertEqual(secret_key_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertIn('message', secret_key_response.data)
		self.assertEqual("Judge users cannot create secret keys", secret_key_response.data['message'])


class SecretKeysGETTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

	def test_judge_secret_keys_none(self) :
		response = self.client.get(API_NAMESPACE + "/auth/secret_key", **get_headers(self.judge_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("keys", response.data)
		self.assertEqual(0, len(response.data['keys']))

	def test_unauthorized(self) :
		response = self.client.get(API_NAMESPACE + "/auth/secret_key")

		self.assertEqual(401, response.status_code)
		self.assertNotIn("keys", response.data)

	def test_teacher_secret_keys_one(self) :
		response = self.client.get(API_NAMESPACE + "/auth/secret_key", **get_headers(self.teacher_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("keys", response.data)
		self.assertEqual(1, len(response.data['keys']))

		required_key_keys = ['secret_key', 'school', 'areas', 'key_type']

		for required_key_key in required_key_keys :
			self.assertIn(required_key_key, response.data['keys'][0])

	def test_team_secret_keys_none(self) :
		response = self.client.get(API_NAMESPACE + "/auth/secret_key", **get_headers(self.team_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("keys", response.data)
		self.assertEqual(0, len(response.data['keys']))

	def test_admin_secret_keys_some(self) :
		response = self.client.get(API_NAMESPACE + "/auth/secret_key", **get_headers(self.admin_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("keys", response.data)
		self.assertEqual(3, len(response.data['keys'])) # 2 teacher and one judge

		required_key_keys = ['secret_key', 'school', 'areas', 'key_type']

		teacher_count = 0
		judge_count = 0

		for key in response.data['keys'] :
			for required_key_key in required_key_keys :
				self.assertIn(required_key_key, key)

			if key['key_type'] == "Teacher" :
				teacher_count += 1
			elif key['key_type'] == "Judge" :
				judge_count += 1

		self.assertEqual(2, teacher_count)
		self.assertEqual(1, judge_count)


class SecretKeySpecificDelete(TestCase):
	def setUp(self) :
		self.client = Client()
		
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.SUPERUSER_TOKEN = tokens_response.data['tokens']['access']
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME_1 = 'admin'
		self.ADMIN_PASSWORD_1 = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME_1, "password" : self.ADMIN_PASSWORD_1, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_ACCESS_1 = tokens_response.data['tokens']['access']
		self.current_credentials = tokens_response.data['tokens']['access']
		response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "school" : "Bellahouston Academy", "areas" : ["Glasgow City"]}), content_type="application/json", **get_headers(self.current_credentials))
		self.SECRET_KEY = response.data['secret_key']

		self.ADMIN_USERNAME_2 = 'admin2'
		self.ADMIN_PASSWORD_2 = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME_2, "password" : self.ADMIN_PASSWORD_2, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_ACCESS_2 = tokens_response.data['tokens']['access']
		self.current_credentials = tokens_response.data['tokens']['access']

	def test_super_user_delete(self) :
		self.current_credentials = self.SUPERUSER_TOKEN

		response = self.client.delete(API_NAMESPACE + '/auth/secret_key/' + self.SECRET_KEY, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

	def test_created_admin_delete(self):
		self.current_credentials = self.ADMIN_ACCESS_1

		response = self.client.delete(API_NAMESPACE + '/auth/secret_key/' + self.SECRET_KEY, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

	def test_did_not_created_admin_delete(self):
		self.current_credentials = self.ADMIN_ACCESS_2

		response = self.client.delete(API_NAMESPACE + '/auth/secret_key/' + self.SECRET_KEY, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn('message', response.data)
		self.assertEqual("You do not have enough priveleges to delete other user's keys", response.data['message'])

	
class InfoLocalAuthorities(TestCase) :
	def setUp(self):
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']

	def test_local_authorities_length(self) :
		response = self.client.get(API_NAMESPACE + "/info/local_authorities", **get_headers(self.current_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("local_authorities", response.data)
		self.assertEqual(34, len(response.data['local_authorities']))


class InfoLocalAuthoritiesSchools(TestCase) :
	def setUp(self): 
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		tokens_response = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']

		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']

	def test_check_full_length(self) :
		response = self.client.get(API_NAMESPACE + "/info/local_authorities/Unknown/schools", **get_headers(self.current_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("schools", response.data)
		self.assertEqual(448, len(response.data['schools']))

	def test_check_partial_length(self) :
		response = self.client.get(API_NAMESPACE + "/info/local_authorities/Glasgow%20City/schools", **get_headers(self.current_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("schools", response.data)
		self.assertLess(len(response.data['schools']), 448)

	def test_check_partial_length_multiple(self) :
		response = self.client.get(API_NAMESPACE + "/info/local_authorities/Glasgow%20City/schools", **get_headers(self.current_credentials))

		original_length = len(response.data['schools'])

		response = self.client.get(API_NAMESPACE + "/info/local_authorities/Glasgow%20City+East%20Renfrewshire/schools", **get_headers(self.current_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("schools", response.data)
		self.assertGreater(len(response.data['schools']), original_length)

	def test_check_full_length_with_other_authority(self) :
		response = self.client.get(API_NAMESPACE + "/info/local_authorities/Glasgow%20City+Unknown/schools", **get_headers(self.current_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("schools", response.data)
		self.assertEqual(448, len(response.data['schools']))

	def test_check_full_length_with_not_unknown(self) :
		all_schools = list(LOCAL_AUTHORITIES.keys())
		all_schools.remove("Unknown")
		response = self.client.get(API_NAMESPACE + "/info/local_authorities/" + "+".join(all_schools).replace(' ', '%20') + "/schools", **get_headers(self.current_credentials))

		self.assertEqual(200, response.status_code)
		self.assertIn("schools", response.data)
		self.assertEqual(448, len(response.data['schools']))

	def test_check_each_returns(self) :
		la_response = self.client.get(API_NAMESPACE + "/info/local_authorities", **get_headers(self.current_credentials))

		for la in la_response.data['local_authorities'] :
			la = la.replace(' ', '%20')
			url = API_NAMESPACE + "/info/local_authorities/" + la + "/schools"
			response = self.client.get(url, **get_headers(self.current_credentials))

			self.assertEqual(200, response.status_code)
			self.assertIn("schools", response.data)
			self.assertGreater(len(response.data['schools']), 0)

			for school in response.data['schools'] :
				self.assertIn("value", school)
				self.assertIn("local_authority", school)


class UserOperationsTestCase(TestCase) :
	def setUp(self):
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		self.SUPERUSER_TOKENS = self.client.put(API_NAMESPACE + '/auth/login/', json.dumps({"username" : SUPERUSER_USERNAME, "password" : SUPERUSER_PASSWORD}), content_type="application/json")
		self.current_credentials = self.SUPERUSER_TOKENS.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.SUPERUSER_SECRET_KEY = secret_key_response.data['secret_key']
		
		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		tokens_response = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.ADMIN_USERNAME, "password" : self.ADMIN_PASSWORD, "secret_key" : self.SUPERUSER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = tokens_response.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', json.dumps({"key_type" : "Teacher", "areas" : ["Glasgow City"], "school": "Bellahouston Academy"}), content_type="application/json", **get_headers(self.current_credentials))
		self.ADMIN_TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEACHER_USERNAME_GOOD = 'teacher'
		self.TEACHER_PASSWORD_GOOD = 'dbshadHGUDBJH789&*'
		self.TEACHER_TOKENS_GOOD = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME_GOOD, "password" : self.TEACHER_PASSWORD_GOOD, "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))
		self.current_credentials = self.TEACHER_TOKENS_GOOD.data['tokens']['access']
		secret_key_response = self.client.post(API_NAMESPACE + '/auth/secret_key', **get_headers(self.current_credentials))
		self.TEACHER_SECRET_KEY = secret_key_response.data['secret_key']

		self.TEACHER_TOKENS_BAD = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEACHER_USERNAME_GOOD + "foo", "password" : self.TEACHER_PASSWORD_GOOD + "foo", "secret_key" : self.ADMIN_TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))

		self.TEAM_USERNAME = "foo"
		self.TEAM_PASSWORD = "gdsakdsja678687&^*"

		self.TEAM_TOKENS = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEAM_USERNAME, "password" : self.TEAM_PASSWORD, "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))

		self.TEAM_USERNAME_2 = "foobar"
		_ = self.client.post(API_NAMESPACE + '/auth/register/', json.dumps({"username" : self.TEAM_USERNAME_2, "password" : self.TEAM_PASSWORD, "secret_key": self.TEACHER_SECRET_KEY}), content_type="application/json", **get_headers(self.current_credentials))

		submission = Submission.objects.create(team=Team.objects.get(user=User.objects.get(username=self.TEAM_USERNAME)))
		with open("testdata/picture1.png", 'rb') as f :
			buffer = f.read()
		submission.logo = UploadedFile.objects.create(
			team=submission.team,
			key="logo",
			media_file=ContentFile(buffer, name='temp.png'),
			mime_type="image/png",
			extension="png"
		)

		if S3_BUCKET != None :
			s3 = boto3.client('s3')

			s3.put_object(
				ACL='public-read',
				Bucket=S3_BUCKET,
				Key=submission.logo.path,
				ContentType=submission.logo.mime_type,
				Body=buffer
			)

			url = 'https://%s.s3.amazonaws.com/%s' % (S3_BUCKET, submission.logo.path)
			
			submission.logo.s3_url = url
			submission.logo.save()

		submission.save()

	def test_good_superuser_delete(self) :
		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(6, len(Profile.objects.all()))
		self.assertEqual(6, len(User.objects.all()))
		
		self.current_credentials = self.SUPERUSER_TOKENS.data['tokens']['access']
		
		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEAM_USERNAME, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(0, len(Submission.objects.all()))

		self.assertEqual(1, len(Team.objects.all()))

		self.assertEqual(5, len(Profile.objects.all()))
		
		self.assertEqual(5, len(User.objects.all()))
		self.assertEqual(1, len(User.objects.filter(username=self.TEAM_USERNAME_2)))
		# since no user, cannot check for submission or team objects, can assume correctly deleted
		self.assertEqual(0, len(User.objects.filter(username=self.TEAM_USERNAME)))

		# correctly deleted profile	
		self.assertEqual(1, len(Profile.objects.filter(user=User.objects.get(username=self.TEAM_USERNAME_2))))

	def test_good_teacher_delete(self) :
		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(6, len(Profile.objects.all()))
		self.assertEqual(6, len(User.objects.all()))
		
		self.current_credentials = self.TEACHER_TOKENS_GOOD.data['tokens']['access']
		
		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEAM_USERNAME, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(0, len(Submission.objects.all()))

		self.assertEqual(1, len(Team.objects.all()))

		self.assertEqual(5, len(Profile.objects.all()))
		
		self.assertEqual(5, len(User.objects.all()))
		self.assertEqual(1, len(User.objects.filter(username=self.TEAM_USERNAME_2)))
		# since no user, cannot check for submission or team objects, can assume correctly deleted
		self.assertEqual(0, len(User.objects.filter(username=self.TEAM_USERNAME)))

		# correctly deleted profile	
		self.assertEqual(1, len(Profile.objects.filter(user=User.objects.get(username=self.TEAM_USERNAME_2))))

	def test_not_my_teacher_team_delete(self) :
		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(6, len(Profile.objects.all()))
		self.assertEqual(6, len(User.objects.all()))
		
		self.current_credentials = self.TEACHER_TOKENS_BAD.data['tokens']['access']
		
		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEAM_USERNAME, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn('message', response.data)
		self.assertEqual('Teachers can only delete their own team users', response.data['message'])

		# check all still present
		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(6, len(Profile.objects.all()))
		self.assertEqual(6, len(User.objects.all()))

	def test_teacher_bad_user_delete(self) :
		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(6, len(Profile.objects.all()))
		self.assertEqual(6, len(User.objects.all()))
		
		self.current_credentials = self.TEACHER_TOKENS_BAD.data['tokens']['access']
		
		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.ADMIN_USERNAME, **get_headers(self.current_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn('message', response.data)
		self.assertEqual('Teachers can only delete team users', response.data['message'])

		# check all still present
		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(6, len(Profile.objects.all()))
		self.assertEqual(6, len(User.objects.all()))

	def tearDown(self) :
		item_media_root = MEDIA_ROOT + "/logos"
		if os.path.exists(item_media_root):
			for l in os.listdir(item_media_root) :
				if l.startswith(self.TEAM_USERNAME) or l.startswith(self.TEAM_USERNAME_2) :
					os.remove(item_media_root + '/' + l)

					# delete S3
					if S3_BUCKET != None :
						s3 = boto3.client('s3')

						s3.delete_object(
							Bucket=S3_BUCKET,
							Key=("logos/" + l)
						)
			
		item_media_root = MEDIA_ROOT + "/posters"
		if os.path.exists(item_media_root):
			for l in os.listdir(item_media_root) :
				if l.startswith(self.TEAM_USERNAME) or l.startswith(self.TEAM_USERNAME_2) :
					os.remove(item_media_root + '/' + l)
			
					# delete S3
					if S3_BUCKET != None :
						s3 = boto3.client('s3')

						s3.delete_object(
							Bucket=S3_BUCKET,
							Key=("posters/" + l)
						)
			
		item_media_root = MEDIA_ROOT + "/target_customers"
		if os.path.exists(item_media_root):
			for l in os.listdir(item_media_root) :
				if l.startswith(self.TEAM_USERNAME) or l.startswith(self.TEAM_USERNAME_2) :
					os.remove(item_media_root + '/' + l)
			
					# delete S3
					if S3_BUCKET != None :
						s3 = boto3.client('s3')

						s3.delete_object(
							Bucket=S3_BUCKET,
							Key=("target_customers/" + l)
						)


class UserOperationsAdminDELETETestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)
		cls.admin_judge_2_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials, areas=["East Renfrewshire"], school="")

		cls.ADMIN_2_USERNAME = 'admin2'
		cls.ADMIN_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_2_credentials = register_user(c, cls.ADMIN_2_USERNAME, cls.ADMIN_2_PASSWORD, cls.superuser_secret_key)
		cls.admin_2_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_2_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.submission_1 = create_submission(cls.TEAM_USERNAME)
		cls.submission_1.save()

		cls.TEAM_2_USERNAME = "test_team_2"
		cls.TEAM_2_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2_credentials = register_user(c, cls.TEAM_2_USERNAME, cls.TEAM_2_PASSWORD, cls.teacher_secret_key)

		cls.submission_2 = create_submission(cls.TEAM_2_USERNAME)
		cls.submission_2.save()

		cls.TEAM_3_USERNAME = "test_team_3"
		cls.TEAM_3_PASSWORD = "gdsakdsja678687&^*"
		cls.team_3_credentials = register_user(c, cls.TEAM_3_USERNAME, cls.TEAM_3_PASSWORD, cls.teacher_2_secret_key)

		cls.submission_3 = create_submission(cls.TEAM_3_USERNAME)
		cls.submission_3.save()

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.JUDGE_2_USERNAME = "judge_2"
		cls.JUDGE_2_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_2_credentials = register_user(c, cls.JUDGE_2_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_2_secret_key)

		cls.JUDGE_3_USERNAME = "judge_3"
		cls.JUDGE_3_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_3_credentials = register_user(c, cls.JUDGE_3_USERNAME, cls.JUDGE_3_PASSWORD, cls.admin_2_judge_secret_key)

		create_judges_submission(cls.JUDGE_USERNAME, cls.TEAM_USERNAME)
		create_judges_submission(cls.JUDGE_2_USERNAME, cls.TEAM_2_USERNAME)
		create_judges_submission(cls.JUDGE_3_USERNAME, cls.TEAM_3_USERNAME)

		create_like(cls.TEAM_USERNAME, cls.TEAM_2_USERNAME)
		create_like(cls.TEAM_2_USERNAME, cls.TEAM_3_USERNAME)

		cls.START_USERS = 11

	def test_good_superuser_team_delete(self) :
		self.assertEqual(3, len(Submission.objects.all()))
		self.assertEqual(3, len(JudgesSubmission.objects.all()))
		self.assertEqual(2, len(SubmissionLike.objects.all()))
		self.assertEqual(3, len(Team.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))

		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEAM_USERNAME, **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(2, len(Submission.objects.all()))
		self.assertEqual(2, len(JudgesSubmission.objects.all()))
		self.assertEqual(1, len(SubmissionLike.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(User.objects.all()))
		self.assertEqual(0, len(User.objects.filter(username=self.TEAM_USERNAME)))

	def test_good_admin_team_delete(self) :
		self.assertEqual(3, len(Submission.objects.all()))
		self.assertEqual(3, len(JudgesSubmission.objects.all()))
		self.assertEqual(2, len(SubmissionLike.objects.all()))
		self.assertEqual(3, len(Team.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))

		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEAM_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(2, len(Submission.objects.all()))
		self.assertEqual(2, len(JudgesSubmission.objects.all()))
		self.assertEqual(1, len(SubmissionLike.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(User.objects.all()))
		self.assertEqual(0, len(User.objects.filter(username=self.TEAM_USERNAME)))

	def test_good_admin_different_team_delete(self) :
		self.assertEqual(3, len(Submission.objects.all()))
		self.assertEqual(3, len(JudgesSubmission.objects.all()))
		self.assertEqual(2, len(SubmissionLike.objects.all()))
		self.assertEqual(3, len(Team.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))

		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEAM_USERNAME, **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(2, len(Submission.objects.all()))
		self.assertEqual(2, len(JudgesSubmission.objects.all()))
		self.assertEqual(1, len(SubmissionLike.objects.all()))
		self.assertEqual(2, len(Team.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(User.objects.all()))
		self.assertEqual(0, len(User.objects.filter(username=self.TEAM_USERNAME)))

	def test_good_admin_teacher_delete(self) :
		self.assertEqual(3, len(Submission.objects.all()))
		self.assertEqual(3, len(JudgesSubmission.objects.all()))
		self.assertEqual(2, len(SubmissionLike.objects.all()))
		self.assertEqual(2, len(Teacher.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))

		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.TEACHER_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(1, len(Submission.objects.all()))
		self.assertEqual(1, len(JudgesSubmission.objects.all()))
		self.assertEqual(0, len(SubmissionLike.objects.all()))
		self.assertEqual(1, len(Teacher.objects.all()))
		self.assertEqual(self.START_USERS - 3, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS - 3, len(User.objects.all()))
		self.assertEqual(0, len(User.objects.filter(username=self.TEACHER_USERNAME)))

	def test_good_admin_judge_delete(self) :
		self.assertEqual(3, len(Submission.objects.all()))
		self.assertEqual(3, len(JudgesSubmission.objects.all()))
		self.assertEqual(3, len(Judge.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))

		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.JUDGE_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.NO_CONTENT.value, response.status_code)
		self.assertEqual({}, response.data)

		self.assertEqual(3, len(Submission.objects.all()))
		self.assertEqual(2, len(SubmissionLike.objects.all()))
		self.assertEqual(2, len(JudgesSubmission.objects.all()))
		self.assertEqual(2, len(Judge.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS - 1, len(User.objects.all()))
		self.assertEqual(0, len(User.objects.filter(username=self.JUDGE_USERNAME)))

	def test_bad_admin_admin_delete(self) :
		self.assertEqual(2, len(Admin.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))

		response = self.client.delete(API_NAMESPACE + '/user/operations/' + self.ADMIN_2_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Admins can only delete team, teacher, or judge accounts", response.data['message'])

		self.assertEqual(2, len(Admin.objects.all()))
		self.assertEqual(self.START_USERS, len(Profile.objects.all()))
		self.assertEqual(self.START_USERS, len(User.objects.all()))


class FrontendFileTestCase(TestCase) :
	def test_robots_txt_retrieve(self) :
		response = self.client.get("/robots.txt")

		stream = b''.join(response.streaming_content)

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertGreater(len(stream), 0)
	
	def test_manifest_json_retrieve(self) :
		response = self.client.get("/manifest.json")

		stream = b''.join(response.streaming_content)

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertGreater(len(stream), 0)

	def test_index_html_retrieve_for_service_worker(self) :
		response = self.client.get("/index.html")

		stream = b''.join(response.streaming_content)

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertGreater(len(stream), 0)

	def test_non_collected_file_graceful_error(self) :
		previous_route = str(BASE_DIR) + '/static/manifest.json'
		moved_route = str(BASE_DIR) + '/manifest.json'

		shutil.move(previous_route, moved_route)

		response = self.client.get("/manifest.json")

		self.assertEqual(STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value, response.status_code)
		self.assertIn('message', response.data)
		self.assertEqual('Cannot successfully retrieve requested frontend file', response.data['message'])

		shutil.move(moved_route, previous_route)


class Handler404TestCase(TestCase) :
	def test_static_404(self) :
		response = self.client.get("/static/")

		self.assertRedirects(response, "/", 302, 200) # response has a status code of 302 and redirects to "/" which has a status code of 200

	def test_not_static_404(self) :
		response = self.client.get(API_NAMESPACE + "/media/fake.png")

		self.assertEqual(400, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual(response.data["message"], "Bad URL used to access content. Please notify the website administrator")


class IsUserAdministratorCheckTestCase(TestCase) :
	def setUp(self):
		# create superuser
		User.objects.create_superuser(SUPERUSER_USERNAME, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)

		# login as superuser
		superuser_login_response = self.client.put(
			API_NAMESPACE + '/auth/login/',
			json.dumps({
				"username": SUPERUSER_USERNAME,
				"password": SUPERUSER_PASSWORD
			}),
			content_type="application/json"
		)
		superuser_credentials = superuser_login_response.data['tokens']['access']

		# retrieve superuser secret key
		superuser_secret_key_response = self.client.post(
			API_NAMESPACE + '/auth/secret_key',
			**get_headers(superuser_credentials)
		)
		self.SUPERUSER_SECRET_KEY = superuser_secret_key_response.data['secret_key']

		# create admin using superuser secret key
		self.ADMIN_USERNAME = 'admin'
		self.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		admin_register_response = self.client.post(
			API_NAMESPACE + '/auth/register/',
			json.dumps({
				"username": self.ADMIN_USERNAME,
				"password": self.ADMIN_PASSWORD,
				"secret_key": self.SUPERUSER_SECRET_KEY
			}),
			content_type="application/json",
			**get_headers(superuser_credentials)
		)

		# use admins access tokens to obtain secret key for teacher account creation
		self.ADMIN_CREDENTIALS = admin_register_response.data['tokens']['access']
		admin_teacher_secret_key_response = self.client.post(
			API_NAMESPACE + '/auth/secret_key',
			json.dumps({
				"key_type": "Teacher",
				"areas": ["Glasgow City"],
				"school": "Bellahouston Academy"
			}),
			content_type="application/json",
			**get_headers(self.ADMIN_CREDENTIALS)
		)
		self.ADMIN_TEACHER_SECRET_KEY = admin_teacher_secret_key_response.data['secret_key']

		# retrieve admin secret key for judge account creation
		admin_judge_secret_key_response = self.client.post(
			API_NAMESPACE + '/auth/secret_key',
			json.dumps({
				"key_type": "Judge",
				"areas": ["Glasgow City"],
				"school": "Bellahouston Academy"
			}),
			content_type="application/json",
			**get_headers(self.ADMIN_CREDENTIALS)
		)
		self.ADMIN_JUDGE_SECRET_KEY = admin_judge_secret_key_response.data['secret_key']

		# create a teacher(1) account
		self.TEACHER_GOOD_USERNAME = 'teacher1'
		self.TEACHER_GOOD_PASSWORD = 'dbshadHGUDBJH789&*'
		teacher_good_register_response = self.client.post(
			API_NAMESPACE + '/auth/register/',
			json.dumps({
				"username": self.TEACHER_GOOD_USERNAME,
				"password": self.TEACHER_GOOD_PASSWORD,
				"secret_key": self.ADMIN_TEACHER_SECRET_KEY
			}),
			content_type="application/json",
			**get_headers(self.ADMIN_CREDENTIALS)
		)
		# use teacher(1) access tokens to retrieve secret key for team account creation
		self.TEACHER_GOOD_CREDENTIALS = teacher_good_register_response.data['tokens']['access']
		secret_key_response = self.client.post(
			API_NAMESPACE + '/auth/secret_key',
			**get_headers(self.TEACHER_GOOD_CREDENTIALS)
		)
		self.TEACHER_GOOD_SECRET_KEY = secret_key_response.data['secret_key']

		# create a teacher(2) account and get credentials
		self.TEACHER_BAD_USERNAME = 'teacher2'
		self.TEACHER_BAD_PASSWORD = 'dbshadHGUDBJH789&*'
		teacher_bad_register_response = self.client.post(
			API_NAMESPACE + '/auth/register/',
			json.dumps({
				"username": self.TEACHER_BAD_USERNAME,
				"password": self.TEACHER_BAD_PASSWORD,
				"secret_key": self.ADMIN_TEACHER_SECRET_KEY
			}),
			content_type="application/json",
			**get_headers(self.ADMIN_CREDENTIALS)
		)
		self.TEACHER_BAD_CREDENTIALS = teacher_bad_register_response.data['tokens']['access']

		# create team using teacher(1) secret key
		self.TEAM_USERNAME = "test_team"
		self.TEAM_PASSWORD = "gdsakdsja678687&^*"
		team_register_response = self.client.post(
			API_NAMESPACE + '/auth/register/',
			json.dumps({
				"username": self.TEAM_USERNAME,
				"password": self.TEAM_PASSWORD,
				"secret_key": self.TEACHER_GOOD_SECRET_KEY
			}),
			content_type="application/json",
			**get_headers(self.TEACHER_GOOD_CREDENTIALS)  # not sure if this is necessary or what should be here
		)
		self.TEAM_CREDENTIALS = team_register_response.data['tokens']['access']

		# create judge account
		self.JUDGE_USERNAME = "judge"
		self.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		judge_register_response = self.client.post(
			API_NAMESPACE + '/auth/register/',
			json.dumps({
				"username": self.JUDGE_USERNAME,
				"password": self.JUDGE_PASSWORD,
				"secret_key": self.ADMIN_JUDGE_SECRET_KEY
			}),
			content_type="application/json",
			**get_headers(self.ADMIN_CREDENTIALS)
		)
		self.JUDGE_CREDENTIALS = judge_register_response.data['tokens']['access']

		# not sure if this is the correct way to create an invalid user, so that
		# `Profile.objects.get(user=request.user).type` throws and exception
		# self.INVALID_USER_USERNAME = 'invalid_user'
		# self.INVALID_USER_PASSWORD = 'invalid_passw'
		# User.objects.create(username=self.INVALID_USER_USERNAME, password=self.INVALID_USER_PASSWORD)
		self.INVALID_CREDENTIALS = 'invalid_credentials'
		self.INVALID_USERNAME = 'invalid_username'

	def test_user_type_admin_true(self):
		# verify admin user is always true
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.TEAM_USERNAME, **get_headers(self.ADMIN_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(response.data["is_users_admin"], True)

	def test_user_type_teacher_bad_false(self):
		# verify teacher user requesting team they don't teach is false
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.TEAM_USERNAME, **get_headers(self.TEACHER_BAD_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(response.data["is_users_admin"], False)

	def test_user_type_teacher_good_true(self):
		# verify teacher user requesting team they teach is true
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.TEAM_USERNAME, **get_headers(self.TEACHER_GOOD_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(response.data["is_users_admin"], True)

	def test_user_type_judge_false(self):
		# verify judge user is false
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.TEAM_USERNAME, **get_headers(self.JUDGE_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(response.data["is_users_admin"], False)

	def test_user_type_team_false(self):
		# verify team user is false
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.TEAM_USERNAME, **get_headers(self.TEAM_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(response.data["is_users_admin"], False)

	def test_user_type_invalid_500(self):
		# verify request with invalid credentials fails
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.TEAM_USERNAME, **get_headers(self.INVALID_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)
		self.assertEqual(response.data["detail"], "Given token not valid for any token type")

		# verify request for nonexistent username fails for teachers
		response = self.client.get(API_NAMESPACE + "/is_users_admin/" + self.INVALID_USERNAME, **get_headers(self.TEACHER_GOOD_CREDENTIALS))
		self.assertEqual(response.status_code, STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)
		self.assertEqual(response.data['message'], "Error encountered while checking if you are the specified user's teacher, please try logging out and logging in again")


class IssueControlTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_1_USERNAME = 'teacher1'
		cls.TEACHER_1_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_1_credentials = register_user(c, cls.TEACHER_1_USERNAME, cls.TEACHER_1_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_1_secret_key = get_secret_key(c, 'Team', cls.teacher_1_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_1_USERNAME = "test_team_1"
		cls.TEAM_1_PASSWORD = "gdsakdsja678687&^*"
		cls.team_1_credentials = register_user(c, cls.TEAM_1_USERNAME, cls.TEAM_1_PASSWORD, cls.teacher_1_secret_key)

		cls.TEAM_2_USERNAME = "test_team_2"
		cls.TEAM_2_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2_credentials = register_user(c, cls.TEAM_2_USERNAME, cls.TEAM_2_PASSWORD, cls.teacher_2_secret_key)

		cls.TEAM_3_USERNAME = "test_team_3"
		cls.TEAM_3_PASSWORD = "gdsakdsja678687&^*"
		cls.team_3_credentials = register_user(c, cls.TEAM_3_USERNAME, cls.TEAM_3_PASSWORD, cls.teacher_2_secret_key)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

	def setUp(self):
		self.submission_1 = create_submission(self.TEAM_1_USERNAME)
		self.submission_1.is_blacklisted = True
		self.submission_1.save()

		self.submission_2 = create_submission(self.TEAM_2_USERNAME)
		self.submission_2.is_blacklisted = True
		self.submission_2.save()

		self.submission_3 = create_submission(self.TEAM_3_USERNAME)
		self.submission_3.is_blacklisted = True
		self.submission_3.save()

	def test_get_response_correct_fields(self):
		teacher_1_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.teacher_1_credentials))
		admin_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.admin_credentials))

		self.assertEqual(teacher_1_response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(admin_response.status_code, STATUS_CODE_2xx.SUCCESS.value)

		for field in ["team_logo_stubs", "team_names", "issue_descriptions"]:
			self.assertIn(field, teacher_1_response.data)
			self.assertIn(field, admin_response.data)

	def test_get_with_teacher_and_admin_profile_length(self):
		teacher_1_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.teacher_1_credentials))
		teacher_2_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.teacher_2_credentials))
		admin_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.admin_credentials))

		self.assertEqual(teacher_1_response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(teacher_2_response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(admin_response.status_code, STATUS_CODE_2xx.SUCCESS.value)

		self.assertEqual(len(teacher_1_response.data['team_logo_stubs']), 1)
		self.assertEqual(len(teacher_2_response.data['team_logo_stubs']), 2)
		self.assertEqual(len(admin_response.data['team_logo_stubs']), 3)

	def test_get_with_judge_or_team_user_fails(self):
		judge_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.judge_credentials))
		team_response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.team_1_credentials))

		self.assertEqual(judge_response.data["message"], "Cannot display issues for a non-Teacher or Admin user")
		self.assertEqual(judge_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)

		self.assertEqual(team_response.data["message"], "Cannot display issues for a non-Teacher or Admin user")
		self.assertEqual(team_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)

	def test_get_with_invalid_user_fails(self):
		response = self.client.get(API_NAMESPACE + '/issues', **get_headers(self.INVALID_CREDENTIALS))
		self.assertEqual(response.data["detail"], "Given token not valid for any token type")
		self.assertEqual(response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)

	def test_post_rejects_if_not_changed_since_blacklisting(self):
		self.submission_1.changed_since_blacklisting = False
		self.submission_1.save()

		response = self.client.post(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, json.dumps({"description": ""}), content_type="application/json", **get_headers(self.team_2_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertEqual(response.data['message'], "This submission has been approved by an administrator. Unless this submisison is changed, it cannot be reported again")

	def test_post_accepts_if_changed_since_blacklisting(self):
		self.submission_1.changed_since_blacklisting = True
		self.submission_1.save()

		response = self.client.post(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, json.dumps({"description": ""}), content_type="application/json", **get_headers(self.team_2_credentials))
		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertEqual(response.data['message'], "Thank you for submitting the issue, we have alerted their teacher to review this content")

	def test_post_fails_with_invalid_username_or_credentials(self):
		invalid_username_response = self.client.post(API_NAMESPACE + '/issue/' + self.INVALID_USERNAME, json.dumps({"description": ""}), content_type="application/json", **get_headers(self.team_2_credentials))
		invalid_credentials_response = self.client.post(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, json.dumps({"description": ""}), content_type="application/json", **get_headers(self.INVALID_CREDENTIALS))

		self.assertEqual(invalid_username_response.status_code, STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)
		self.assertEqual(invalid_username_response.data['message'], "Error occured while submitting the issue, please try again")

		self.assertEqual(invalid_credentials_response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)
		self.assertEqual(invalid_credentials_response.data["detail"], "Given token not valid for any token type")

	def test_delete_issue_of_own_team_as_teacher_or_admin_succeeds(self):
		teacher_response = self.client.delete(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, **get_headers(self.teacher_1_credentials))
		admin_response = self.client.delete(API_NAMESPACE + '/issue/' + self.TEAM_2_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(teacher_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertEqual(teacher_response.data['message'], "Issue successfully revoked, the submission is now available on the marketplace again")

		self.assertEqual(admin_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertEqual(admin_response.data['message'], "Issue successfully revoked, the submission is now available on the marketplace again")

		self.submission_1.refresh_from_db()
		self.submission_2.refresh_from_db()

		self.assertFalse(self.submission_1.is_blacklisted)
		self.assertFalse(self.submission_2.is_blacklisted)

	def test_delete_issue_of_someone_elses_team_as_teacher_fails(self):
		teacher_2_response = self.client.delete(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, **get_headers(self.teacher_2_credentials))

		self.assertEqual(teacher_2_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(teacher_2_response.data['message'], "A Teacher can only revoke issues of their own teams")

		self.submission_1.refresh_from_db()
		self.assertTrue(self.submission_1.is_blacklisted)

	def test_delete_issue_as_judge_or_team_fails(self):
		judge_response = self.client.delete(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, **get_headers(self.judge_credentials))
		team_response = self.client.delete(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, **get_headers(self.team_1_credentials))

		self.assertEqual(judge_response.data["message"], "Cannot revoke issues as a non-Teacher or Admin user")
		self.assertEqual(judge_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)

		self.assertEqual(team_response.data["message"], "Cannot revoke issues as a non-Teacher or Admin user")
		self.assertEqual(team_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)

	def test_delete_issue_with_invalid_username_or_credentials_fails(self):
		invalid_username_response = self.client.delete(API_NAMESPACE + '/issue/' + self.INVALID_USERNAME, **get_headers(self.teacher_1_credentials))
		invalid_credentials_response = self.client.delete(API_NAMESPACE + '/issue/' + self.TEAM_1_USERNAME, **get_headers(self.INVALID_CREDENTIALS))

		self.assertEqual(invalid_username_response.status_code, STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)
		self.assertEqual(invalid_username_response.data['message'], "Error occured while removing the issue flag from the submission, please try again")

		self.assertEqual(invalid_credentials_response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)
		self.assertEqual(invalid_credentials_response.data["detail"], "Given token not valid for any token type")


class JudgeSubmissionTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

		create_active_judging_deadline()

	def setUp(self):
		self.judge_submission = JudgesSubmission.objects.create(judge=Judge.objects.get(user__username='judge'), submission=self.submission)

	def test_get_put_correct_shape(self):
		get_response = self.client.get(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, **get_headers(self.judge_credentials))
		put_response = self.client.put(
			API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME,
			json.dumps({
				"target_customer_score": 0,
				"poster_score": 0,
				"tagline_score": 0,
				"logo_score": 0,
				"manifest": [],
			}),
			**get_headers(self.judge_credentials))

		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score", "manifest"]:
			self.assertIn(field, get_response.data)
			self.assertIn(field, put_response.data)
		self.assertEqual(get_response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertEqual(put_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)

	def test_get_errors_on_invalid_username(self):
		get_response = self.client.get(API_NAMESPACE + '/marketplace_judge/' + self.INVALID_USERNAME, **get_headers(self.judge_credentials))
		self.assertEqual(get_response.data["message"], "Error encountered, please try logging out and logging in again")
		self.assertEqual(get_response.status_code, STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

	def test_put_rejects_if_not_judge(self):
		team_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 0, "poster_score": 0, "tagline_score": 0, "logo_score": 0, "manifest": []}), **get_headers(self.team_credentials))
		teacher_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 0, "poster_score": 0, "tagline_score": 0, "logo_score": 0, "manifest": []}), **get_headers(self.teacher_credentials))
		admin_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 0, "poster_score": 0, "tagline_score": 0, "logo_score": 0, "manifest": []}), **get_headers(self.admin_credentials))

		self.assertEqual(team_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(teacher_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)
		self.assertEqual(admin_response.status_code, STATUS_CODE_4xx.FORBIDDEN.value)

		self.assertEqual(teacher_response.data["message"], "Only Judges can assign scores to submission fields")
		self.assertEqual(team_response.data["message"], "Only Judges can assign scores to submission fields")
		self.assertEqual(admin_response.data["message"], "Only Judges can assign scores to submission fields")

	def test_put_updates_and_returns_correct_fields(self):
		no_field_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": []}), **get_headers(self.judge_credentials))

		self.assertEqual(no_field_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score"]:
			self.assertEqual(no_field_response.data[field], 0)

		self.judge_submission.refresh_from_db()
		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score"]:
			self.assertEqual(getattr(self.judge_submission, field), None)

		all_field_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer", "poster", "tagline", "logo"]}), **get_headers(self.judge_credentials))

		self.assertEqual(all_field_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score"]:
			self.assertEqual(all_field_response.data[field], 10)

		self.judge_submission.refresh_from_db()
		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score"]:
			self.assertEqual(getattr(self.judge_submission, field), 10)

		tagline_logo_field_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 20, "poster_score": 20, "tagline_score": 20, "logo_score": 20, "manifest": ["tagline", "logo"]}), **get_headers(self.judge_credentials))
		self.assertEqual(tagline_logo_field_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		for field in ["target_customer_score", "poster_score"]:
			self.assertEqual(tagline_logo_field_response.data[field], 10)
		for field in ["tagline_score", "logo_score"]:
			self.assertEqual(tagline_logo_field_response.data[field], 20)

		self.judge_submission.refresh_from_db()
		for field in ["target_customer_score", "poster_score"]:
			self.assertEqual(getattr(self.judge_submission, field), 10)
		for field in ["tagline_score", "logo_score"]:
			self.assertEqual(getattr(self.judge_submission, field), 20)

	def test_put_rejects_invalid_values(self):
		invalid_value_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 100, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer"]}), **get_headers(self.judge_credentials))
		self.assertEqual(invalid_value_response.data["message"], "The submitted scores are not in the correct range (0-25)")
		self.assertEqual(invalid_value_response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)

		self.judge_submission.refresh_from_db()
		for field in ["target_customer_score", "poster_score", "tagline_score", "logo_score"]:
			self.assertEqual(getattr(self.judge_submission, field), None)

	def test_put_rejects_invalid_username(self):
		invalid_username_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.INVALID_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer"]}), **get_headers(self.judge_credentials))
		self.assertEqual(invalid_username_response.data["message"], "Error encountered, please try logging out and logging in again")
		self.assertEqual(invalid_username_response.status_code, STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value)

	def test_put_creates_judge_submission_if_non_existent(self):
		self.judge_submission.delete()
		try:
			judge_submission = JudgesSubmission.objects.get(submission__team__user__username=self.TEAM_USERNAME)
		except JudgesSubmission.DoesNotExist:
			judge_submission = None
		self.assertIsNone(judge_submission)

		invalid_username_response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer"]}), **get_headers(self.judge_credentials))
		self.assertEqual(invalid_username_response.status_code, STATUS_CODE_2xx.ACCEPTED.value)

		try:
			judge_submission = JudgesSubmission.objects.get(submission__team__user__username=self.TEAM_USERNAME)
		except JudgesSubmission.DoesNotExist:
			judge_submission = None
		self.assertIsNotNone(judge_submission)

	def test_update_judge_submission_after_judging(self) :
		create_active_voting_deadline()

		response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer"]}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot update or make judge submissions before the marketplace has opened, or after the judging deadline has passed", response.data['message'])

	def test_update_judge_submission_after_all(self) :
		create_passed_deadline()

		response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer"]}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot update or make judge submissions before the marketplace has opened, or after the judging deadline has passed", response.data['message'])

	def test_update_judge_submission_before_judging(self) :
		create_active_submission_deadline()

		response = self.client.put(API_NAMESPACE + '/marketplace_judge/' + self.TEAM_USERNAME, json.dumps({"target_customer_score": 10, "poster_score": 10, "tagline_score": 10, "logo_score": 10, "manifest": ["target_customer"]}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot update or make judge submissions before the marketplace has opened, or after the judging deadline has passed", response.data['message'])


class DeadlineTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

	def test_deadline_get_unauthorized(self) :
		response = self.client.get(API_NAMESPACE + '/deadline')

		self.assertEqual(STATUS_CODE_4xx.UNAUTHORIZED.value, response.status_code)

	def test_deadline_get_doesnt_exit(self) :
		response = self.client.get(API_NAMESPACE + '/deadline', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.NOT_FOUND.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("No deadlines have been set yet. Please contact your YES representative about this issue", response.data['message'])

	def test_deadline_create(self) :
		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("submission_deadline", response.data)
		self.assertIn("voting_deadline", response.data)
		self.assertIn("judging_deadline", response.data)
		self.assertEqual("2022-02-02", response.data['submission_deadline'])
		self.assertEqual("2022-03-02", response.data['judging_deadline'])
		self.assertEqual("2022-04-02", response.data['voting_deadline'])

	def test_deadline_create_not_all_fields(self) :
		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Deadline data is incomplete. Cannot correctly set deadlines", response.data['message'])

	def test_deadline_put_unauthorized(self) :
		self.TEACHER_USERNAME = 'teacher1'
		self.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		self.teacher_credentials = register_user(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD, self.admin_teacher_secret_key)
		self.teacher_secret_key = get_secret_key(self.client, 'Team', self.teacher_credentials)

		self.TEAM_USERNAME = "test_team_1"
		self.TEAM_PASSWORD = "gdsakdsja678687&^*"
		self.team_credentials = register_user(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD, self.teacher_secret_key)

		self.JUDGE_USERNAME = "judge"
		self.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		self.judge_credentials = register_user(self.client, self.JUDGE_USERNAME,self.JUDGE_PASSWORD, self.admin_judge_secret_key)

		response = self.client.put(API_NAMESPACE + '/deadline', json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.teacher_credentials))
		self.assertEqual(STATUS_CODE_4xx.UNAUTHORIZED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Non-Admin users cannot create or update deadlines", response.data['message'])

		response = self.client.put(API_NAMESPACE + '/deadline', json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.team_credentials))
		self.assertEqual(STATUS_CODE_4xx.UNAUTHORIZED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Non-Admin users cannot create or update deadlines", response.data['message'])

		response = self.client.put(API_NAMESPACE + '/deadline', json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.judge_credentials))
		self.assertEqual(STATUS_CODE_4xx.UNAUTHORIZED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Non-Admin users cannot create or update deadlines", response.data['message'])

	def test_wrong_formatted_date_put(self) :
		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "20-02-2022", "voting_deadline" : "03-02-2022", "judging_deadline" : "01-02-2022"}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_5xx.INTERNAL_SERVER_ERROR.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Failed to set deadlines. Please try again later or contact the website administrator", response.data['message'])

	def test_update_date_put(self) :
		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		self.assertEqual(1, len(Deadline.objects.all()))

		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("submission_deadline", response.data)
		self.assertIn("voting_deadline", response.data)
		self.assertIn("judging_deadline", response.data)
		self.assertEqual("2022-02-02", response.data['submission_deadline'])
		self.assertEqual("2022-03-02", response.data['judging_deadline'])
		self.assertEqual("2022-04-02", response.data['voting_deadline'])

		self.assertEqual(1, len(Deadline.objects.all()))

	def test_date_put_voting_earlier_than_submission(self) :
		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-01-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot set dates as supplied: submission deadline must be before the voting and judging deadlines", response.data['message'])

	def test_date_put_judging_earlier_than_submission(self) :
		response = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-03-02", "judging_deadline" : "2022-01-02"}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot set dates as supplied: submission deadline must be before the voting and judging deadlines", response.data['message'])

	def test_date_created_get(self) :
		_ = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		response = self.client.get(API_NAMESPACE + "/deadline", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("submission_deadline", response.data)
		self.assertIn("voting_deadline", response.data)
		self.assertIn("judging_deadline", response.data)
		self.assertEqual("2022-02-02", response.data['submission_deadline'])
		self.assertEqual("2022-03-02", response.data['judging_deadline'])
		self.assertEqual("2022-04-02", response.data['voting_deadline'])

	def test_date_created_get_other_users(self) :
		_ = self.client.put(API_NAMESPACE + "/deadline", json.dumps({"submission_deadline" : "2022-02-02", "voting_deadline" : "2022-04-02", "judging_deadline" : "2022-03-02"}), **get_headers(self.admin_credentials))

		self.TEACHER_USERNAME = 'teacher1'
		self.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		self.teacher_credentials = register_user(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD, self.admin_teacher_secret_key)
		self.teacher_secret_key = get_secret_key(self.client, 'Team', self.teacher_credentials)

		self.TEAM_USERNAME = "test_team_1"
		self.TEAM_PASSWORD = "gdsakdsja678687&^*"
		self.team_credentials = register_user(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD, self.teacher_secret_key)

		self.JUDGE_USERNAME = "judge"
		self.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		self.judge_credentials = register_user(self.client, self.JUDGE_USERNAME,self.JUDGE_PASSWORD, self.admin_judge_secret_key)

		response = self.client.get(API_NAMESPACE + "/deadline", **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("submission_deadline", response.data)
		self.assertIn("voting_deadline", response.data)
		self.assertIn("judging_deadline", response.data)
		self.assertEqual("2022-02-02", response.data['submission_deadline'])
		self.assertEqual("2022-03-02", response.data['judging_deadline'])
		self.assertEqual("2022-04-02", response.data['voting_deadline'])

		response = self.client.get(API_NAMESPACE + "/deadline", **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("submission_deadline", response.data)
		self.assertIn("voting_deadline", response.data)
		self.assertIn("judging_deadline", response.data)
		self.assertEqual("2022-02-02", response.data['submission_deadline'])
		self.assertEqual("2022-03-02", response.data['judging_deadline'])
		self.assertEqual("2022-04-02", response.data['voting_deadline'])

		response = self.client.get(API_NAMESPACE + "/deadline", **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("submission_deadline", response.data)
		self.assertIn("voting_deadline", response.data)
		self.assertIn("judging_deadline", response.data)
		self.assertEqual("2022-02-02", response.data['submission_deadline'])
		self.assertEqual("2022-03-02", response.data['judging_deadline'])
		self.assertEqual("2022-04-02", response.data['voting_deadline'])


class DownloadCSVsTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_1_USERNAME = 'teacher1'
		cls.TEACHER_1_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_1_credentials = register_user(c, cls.TEACHER_1_USERNAME, cls.TEACHER_1_PASSWORD,
												  cls.admin_teacher_secret_key)
		cls.teacher_1_secret_key = get_secret_key(c, 'Team', cls.teacher_1_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD,
												  cls.admin_teacher_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_1_USERNAME = "test_team_1"
		cls.TEAM_1_PASSWORD = "gdsakdsja678687&^*"
		cls.team_1_credentials = register_user(c, cls.TEAM_1_USERNAME, cls.TEAM_1_PASSWORD, cls.teacher_1_secret_key)

		cls.TEAM_2_USERNAME = "test_team_2"
		cls.TEAM_2_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2_credentials = register_user(c, cls.TEAM_2_USERNAME, cls.TEAM_2_PASSWORD, cls.teacher_2_secret_key)

		cls.TEAM_3_USERNAME = "test_team_3"
		cls.TEAM_3_PASSWORD = "gdsakdsja678687&^*"
		cls.team_3_credentials = register_user(c, cls.TEAM_3_USERNAME, cls.TEAM_3_PASSWORD, cls.teacher_2_secret_key)

		cls.JUDGE_1_USERNAME = "judge1"
		cls.JUDGE_2_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_1_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_secret_key)

		cls.JUDGE_2_USERNAME = "judge2"
		cls.JUDGE_2_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_2_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

		# team 1 will have 2 judge submissions, team 2 will have 0
		cls.submission_1 = create_submission(cls.TEAM_1_USERNAME)
		cls.submission_2 = create_submission(cls.TEAM_2_USERNAME)
		cls.submission_3 = create_submission(cls.TEAM_3_USERNAME)

		cls.judge_submission_1 = create_judges_submission(cls.JUDGE_1_USERNAME, cls.TEAM_1_USERNAME)
		cls.judge_submission_1.poster_score = 10
		cls.judge_submission_1.logo_score = 10
		cls.judge_submission_1.target_customer_score = 10
		cls.judge_submission_1.tagline_score = 10
		cls.judge_submission_1.save()

		cls.judge_submission_2 = create_judges_submission(cls.JUDGE_2_USERNAME, cls.TEAM_1_USERNAME)
		cls.judge_submission_2.poster_score = 6
		cls.judge_submission_2.logo_score = 6
		cls.judge_submission_2.target_customer_score = 6
		cls.judge_submission_2.tagline_score = 6
		cls.judge_submission_2.save()

		# create answers
		q1 = Question.objects.create(question_id=1, question_text="test_question1")
		a1_dragon_scores = {"finance_score": 1, "hr_score": 1, "environmental_score": 1, "marketing_score": 1, "customers_score": 1}
		a1 = Answer.objects.create(answer_no=1, answer_text="test_answer", answer_dragon_scores=a1_dragon_scores, question=q1)

		q2 = Question.objects.create(question_id=2, question_text="test_question2")
		a2_dragon_scores = {"finance_score": 2, "hr_score": 2, "environmental_score": 2, "marketing_score": 2, "customers_score": 2}
		a2 = Answer.objects.create(answer_no=2, answer_text="test_answer", answer_dragon_scores=a2_dragon_scores, question=q2)

		# team1 has answered q1:a1 and q2:a2
		team1 = Team.objects.get(user=User.objects.get(username=cls.TEAM_1_USERNAME))
		team1.question_answer_dict = {1: 1, 2: 2}
		team1.save()

	def test_summary_returns_file(self) :
		response = self.client.get(API_NAMESPACE + f"/csv/summary?token={self.admin_credentials}")

		self.assertEqual(200, response.status_code)
		self.assertGreater(len(response.content), 0)

	def test_detail_returns_file(self) :
		response = self.client.get(API_NAMESPACE + f"/csv/detail?token={self.admin_credentials}")

		self.assertEqual(200, response.status_code)
		self.assertGreater(len(response.content), 0)

	def test_summary_returns_correct_shape_and_values(self) :
		response = self.client.get(API_NAMESPACE + f"/csv/summary?token={self.admin_credentials}")

		rows = response.content.decode().strip().split('\r\n')
		# assert there's 4 rows (1 header + 3 teams) and 12 columns
		self.assertEqual(4, len(rows))
		for r in rows:
			self.assertEqual(12, len(r.split(',')))

		# assert the values are correct
		self.assertEqual(rows[1].split(',')[3:], ['0', '8.0', '2', '3', '3', '3', '3', '3', '2'])

	def test_detail_returns_correct_shape_and_values(self) :
		response = self.client.get(API_NAMESPACE + f"/csv/detail?token={self.admin_credentials}")

		rows = response.content.decode().strip().split('\r\n')
		# assert there's 5 rows (1 header + 1 team with 2 votes + 2 teams with 0 votes) and 8 columns
		self.assertEqual(5, len(rows))
		for r in rows:
			self.assertEqual(8, len(r.split(',')))
		self.assertEqual(rows[1].split(',')[4:], ['10', '10', '10', '10'])
		self.assertEqual(rows[2].split(',')[4:], ['6', '6', '6', '6'])


class CanLikeMarketplaceStallViewTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

	def test_can_like_different_teacher_unliked(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(True, response.data['can_like'])

	def test_cannot_like_same_teacher_unliked(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_same_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(True, response.data['can_like'])

	def test_cannot_like_does_not_exist(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_SAME_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_different_teacher_liked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_can_like_different_teacher_liked_unliked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_DIFFERENT_USERNAME)
		delete_like(self.TEAM_USERNAME, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(True, response.data['can_like'])

	def test_cannot_like_same_team_liked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_SAME_USERNAME)

		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_same_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_same_team_liked_unliked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_SAME_USERNAME)
		delete_like(self.TEAM_USERNAME, self.TEAM_SAME_USERNAME)

		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_same_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(True, response.data['can_like'])

	def test_cannot_access_no_auth(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME)

		self.assertEqual(response.status_code, 401)

	def test_cannot_like_same_teacher(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.teacher_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_different_teacher(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.teacher_2_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_admin(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_judge(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.judge_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_same_team(self) :
		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_cannot_like_different_team_no_likes_left(self) :
		for i in range(MAXIMUM_NUMBER_OF_LIKES) :
			new_team_username = "test_team_new_" + str(i)
			password = "gdsakdsja678687&^*"
			_ = register_user(self.client, new_team_username, password, self.teacher_secret_key)
			create_submission(new_team_username)

			create_like(new_team_username, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(False, response.data['can_like'])

	def test_can_like_different_team_no_likes_left_plus_unlike(self) :
		for i in range(MAXIMUM_NUMBER_OF_LIKES) :
			new_team_username = "test_team_new_" + str(i)
			password = "gdsakdsja678687&^*"
			_ = register_user(self.client, new_team_username, password, self.teacher_secret_key)
			create_submission(new_team_username)

			create_like(new_team_username, self.TEAM_DIFFERENT_USERNAME)

		delete_like("test_team_new_0", self.TEAM_DIFFERENT_USERNAME)

		response = self.client.get(API_NAMESPACE + "/can_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("can_like", response.data)
		self.assertEqual(True, response.data['can_like'])


class MarketplaceLikeViewGETTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

	def test_get_unliked_different_unliked(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_unliked_does_not_exist(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_SAME_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_unliked_same_unliked(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_same_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_unliked_literalsame_unliked(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_liked_different_liked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(True, response.data['liked'])

	def test_get_liked_same_liked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_SAME_USERNAME)

		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_same_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(True, response.data['liked'])

	def test_get_liked_literalsame_liked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_USERNAME)

		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(True, response.data['liked'])

	def test_get_liked_same_teacher(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.teacher_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_liked_different_teacher(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.teacher_2_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_liked_admin(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.admin_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_get_liked_judge(self) :
		response = self.client.get(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.judge_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.SUCCESS.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])


class MarketplaceLikeViewPOSTTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

	def test_post_fail_does_not_exist(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_SAME_USERNAME, None, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot like a submisison that does not exist", response.data['message'])

	def test_post_liked_different_unliked(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertIn("liked", response.data)
		self.assertEqual(True, response.data['liked'])

	def test_post_fail_different_liked(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot like a marketplace stall that you have liked already", response.data['message'])

	def test_post_fail_same_unliked(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.team_same_credentials))


		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertIn("liked", response.data)
		self.assertEqual(True, response.data['liked'])

	def test_post_fail_literalsame_unliked(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.team_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot like your own marketplace stall", response.data['message'])

	def test_post_fail_teacher(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.teacher_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Non-Team user cannot give likes", response.data['message'])

	def test_post_fail_judge(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.judge_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Non-Team user cannot give likes", response.data['message'])

	def test_post_fail_admin(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.admin_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Non-Team user cannot give likes", response.data['message'])

	def test_post_fail_different_max_liked(self) :
		for i in range(MAXIMUM_NUMBER_OF_LIKES) :
			new_team_username = "test_team_new_" + str(i)
			password = "gdsakdsja678687&^*"
			_ = register_user(self.client, new_team_username, password, self.teacher_secret_key)
			create_submission(new_team_username)

			create_like(new_team_username, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot give more than " + str(MAXIMUM_NUMBER_OF_LIKES) + " likes", response.data['message'])

	def test_post_liked_different_max_liked_unliked(self) :
		for i in range(MAXIMUM_NUMBER_OF_LIKES) :
			new_team_username = "test_team_new_" + str(i)
			password = "gdsakdsja678687&^*"
			_ = register_user(self.client, new_team_username, password, self.teacher_secret_key)
			create_submission(new_team_username)

			create_like(new_team_username, self.TEAM_DIFFERENT_USERNAME)

		delete_like("test_team_new_0", self.TEAM_DIFFERENT_USERNAME)

		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertIn("liked", response.data)
		self.assertEqual(True, response.data['liked'])

	def test_post_unauthorized(self) :
		response = self.client.post(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, None)

		self.assertEqual(response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)


class MarketplaceLikeViewDELETETestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.INVALID_CREDENTIALS = 'invalid_credentials'
		cls.INVALID_USERNAME = 'invalid_username'

	def test_delete_unliked_fail(self) :
		response = self.client.delete(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_4xx.BAD_REQUEST.value)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot unlike a submisison that you have not liked", response.data['message'])

	def test_delete_liked_unlikes(self) :
		create_like(self.TEAM_USERNAME, self.TEAM_DIFFERENT_USERNAME)

		response = self.client.delete(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME, **get_headers(self.team_different_credentials))

		self.assertEqual(response.status_code, STATUS_CODE_2xx.ACCEPTED.value)
		self.assertIn("liked", response.data)
		self.assertEqual(False, response.data['liked'])

	def test_delete_unauthorized(self) :
		response = self.client.delete(API_NAMESPACE + "/marketplace_like/" + self.TEAM_USERNAME)

		self.assertEqual(response.status_code, STATUS_CODE_4xx.UNAUTHORIZED.value)

class ChangePasswordResetTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)

		cls.ADMIN_2_USERNAME = 'admin2'
		cls.ADMIN_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_2_credentials = register_user(c, cls.ADMIN_2_USERNAME, cls.ADMIN_2_PASSWORD, cls.superuser_secret_key)

		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.JUDGE_2_USERNAME = "judge2"
		cls.JUDGE_2_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_2_credentials = register_user(c, cls.JUDGE_2_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_secret_key)

	def test_reset_password_unauthorized(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}))

		self.assertEqual(401, response.status_code)
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_same_team_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEAM_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_same_teacher_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEAM_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_same_admin_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEAM_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_same_superadmin_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEAM_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_different_team_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.team_different_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Team users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_different_teacher_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.teacher_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Cannot reset the passwords of Team users that are not your students", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_different_admin_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEAM_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_judge_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, json.dumps({}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Judge users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_reset_password_same_team_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Team users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_same_teacher_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEACHER_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_same_admin_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEACHER_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_same_superadmin_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEACHER_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_different_team_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.team_different_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Team users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_different_teacher_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.teacher_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Teacher cannot change non-team users' passwords", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_different_admin_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.TEACHER_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_judge_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, json.dumps({}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Judge users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_reset_password_same_team_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, json.dumps({}), **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Team users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_reset_password_same_teacher_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, json.dumps({}), **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Teacher cannot change non-team users' passwords", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_reset_password_same_admin_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, json.dumps({}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.ADMIN_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_reset_password_same_superadmin_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, json.dumps({}), **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.ADMIN_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_reset_password_judge_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, json.dumps({}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Judge users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_reset_password_different_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, json.dumps({}), **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Admins cannot reset other admin's passwords. Operation can only be done by the superuser", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_reset_same_judge_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, json.dumps({}), **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.JUDGE_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_reset_different_judge_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, json.dumps({}), **get_headers(self.judge_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Judge users cannot reset the passwords of other users", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_reset_admin_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, json.dumps({}), **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.JUDGE_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_reset_superadmin_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, json.dumps({}), **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertIn("Successfully reset password for " + self.JUDGE_USERNAME +  " to ", response.data['message'])
		self.assertFalse(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))


class ChangePasswordChangeTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)

		cls.ADMIN_2_USERNAME = 'admin_2'
		cls.ADMIN_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_2_credentials = register_user(c, cls.ADMIN_2_USERNAME, cls.ADMIN_2_PASSWORD, cls.superuser_secret_key)

		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, school="Hillpark Secondary School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_SAME_USERNAME = "test_team_same"
		cls.TEAM_SAME_PASSWORD = "gdsakdsja678687&^*"
		cls.team_same_credentials = register_user(c, cls.TEAM_SAME_USERNAME, cls.TEAM_SAME_PASSWORD, cls.teacher_secret_key)

		cls.TEAM_DIFFERENT_USERNAME = "test_team_2"
		cls.TEAM_DIFFERENT_PASSWORD = "gdsakdsja678687&^*"
		cls.team_different_credentials = register_user(c, cls.TEAM_DIFFERENT_USERNAME, cls.TEAM_DIFFERENT_PASSWORD, cls.teacher_2_secret_key)

		cls.submission = create_submission(cls.TEAM_USERNAME)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.JUDGE_2_USERNAME = "judge_2"
		cls.JUDGE_2_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_2_credentials = register_user(c, cls.JUDGE_2_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_secret_key)

		cls.NEW_PASSWORD_DATA = json.dumps({"new_password" : "gdhjsaJHDVSJ657%^&"})
		cls.NEW_PASSWORD_DATA_BAD = json.dumps({"new_password" : "abc"})

	def test_change_password_unauthorized(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA)

		self.assertEqual(401, response.status_code)
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_same_team_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertFalse(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_same_team_team_bad(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA_BAD, **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("This password is too short. It must contain at least 6 characters, and this password is too common", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_same_teacher_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_same_admin_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_same_superadmin_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_different_team_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.team_different_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_different_teacher_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.teacher_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_different_admin_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_judge_team(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEAM_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEAM_USERNAME, self.TEAM_PASSWORD))

	def test_change_password_same_team_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_same_teacher_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertFalse(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_same_teacher_teacher_bad(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA_BAD, **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("This password is too short. It must contain at least 6 characters, and this password is too common", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_same_admin_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_same_superadmin_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_different_team_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.team_different_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_different_teacher_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.teacher_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_different_admin_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_judge_teacher(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.TEACHER_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.TEACHER_USERNAME, self.TEACHER_PASSWORD))

	def test_change_password_same_team_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_password_same_teacher_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_password_same_admin_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertFalse(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_password_same_admin_admin_bad(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA_BAD, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("This password is too short. It must contain at least 6 characters, and this password is too common", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_password_same_superadmin_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_password_judge_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_password_different_admin(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.ADMIN_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.ADMIN_USERNAME, self.ADMIN_PASSWORD))

	def test_change_same_judge_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_2xx.ACCEPTED.value, response.status_code)
		self.assertFalse(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_change_password_same_judge_judge_bad(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, self.NEW_PASSWORD_DATA_BAD, **get_headers(self.judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("This password is too short. It must contain at least 6 characters, and this password is too common", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_change_different_judge_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.judge_2_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_change_admin_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))

	def test_change_superadmin_judge(self) :
		response = self.client.put(API_NAMESPACE + '/auth/change_password/' + self.JUDGE_USERNAME, self.NEW_PASSWORD_DATA, **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot change password for another user", response.data['message'])
		self.assertTrue(can_log_in(self.client, self.JUDGE_USERNAME, self.JUDGE_PASSWORD))


class NumberOfNotificationTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.submission_1 = create_submission(cls.TEAM_USERNAME)
		cls.submission_1.is_blacklisted = True
		cls.submission_1.save()

	def test_teacher_has_issues_notifications(self) :
		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(1, response.data['number_of_issues'])

	def test_admin_has_issues_notifications(self) :
		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(1, response.data['number_of_issues'])

	def test_fails_when_bad_object(self) :
		response = self.client.get(API_NAMESPACE + "/number_of?object=Marketplace", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("'object' field is of invalid type for this user. Please specifiy a correct object field", response.data['message'])

	def test_fails_when_no_object(self) :
		response = self.client.get(API_NAMESPACE + "/number_of", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot return the number of something is the 'object' is not specified", response.data['message'])

	def test_fails_when_no_authentication(self) :
		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues")

		self.assertEqual(STATUS_CODE_4xx.UNAUTHORIZED.value, response.status_code)

	def test_fails_when_judge(self) :
		admin_judge_secret_key = get_secret_key(self.client, 'Judge', self.admin_credentials)

		judge_username = 'judge'
		judge_credentials = register_user(self.client, judge_username, self.TEACHER_PASSWORD, admin_judge_secret_key)

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(judge_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("'object' field is of invalid type for this user. Please specifiy a correct object field", response.data['message'])

	def test_fails_when_team(self) :
		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.BAD_REQUEST.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("'object' field is of invalid type for this user. Please specifiy a correct object field", response.data['message'])

	def test_no_issues_when_no_blacklisted_teacher(self) :
		self.submission_1.is_blacklisted = False
		self.submission_1.save()

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(0, response.data['number_of_issues'])

	def test_no_issues_when_no_blacklisted_admin(self) :
		self.submission_1.is_blacklisted = False
		self.submission_1.save()

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(0, response.data['number_of_issues'])

	def test_larger_than_one_for_multiple_issues_teacher(self) :
		team_2_username = "test_team_2"
		_ = register_user(self.client, team_2_username, self.TEAM_PASSWORD, self.teacher_secret_key)

		self.submission_2 = create_submission(team_2_username)
		self.submission_2.is_blacklisted = True
		self.submission_2.save()

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(2, response.data['number_of_issues'])

	def test_larger_than_one_for_multiple_issues_admin(self) :
		team_2_username = "test_team_2"
		_ = register_user(self.client, team_2_username, self.TEAM_PASSWORD, self.teacher_secret_key)

		self.submission_2 = create_submission(team_2_username)
		self.submission_2.is_blacklisted = True
		self.submission_2.save()

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(2, response.data['number_of_issues'])

	def test_larger_than_one_issue_for_multiple_issues_from_multiple_teachers_admin(self) :
		teacher_2_username = 'teacher2'
		self.teacher_2_credentials = register_user(self.client, teacher_2_username, self.TEACHER_PASSWORD, self.admin_teacher_secret_key)
		self.teacher_2_secret_key = get_secret_key(self.client, 'Team', self.teacher_2_credentials)

		team_2_username = "test_team_2"
		_ = register_user(self.client, team_2_username, self.TEAM_PASSWORD, self.teacher_2_secret_key)

		self.submission_2 = create_submission(team_2_username)
		self.submission_2.is_blacklisted = True
		self.submission_2.save()

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(2, response.data['number_of_issues'])

	def test_one_issue_for_multiple_issues_from_multiple_teachers_teacher(self) :
		teacher_2_username = 'teacher2'
		self.teacher_2_credentials = register_user(self.client, teacher_2_username, self.TEACHER_PASSWORD, self.admin_teacher_secret_key)
		self.teacher_2_secret_key = get_secret_key(self.client, 'Team', self.teacher_2_credentials)

		team_2_username = "test_team_2"
		_ = register_user(self.client, team_2_username, self.TEAM_PASSWORD, self.teacher_2_secret_key)

		self.submission_2 = create_submission(team_2_username)
		self.submission_2.is_blacklisted = True
		self.submission_2.save()

		response = self.client.get(API_NAMESPACE + "/number_of?object=Issues", **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("number_of_issues", response.data)
		self.assertEqual(1, response.data['number_of_issues'])


class JudgesProgressTestCase(TestCase) :
	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)
		cls.admin_judge_2_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials, areas=["East Renfrewshire"], school="")

		cls.ADMIN_2_USERNAME = 'admin2'
		cls.ADMIN_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_2_credentials = register_user(c, cls.ADMIN_2_USERNAME, cls.ADMIN_2_PASSWORD, cls.superuser_secret_key)
		cls.admin_2_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_2_credentials)

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.submission_1 = create_submission(cls.TEAM_USERNAME)
		cls.submission_1.save()

		cls.TEAM_2_USERNAME = "test_team_2"
		cls.TEAM_2_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2_credentials = register_user(c, cls.TEAM_2_USERNAME, cls.TEAM_2_PASSWORD, cls.teacher_secret_key)

		cls.submission_2 = create_submission(cls.TEAM_2_USERNAME)
		cls.submission_2.save()

		cls.TEAM_3_USERNAME = "test_team_3"
		cls.TEAM_3_PASSWORD = "gdsakdsja678687&^*"
		cls.team_3_credentials = register_user(c, cls.TEAM_3_USERNAME, cls.TEAM_3_PASSWORD, cls.teacher_2_secret_key)

		cls.submission_3 = create_submission(cls.TEAM_3_USERNAME)
		cls.submission_3.save()

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.JUDGE_2_USERNAME = "judge_2"
		cls.JUDGE_2_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_2_credentials = register_user(c, cls.JUDGE_2_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_2_secret_key)

		cls.JUDGE_3_USERNAME = "judge_3"
		cls.JUDGE_3_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_3_credentials = register_user(c, cls.JUDGE_3_USERNAME, cls.JUDGE_3_PASSWORD, cls.admin_2_judge_secret_key)

	def test_cannot_access_unathorized(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress')

		self.assertEqual(401, response.status_code)

	def test_cannot_access_team(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.team_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot return information about judge progress when logged in as a non-admin user", response.data['message'])

	def test_cannot_access_teacher(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.teacher_credentials))

		self.assertEqual(STATUS_CODE_4xx.FORBIDDEN.value, response.status_code)
		self.assertIn("message", response.data)
		self.assertEqual("Cannot return information about judge progress when logged in as a non-admin user", response.data['message'])

	def test_can_access_admin(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(2, len(response.data['judge_names']))

	def test_can_access_different_judge_different_admin(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_2_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(1, len(response.data['judge_names']))

	def test_correct_progress_update_made_submission(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(2, len(response.data['judge_names']))

		self.assertEqual(0, response.data['judge_marked'][0])
		self.assertEqual(0, response.data['judge_progress'][0])
		self.assertEqual(2, response.data['judge_total'][0])
		self.assertEqual(0, response.data['judge_marked'][1])
		self.assertEqual(0, response.data['judge_progress'][1])
		self.assertEqual(1, response.data['judge_total'][0])

		create_judges_submission(self.JUDGE_USERNAME, self.TEAM_USERNAME)

		response2 = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(1, response2.data['judge_marked'][0])
		self.assertEqual(50, response2.data['judge_progress'][0])
		self.assertEqual(2, response2.data['judge_total'][0])
		self.assertEqual(0, response2.data['judge_marked'][1])
		self.assertEqual(0, response2.data['judge_progress'][1])
		self.assertEqual(1, response2.data['judge_total'][1])

	def test_correct_progress_update_made_submission(self) :
		create_judges_submission(self.JUDGE_USERNAME, self.TEAM_USERNAME)

		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(2, len(response.data['judge_names']))

		self.assertEqual(1, response.data['judge_marked'][0])
		self.assertEqual(50, response.data['judge_progress'][0])
		self.assertEqual(2, response.data['judge_total'][0])
		self.assertEqual(0, response.data['judge_marked'][1])
		self.assertEqual(0, response.data['judge_progress'][1])
		self.assertEqual(1, response.data['judge_total'][1])

		self.submission_1.is_blacklisted = True
		self.submission_1.save()

		response2 = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(0, response2.data['judge_marked'][0])
		self.assertEqual(0, response2.data['judge_progress'][0])
		self.assertEqual(1, response2.data['judge_total'][0])
		self.assertEqual(0, response2.data['judge_marked'][1])
		self.assertEqual(0, response2.data['judge_progress'][1])
		self.assertEqual(1, response2.data['judge_total'][1])

	def test_100percent_if_none_exist(self) :
		self.submission_3.is_blacklisted = True
		self.submission_3.save()

		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(2, len(response.data['judge_names']))

		self.assertEqual(0, response.data['judge_marked'][0])
		self.assertEqual(0, response.data['judge_progress'][0])
		self.assertEqual(2, response.data['judge_total'][0])
		self.assertEqual(0, response.data['judge_marked'][1])
		self.assertEqual(100, response.data['judge_progress'][1])
		self.assertEqual(0, response.data['judge_total'][1])

	def test_100percent_if_all_marked(self) :
		create_judges_submission(self.JUDGE_2_USERNAME, self.TEAM_3_USERNAME)

		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.admin_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(2, len(response.data['judge_names']))

		self.assertEqual(0, response.data['judge_marked'][0])
		self.assertEqual(0, response.data['judge_progress'][0])
		self.assertEqual(2, response.data['judge_total'][0])
		self.assertEqual(1, response.data['judge_marked'][1])
		self.assertEqual(100, response.data['judge_progress'][1])
		self.assertEqual(1, response.data['judge_total'][1])

	def test_superuser_access_all(self) :
		response = self.client.get(API_NAMESPACE + '/judges_progress', **get_headers(self.superuser_credentials))

		self.assertEqual(STATUS_CODE_2xx.SUCCESS.value, response.status_code)
		self.assertIn("judge_names", response.data)
		self.assertIn("judge_marked", response.data)
		self.assertIn("judge_total", response.data)
		self.assertIn("judge_progress", response.data)

		self.assertGreater(len(response.data['judge_names']), 0)
		self.assertEqual(len(response.data['judge_marked']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_total']), len(response.data['judge_names']))
		self.assertEqual(len(response.data['judge_progress']), len(response.data['judge_total']))

		self.assertEqual(3, len(response.data['judge_names']))

class DecisionsTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)
		cls.admin_judge_2_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials, areas=["East Renfrewshire"], school="")

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

		cls.JUDGE_USERNAME = "judge"
		cls.JUDGE_PASSWORD = "gdsakdsja678687&^*"
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		for question_id in range(6):
			question_text = "question {}".format(question_id)
			question = Question.objects.create(question_id=question_id, question_text=question_text)
			for answer_no in range(4):
				answer_text = "answer {}".format(answer_no)
				Answer.objects.create(question=question, answer_no=answer_no, answer_text=answer_text, 
				answer_dragon_scores={"scores": True})

	def test_get_unauthorized_access(self):
		response = self.client.get(API_NAMESPACE + '/decisions')
		self.assertEqual(response.status_code, 401)

		for user_type_credentials in [self.admin_credentials, self.teacher_credentials, self.judge_credentials]:
			response = self.client.get(API_NAMESPACE + '/decisions', **get_headers(user_type_credentials))
			self.assertEqual(response.status_code, 401)
			self.assertEqual(response.data["message"], "Only teams have access to business decisions")

	def test_post_unauthorized_access(self):
		for user_type_credentials in [self.admin_credentials, self.teacher_credentials, self.judge_credentials]:
			response = self.client.post(API_NAMESPACE + '/decisions', **get_headers(user_type_credentials))
			self.assertEqual(response.status_code, 401)
			self.assertEqual(response.data["message"], "Only teams have access to business decisions")

	def test_no_access_after_submission_deadline(self):
		create_passed_deadline()

		response = self.client.get(API_NAMESPACE + '/decisions', **get_headers(self.team_credentials))
		self.assertEqual(response.status_code, 404)
		self.assertEqual(response.data["message"], "Submission deadline has passed")

		delete_all_deadlines()

	def test_normal_access_before_submission_deadline(self):
		create_active_submission_deadline()

		response= self.client.get(API_NAMESPACE + '/decisions', **get_headers(self.team_credentials))
		self.assertEqual(response.status_code, 200)

		delete_all_deadlines()

	def test_answer_selection_is_saved(self):
		response = self.client.get(API_NAMESPACE + '/decisions', **get_headers(self.team_credentials))

		selection = 1
		selection_dict = {"questionId": response.data[1]["questionId"], "selection": selection}
		for i, question in enumerate(response.data):
			selection_dict = {"questionId": question["questionId"], "selection": i%4}
			response = self.client.post(API_NAMESPACE + '/decisions', json.dumps(selection_dict), content_type='application/json', **get_headers(self.team_credentials))
			self.assertEqual(response.status_code, 200)
		
		response = self.client.get(API_NAMESPACE + '/decisions', **get_headers(self.team_credentials))

		for i,question in enumerate(response.data):
			self.assertIn("selection", question.keys())
			self.assertEqual(question["selection"], i%4)

	# def test_animation_upload(self):
	# 	question = Question.objects.all()[0]
		
	# 	with open("testdata/animation1.mp4", "rb") as f:
	# 		buffer = f.read()
		
	# 	Animation.objects.create(animation=ContentFile(buffer, name="animation1.mp4"), question=question)

	# 	self.assertTrue(Animation.objects.all())

	# 	response = self.client.get(API_NAMESPACE + '/decisions', **get_headers(self.team_credentials))
	# 	self.assertIn("animationLink", response.data[0].keys())
	# 	self.assertNotEqual(response.data[0]["animationLink"], '')


class EmptyDecisionsTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)
		cls.admin_judge_2_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials, areas=["East Renfrewshire"], school="")

		cls.TEACHER_USERNAME = 'teacher1'
		cls.TEACHER_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)

		cls.TEAM_USERNAME = "test_team_1"
		cls.TEAM_PASSWORD = "gdsakdsja678687&^*"
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_secret_key)

	def test_appropriate_response_with_no_decisions_in_database(self):
		response = self.client.get(API_NAMESPACE + '/decisions', **get_headers(self.team_credentials))
		self.assertEqual(response.status_code, 404)
		self.assertEqual(response.data["message"], "No business decision questions were found")


class MarketPlaceFilteringTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)

		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_teacher_2_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials, areas=["East Renfrewshire"], school="Woodfarm High School")
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)
		cls.admin_judge_2_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials, areas=["East Renfrewshire"], school="")

		cls.TEACHER_1_USERNAME = 'teacher1'
		cls.TEACHER_1_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_1_credentials = register_user(c, cls.TEACHER_1_USERNAME, cls.TEACHER_1_PASSWORD, cls.admin_teacher_secret_key)
		cls.teacher_1_secret_key = get_secret_key(c, 'Team', cls.teacher_1_credentials)

		cls.TEACHER_2_USERNAME = 'teacher2'
		cls.TEACHER_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.teacher_2_credentials = register_user(c, cls.TEACHER_2_USERNAME, cls.TEACHER_2_PASSWORD, cls.admin_teacher_2_secret_key)
		cls.teacher_2_secret_key = get_secret_key(c, 'Team', cls.teacher_2_credentials)
		
		cls.JUDGE_USERNAME = 'judge'
		cls.JUDGE_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)

		cls.JUDGE_2_USERNAME = 'judge2'
		cls.JUDGE_2_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.judge_2_credentials = register_user(c, cls.JUDGE_2_USERNAME, cls.JUDGE_2_PASSWORD, cls.admin_judge_2_secret_key)

		cls.TEAM_1A_USERNAME = "test_team_1a"
		cls.TEAM_1A_PASSWORD = "gdsakdsja678687&^*"
		cls.team_1a_credentials = register_user(c, cls.TEAM_1A_USERNAME, cls.TEAM_1A_PASSWORD, cls.teacher_1_secret_key)
		#make full submission
		cls.submission_1a = create_full_submission(cls.TEAM_1A_USERNAME)
		#make full judge_submission
		cls.judge_sub_1a = create_judges_submission(cls.JUDGE_USERNAME, cls.TEAM_1A_USERNAME)

		cls.TEAM_1B_USERNAME = "test_team_1b"
		cls.TEAM_1B_PASSWORD = "gdsakdsja678687&^*"
		cls.team_1b_credentials = register_user(c, cls.TEAM_1B_USERNAME, cls.TEAM_1B_PASSWORD, cls.teacher_1_secret_key)
		#make full submission
		cls.submission_1b = create_full_submission(cls.TEAM_1B_USERNAME)
		#make not full judge_submission
		cls.judge_sub_1b = create_incomplete_judges_submission(cls.JUDGE_USERNAME, cls.TEAM_1B_USERNAME)

		cls.TEAM_1C_USERNAME = "test_team_1c"
		cls.TEAM_1C_PASSWORD = "gdsakdsja678687&^*"
		cls.team_1c_credentials = register_user(c, cls.TEAM_1C_USERNAME, cls.TEAM_1C_PASSWORD, cls.teacher_1_secret_key)
		#make not full submission
		cls.submission_1c = create_submission(cls.TEAM_1C_USERNAME)

		cls.TEAM_1D_USERNAME = "test_team_1d"
		cls.TEAM_1D_PASSWORD = "gdsakdsja678687&^*"
		cls.team_1d_credentials = register_user(c, cls.TEAM_1D_USERNAME, cls.TEAM_1D_PASSWORD, cls.teacher_1_secret_key)
		#make full blacklisted submission
		submission_1d = create_full_submission(cls.TEAM_1D_USERNAME)
		submission_1d.is_blacklisted = True
		#submission_1d.is_blacklisted.save()
		submission_1d.save()
		#make full judge_submission
		cls.judge_sub_1d = create_judges_submission(cls.JUDGE_USERNAME, cls.TEAM_1D_USERNAME)


		cls.TEAM_2A_USERNAME = "test_team_2a"
		cls.TEAM_2A_PASSWORD = "gdsakdsja678687&^*"
		cls.team_2a_credentials = register_user(c, cls.TEAM_2A_USERNAME, cls.TEAM_2A_PASSWORD, cls.teacher_2_secret_key)
		#make full submission
		cls.submission_2a = create_full_submission(cls.TEAM_2A_USERNAME)
		#make full judge_submission
		cls.judge_sub_2a = create_judges_submission(cls.JUDGE_2_USERNAME, cls.TEAM_2A_USERNAME)

	
	def test_before_submission_deadline_fail(self):

		create_active_submission_deadline()

		response_team = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.team_1a_credentials))
		response_teacher = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.teacher_1_credentials))
		response_judge_marketplace = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.judge_credentials))
		response_judge_marked = response_judge_marketplace = self.client.get(API_NAMESPACE + '/marked_marketplace', **get_headers(self.judge_credentials))
		
		self.assertEqual(response_team.status_code, 403)
		self.assertEqual(response_team.data['message'], "Cannot view marketplace before submission deadline or after voting deadline has passed")

		self.assertEqual(response_teacher.status_code, 403)
		self.assertEqual(response_teacher.data['message'], "Cannot view marketplace before submission deadline or after voting deadline has passed")

		self.assertEqual(response_judge_marketplace.status_code, 403)
		self.assertEqual(response_judge_marketplace.data['message'], "Cannot view marketplace before submission deadline or after judging deadline has passed")

		self.assertEqual(response_judge_marked.status_code, 403)
		self.assertEqual(response_judge_marked.data['message'], "Cannot view marketplace before submission deadline or after judging deadline has passed")

		delete_all_deadlines()



	def test_after_voting_deadline_fail(self):

		create_passed_deadline()

		response_team = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.team_1a_credentials))
		response_teacher = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.teacher_1_credentials)) 

		self.assertEqual(response_team.status_code, 403)
		self.assertEqual(response_team.data['message'], "Cannot view marketplace before submission deadline or after voting deadline has passed")

		self.assertEqual(response_teacher.status_code, 403)
		self.assertEqual(response_teacher.data['message'], "Cannot view marketplace before submission deadline or after voting deadline has passed")

		delete_all_deadlines()


	def test_after_judging_deadline_fail(self):

		create_passed_deadline()

		response_judge_marketplace = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.judge_credentials))
		response_judge_marked = self.client.get(API_NAMESPACE + '/marked_marketplace', **get_headers(self.judge_credentials))

		self.assertEqual(response_judge_marketplace.status_code, 403)
		self.assertEqual(response_judge_marketplace.data['message'], "Cannot view marketplace before submission deadline or after judging deadline has passed")

		self.assertEqual(response_judge_marked.status_code, 403)
		self.assertEqual(response_judge_marked.data['message'], "Cannot view marketplace before submission deadline or after judging deadline has passed")

		delete_all_deadlines()

	def test_correct_team_view(self):
		#to test i need multiple submissions, one not complete and one blacklisted
		#team1a should see 1b and 2a
		create_active_voting_deadline()

		response_team = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.team_1a_credentials))

		test_stalls = turn_submission_to_stalls([self.submission_1b, self.submission_2a])

		self.assertEqual(response_team.status_code, 200)
		self.assertCountEqual(response_team.data['stalls'], test_stalls)

		delete_all_deadlines()

		
	def test_correct_teacher_view(self):
		#To test I need multiple submissions from at least 2 different teachers, one not complete and one blacklisted
		#teacher1 should see 1a and 1b
		create_active_voting_deadline()

		response_teacher  = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.teacher_1_credentials))

		test_stalls = turn_submission_to_stalls([self.submission_1a, self.submission_1b])

		self.assertEqual(response_teacher.status_code, 200)
		self.assertCountEqual(response_teacher.data['stalls'], test_stalls)

		delete_all_deadlines()

	def test_correct_admin_view(self):
		#To test I need multiple submissions, one not complete and one blacklisted
		#admin should see 1a, 1b, 2a
		response_admin  = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.admin_credentials))

		test_stalls = turn_submission_to_stalls([self.submission_1a, self.submission_1b, self.submission_2a])

		self.assertEqual(response_admin.status_code, 200)
		self.assertCountEqual(response_admin.data['stalls'], test_stalls)

	def test_correct_judge_view(self):
		#To test I need multiple submissions from 2 different areas, one not complete and one blacklisted and one with no judge submission
		#judge should see 1b
		create_active_judging_deadline()

		response_judge  = self.client.get(API_NAMESPACE + '/marketplace', **get_headers(self.judge_credentials))

		test_stalls = turn_submission_to_stalls([self.submission_1b])

		self.assertEqual(response_judge.status_code, 200)
		self.assertCountEqual(response_judge.data['stalls'], test_stalls)

		delete_all_deadlines()

	def test_correct_marked_judge_view(self):
		# need multiple submissions from 2 different areas, one not complete and one blacklisted and one with no judge submission
		#judge should see 1a
		create_active_judging_deadline()

		response_judge  = self.client.get(API_NAMESPACE + '/marked_marketplace', **get_headers(self.judge_credentials))

		test_stalls = turn_submission_to_stalls([self.submission_1a])

		self.assertEqual(response_judge.status_code, 200)
		self.assertCountEqual(response_judge.data['stalls'], test_stalls)

		delete_all_deadlines()


class IntroductionTestCase(TestCase):

	@classmethod
	def setUpTestData(cls):
		c = Client()
		cls.superuser_credentials = create_superuser(c, SUPERUSER_USERNAME, SUPERUSER_PASSWORD)
		cls.superuser_secret_key = get_secret_key(c, 'Admin', cls.superuser_credentials)

		cls.ADMIN_USERNAME = 'admin'
		cls.ADMIN_PASSWORD = 'dbshadHGUDBJH789&*'
		cls.admin_credentials = register_user(c, cls.ADMIN_USERNAME, cls.ADMIN_PASSWORD, cls.superuser_secret_key)
		cls.admin_teacher_secret_key = get_secret_key(c, 'Teacher', cls.admin_credentials)
		cls.admin_judge_secret_key = get_secret_key(c, 'Judge', cls.admin_credentials)
		cls.TEACHER_USERNAME = 'teacher'
		cls.TEACHER_PASSWORD = 'hiasdasdbk!!&'
		cls.teacher_credentials = register_user(c, cls.TEACHER_USERNAME, cls.TEACHER_PASSWORD, cls.admin_teacher_secret_key)
		cls.JUDGE_USERNAME = 'judge'
		cls.JUDGE_PASSWORD = 'asdjaosd))!'
		cls.judge_credentials = register_user(c, cls.JUDGE_USERNAME, cls.JUDGE_PASSWORD, cls.admin_judge_secret_key)
		cls.teacher_team_secret_key = get_secret_key(c, 'Team', cls.teacher_credentials)
		cls.TEAM_USERNAME = 'team'
		cls.TEAM_PASSWORD = 'adjoashdo**881'
		cls.team_credentials = register_user(c, cls.TEAM_USERNAME, cls.TEAM_PASSWORD, cls.teacher_team_secret_key)

		cls.credential_list = [cls.admin_credentials, cls.teacher_credentials, cls.judge_credentials, cls.team_credentials]

	def test_error_when_unauthorized(self):
		response = self.client.get(API_NAMESPACE + '/intro')

		self.assertEqual(response.status_code, 401)

	def test_error_when_introduction_does_not_exist(self):
		responses = [self.client.get(API_NAMESPACE + '/intro', **get_headers(credentials)) for credentials in self.credential_list]

		for response in responses:
			self.assertEqual(response.status_code, 404)

	def test_intro_returned_successfully(self):
		with open('testdata/animation1.mp4', 'rb') as f:
			buffer = f.read()

		# Create intro object to test that it is returned succefully from the backend
		intro = Intro(intro_text="Welcome!", intro_video=ContentFile(buffer, name="anim.mp4"), 
										yt_url="https://www.youtube.com")
		intro.save()

		response = self.client.get(API_NAMESPACE + '/intro', **get_headers(self.team_credentials))

		self.assertEqual(response.status_code, 200)
		self.assertNotEqual(response.data['introVideoUrl'], '')
		self.assertIsNotNone(response.data['introVideoUrl'])
		self.assertEqual(response.data['introYtUrl'], 'https://www.youtube.com')
		self.assertEqual(response.data['introText'], 'Welcome!')
	
	def test_only_text_intro_saved_successfully(self):
		intro = Intro(intro_text='Welcome!')
		intro.save()

		response = self.client.get(API_NAMESPACE + '/intro', **get_headers(self.team_credentials))

		self.assertEqual(response.status_code, 200)
		self.assertEqual(response.data['introText'], 'Welcome!')
		self.assertNotIn('introVideoUrl', response.data.keys())
		