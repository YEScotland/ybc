#
#  Copyright Jim Carty and Usmaan Karim © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import UserChangeForm

from .models import Admin, Answer, Deadline, Judge, JudgesSubmission, Profile, Question, SecretKey, Submission, SubmissionLike, Teacher, Team, UploadedFile, Animation, User, Intro

#The below class is from Jim Carty in CS11, he recommended it be added to allow for full support from Django's
#authentication process for the log in method we are using

class UserAdmin(BaseUserAdmin):
	form = UserChangeForm
	fieldsets = (
		(None, {'fields': ('username', 'email', 'password', 'first_name', 'last_name')}),
		(_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
		(_('Important dates'), {'fields': ('last_login', 'date_joined')}),
		# (_('user_info'), {'fields': ('native_name', 'phone_no')}),
	)
	add_fieldsets = (
		(None, {'classes': ('wide', ), 'fields': ('username', 'email', 'password1', 'password2', 'first_name', 'last_name'), }),
	)
	list_display = ['username', 'email', 'first_name', 'last_name', 'is_staff', 'is_superuser']
	search_fields = ('username', 'email', 'first_name', 'last_name')
	ordering = ('username', )


class ProfileAdmin(admin.ModelAdmin):
	fields = ('user', 'type')
	ordering = ('type', 'user')


class TeacherAdmin(admin.ModelAdmin):
	fields = ('user', 'school', 'area', 'administrator')
	ordering = ('area', 'school', 'administrator', 'user')


class AdminAdmin(admin.ModelAdmin):
	fields = ('user',)
	ordering = ('user',)


class TeamAdmin(admin.ModelAdmin):
	fields = ('user', 'teacher')
	ordering = ('teacher', 'user')


class IntroAdmin(admin.ModelAdmin):
	fields = ('intro_text', 'intro_video', 'yt_url')
	ordering = ('intro_text',)


class SubmissionAdmin(admin.ModelAdmin):
	fields = ('team', 'company_name', 'tagline', 'logo', 'poster', 'target_customer', 'is_blacklisted', 'blacklisted_reason', 'changed_since_blacklisting')
	ordering = ('team',)


class SubmissionLikeAdmin(admin.ModelAdmin) :
	fields = ('team', 'submission')
	ordering = ('team',)


class JudgeAdmin(admin.ModelAdmin):
	fields = ('user', 'areas', 'administrator')
	ordering = ('user', 'administrator')


class JudgeSubmissionAdmin(admin.ModelAdmin):
	fields = ('judge', 'submission', 'poster_score', 'logo_score', 'target_customer_score', 'tagline_score')
	ordering = ('judge',)


class DeadlineAdmin(admin.ModelAdmin):
	fields = ('submission_deadline', 'voting_deadline', 'judging_deadline')
	ordering = ('id',)


class QuestionAdmin(admin.ModelAdmin):
	fields = ('question_id', 'question_text')
	ordering = ('question_id',)


class AnswerAdmin(admin.ModelAdmin):
	fields = ('answer_no', 'answer_text', 'answer_dragon_scores', 'question')
	ordering = ('question', 'answer_no')


class SecretKeyAdmin(admin.ModelAdmin):
	fields = ('key_text', 'user', 'type', 'school', 'areas')
	ordering = ('type', 'user')


class UploadedFileAdmin(admin.ModelAdmin):
	fields = ('key', 'media_file', 'mime_type', 'extension', 'team', 's3_url', 'path')
	ordering = ('team', 'key')

class AnimationAdmin(admin.ModelAdmin):
	fields = ('animation', 'question', 'yt_url')


admin.site.register(User, UserAdmin)
admin.site.register(Profile, ProfileAdmin)
admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Admin, AdminAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(Intro, IntroAdmin)
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(SubmissionLike, SubmissionLikeAdmin)
admin.site.register(Judge, JudgeAdmin)
admin.site.register(JudgesSubmission, JudgeSubmissionAdmin)
admin.site.register(Deadline, DeadlineAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Answer, AnswerAdmin)
admin.site.register(SecretKey, SecretKeyAdmin)
admin.site.register(UploadedFile, UploadedFileAdmin)
admin.site.register(Animation, AnimationAdmin)
