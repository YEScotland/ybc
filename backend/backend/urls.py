#
#  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

import os

from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, re_path
from django.views.static import serve

from rest_framework_simplejwt.views import TokenRefreshView

from . import settings
from . import views

ALL_FRONTEND_FILES = [os.path.join(dp, f).split("frontend_build")[-1][1:] for dp, _, fn in os.walk(os.path.expanduser(str(settings.BASE_DIR) + "/frontend_build")) for f in fn]
# ALL_FRONTEND_FILES.remove('index.html')
frontend_urlpatterns = [path(filename, views.RetrieveFrontendFileView.as_view(), name=('frontend-file-' + filename)) for filename in ALL_FRONTEND_FILES]

urlpatterns = [
	path(settings.API_NAMESPACE + '/admin/', admin.site.urls, name="admin-page"),

	path(settings.API_NAMESPACE + '/auth/login/', views.LoginView.as_view(), name="authentication-login"),
	path(settings.API_NAMESPACE + '/auth/register/', views.RegisterView.as_view(), name="authentication-register"),
	path(settings.API_NAMESPACE + '/auth/refresh_tokens/', TokenRefreshView.as_view(), name="authentication-refresh"),
	path(settings.API_NAMESPACE + '/auth/secret_key', views.SecretKeyView.as_view(), name="authentication-secret-key"),
	path(settings.API_NAMESPACE + '/auth/secret_key/<str:secret_key>', views.SecretKeySpecificView.as_view(), name="authentication-secret-key-specific"),
	path(settings.API_NAMESPACE + '/auth/change_password/<str:username>', views.ChangePasswordView.as_view(), name="authentication-change-password"),

	path(settings.API_NAMESPACE + '/user/operations/<str:username>', views.UserOperationsView.as_view(), name='user-operations'),
	path(settings.API_NAMESPACE + '/user/milestones', views.UserMilestonesView.as_view(), name='user-milestones'),

	path(settings.API_NAMESPACE + '/intro', views.IntroductionView.as_view(), name='introduction'),
	
	path(settings.API_NAMESPACE + '/marketplace', views.MarketplaceView.as_view(), name='marketplace'),
	path(settings.API_NAMESPACE + '/marked_marketplace', views.MarkedMarketplaceView.as_view(), name='marked-marketplace'),
	path(settings.API_NAMESPACE + '/marketplace/<str:username>', views.TeamMarketplaceSubmissionView.as_view(), name='marketplace-specific'),

	path(settings.API_NAMESPACE + '/decisions', views.BusinessDecisionView.as_view(), name='business-decisions'),
	
	path(settings.API_NAMESPACE + '/info/local_authorities', views.InfoLocalAuthorities.as_view(), name='info-local-authorities'),
	path(settings.API_NAMESPACE + '/info/local_authorities/<str:local_authority>/schools', views.InfoLocalAuthoritiesSchools.as_view(), name='info-local-authority-schools'),

	path(settings.API_NAMESPACE + '/marketplace_judge/<str:username>', views.JudgeSubmissionView.as_view(), name='judge-submission'),

	path(settings.API_NAMESPACE + '/marketplace_like/<str:username>', views.MarketplaceLikeView.as_view(), name='marketplace-like'),
	path(settings.API_NAMESPACE + '/can_like/<str:username>', views.CanLikeMarketplaceStall.as_view(), name='marketplace-can-like'),

	path(settings.API_NAMESPACE + '/issues', views.IssueControlView.as_view(), name='issue-control'),
	path(settings.API_NAMESPACE + '/issue/<str:team_username>', views.IssueControlSpecificView.as_view(), name='issue-control-specific'),

	path(settings.API_NAMESPACE + "/is_users_admin/<str:username>", views.IsUsersAdministratorCheckView.as_view(), name='is-users-admin'),

	path(settings.API_NAMESPACE + "/deadline", views.DeadlineControlView.as_view(), name='deadline-control'),

	path(settings.API_NAMESPACE + "/csv/detail", views.DownloadCSVDetailView.as_view(), name='csv-detail'),
	path(settings.API_NAMESPACE + "/csv/summary", views.DownloadCSVSummaryView.as_view(), name='csv-summary'),

	path(settings.API_NAMESPACE + "/number_of", views.NumberOfNotificationsView.as_view(), name='number-of-notifications'),

	path(settings.API_NAMESPACE + '/judges_progress', views.JudgesProgressView.as_view(), name='judges-progress'),

	path(settings.API_NAMESPACE + '/teacher_info', views.TeachersView.as_view(), name='judges-progress'),

	path('', views.FrontendView.as_view(), name="frontend-home"),
	path('login', views.FrontendView.as_view(), name="frontend-login"),
	path('marketplace', views.FrontendView.as_view(), name="frontend-marketplace"),
	path('marketplace/<str:username>', views.FrontendView.as_view(), name="frontend-marketplace-specific"),
	path('schools', views.FrontendView.as_view(), name="frontend-schools"),
	path('teams', views.FrontendView.as_view(), name="frontend-teams"),
	path('glossary', views.FrontendView.as_view(), name="frontend-glossary"),
	path('issues', views.FrontendView.as_view(), name="frontend-issues"),
	path('decisions', views.FrontendView.as_view(), name="frontend-decisions"),
	
	path('settings', views.FrontendView.as_view(), name="frontend-settings"),
	path('settings/deadlines', views.FrontendView.as_view(), name="frontend-settings-deadlines"),
	path('marked', views.FrontendView.as_view(), name="frontend-marked"),

	path('judges', views.FrontendView.as_view(), name="frontend-judges"),
	path('teachers', views.FrontendView.as_view(), name="frontend-teachers"),

	re_path(r'^api/media/(?P<path>.*)$', serve, name="media-paths", kwargs={'document_root': settings.MEDIA_ROOT})
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + frontend_urlpatterns

handler404 = 'backend.views.handler404View'
