##===-- ugrdv/scripts/CodeCoverage.bash - Compares Coverage ----*- bash -*-===##
##
## Part of the UGRDV Project, under the UGRACING DRIVERLESS PRIVATE LICENSE.
## See the attached LICENSE.txt file in this projects upper most directory for 
## license information.
##
## Copyright Jim Carty © 2020-2021
##
##===----------------------------------------------------------------------===##
###
### \file
### Checks the code coverage of the current master branch and the branch that is
### executing this script. Can only be ran on GitLab as uses GitLab global
### variables
###
##===----------------------------------------------------------------------===##

#!/bin/bash

# get coverage for latest current pipeline

curl -H "PRIVATE-TOKEN: ${GITLAB_PROJECT_API_TOKEN}" -s https://stgit.dcs.gla.ac.uk/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}

echo ""

latest=`curl -H "PRIVATE-TOKEN: ${GITLAB_PROJECT_API_TOKEN}" -s https://stgit.dcs.gla.ac.uk/api/v4/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID} | jq '.coverage'`
latest="${latest%\"}"
latest="${latest#\"}"
echo "Project " ${CI_PROJECT_ID}
echo "Pipline " ${CI_PIPELINE_ID} " coverage value = " $latest

# get coverage for master
tmp=`curl -H "PRIVATE-TOKEN: ${GITLAB_PROJECT_API_TOKEN}" -s https://stgit.dcs.gla.ac.uk/api/v4/projects/${CI_PROJECT_ID}/pipelines\?ref\=master\&status\=success | jq '.[0] | .id'`

# pass that into the curl below
master=`curl -H "PRIVATE-TOKEN: ${GITLAB_PROJECT_API_TOKEN}" -s https://stgit.dcs.gla.ac.uk/api/v4/projects/${CI_PROJECT_ID}/pipelines/${tmp} | jq '.coverage'`
master="${master%\"}"
master="${master#\"}"
echo "Master coverage value =" $master

# if master == null or latest >= master exit 0
if [[ "`echo "$latest >= $master" | bc`" -eq 1 ]]
then
  echo "Latest pipeline coverage >= master"
  exit 0
# else exit 1
else
  echo "Latest pipeline coverage < master"
  exit 1
fi