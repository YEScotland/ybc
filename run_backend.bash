#
#  Copyright Jim Carty © 2021-2022: cartyjim1@gmail.com
#
#  This file is subject to the terms and conditions defined in file 'LICENSE.txt', which is part of this source code package.
#

run=$1

cd backend

mkvirtualenv tp-venv -p $(which python3)
workon tp-venv

python -m pip install -r requirements.txt

python manage.py makemigrations backend
python manage.py migrate

python manage.py collectstatic --no-input --clear

if [[ ! -z "$run" && "$run" == "y" ]]; then
	python manage.py runserver 0.0.0.0:8000
fi

cd ..